#!/bin/bash

##############################################
#                                            #
#     This script will generate a unique     #
#   single fasta file from all your queris   #
#      chains given in your INPUT-FILE.      #
#                                            #
#   - Query format:                          #
#           * PDB:CHAIN (format XXXX:chain)  #
#                                            #
#      Author: Martín González Buitrón       #
#   email: martingonzalezbuitron@gmail.com   #
#                                            #
#       Universidad Nacional de Quilmes      #
#                                            #
##############################################

# Purpose: This script will create a file that has listed all the fasta sequences
#          taken from each query listed in INPUT-FILE and, if you want, will also
#          create a folder structure with your OUTPUT-FILE name.
#
#       Important: If the OUTPUT-FILE exist before run this script, is going to be removed.
#
#       Files created:
#                       # <OUTPUT-FILE>  // has all fastas sequences

# Preconditions: 
#            * Inside INPUT-FILE each line need to be one query in this format "PDB:Chain".
#            *
#

# Positional and Mandatory Parameters:
# ===================================
# 1 - File contaning PDB queries      // file    (One query per line, as provided by RCSB)
# 2 - Fasta directoy                  // string
# 3 - Output file name                // string


# Observation: If you want improve it, feel free to make changes. FOSS!


#Stop if any error happend
set -e

if [[ -d "${2}" && -n "${3}" && ! -d "${3}" && "${3}" != "/" && "${3}" != "." ]]
then
    output_path=$(dirname "${3}")
    output_name=$(basename "${3}")
    if [[ -e "${output_path}"/"${output_name}.fasta" ]]
    then
        rm -rf "${output_path}"/"${output_name}.fasta"
    fi
else
    echo "Check that all your arguments are written correctly."
    echo
    echo "Usage:"
    echo "        ./<script-name>.sh  <INPUT-FILE> <FASTA-DIRECTORY> <OUTPUT-FILE>"
    echo
    echo
    echo " Positional and Mandatory Parameters:"
    echo " ==================================="
    echo " 1 - File contaning PDB queries      // file    (One query per line, as provided by RCSB)"
    echo " 2 - Fasta directoy                  // string"
    echo " 3 - Output file name                // string"
    echo
    echo "For more information, run ./<script-name>.sh --help"
    exit
fi

if [[ -e "${1}" && -s "${1}" && ! -d "${1}" && -d "${2}" && "${1}" != "--help" && "${1}" != "-h" && "${1}" != "-help" ]]
then
    fasta_path="${2}"
    while read -r line
    do
        pdb_code=$(echo ${line} | cut -d ':' -f 1 | tr '[:upper:]' '[:lower:]')
        chain=$(echo ${line} | cut -d ':' -f 2)
        if [[ -n "${3}" && ! -d "${3}" && "${3}" != "/" && "${3}" != "." ]]
        then
            output_path=$(dirname "${3}")
            output_name=$(basename "${3}")
            if [[ -n "${chain}" ]]
            then
                 mkdir -p "${output_path}"
                 # Search the file in fasta_path given
                 fasta_file_path=$(find "${fasta_path}" -type f -name "${pdb_code}_${chain}.fasta")
                 cat "${fasta_file_path}" >> "${output_path}"/"${output_name}.fasta"
            else
                echo "Check that your PDB:Chain entry line has a correct format."
                exit
            fi
        else
            echo "Check that you have given a correct output path and file name."
            echo
            echo "For more information, run ./<script-name>.sh --help"
            exit
        fi
    done < "${1}"
else
    if [[ "${1}" == "--help" || "${1}" == "-h" && "${1}" == "-help" ]]
    then
        echo
        echo "Usage:"
        echo "        ./<script-name>.sh  <INPUT-FILE> <FASTA-DIRECTORY> <OUTPUT-FILE>"
        echo
        echo
        echo " Positional and Mandatory Parameters:"
        echo " ==================================="
        echo " 1 - File contaning PDB queries      // file    (One query per line, as provided by RCSB)"
        echo " 2 - Fasta directoy                  // string"
        echo " 3 - Output file name                // string"
        exit
    else
        echo "Check that all your arguments are written correctly."
        echo
        echo "For more information, run ./<script-name>.sh --help"
        exit
    fi
fi

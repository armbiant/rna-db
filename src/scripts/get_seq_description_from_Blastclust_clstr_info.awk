#!/usr/bin/awk -f

##############################################
#                                            #
#      This script report information of     #
# each sequence en two CDHit standard output #
#   files (ended in *.clstr) in a CSV file   #
#   called:  table_seq_blastclust_info.csv.  #
#                                            #
#                                            #
#  - Input files                             #
#           * blastclust *.clstr files       #
#                                            #
#  - Output dir                              #
#           * Default: results saved in      #
#             current directory under        #
#            "blastclust_seq_info" folder.   # 
#           * When an OUTPUT-FOLDER is given #
#             in last argument, results are  #
#             save there.                    #
#                                            #
#  - HEADER of table_seq_blastclust_info.csv #
#           * HEADER:                        #
#             query_code_in_file_1,\         #
#             query_length_in_file_1,\       #
#             cluster_number_in_file_1,\     #
#             cluster_size_in_file_1,\       #
#             target_code_in_file_2,\        #
#             target_length_in_file_2,\      #
#             cluster_number_in_file_2,\     #
#             cluster_size_in_file_2,\       #
#             cluster_size_diff,\            #
#             %_diff_cluster_size_in_file_2,\#
#             same_cluster                   #  
#                                            #
#      Author: Martín González Buitrón       #
#   email: martingonzalezbuitron@gmail.com   #
#                                            #
#       Universidad Nacional de Quilmes      #
#                                            #
##############################################

# Purpose: This script extract information of each sequence in two blastclust cluster files.
#          With all that information create a CSV file called table_seq_blastclust_info.csv.
#          Also perform the a difference between cluster sizes where that sequence belong
#          (always cluster size from "file 2 - file 1").
#          The last argument it is the <OUTPUT-FOLDER> and it is optional. Otherwise, the
#          table_seq_blastclust_info.csv file is saved in the current directory under a folder called
#          "blastclust_seq_info".
#
#

# Preconditions: 
#            * blastclust <INPUT-FILES> (*.clstr)

# Positional and Mandatory Parameters:
# ===================================
#   1   - blastclust *.clstr file         // file
#   2   - blastclust *.clstr file         // file
#
# Positional and Optional Parameters:
# ===================================
#   3   - <OUTPUT-FOLDER>            // string (Default: ./blastclust_seq_info)


# Observation: If you want to improve it, feel free to make changes. FOSS!

BEGIN{
    if(ARGC <= 2 || ARGC > 4 || ARGV[1] == "help" || ARGV[1] == "h" || ARGV[1] == "--SOS"){
        print "\nUsage:\n\n ./script.awk <blastclust-1.clstr> <blastclust-2.clstr> <OUTPUT-FOLDER>\n"
        print "\nFor more information run:\n"
        print " head -66 ./script.awk\n"
        print "The last argument is optional and could be an OUTPUT-FOLDER.\n"
        exit
    }
    #Build output dir
    if(ARGC > 2){
        #Getting total length of output folder
        length_dir=length(ARGV[ARGC-1])
        if(substr(ARGV[ARGC-1],(length_dir - 5),6) == ".clstr"){
            #Create output directory
            output_dir=ENVIRON["PWD"]"/""blastclust_seq_info"
        }
        if(substr(ARGV[ARGC-1],length_dir,1) == "/"){
            last_level=split(ARGV[ARGC-1], a, "/")
            output_dir=""
            for(i=1;i<(last_level-1);i++){
                output_dir= output_dir a[i] "/"
            }
            output_dir= output_dir a[last_level-1]
        }
        if((substr(ARGV[ARGC-1],(length_dir - 5),6) != ".clstr") && (substr(ARGV[ARGC-1],length_dir,1) != "/")){
            last_level=split(ARGV[ARGC-1], a, "/")
            output_dir=""
            for(i=1;i<last_level;i++){
                output_dir= output_dir a[i] "/"
            }
            output_dir= output_dir a[last_level]
        }
    }
    else{
        #Create output directory
        output_dir=ENVIRON["PWD"]"/""blastclust_seq_info"
    }
    #Build command to be used in system
    command="mkdir -p " output_dir
    #Create output directory
    system(command)
    #Table CSV with seq info per blastclust running configuration
    CSV_file="table_seq_blastclust_info.csv"
    #Print header of CSV_file
    printf("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n",\
            "query_code_in_file_1",\
            "query_length_in_file_1",\
            "cluster_number_in_file_1",\
            "cluster_size_in_file_1",\
            "query_code_in_file_2",\
            "query_length_in_file_2",\
            "cluster_number_in_file_2",\
            "cluster_size_in_file_2",\
            "cluster_size_diff",\
            "%_diff_cluster_size_in_file_2",\
            "same_cluster") > output_dir "/" CSV_file
    #Set ARGC equal 2 so only process until file 1
    ARGC=2
}
{
    #Start reading RECORDS per FILE
    if (NF > 0){
        #Save amount of all queries in this cluster
        total_seqs_in_cluster_file_1=NF
        #Go through one cluster in file 1
        for(i=1;i<=total_seqs_in_cluster_file_1;i++){
            query=$i
            #Processing query string
            split(query,query_array,"_")
            #Save query
            query_code=query_array[2]
            #Save length of query
            query_length="NaN"
            #Get cluster info
            cluster_number_in_file_1=NR-1
            cluster_size_in_file_1=total_seqs_in_cluster_file_1
            #Start cluster counter for file 2
            counter=0
            #Load second file file to search the query
            while(getline cluster_in_file_2 < ARGV[2]){
                #Split cluster_in_file_2 to be used
                NF2=split(cluster_in_file_2,all_cluster_seqs_array_file_2," ")
                #Count 1 cluster to counter
                counter++
                #Save amount of all targets in this cluster
                total_seqs_in_cluster_file_2=NF2
                #Go through one cluster in file 2
                for(j=1;j<=total_seqs_in_cluster_file_2;j++){
                    target=all_cluster_seqs_array_file_2[j]
                    #Processing member string
                    split(target,target_array,"_")
                    #Save possible target
                    target_code=target_array[2]
                    #Save length of possible target
                    target_length="NaN"
                    # Check if query is the same as possible target
                    if(query_code == target_code){
                        #Get cluster info
                        cluster_number_in_file_2=counter-1
                        cluster_size_in_file_2=total_seqs_in_cluster_file_2
                        #Get cluster sizes difference
                        cluster_size_diff=(cluster_size_in_file_2-cluster_size_in_file_1)
                        #Get percentage size difference
                        perc_diff_cluster_size_in_file_2=(cluster_size_diff/cluster_size_in_file_2)
                        #Get cluster location comparation
                        same_cluster=(cluster_number_in_file_2-cluster_number_in_file_1)
                        if (same_cluster == 0){
                            same_cluster="Yes"
                        }
                        else{
                            same_cluster="No"
                        }
                        printf("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n",
                                    query_code,\
                                    query_length,\
                                    cluster_number_in_file_1,\
                                    cluster_size_in_file_1,\
                                    target_code,\
                                    target_length,\
                                    cluster_number_in_file_2,\
                                    cluster_size_in_file_2,\
                                    cluster_size_diff,\
                                    perc_diff_cluster_size_in_file_2,\
                                    same_cluster) > output_dir "/" CSV_file
                        break
                    }
                }
                #Keep searching this query or go for the next one?
                if(query_code == target_code){
                    #Query was found it in earlier cluster
                    #so close second file and go to the next query.
                    close(ARGV[2])
                    break
                }
                else{
                    #Query was not found it in earlier cluster
                    #so go to next cluster.
                    continue
                }
            }
            continue
        }
    }
}
# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
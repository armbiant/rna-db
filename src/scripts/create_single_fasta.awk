#!/usr/bin/awk -f

##############################################
#                                            #
#        This script generate a unique       #
#   single fasta file from all your queris   #
#      chains given in your INPUT-FILE.      #
#                                            #
#   - Query format:                          #
#           * PDB     (format XXXX:chain)    #
#           * Uniprot (format XXXXXX)        #
#                                            #
#      Author: Martín González Buitrón       #
#   email: martingonzalezbuitron@gmail.com   #
#                                            #
#       Universidad Nacional de Quilmes      #
#                                            #
##############################################

# Purpose: This script create a file listening all the fasta sequences
#          taken from each query present in <INPUT-FILE> and, if you want, will also
#          create a folder structure with your <OUTPUT-FILE>.
#
#       Important: If the OUTPUT-FILE exist before run this script, is going to be overwritten.
#
#       Files created:
#                       # <OUTPUT-FILE>  // single file with all fastas query sequences

# Preconditions: 
#            * Inside <INPUT-FILE> each line need to be one query.
#                - RCSB format "PDB:Chain".
#                - UNIPROT format "XXXXXX".


# Positional and Mandatory Parameters:
# ===================================
# 1 - File contaning query codes         // file    (One query per line, e.g. as provided by RCSB)
# 2 - Fasta file where to find queries   // string
# 3 - Output file name                   // string


# Observation: If you want improve it, feel free to make changes. FOSS!


{
    #Start reading the list of queries (first file)
    while (getline < ARGV[1]){
        #Get the query code (PDB:Chain | Uniprot)
        query=$1
        #Set record separator to be used in next file
        RS=">"
        #Set field separator to be used in next file
        FS="|"
        #Start reading second file (fasta format file)
        while (getline < ARGV[2]){
            #From record, get the target code (PDB:Chain | Uniprot)
            pdb_target=$1
            uniprot_target=$2
            #Check if quey and target are equal
            if(query == pdb_target || query == uniprot_target) {
                #Save match in fasta format
                printf("%s", ">"$0) > ARGV[3]
                #Reboot record separator for first file
                RS="\n"
                #Reboot field separator for first file
                FS=" "
                #Go to next query
                next
            }
        }
    }
}

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
#!/bin/bash

##############################################
#                                            #
#         Run and handle CD-Hit-Est          #
#          for clustering analisys           #
#                                            #
#      Author: Martín González Buitrón       #
#   email: martingonzalezbuitron@gmail.com   #
#                                            #
#       Universidad Nacional de Quilmes      #
#                                            #
##############################################

# Purpose: This script run and handle CD-Hit running configuration.
#          It will create many files as many IDE values and COV
#          were given.
#
#          Files created:
#                       # As many (IDE x COV) were given

# Preconditions: First line of fasta file need to begin with ">".
#                Each line of IDENTITY file need to be a float between 0.00 - 1.00
#                Each line of COVERAGE file need to be a float between 0.00 - 1.00
#
#                IMPORTANT: Check CD-Hit binary.

# Positional Parameters:
#         MANDATORY
#         1- FASTA file      // file (first line need to begin with ">")
#         2- IDENTITY file   // file (each line with float between 0.00 - 1.00)
#         3- COVERAGE file   // file (each line with float between 0.00 - 1.00)
#
#         OPTIONAL
#         4- OUTPUT filename + [TAG: IDE_COV] values  // file (Default: in current execution directory)

# Observation: If you want improve it, feel free to make changes. FOSS!

#Stop if any error happend
set -e

##########################################################################################
#Clustering secuencial:
#%IDE: 100, 99, 98, 95, y 90.
#% coverage: 98, 95 y 90.

#Output para observar resultados del clustering:
#RMSDmax
#número de clusters
#número de miembros en cada cluster
#      -    Tipo de RNA
#      -    longitud máxima del RNA en cada cluster
#      -    Técnica experimental de donde deriva la información estructural (DRX, RMN, EM).
##########################################################################################

########################################################################

cd_hit_binary=$(which cd-hit-est)

########################################################################

#Cheking first argument
if [[ -f "${1}" && "${1}" != "--help" && "${1}" != "-help" && "${1}" != "-h" && "${1}" != "--h" ]]
then
    fasta_file="${1}"
else
    echo
    echo " Usage:"
    echo " ====="
    echo "      ./<script.sh> <FASTA-FILE> <IDENTITY-FILE> <COVERAGE-FILE> [OUTPUT-FILE]"
    echo
    echo
    echo " Purpose: This script will handle CD-Hit running configuration."
    echo " =======  It will create many files as many IDE values and COV"
    echo "          were given."
    echo
    echo "          Files created:"
    echo "          ============="
    echo "                       # As many (IDE x COV) were given"
    echo
    echo
    echo
    echo
    echo " Preconditions: First line of fasta file need to begin with \">\"."
    echo " =============  Each line of IDENTITY file need to be a float between 0.00 - 1.00"
    echo "                Each line of COVERAGE file need to be a float between 0.00 - 1.00"
    echo
    echo "                IMPORTANT: Check CD-Hit binary."
    echo
    echo
    echo " Positional Parameters:"
    echo " ====================="
    echo
    echo "         MANDATORY"
    echo "         =========="
    echo "         1- FASTA file      // file (first line need to begin with \">\")"
    echo "         2- IDENTITY file   // file (each line with float between 0.00 - 1.00)"
    echo "         3- COVERAGE file   // file (each line with float between 0.00 - 1.00)"
    echo
    echo "         OPTIONAL"
    echo "         ========="
    echo "         4- OUTPUT filename + [TAG: IDE_COV] values  // file (Default: in current execution directory)"
    echo
    echo
    exit
fi

#Cheking second argument
if [[ -f "${2}" ]]
then
    list_ide_file="${2}"
else
    if [[ -n "${2}" || -d "${2}" ]]
    then
        echo
        echo "Create a file listing all IDE values."
        echo
        echo "Use \"--help\" to see more detail info."
        exit
    else
        echo
        echo "Create a file listing all IDE values."
        echo
        echo "Use \"--help\" to see more detail info."
        exit
    fi
fi

#Cheking thirth argument
if [[ -f "${2}" && -f "${3}" ]]
then
    list_cov_file="${3}"
else
    if [[ -n "${3}" || -d "${3}" ]]
    then
        echo
        echo "Create a file listing all COV values."
        echo
        echo "Use \"--help\" to see more detail info."
        exit
    else
        echo
        echo "Create a file listing all COV values."
        echo
        echo "Use \"--help\" to see more detail info."
        echo
        exit
    fi
fi

#Cheking fourth argument
if [[ -n "${4}" && ! -d "${4}" ]]
then
    output_name=$(basename "${4}")
    output_dir=$(dirname "${4}")
else
    output_name=$(date +%Y-%m-%d)"_cd-hit_output"
    output_dir=$(pwd)
fi


if [[ -n "${fasta_file}" && -f "${list_ide_file}" && -f "${list_cov_file}" ]]
then
    echo "Running ..."
    while read -r ide_line
    do
        case "${ide_line}" in
            (1.0|1.00|0.99|0.98|0.97|0.96)
                word_length="10" # 11 is another aviable length
                ;;
            (0.95|0.94|0.93|0.92|0.91)
                word_length="8" # 9 is another aviable length
                ;;
            (0.9|0.90|0.89)
                word_length="7"
                ;;
            (0.88|0.87|0.86)
                word_length="6"
                ;;
            (0.85|0.84|0.83|0.82|0.81)
                word_length="5"
                ;;
            (0.8|0.80|0.79|0.78|0.77|0.76|0.75)
                word_length="4"
                ;;
            (*)
                echo "ERROR: IDE value: ${ide_line}"
                echo
                echo "Use \"--help\" or try again with a correct IDE value."
                exit
        esac
        while read -r cov_line
        do
            if [[ $(echo "${cov_line}" | cut -d '.' -f 1) != "0" && "${cov_line}" != "1" && "${cov_line}" != "1.0" && "${cov_line}" != "1.00" ]]
            then
                echo "ERROR: COV value: ${cov_line}"
                echo
                echo "Use \"--help\" or try again with a correct COV value."
                exit
            fi
            #Checking if output_name exist 
            if [[ -n "${output_name}" ]]
            then
                #Calling CD-Hit
                if [[ -e "${cd_hit_binary}" ]]
                then
                    #Creating output_dir
                    mkdir -p "${output_dir}"
                    #Running settings (customized until -T 0)
                    "${cd_hit_binary}" -i "${fasta_file}" \
                                         -c "${ide_line}" \
                                         -n "${word_length}" \
                                         -aL "${cov_line}" \
                                         -G 0 \
                                         -b 20 \
                                         -gap -6 \
                                         -gap-ext -1 \
                                         -l 9 \
                                         -sc 1 \
                                         -sf 1 \
                                         -d 0 \
                                         -o "${output_dir}"/"${output_name}"_ide_"${ide_line}"_cov_"${cov_line}" \
                                         -M 1000 \
                                         -T 0 \
                                         -g 1 \
                                         -s 0.0 \
                                         -AL 99999999 \
                                         -aS 0.0 \
                                         -AS 99999999 \
                                         -A 0 \
                                         -uL 1.0 \
                                         -uS 1.0 \
                                         -U 99999999 \
                                         -B 0 \
                                         -P 0 \
                                         -cx 0 \
                                         -cy 0 \
                                         -ap 0 \
                                         -p 0 \
                                         -r 1 \
                                         -match 2 \
                                         -mismatch -2 \
                                         -bak 0 > "${output_dir}"/"${output_name}"_ide_"${ide_line}"_cov_"${cov_line}".output
                else
                    echo "Check that you have installed CD-Hit binarity."
                    exit
                fi
            fi
        done < "${list_cov_file}"
    done < "${list_ide_file}"
    echo "Finished !"
fi

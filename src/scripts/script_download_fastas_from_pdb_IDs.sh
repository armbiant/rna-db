#!/bin/bash

##############################################
#                                            #
#      This script will download all your    #
#          fastas from your query PDB        #
#                    list.                   #
#                                            #
#   - Query:                                 #
#           * PDB ID    (format XXXX)        #
#           * PDB:CHAIN (format XXXX:chain)  #
#                                            #
#      Author: Martín González Buitrón       #
#   email: martingonzalezbuitron@gmail.com   #
#                                            #
#       Universidad Nacional de Quilmes      #
#     Stockholm University - Scilifelab      #
#                                            #
##############################################

# Purpose: This script download all your fastas from your file that has
#          listed queries and, if you want, will also create a folder in
#          place where is executed and store all downloaded fastas files there.
#
#       Important: if any download fails, it will generate a
#       file in place (fail_to_download_these_fastas) with all the
#       failures.

# Positional Parameters:
#
#         MANDATORY
#         1 - file contaning PDB queries   // One query per line (as provided by RCSB)
#         OPTIONAL
#         2 - folder name                  // string
#         3 - file_name                    // string

#Break if something is wrong
set -e


# Observation: If you want more complex downloads, feel free to make changes. FOSS!

if [[ "${1}" == "--help" || "${1}" == "-help" || "${1}" == "-h" || "${1}" == "--h" || "${1}" == "--SOS" || "${1}" == "" ]]
then
    echo
    echo "Usage:"
    echo "         <script.sh> <file-with-PDB-queries> [<FOLDER-NAME>] [<FILE-NAME>]"
    echo
    echo "Purpose: This script download all your fastas from your file that has"
    echo "         listed queries and, if you want, will also create a folder in"
    echo "         place where is executed and store all downloaded fastas files there."
    echo
    echo "         Important: if any download fails, it will generate a file called"
    echo "                    \"fail_to_download_these_fastas\" with all the failures."
    echo
    echo "Positional parameters:"
    echo
    echo "         MANDATORY"
    echo "         1 - file contaning PDB queries   // One query per line (as provided by RCSB)"
    echo "         OPTIONAL"
    echo "         2 - folder name                  // string"
    echo "         3 - file name                    // string"
    echo
    exit
fi


if [[ -e "${1}" && -s "${1}" && ! -d "${1}" ]]
then
    while read -r line
    do
        pdb_code=$(echo ${line} | cut -d ':' -f 1 | tr '[:upper:]' '[:lower:]')
        chain=$(echo ${line} | cut -d ':' -f 2)
        if [[ -n "${2}" ]]
        then
            dir_name=$(dirname "${2}")
            base_name=$(basename "${2}")
            directory="${dir_name}/${base_name}"
            if [[ -n ${chain} ]]
            then
                 mkdir -p "${directory}"
                 if [[ -f "${directory}/${pdb_code}_${chain}.fasta" ]]
                 then
                     continue
                 else
                     wget -P "${directory}" "https://www.rcsb.org/pdb/download/downloadFile.do?fileFormat=fastachain&compression=NO&structureId=${pdb_code}&chainId=${chain}"
                     mv "${directory}/downloadFile.do?fileFormat=fastachain&compression=NO&structureId=${pdb_code}&chainId=${chain}" "${directory}/${pdb_code}_${chain}".fasta
                 fi
                 if [[ ! -e "${directory}/${pdb_code}_${chain}.fasta" ]]
                 then
                     echo "${pdb_code}_${chain}.fasta" >> "${directory}/fail_to_download_these_fastas"
                     echo "Warning, you should check why you couldn't download ${pdb_code}_${chain}.fasta"
                 fi
            else
                 mkdir -p "${directory}"
                 if [[ -f "${directory}/${pdb_code}.fasta" ]]
                 then
                     continue
                 else
                     wget -P "${directory}" "https://www.rcsb.org/pdb/download/downloadFastaFiles.do?structureIdList=${pdb_code}&compressionType=uncompressed"
                     mv "${directory}/downloadFastaFiles.do?structureIdList=${pdb_code}&compressionType=uncompressed" "${directory}/${pdb_code}".fasta
                 fi
                 if [[ ! -e "${directory}/${pdb_code}".fasta ]]
                 then
                     echo "${pdb_code}.fasta" >> "${directory}/fail_to_download_these_fastas"
                     echo "Warning, you should check why you couldn't download ${pdb_code}.fasta"
                 fi
            fi
        else
            if [[ -n ${chain} ]]
            then
                 if [[ -f "${pdb_code}_${chain}.fasta" ]]
                 then
                     continue
                 else
                     wget "https://www.rcsb.org/pdb/download/downloadFile.do?fileFormat=fastachain&compression=NO&structureId=${pdb_code}&chainId=${chain}"
                     mv "downloadFile.do?fileFormat=fastachain&compression=NO&structureId=${pdb_code}&chainId=${chain}" "${pdb_code}_${chain}".fasta
                 fi
                 if [[ ! -e "${pdb_code}_${chain}.fasta" ]]
                 then
                     echo "${pdb_code}_${chain}.fasta" >> fail_to_download_these_fastas
                     echo "Warning, you should check why you couldn't download ${pdb_code}_${chain}.fasta"
                 fi
            else
                if [[ -f "${pdb_code}.fasta" ]]
                then
                    continue
                else
                    wget "https://www.rcsb.org/pdb/download/downloadFastaFiles.do?structureIdList=${pdb_code}&compressionType=uncompressed"
                    mv "downloadFastaFiles.do?structureIdList=${pdb_code}&compressionType=uncompressed" "${pdb_code}".fasta
                fi
                if [[ ! -e "${pdb_code}.fasta" ]]
                then
                    echo "${pdb_code}.fasta" >> fail_to_download_these_fastas
                    echo "Warning, you should check why you couldn't download ${pdb_code}.fasta"
                fi
            fi
        fi
    done < "${1}"
else
    echo "Warning, your file with PDB queries is empty or doesn't exist or is a directory."
    echo "Check it and don't give up, keep trying!"
    echo
    echo "For more details, try with \"--help\""
    echo
    exit
fi

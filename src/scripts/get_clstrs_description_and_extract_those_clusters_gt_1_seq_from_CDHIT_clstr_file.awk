#!/usr/bin/awk -f

##############################################
#                                            #
#    This script extract sequence code and   #
#   length of each sequence that belong to   #
#  a cluster reported in CDHit *.clstr file  #
#      and create a CSV file per cluster     #
#          with all that information.        #
#                                            #
#  Also generate a CSV table with the number #
#   of clusters of each CDHit *.clstr file.  #
#                                            #
#                                            #
#   - Input file or files                    #
#           * CDHit *.clstr file             #
#                                            #
#   - Output dir                             #
#           * Default: results saved in      #
#             current directory under        #
#            "cdhit_clustr_info" folder.     # 
#           * When an OUTPUT-FOLDER is given #
#             in last argument, results are  #
#             save there.                    #
#                                            #
#   - Standard output per cluster (.CSV)     #
#           * HEADER:                        #
#             Seq_code,Length                #
#           * Content:                       #
#             <SEQ-CODE>,<NUMBER>            #
#             <SEQ-CODE>,<NUMBER>            #
#             <SEQ-CODE>,<NUMBER>            #
#                                            #
#                                            #
#   - Standard output for CDHit run (.CSV)   #
#           * HEADER:                        #
#             File_name,Clusters_gt_1seq,\   #
#             Total_clusters                 #
#           * Content:                       #
#             <INPUT-FILE-1>,<NUM>,<NUM>     #
#             <INPUT-FILE-2>,<NUM>,<NUM>     #
#             <INPUT-FILE-3>,<NUM>,<NUM>     #
#             <INPUT-FILE-4>,<NUM>,<NUM>     #
#                                            #
#                                            #
#      Author: Martín González Buitrón       #
#   email: martingonzalezbuitron@gmail.com   #
#                                            #
#       Universidad Nacional de Quilmes      #
#                                            #
##############################################

# Purpose: This script extract each sequence code and length of each sequence
#          that belong to a cluster reported in CDHit *.clstr file and create a
#          CSV file per cluster with all that information. Also generate a CSV
#          table listening the name of the CDHit <INPUT-FILE> (*.clstr) and the
#          number of clusters with more than 1 sequence.
#          You can pass multiple *.clstr files. The last argument it is the
#          <OUTPUT-FOLDER> and it is optional.
#
#       Important: The standard output is printed on screen but could be
#                  redirected to a regular file.
#

# Preconditions: 
#            * CDHit <INPUT-FILE> (*.clstr)

# Positional and Mandatory Parameters:
# ===================================
#   1   - CDHit *.clstr file/s         // file
#
# Positional and Optional Parameters:
# ===================================
# @LAST - <OUTPUT-FOLDER>              // string (Default: ./cdhit_clustr_info)


# Observation: If you want to improve it, feel free to make changes. FOSS!

BEGIN{
    if(ARGC <= 1){
        print "\nUsage:\n\n ./script.awk <CDHit.clstr> [<OUTPUT-FOLDER>]\n"
        print "\nFor more information run:\n"
        print " head -77 ./script.awk\n"
        print "The last argument is optional and could be an OUTPUT-FOLDER.\n"
        exit
    }
    #CDHit use ">Cluster" for each cluster
    #so it was chose for Record Separator
    #because of that.
    RS=">Cluster"
    #CDHit separate each seq member with a number
    #plus "\t".
    FS="\t"
    #Build output dir
    if(ARGC > 2){
        #Getting total length of output folder
        length_dir=length(ARGV[ARGC-1])
        if(substr(ARGV[ARGC-1],(length_dir - 5),6) == ".clstr"){
            #Create output directory
            output_dir=ENVIRON["PWD"]"/""cdhit_clustr_info"
        }
        if(substr(ARGV[ARGC-1],length_dir,1) == "/"){
            last_level=split(ARGV[ARGC-1], a, "/")
            output_dir=""
            for(i=1;i<(last_level-1);i++){
                output_dir= output_dir a[i] "/"
            }
            output_dir= output_dir a[last_level-1]
        }
        if((substr(ARGV[ARGC-1],(length_dir - 5),6) != ".clstr") && (substr(ARGV[ARGC-1],length_dir,1) != "/")){
            last_level=split(ARGV[ARGC-1], a, "/")
            output_dir=""
            for(i=1;i<last_level;i++){
                output_dir= output_dir a[i] "/"
            }
            output_dir= output_dir a[last_level]
        }
    }
    else{
        #Create output directory
        output_dir=ENVIRON["PWD"]"/""cdhit_clustr_info"
    }
    #Build command to be used in system
    command="mkdir -p " output_dir
    #Create output directory
    system(command)
    #Table CSV with number of clusters gt 2 members per CDHit running configuration
    CSV_file="table_num_clusters_gt_1_seq_per_CDHit_config.csv"
    #Print header of CSV_file
    printf("File_name,Clusters_gt_1seq,Total_clusters\n") > output_dir "/" CSV_file
}
BEGINFILE{
    #Finish when you are in last argument
    if(FILENAME == ARGV[ARGC-1] && substr(ARGV[ARGC-1],length_dir - 5,6) != ".clstr"){
        exit
    }
    #Setting FILE conditions
    #Get exact file name
    file_name=split(FILENAME, a, "/")
    filename=a[file_name]
    #Build command to be used in system
    command="mkdir -p " output_dir "/" filename
    #Create output directory
    system(command)
    #Set cluster counter in zero
    count=0
}
{
    #Start reading RECORDS per FILE
    #Only count those cluster with more than 1 sequence
    if(NF > 2){
        split($1,cluster_line_array," ")
        cluster_number=cluster_line_array[1]
        cluster_name="Cluster_"cluster_number".csv"
        #Print header of this cluster
        printf("Seq_code,Length\n") > output_dir "/" filename "/" cluster_name
        for(i=2;i<=NF;i++){
            member=$i
            split(member,member_array,"|")
            split(member_array[1],length_code_array,"nt, >")
            printf("%s,%s\n", length_code_array[2], length_code_array[1]) > output_dir "/" filename "/" cluster_name
        }
        #Count cluster entry
        count++
    }
    next
}
ENDFILE{
    #Print output per file
    printf("%s,%s,%s\n", filename,count,FNR-1) > output_dir "/" CSV_file
}

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
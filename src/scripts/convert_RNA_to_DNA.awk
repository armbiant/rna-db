#!/usr/bin/awk -f

BEGIN{
    RS=">"
    FS="\n"
}
{
    #Skip empty lines
    if (NF == 0){
        next
    }
    #Get query info line
    printf("%s\n", ">"$1) > "dna_file"
    #Start count character in zero
    char_position=0
    #Get seq per query
    for(i=2;i<=(NF-1);i++){
        #Get length of field to go over
        total_chars=length($i)
        #Split chunck of seq in characters
        split($i, chars, "")
        #Start to see each character
        for(j=1;j<=total_chars;j++){
            #Count character to get position
            char_position=char_position + 1
            if (chars[j] == "a" || chars[j] == "A"){
                printf("%s", "T") > "dna_file"
                continue
            }
            else if (chars[j] == "t" || chars[j] == "T"){
                printf("%s", "A") > "dna_file"
                #Split query info line
                split($1, query_array, "|")
                #Save query position and character to be reported
                unk_chars[query_array[1]]=char_position","chars[j]
                continue
            }
            else if (chars[j] == "c" || chars[j] == "C"){
                printf("%s", "G") > "dna_file"
                continue
            }
            else if (chars[j] == "g" || chars[j] == "G"){
                printf("%s", "C") > "dna_file"
                continue
            }
            else if (chars[j] == "u" || chars[j] == "U"){
                printf("%s", "A") > "dna_file"
                continue
            }
            else if (chars[j] == "n" || chars[j] == "N"){
                printf("%s", "N") > "dna_file"
                #Split query info line
                split($1, query_array, "|")
                #Save query position and character to be reported
                unk_chars[query_array[1]]=char_position","chars[j]
                continue
            }
            else if (chars[j] == "x" || chars[j] == "X"){
                printf("%s", "X") > "dna_file"
                #Split query info line
                split($1, query_array, "|")
                #Save query position and character to be reported
                unk_chars[query_array[1]]=char_position","chars[j]
                continue
            }
            else if (chars[j] == "i" || chars[j] == "I"){
                printf("%s", "I") > "dna_file"
                #Split query info line
                split($1, query_array, "|")
                #Save query position and character to be reported
                unk_chars[query_array[1]]=char_position","chars[j]
                continue
            }
            else if (chars[j] == "f" || chars[j] == "F"){
                printf("%s", "F") > "dna_file"
                #Split query info line
                split($1, query_array, "|")
                #Save query position and character to be reported
                unk_chars[query_array[1]]=char_position","chars[j]
                continue
            }
            else if (chars[j] == "w" || chars[j] == "W"){
                printf("%s", "W") > "dna_file"
                #Split query info line
                split($1, query_array, "|")
                #Save query position and character to be reported
                unk_chars[query_array[1]]=char_position","chars[j]
                continue
            }
            else if (chars[j] == "m" || chars[j] == "M"){
                printf("%s", "M") > "dna_file"
                #Split query info line
                split($1, query_array, "|")
                #Save query position and character to be reported
                unk_chars[query_array[1]]=char_position","chars[j]
                continue
            }
            #Unknown character found it
            else{
                #Split query info line
                split($1, query_array, "|")
                #Save query position and character to be reported
                unk_chars[query_array[1]]=char_position","chars[j]
                continue
            }
        }
        #Print new line for next query
        printf("\n") > "dna_file"
        continue
    }
    next
}
END{
    #Check if there is something to report
    if (length(unk_chars) > 0){
        #Print header report
        printf("%s\n%s\n", "Error::: character unknown","Query,Position,Character")
        #Check every unknown character and print it
        for(query in unk_chars){
            printf("%s\n", query","unk_chars[query])
        }
    }
}
# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
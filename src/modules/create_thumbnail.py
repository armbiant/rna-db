"""\
Purpose: With this module you can create a JPG thumbnail image from a PNG image.

Preconditions:
                - Files for input
                    * img
                - Libraries
                    * PIL

Arguments:    -Positional
                * [img]                       // string
                * [output_file]               // string

Observations:
            - 

TODO: -
"""


import argparse
from PIL import Image # https://python-pillow.org/
# Ignore warning message below.
from warnings import simplefilter
simplefilter('ignore', Image.DecompressionBombWarning)


def create_thumbnail(img, output_file):
    """
    Creates a nearly ~(1071, 958)px thumbnail image (TIMG). If the input
    PNG image is smaller than 1071px, nothing is done.
    Parameters:
                * img:          a string. Correspond to a PNG image.
                * output_file:  a string. Correspond to an output filename
                                          to store the JPG file.
    Observations:
                * Takes a PNG (>1071, 958)px --> JPG, (1071, 958)px
                * Values (1071, 958) were setted by hand for CoDNaS-RNA Web.
    TODO: * Parametrize height and width sizes.
    Return: nothing
    """
    img_png = Image.open(img)
    if img_png.size[0] > 1071:
        img_rgb = img_png.convert('RGB')
        img_rgb.thumbnail((1071, 958))
        img_rgb.save(output_file)
        img_rgb.close()
    img_png.close()
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create a JPG ~(1071, 958)px thumbnail image from another PNG image."
    )
    # Positional arguments
    p.add_argument(
        'img',
        type=str,
        help='Path of a PNG image.'
    )
    p.add_argument(
        'output_file',
        type=str,
        help='Path to store JPG thumbnail image.'
    )

    args = p.parse_args()
    create_thumbnail(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
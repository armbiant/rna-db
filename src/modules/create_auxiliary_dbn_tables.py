"""\
Purpose: With this module you can create all auxiliary dbn tables using raw DSSR dbn
         tables. These auxiliary tables will have only CoDNaS-RNA's conformers info inside.
         Created tables preserve the same separate value format as originals.

Preconditions:
                - Files for input
                    * DSSR/dbn/               (all dbn tables)
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [dbnDir]                       // string
                * [a_separator]                  // string
                * [pdbchains_databases_codes]    // string
                * [outputDir]                    // string

Observations:
            - 

TODO: -
"""


import argparse
import os
import pandas as pd
import glob
from collections import defaultdict
from build_ok_outputDir_string import build_ok_outputDir_string
from get_website_df import get_df_if_file_exist
from guess_extension import guess_extension_from_separator
from create_dir import create_dir
from save_website_table import save_website_table


def get_filename(afile):
    """
    Get a file name from an absolute file path.
    Parameters:
                * afile:                 a string. Correspond to a full
                                                   path of a file.
    Return: a string
    """
    file_name = os.path.basename(afile)
    file_name = os.path.splitext(file_name)[0]
    return file_name


def get_dbn_table(a_file_path, a_separator):
    """
    Get a dataframe from an absolute file path using the given separator.
    Parameters:
                * a_file_path                a string. Correspond to a raw DSSR dbn table.
                * a_separator:               a string. Correspond to the dbn tables separator.
    Return: a dataframe
    """
    # All raw DSSR hbond tables have the same column arrange.
    if os.path.exists(a_file_path) and os.path.getsize(a_file_path):
        dbn_table = pd.read_table(
            a_file_path,
            sep=a_separator,
            dtype=str,
            keep_default_na=False,
        )
    else:
        if os.path.getsize(a_file_path):
            print("Error:\t" + a_file_path + "\thas not a dbn")
        else:
            print("Error:\t" + a_file_path + "\tan empty dbn")
        dbn_table = pd.DataFrame()
    return dbn_table


def get_list_of_files(aDir, extformat):
    """
    Get a list of absolute paths for all "extformat" files present in "aDir".
    Parameters:
                * aDir:                     a string. Correspond to a directory path where all
                                                      extformat files are stored.
                * extformat:                a string. Correspond to format extention (e.g.: tsv).
    Return: a list
    """
    if os.path.exists(aDir):
        #Get data
        file_list = glob.glob(aDir + '*.' + extformat)
        if not file_list:
            raise ValueError('[Error] No files ended with {} were found on {}.'.format('.'+extformat, aDir))
    else:
        raise NameError('[Error] Directory not exists: {}.'.format(aDir))
    return file_list


def get_pdb_chain_dict(pdbchains_databases_codes, a_separator):
    """
    Get a pdb_chain dictionary containing all unique chains codes per PDB
    in pdbchains_databases_codes.tsv table. 
    Parameters:
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * a_separator:               a string. Correspond to the pdbchains_databases_codes.tsv
                                                       table separator.
    Return: a dict
    """
    # Get a dataframe from table
    mapping_cltrs = get_df_if_file_exist(
        "mapping_cltrs", pdbchains_databases_codes, a_separator, True
    )

    # Remove those rows that do not have cluster assigned.
    mapping_cltrs_copy = mapping_cltrs.dropna(subset=["Cluster_ID"]).copy()

    # Generate two columns from "PDB_chain" to be used in zip function.
    mapping_cltrs_copy["pdb"], mapping_cltrs_copy["chain"] = (
        mapping_cltrs_copy["PDB_chain"].str.split("\:").str
    )

    # Ordered dict chain-chain
    pdb_chain_dict = defaultdict(list)
    # Make a zip data variable of cluster with PDB:chain code.
    data = zip(mapping_cltrs_copy["pdb"], mapping_cltrs_copy["chain"])
    # Create a dict of clusters with a list of PDB:chain codes present per cluster.
    for pdb, chain in data:
        # PDB codes are in lower case
        pdb = pdb.lower()
        if pdb in pdb_chain_dict.keys():
            pdb_chain_dict[pdb].append(chain)
        else:
            pdb_chain_dict[pdb] = [chain]

    return pdb_chain_dict


def get_only_cdrna_dbn_table(dbn_table, chain_list):
    """
    Get a sub-set dataframe with only the given chain list.
    Parameters:
                * dbn_table                a dataframe. Correspond to a raw DSSR dbn table.
                * chain_list:              a list. Correspond to a list of PDB chains.
    Return: a dataframe
    """
    # Get only rows with correct conformers chains (let outside chains not present in CoDNaS-RNA)
    dbn_cdrna_table = dbn_table[dbn_table["chain_id"].isin(chain_list)].copy()
    return dbn_cdrna_table


def create_files( dbnDir
                , a_separator
                , pdbchains_databases_codes
                , outputDir ):
    """
    Create all auxilary dbn tables using raw DSSR dbn tables.
    Parameters:
                * dbnDir:                    a string. Correspond to a directory path where all
                                                       raw DSSR dbn tables are stored.
                * a_separator:               a string. Correspond to the raw DSSR dbn tables separator.
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       processed auxiliary dbn tables.
    Return: nothing
    """
    # Before start to do anything, check if input directory is OK
    dbnDir = build_ok_outputDir_string(dbnDir)

    a_separator, ext = guess_extension_from_separator(a_separator)

#    cltrs_list = ["529","851", "7", "733", "516", "846", "726", "410", "812", "915", "737", "499", "952", "12"]

    dbn_files_list = get_list_of_files(dbnDir, ext.replace(".", ""))
    pdb_chain_dict = get_pdb_chain_dict(pdbchains_databases_codes, "\t")

    main_table_dict = {}
    for a_file_path in dbn_files_list:
        afilename = get_filename(a_file_path)
        dbn_table = get_dbn_table(a_file_path, a_separator)
        if not dbn_table.empty:
            if not os.path.exists(outputDir):
                create_dir(outputDir)
            # Only check for CoDNaS-RNA chains
            chain_list = pdb_chain_dict[afilename]
            dbn_cdrna_table = get_only_cdrna_dbn_table(dbn_table, chain_list)
            # Save auxliary dbn table.
            save_website_table(dbn_cdrna_table,
                               outputDir,
                               afilename,
                               a_separator
            )
    print("All done !")
    return


# dbnDir = '/home/martingb/Projects/2020/CoDNaS-RNA/outputs/2020-05-15/DSSR/dbn/'
# pdbchains_databases_codes = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/PDBchains_with_databases_codes.tsv'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/DSSR/dbn/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create auxilary dbn tables from raw DSSR dbn tables."
    )
    # Positional arguments
    p.add_argument(
        "dbnDir",
        type=str,
        help="Path where are stored all raw DSSR dbn tables."
    )
    p.add_argument(
        "a_separator",
        type=str,
        help="Correspond to the raw DSSR dbn tables separator."
    )
    p.add_argument(
        "pdbchains_databases_codes",
        type=str,
        help="Table with all PDB chains with their related codes.",
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store auxiliary dbn tables."
    )

    args = p.parse_args()
    create_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
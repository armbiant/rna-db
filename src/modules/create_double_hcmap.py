"""\
Purpose: With this module you can create a PNG image of a symmetric
         double matrix dataset as a hierarchically-clustered heatmap.
         Example of dataset:
                            * conformers       // string
                            * conformers       // string
                            * rmsd             // string

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * Numpy
                    * Pandas
                    * Matplotlib
                    * Seaborn
                    * Scipy
                    * PIL

Arguments:    -Positional
                * [data]                         // dataframe
                * [xlabel_triu]                  // string
                * [xlabel_tril]                  // string
                * [vmin_cbar_tril]               // number
                * [vmax_cbar_tril]               // number
                * [font_size]                    // number
                * [output_file]                  // string

Observations:
            - 

TODO: - Improve get_hcmap(data, aHcType) to be used in the future.
"""


import argparse
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import math
from scipy.cluster import hierarchy
from PIL import Image # https://python-pillow.org/
# Ignore warning message below. Taken from: https://stackoverflow.com/questions/60817473/warning-uncondensed-distance-matrix-in-python
from warnings import simplefilter
simplefilter("ignore", hierarchy.ClusterWarning)
simplefilter('ignore', Image.DecompressionBombWarning)


# Function to extract clustered data based on original data.
def extract_clustered_table(res, data):
    """
    input
    =====
    res:     <sns.matrix.ClusterGrid>  the clustermap object
    data:    <pd.DataFrame>            input table
    
    Observations:
        * Taken from: https://www.linkedin.com/pulse/retrieving-clustered-table-stefano-manzini/
    Returns: <pd.DataFrame>            reordered input table
    """
    # if sns.clustermap is run with row_cluster=False:
    if res.dendrogram_row is None:
        print("Apparently, rows were not clustered.")
        return -1
    
    if res.dendrogram_col is not None:
        # reordering index and columns
        new_cols = data.columns[res.dendrogram_col.reordered_ind]
        new_ind = data.index[res.dendrogram_row.reordered_ind]
        return data.loc[new_ind, new_cols]
    else:
        # reordering the index
        new_ind = data.index[res.dendrogram_row.reordered_ind]
        return data.loc[new_ind,:]


def get_heatmap_masks(data):
    """
    Get aboth heatmap masks (lower and upper triangle) for a dataset.
    Matrix (data) must be symmetric.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            heatmap dataset.
    Return: two dataframes
    """
    maskl = np.zeros_like(data)
    maskl[np.tril_indices_from(maskl)] = True
    masku = np.zeros_like(data)
    masku[np.triu_indices_from(masku)] = True
    return maskl, masku


def get_cellSizePixels(data):
    """
    Get a customized pixel cell size for a clustermap dataset.
    Matrix (data) must be symmetric.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            clustermap dataset.
    Observation:
        Tests were done using extreme clusters:
            # Tested on (3, 100, 16) and (38, 30, 16).
                * Format (#conf, cellSizePixels, fontSize)
            # For bigger clusters  and (>38, ?, 16) shows a 
              hyperbolic asymptote constant value at cellSizePixels~=30.
                * Tested on (271, ?, 16), (205, ?, 16).
    Return: an int
    """
    if data.shape[0] > 38:
        cellSizePixels = 30
    else:
        # Logaritmic function created using https://www.wolframalpha.com/ 
        cellSizePixels = -27.5702*math.log(0.00886435*data.shape[0])
    return cellSizePixels


def get_fontCellSize(data):
    """
    Get a customized font cell size for a clustermap dataset.
    Matrix (data) must be symmetric.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            clustermap dataset.
    Observation:
        Tests were done using extreme clusters and 
        get_cellSizePixels() function:
            # Tested on (3, 100, 16) and (38, 30, 16).
                * Format (#conf, cellSizePixels, fontSize)
            #
    Return: a string
    """
    if data.shape[0] >= 38:
        fontCellSize = "small"
    elif data.shape[0] >= 20:
        fontCellSize = "medium"
    elif data.shape[0] >= 10:
        fontCellSize = "large"
    else:
        fontCellSize = "x-large"
    return fontCellSize


def get_figsize(data):
    """
    Get a customized figure size for a clustermap dataset.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            clustermap dataset.
    Observation:
        * Taken from: https://stackoverflow.com/questions/52806878/seaborn-clustermap-fixed-cell-size
    Return: a tuple of two ints
    """
    cellSizePixels = get_cellSizePixels(data)
    dpi = plt.rcParams['figure.dpi']
    marginWidth = plt.rcParams['figure.subplot.right']-plt.rcParams['figure.subplot.left']
    marginHeight = plt.rcParams['figure.subplot.top']-plt.rcParams['figure.subplot.bottom']
    Ny,Nx = data.shape
    figWidth = (Nx*cellSizePixels/dpi)/0.8/marginWidth
    figHeigh = (Ny*cellSizePixels/dpi)/0.8/marginHeight
    return (figWidth,figHeigh)


def get_rowLinkage(data):
    """
    Get a hierarchy dendrogram for a clustermap dataset.
    It use 'complete' method and 'euclidean' distance as default.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            clustermap dataset.
    Return: a dendrogram
    """
    return hierarchy.linkage( data
                            , method='complete'
                            , metric='euclidean'
                            , optimal_ordering=False)

def get_hcmap(data, aHcType):
    """
    Get a hierarchy dendrogram for a clustermap dataset.
    It use 'complete' method and 'euclidean' distance as default.
    Matrix (data) must be symmetric.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            clustermap dataset.
                * aHcType:     a string. Correspond to a clustermap type
                                         of base, lower or upper.
    Return: a clustermap
    """
    try:
        data.shape[0] > 2 and data.shape[0] == data.shape[1]
    except ValueError:
        raise ValueError('ValueError: DataFrame has not a square shape or has less than 3 rows/columns')

    if aHcType.lower() == 'base':
        mask        = None
        row_linkage = get_rowLinkage(data)
        cmap        = 'inferno'
        vmin        = None
        vmax        = None
        cbar_kws    = None
        annot_kws   = None
        tree_kws    = None
        figsize     = None
    elif aHcType.lower() == 'lower':
        _, mask     = get_heatmap_masks(data)
        row_linkage = get_rowLinkage(data)
        cmap        ='Blues'
        vmin        = 0
        vmax        = 5
        cbar_kws    = None
        annot_kws   = {'fontsize': get_fontCellSize(data)}
        tree_kws    = {'colors': 'white', 'linewidths':0.9}
        figsize     = get_figsize(data)
    elif aHcType.lower() == 'upper':
        mask, _     = get_heatmap_masks(data)
        print(mask)
        row_linkage = get_rowLinkage(data)
        cmap        = 'crest'
        vmin        = None
        vmax        = None
        cbar_kws    = {'orientation': 'horizontal'}
        annot_kws   = {'fontsize': get_fontCellSize(data)}
        tree_kws    = {'colors': 'black', 'linewidths':0.9}
        figsize     = get_figsize(data)
    else:
        raise ValueError('ValueError: {} is not \'lower\' or \'upper'.format(aHcType))

    return sns.clustermap( data=data
                         , mask=mask
                         , row_linkage=row_linkage
                         , col_linkage=row_linkage
                         , cmap=cmap
                         , linewidths=0.5
                         , annot=True
                         , col_cluster=False
                         , row_cluster=True
                         , fmt='0.2f'
                         , vmin=vmin
                         , vmax=vmax
                         , cbar_pos=None
                         , cbar_kws=cbar_kws
                         , annot_kws=annot_kws
                         , tree_kws=tree_kws
                         , figsize=figsize
                         , square=True )

def get_hcmap_base(data):
    """
    Get a base hierarchy dendrogram for a clustermap dataset.
    Matrix (data) must be symmetric.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            clustermap dataset.
    Return: a clustermap
    """
    row_linkage = get_rowLinkage(data)
    return sns.clustermap( data=data
                         , row_linkage=row_linkage
                         , col_linkage=row_linkage
                         , annot=True
                         , fmt='0.2f'
                         , square=True )


def get_hcmap_tril(data, vmin, vmax):
    """
    Get a lower clustermap triangle for a clustermap dataset.
    Matrix (data) must be symmetric.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            clustermap dataset.
                * vmin:        a number. Correspond to a minimum value
                                         of the colorbar (int or float).
                * vmax:        a number. Correspond to a maximum value
                                         of the colorbar (int or float).
    Return: a clustermap
    """
    _, mask = get_heatmap_masks(data)
    row_linkage = get_rowLinkage(data)
    return sns.clustermap( data=data
                         , mask=mask
                         , row_linkage=row_linkage
                         , col_linkage=row_linkage
                         , cmap='Blues'
                         , linewidths=0.5
                         , annot=True
                         , col_cluster=False
                         , row_cluster=True
                         , fmt='0.2f'
                         , vmin=vmin
                         , vmax=vmax
                         , cbar_pos=None
                         , annot_kws={'fontsize': get_fontCellSize(data)}
                         , tree_kws={'colors': 'black', 'linewidths':0.9}
                         , figsize=get_figsize(data)
                         , square=True )


def get_hcmap_triu(data):
    """
    Get a upper clustermap triangle for a clustermap dataset.
    Matrix (data) must be symmetric.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            clustermap dataset.
    Return: a clustermap
    """
    mask, _ = get_heatmap_masks(data)
    row_linkage = get_rowLinkage(data)
    return sns.clustermap( data=data
                         , mask=mask
                         , row_linkage=row_linkage
                         , col_linkage=row_linkage
                         , cmap='crest'
                         , linewidths=0.5
                         , annot=True
                         , col_cluster=False
                         , row_cluster=True
                         , fmt='0.2f'
                         , cbar_pos=None
                         , cbar_kws={'orientation': 'horizontal'}
                         , annot_kws={'fontsize': get_fontCellSize(data)}
                         , tree_kws={'colors': 'white', 'linewidths':0.9}
                         , figsize=get_figsize(data)
                         , square=True )


def create_superpose_images( bg_img
                           , fg_img
                           , output_file ):
    """
    Create an image (PNG) superposing two PNG images.
    Only fg_img must be transparent.
    Superposition is done from (0.0) coordinates.
    Parameters:
                * bg_img:          a string. Correspond to a background PNG file.
                * fg_img:          a string. Correspond to a foreground PNG file.
                * output_file:     a string. Correspond to an output filename
                                             to store the superposed PNG file.
    Return: nothing
    """
    background = Image.open(bg_img)
    foreground = Image.open(fg_img)

    background.paste(foreground, (0, 0), foreground)
    background.save(output_file)
    return


def remove_hcmap_tri_imgs(aList):
    """
    Remove created clustermap images (*hcmaptril.png and *hcmaptriu.png).
    Parameters:
                * aList:          a list. Correspond to a list of triangle
                                          PNG files.
    Return: nothing
    """
    for aFile in aList:
        os.remove(aFile)
    return


def create_hcmap_figure( dataset
                       , xlabel_triu
                       , xlabel_tril
                       , vmin_cbar_tril
                       , vmax_cbar_tril
                       , font_size
                       , output_file ):
    """
    Create a full PNG image of a symmetric clustermap from a dataset.
    Parameters:
                * data:               a dataframe. Correspond to a CoDNaS-RNA
                                                   clustermap dataset.
                * xlabel_triu:        a string. Correspond to the label of axis 'x'
                                                of the upper triangle.
                * xlabel_tril:        a string. Correspond to the label of axis 'x'
                                                of the lower triangle.
                * vmin_cbar_tril:     a number. Correspond to a minimum value
                                                of the lower triangle colorbar
                                                (int or float).
                * vmax_cbar_tril:     a number. Correspond to a maximum value
                                                of the lower triangle colorbar
                                                (int or float).
                * font_size:          a number. Correspond to a number (int/float)
                                                value of the tick labels of the
                                                clustermap.
                * output_file:        a string. Correspond to an output filename
                                                to store the PNG clustermap file.
    Return: nothing
    """
    # Check dataset shape
    try:
        dataset.shape[0] > 2 and dataset.shape[0] == dataset.shape[1]
    except ValueError:
        raise ValueError('ValueError: DataFrame has not a square shape or has less than 3 rows/columns')
    
    outputDirName = os.path.dirname(output_file)
    hcmaptril = outputDirName+'/'+'hcmaptril.png'
    hcmaptriu = outputDirName+'/'+'hcmaptriu.png'

    ####################################################
    # Clustermap base
    ####################################################
    # Get base clustermap
    # cg_base = get_hcmap(dataset, 'base')
    cg_base = get_hcmap_base(dataset)

    # Get reordered data
    ordered = extract_clustered_table(cg_base, dataset)

    # To close any ploting process
    plt.close()

    ####################################################
    # Clustermap tril
    ####################################################
    # Get clustermap triangle lower
    #cg_tril = get_hcmap(ordered, 'lower')
    cg_tril = get_hcmap_tril( ordered
                            , vmin=vmin_cbar_tril
                            , vmax=vmax_cbar_tril )


    fig, ax = cg_tril.fig, cg_tril.ax_heatmap

    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)

    ax.set_yticklabels( ax.get_yticklabels()
                      , fontdict={ 'fontsize': font_size
                                 , 'verticalalignment': 'center'}
                      , rotation=0 )
    ax.set_xticklabels( ax.get_xticklabels()
                      , fontdict={ 'fontsize': font_size
                                 , 'horizontalalignment': 'center'}
                      , rotation=90 )

    cg_tril.ax_col_dendrogram.set_visible(False)

    #Get default parameters for positioning of axes
    ax_tightbbox = ax.get_tightbbox(fig.canvas.get_renderer()).inverse_transformed(fig.transFigure)
    ax_extent = ax.get_window_extent(fig.canvas.get_renderer()).inverse_transformed(fig.transFigure)

    #Create colorbar and make other modificaitions to figure:
    cb_ax_loc = [ ax.get_position().x0
                , ax_tightbbox.y0
                , (ax_extent.x1 - ax_extent.x0)
                , 0.025 ] #in figure coordinates, left margin, bottom margin, width, and height for new colorbar axis
    cb_ax = fig.add_axes(cb_ax_loc) #create new axes solely for colorbar
    cbar1_pos = cb_ax.get_tightbbox(fig.canvas.get_renderer()).inverse_transformed(fig.transFigure)
    width_cbar1 = cbar1_pos.y1 - cbar1_pos.y0
    space = width_cbar1*0.5
    cb_ax_loc = [ ax.get_position().x0
                , ax_tightbbox.y0 - (width_cbar1 + space)
                , (ax_extent.x1 - ax_extent.x0)
                , 0.025 ]
    cb_ax.remove()
    cb_ax = fig.add_axes(cb_ax_loc)
    c = fig.colorbar( ax.collections[0]
                    , cax=cb_ax
                    , orientation='horizontal'
                    , shrink=0.5 ) #generate colorbar
    c.ax.set_xlabel( xlabel=xlabel_tril
                   , labelpad=10
                   , weight='bold'
                   , fontdict={'fontsize': font_size} )
    
    c.ax.tick_params(labelsize=font_size)
    c.ax.xaxis.set_ticks_position('bottom')
    c.ax.xaxis.set_label_position('top')

    # To be used on the triu clustermap
    cbar1_pos_new = c.ax.get_tightbbox(fig.canvas.get_renderer()).inverse_transformed(fig.transFigure)

    plt.savefig(hcmaptril, transparent=True, bbox_inches='tight')
    plt.close(fig)

    ####################################################
    # Clustermap triu
    ####################################################
    # Get clustermap triangle upper
    #cg_triu = get_hcmap(ordered, 'upper')
    cg_triu = get_hcmap_triu(ordered)

    cg_triu.ax_col_dendrogram.set_visible(False)

    fig2, ax2 = cg_triu.fig, cg_triu.ax_heatmap

    ax2.yaxis.label.set_visible(False)
    ax2.xaxis.label.set_visible(False)
    
    ax2.set_yticklabels( ax2.get_yticklabels()
                       , fontdict={ 'fontsize': font_size
                                  , 'verticalalignment': 'center'}
                       , rotation=0
                       , color='white' )
    ax2.set_xticklabels( ax2.get_xticklabels()
                       , fontdict={ 'fontsize': font_size
                                  , 'horizontalalignment': 'center'}
                       , rotation=90
                       , color='white' )
    cb_ax_loc = [ ax2.get_position().x0
                , cbar1_pos_new.y0 - (width_cbar1 + space)
                , (ax_extent.x1 - ax_extent.x0)
                , 0.025 ] #in figure coordinates, left margin, bottom margin, width, and height for new colorbar axis
    cb_ax = fig2.add_axes(cb_ax_loc) #create new axes solely for colorbar
    c2 = fig2.colorbar( ax2.collections[0]
                      , cax=cb_ax
                      , orientation='horizontal'
                      , shrink=0.5 )

    c2.ax.set_xlabel( xlabel=xlabel_triu
                    , labelpad=10
                    , weight='bold'
                    , fontdict={'fontsize': font_size} )
    
    c2.ax.tick_params(labelsize=font_size)
    c2.ax.xaxis.set_ticks_position('bottom')
    c2.ax.xaxis.set_label_position('top')

    plt.savefig(hcmaptriu, transparent=False, bbox_inches='tight')
    plt.close(fig2)

    ####################################################
    # Superpose clustermap triangles
    ####################################################
    create_superpose_images( bg_img=hcmaptriu
                           , fg_img=hcmaptril
                           , output_file=output_file )

    remove_hcmap_tri_imgs([hcmaptril, hcmaptriu])

    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create a PNG image of a symmetric double matrix dataset as a hierarchically-clustered heatmap."
    )
    # Positional arguments
    p.add_argument(
        "dataset",
        type=pd.Dataframe,
        help="A dataset (2D Pandas dataframe) to be plotted ."
    )
    p.add_argument(
        "xlabel_triu",
        type=str,
        help="Correspond to the label of axis 'x' of the upper triangle."
    )
    p.add_argument(
        "xlabel_tril",
        type=str,
        help="Correspond to the label of axis 'x' of the lower triangle."
    )
    p.add_argument(
        "vmin_cbar_tril",
        help="Correspond to a minimum value of the lower triangle colorbar (int or float).",
    )
    p.add_argument(
        "vmax_cbar_tril",
        help="Correspond to a maximum value of the lower triangle colorbar (int or float).",
    )
    p.add_argument(
        "font_size",
        help="Correspond to a number (int/float) value of the tick labels of the clustermap",
    )
    p.add_argument(
        "output_file",
        type=str,
        help="Correspond to an output filename to store the PNG clustermap file"
    )

    args = p.parse_args()
    create_hcmap_figure(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
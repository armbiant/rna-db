"""\
Purpose: With this module you can create all auxilary hbond tables using raw hbond DSSR
         tables. Tables created preserve the same separate value format as originals.

Preconditions:
                - Files for input
                    * DSSR/hbonds/               (all hbonds tables)
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [hbondDir]                     // string
                * [a_separator]                  // string
                * [outputDir]                    // string

Observations:
            - 

TODO: -
"""


import argparse
import os
import glob
import pandas as pd
from save_website_table import save_website_table
from guess_extension import guess_extension_from_separator
from create_dir import create_dir
from build_ok_outputDir_string import build_ok_outputDir_string


def get_list_of_files(aDir, extformat):
    """
    Get a list of absolute paths for all "extformat" files present in "aDir".
    Parameters:
                * aDir:                     a string. Correspond to a directory path where all
                                                      extformat files are stored.
                * extformat:                a string. Correspond to format extention (e.g.: tsv).
    Return: a list
    """
    if os.path.exists(aDir):
        #Get data
        file_list = glob.glob(aDir + '*.' + extformat)
        if not file_list:
            raise ValueError('[Error] No files ended with {} were found on {}.'.format('.'+extformat, aDir))
    else:
        raise NameError('[Error] Directory not exists: {}.'.format(aDir))
    return file_list


def get_filename(afile):
    """
    Get a file name from an absolute file path.
    Parameters:
                * afile:                 a string. Correspond to a full
                                                   path of a file.
    Return: a string
    """
    file_name = os.path.basename(afile)
    file_name = os.path.splitext(file_name)[0]
    return file_name


def get_raw_hbond_table(a_file_path, a_separator):
    """
    Get a file name from an absolute file path.
    Parameters:
                * a_file_path                a string. Correspond to a raw DSSR hbond table.
                * a_separator:               a string. Correspond to the hbond tables separator.
    Return: a dataframe
    """
    # All raw DSSR hbond tables have the same column arrange.
    hb_columns = [ "index"
                 , "atom1_serNum"
                 , "atom2_serNum"
                 , "donAcc_type"
                 , "distance"
                 , "atom1_id"
                 , "atom2_id"
                 , "atom_pair"
                 , "residue_pair" ]

    if os.path.exists(a_file_path) and os.path.getsize(a_file_path):
        raw_hbond_table = pd.read_table(
            a_file_path,
            sep=a_separator,
            dtype=str,
            keep_default_na=False,
            names=hb_columns,
        )
    else:
        if os.path.getsize(a_file_path):
            print("Error:\t" + a_file_path + "\thas not a hbond")
        else:
            print("Error:\t" + a_file_path + "\tan empty hbond")
        raw_hbond_table = pd.DataFrame()
    return raw_hbond_table


def get_auxiliary_hbond_table(raw_hbond_table):
    """
    Create a clean hbond table from "raw_hbond_table" and save it.
    Parameters:
                * raw_hbond_table            a string. Correspond to a raw DSSR hbond table.
    Return: a dataframe
    """

    for num in ["1", "2"]:
        # Generate 4 columns from "atom1_id" and "atom2_id" to be used in zip function. (ATOM@MODEL..CHAIN.RESID.RESID_NUM)
        raw_hbond_table["atom_model_" + num], raw_hbond_table[
            "chain_resid_redidNum_" + num
        ] = (raw_hbond_table["atom" + num + "_id"].str.split("\.\.").str)
        raw_hbond_table["atom_" + num], raw_hbond_table["model_" + num] = (
            raw_hbond_table["atom_model_" + num].str.split("\@").str
        )
        raw_hbond_table["chain_" + num], raw_hbond_table["resid_" + num], raw_hbond_table[
            "residNum_" + num
        ], raw_hbond_table["insertion_code_" + num] = (
            raw_hbond_table["chain_resid_redidNum_" + num].str.split("\.").str
        )
        # Remove unused columns
        columns_to_remove = ["chain_resid_redidNum_" + num, "atom_model_" + num]
        hbond_table = raw_hbond_table.drop(columns=columns_to_remove)

    return hbond_table


def create_files( hbondDir
                , a_separator
                , outputDir ):
    """
    Create all auxilary hbond tables using raw DSSR hbond tables.
    Parameters:
                * hbondDir:                  a string. Correspond to a directory path where all
                                                       raw DSSR hbond tables are stored.
                * a_separator:               a string. Correspond to the raw DSSR hbond tables separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       processed auxiliary hbond tables.
    Return: nothing
    """

    # Before start to do anything, check if outputDir is correct and create it.
    hbondDir = build_ok_outputDir_string(hbondDir)
    outputDir = build_ok_outputDir_string(outputDir)
    create_dir(outputDir)

    a_separator, ext = guess_extension_from_separator(a_separator)
    

    hbond_files_list = get_list_of_files(hbondDir, ext.replace(".", ""))

    for a_file_path in hbond_files_list:
        afilename = get_filename(a_file_path)
        raw_hbond_table = get_raw_hbond_table( a_file_path
                                             , a_separator )
        if raw_hbond_table.empty:
            continue
        else:
            hbond_table = get_auxiliary_hbond_table(raw_hbond_table)
            # Save auxliary atom table.
            save_website_table( hbond_table
                              , outputDir
                              , afilename
                              , a_separator )

    print("All done !")
    return

# hbondDir = '/home/martingb/Projects/2020/CoDNaS-RNA/outputs/2020-05-15/DSSR/hbonds/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/DSSR/hbonds/'

if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create auxilary hbond tables to be used in cdrna_interactions.csv.gz."
    )
    # Positional arguments
    p.add_argument(
        "hbondDir",
        type=str,
        help="Path where are stored all raw DSSR hbonds tables."
    )
    p.add_argument(
        "a_separator",
        type=str,
        help="Correspond to the raw DSSR hbond tables separator."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store auxiliary hbonds interaction tables."
    )

    args = p.parse_args()
    create_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
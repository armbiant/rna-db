"""\
Purpose: With this module you can get a list of PDB_MODEL_CHAIN names
         from a wwPDB_code.cif structure file".

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * gemmi

Arguments:    -Positional
                * [pdb_chain]                      // string
                * [outputDir]                      // string

Observations: -

TODO: - Make it work for:
                * another formats (PDB, MMTF, bundle, etc)
                * gzip files
"""

import argparse
import os.path
import gemmi
from build_ok_outputDir_string import build_ok_outputDir_string
#from Bio.PDB.MMCIFParser import MMCIFParser
#import Bio.PDB.Selection as Selection


def get_structure_cif_file(pdb_code, cif_dir):
    """
    Get a stucture file.
    Parameters:
                * pdb_code:              a string. Correspond to PDB code.
                * cif_dir:               a string. Correspond to path directory
                                                   where all cif files are stored.
    Return: a file of one PDB structure.
    """
    cif_dir = build_ok_outputDir_string(cif_dir)
    if not os.path.exists(cif_dir + pdb_code + ".cif"):
        raise FileNotFoundError('[Error] File not exists: {}.cif'.format(cif_dir + pdb_code))

    # This two lines are from old implementation with Bio.PDB
    # filename = open(cif_dir + pdb_code + ".cif", "r")
    # structure_file = MMCIFParser().get_structure(pdb_code, filename)

    # This lines are from a new implementation with gemmi
    filename = cif_dir + pdb_code + ".cif"
    structure_file = gemmi.read_structure(filename)

    return structure_file


def create_full_conformer_basename(pdb_chain, model):
    """
    Create a complete conformer filename, like "PDB_MODEL_CHAIN.cif".
    Parameters:
                * pdb_chain:             a string. Correspond to PDB:Chain code.
                * model:                 a string. Correspond to one model name.
    Return: a string
    """
    pdb_code = pdb_chain.split(":")[0].lower()
    chain = pdb_chain.split(":")[1]
    conf_filename = pdb_code + "_" + model + "_" + chain + ".cif"
    return conf_filename


def get_pdb_model_chains_list(pdb_chain, cif_dir):
    """
    Get a list of PDB_MODEL_CHAIN filenames of one PDB:Chain code.
    Parameters:
                * pdb_chain:                 a string. Correspond to PDB:Chain code.
                * cif_dir:                   a string. Directory where all cif files
                                                       are stored.
    Return: a list of PDB MODEL CHAINS.
    """
    pdb_code = pdb_chain.split(":")[0].lower()
    structure_file = get_structure_cif_file(pdb_code, cif_dir)

    # This two lines are from old implementation with Bio.PDB
    # model_list = Selection.unfold_entities(structure_file, 'M')
    # model_num_list = [(model_list[a_model]).id for a_model in range(len(model_list))]

    # This lines are from a new implementation with gemmi
    model_list = [structure_file[model_num].name for model_num in range(len(structure_file))]
    conf_filenames = [
        create_full_conformer_basename(pdb_chain, model) for model in model_list
    ]
    return conf_filenames


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Get PDB_MODEL_CHAIN names list from a wwPDB_code.cif structure file."
    )
    # Positional arguments
    p.add_argument(
        "pdb_chain",
        type=str,
        help="PDB:chain code to get models from .cif file."
    )
    p.add_argument(
        "cif_dir",
        type=str,
        help="Output directory to search *.cif file."
    )

    args = p.parse_args()
    get_pdb_model_chains_list(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
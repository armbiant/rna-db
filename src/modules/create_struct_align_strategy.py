"""\
Purpose: With this module you can create gzips group packages to
         run whole combinations from all clusters in CoDNaS-RNA in
         every GNU/linux system that has build-essentials installed.
         The main purpose is to build a well distributed running strategy.

Preconditions:
                - Inputs
                    * struct_alig_report: file,   "Structural alignment report tsv table".
                    * comb_files_Dir:     string, "Path where all clusters combinations files are stored", (Clusters_combinations/).
                    * data_pdbs_models:   string, "Path where all pdb models are stored", (data_codnas_rna_model_chains/).
                    * scripts_Dir:        string, "Path where all script files are stored", (run_tmalign.sh & sbg-cetools).
                - Modules
                    * In-House: structural_align_report

Arguments:    -Positional
                * [struct_alig_report]     // string
                * [struct_packs_Dir]       // string
                * [comb_files_Dir]         // string
                * [data_pdbs_models]       // string
                * [scripts_Dir]            // string

              -Optional
                * "-t", "--time_percent"      // float, default=0.43.

Observations:
            - README.md is build it here on the fly.

            - Check that you have enough space to create these packages
              each one has copied the requiered pdb models.



TODO: - Improved number of packages creation. Now has 3 and were manually selected.

"""

import argparse
import os
import pandas as pd
import shutil
import glob
import structural_align_report
import math


def get_packs_list(struct_alig_report, time_percent):
    """
    Get a list of package groups considering a time percentage run strategy.
    Parameters:
                    * struct_alig_report: a string. Correspond to a "Structural alignment report tsv table".
                    * time_percent: a float. Correspond to decide the percentage of total time per package group.
    Return: a list of packages.
    """
    structural_alig_report = pd.read_table(struct_alig_report,  sep='\t', header=0)
    columns = ['Cluster_ID', 'Total_conformers', 'Total_models', 'Total_combinations', 'Time_per_pair_seconds', 'Size_per_pair_bytes', 'Total_Min', 'Total_MB']
    clusters_pack = pd.DataFrame(columns=columns)

#   TODO: Ckeck which is/are the big/gger cluster/s in total time to keep it/them separately from the rest.
#          -In this case, looking the table, we see Cluster_4

    #Create a list of cluster names sorted (is better handle cluster numbers)
    clusters_list = sorted([int(cltr_id.strip("Cluster_")) for cltr_id in structural_alig_report['Cluster_ID'].to_list()], reverse=True)
    # Re-make names to get all correct names
    clusters_list = ["Cluster_" + str(cltr_id) for cltr_id in clusters_list]

    #Initialize pack_time
    pack_time = 0
    #Initialize pack_size
    pack_size = 0
    #Initialize packs_list
    packs_list = []
    #Initialize cltrs_pack
    cltrs_pack = []
    #Start to process all clusters
    for cltr_id in clusters_list:
        if cltr_id == 'Cluster_4':
            continue
        #Search if you already have stored the size per sup file.
        if not pd.isnull(structural_alig_report['Total_Min'].loc[structural_alig_report['Cluster_ID'] == cltr_id].to_list()[0]):
            #Get cluster total time
            total_time = structural_alig_report['Total_Min'].loc[structural_alig_report['Cluster_ID'] == cltr_id].to_list()[0]
            if not pd.isnull(structural_alig_report['Total_MB'].loc[structural_alig_report['Cluster_ID'] == cltr_id].to_list()[0]):
                #Get cluster total size
                total_size = structural_alig_report['Total_MB'].loc[structural_alig_report['Cluster_ID'] == cltr_id].to_list()[0]
            else:
                print('Error:'+'\t'+'empty size value in'+'\t'+cltr_id)
                total_size = 0
            #In this case 55300 is ~50% (replace for "time_percent")
            if (pack_time+total_time) <= 55300:
                #Add total_time of this cluster
                pack_time+=total_time
                #Add total_size of this cluster
                pack_size+=total_size
                #Add this cluster to the cluster package
                cltrs_pack.append(cltr_id)
                #That's it, go to the next cluster
                continue
            else:
                #Save theses packages to the package's list.
                packs_list.append((cltrs_pack, pack_time, pack_size))
                #Restart time count
                pack_time = 0
                #Restart size count
                pack_size = 0
                #Restart cluster count
                cltrs_pack = []
                #Add the new total time to this new package (should not be bigger than percentage, otherwise I must have detected before.
                pack_time+=total_time
                #Add the new total size to this new package.
                pack_size+=total_size
                #Add this cluster to cluster package.
                cltrs_pack.append(cltr_id)
                #That's it, go to the next cluster
                continue
        else:
            print('Error:'+'\t'+'empty time value in'+'\t'+cltr_id)
            #That's it, go to the next cluster
            continue
    #Save last cluster pack
    packs_list.append((cltrs_pack, pack_time, pack_size))
    #Save those clusters that are too bigg (like cluster 4) separated.
    cltr_id = 'Cluster_4'
    #Get cluster total time
    total_time = structural_alig_report['Total_Min'].loc[structural_alig_report['Cluster_ID'] == cltr_id].to_list()[0]
    #Get cluster total size
    total_size = structural_alig_report['Total_MB'].loc[structural_alig_report['Cluster_ID'] == cltr_id].to_list()[0]
    #Restart time count
    pack_time = 0
    #Restart size count
    pack_size = 0
    #Restart cluster count
    cltrs_pack = []
    #Add the new total time to this new time package.
    pack_time+=total_time
    #Add the new total size to this new size package.
    pack_size+=total_size
    #Add this cluster to cluster package.
    cltrs_pack.append(cltr_id)
    #Save this bigger cluster to one single cluster pack
    packs_list.append((cltrs_pack, pack_time, pack_size))

    return packs_list


def make_readme(packs_list, struct_packs_Dir):
    """
    Create readme file to be used in each package group.
    Parameters:
                    * packs_list: a list. Is a list of package groups.
                    * struct_packs_Dir: a string. Correspond to path directory where all gzip packages are
                                        going to be stored.
    Return: nothing.
    """
    os.makedirs(struct_packs_Dir, exist_ok=True)
    readme_file = struct_packs_Dir+'README.md'
    if not os.path.exists(readme_file):
        with open(readme_file, 'a') as outfile:
            outfile.write('# Alignment strategy'+'\n\n')
            outfile.write('## Install TMalign. Follow these steps:'+'\n')
            outfile.write('+ First, read how the installation tools works:'+'\n\n')
            outfile.write('\t\t\t./sbg-cetools --help'+'\n\n\n')
            outfile.write('+ Second, if you chose default installation:'+'\n')
            outfile.write('++ Execute with root privileges:\n'+'\t\t\tsudo ./sbg-cetools tm-align'+'\n\n')
            outfile.write('+ Third, test TMalign:'+'\n\n')
            outfile.write('\t\t\twhich TMalign'+'\n\n')
            outfile.write('+ Fourth, if you did not see where TMalign is localted:'+'\n\n')
            outfile.write('++ Copy manually the path to TMalign binary where was installed.'+'\n\n')
            outfile.write('## Usage:\n\n'+'./run_tmalign.sh -e --binany=<path-to-TMalign> --list=<name-of-one-child-list>'+'\n\n')
            outfile.write('Execute many times as many comb_child_list_* are present in the group folder.'+'\n\n')
            outfile.write('Example: \t\t./run_tmalign.sh -e --binany=/home/user/Group_2/bin/TMalign --list=comb_child_list_00'+'\n')
            outfile.write('        \t\t./run_tmalign.sh -e --binany=/home/user/Group_2/bin/TMalign --list=comb_child_list_01'+'\n\n')
            outfile.write('## Detail info per group'+'\n\n')
            outfile.write('Total groups: '+str(len(packs_list))+'\n')
            num = 0
            Total_clusters = 0
            Total_time = 0
            Total_size = 0
            for group in packs_list:
                outfile.write('Group '+str(num+1)+':'+'\t'+str(len(group[0]))+'\t'+'clusters'+'\t'+'Total time:'+'\t'+str(round(((group[1]/60)/24), 2))+' Days/core'+'\t'+'Total size:'+'\t'+str(round((group[2]/1024),2))+' GB'+'\n')
                num += 1
                Total_clusters += len(group[0])
                Total_time += (group[1]/60)/24
                Total_size += (group[2]/1024)
            outfile.write('Total_clusters: '+str(Total_clusters)+'\t'+'Total_time:'+'\t'+str(round(Total_time, 2))+' Days/core'+'\t'+'Total_size:'+'\t'+str(round(Total_size, 2))+' GB'+'\n\n\n')
    return


def create_gz_groups(packs_list, struct_packs_Dir, comb_files_Dir, data_pdbs_models, scripts_Dir):
    """
    Create gzip package groups considering a run strategy.
    Parameters:
                    * packs_list: a list. Is a list of package groups.
                    * struct_packs_Dir: a string. Correspond to path directory where all gzip packages are
                                        going to be stored.
                    * comb_files_Dir: a string. Correspond to a path where all clusters combinations files
                                      are stored. (Clusters_combinations/).
                    * data_pdbs_models: a string. Correspond to a path where all pdb models are stored.
                                        (data_codnas_rna_model_chains/).
                    * scripts_Dir: a string. Correspond to a path where all script files are stored.
                                   (run_tmalign.sh & sbg-cetools).
    Return: nothing.
    """
    #Check if scripts exists
    if (os.path.exists(scripts_Dir+'run_tmalign.sh')) & (os.path.exists(scripts_Dir+'sbg-cetools')):
        tmalign_script = scripts_Dir+'run_tmalign.sh'
        sbg_cetools_script = scripts_Dir+'sbg-cetools'
    else:
        print('Error: problem to find run_tmalign.sh or sbg-cetools scripts.')
    #Check is README.md exists:
    if os.path.exists(struct_packs_Dir+'README.md'):
        readme_file = struct_packs_Dir+'README.md'
    else:
        print('Error: problem to find README.md file.')
    #Start group count in zero.
    num = 0
    for group in packs_list:
        group_name = 'Group_'+str(num+1)
        #Aks for groups and if they already exists.
        if (group_name != 'Group_1') & (not os.path.exists(struct_packs_Dir+group_name)):
            #Build pack folder
            os.makedirs(struct_packs_Dir+group_name, exist_ok=True)
            os.makedirs(struct_packs_Dir+group_name+'/'+'TMalign/', exist_ok=True)
            os.makedirs(struct_packs_Dir+group_name+'/'+'Clusters_combinations/', exist_ok=True)
            os.makedirs(struct_packs_Dir+group_name+'/'+'data_codnas_rna_model_chains/', exist_ok=True)
            #Get tmalign script
            shutil.copy2(tmalign_script, struct_packs_Dir+group_name+'/')
            #Get sbg-cetools script
            shutil.copy2(sbg_cetools_script, struct_packs_Dir+group_name+'/')
            #Get README file
            shutil.copy2(readme_file, struct_packs_Dir+group_name+'/')
            #Start empty combination list
            comb_list = []
            total_models = set()
            for cltr_id in group[0]:
                #Build pack files
                #Get comb files
                shutil.copy2(comb_files_Dir+cltr_id+'_comb.list', struct_packs_Dir+group_name+'/'+'Clusters_combinations/')
                comb_list.append(struct_packs_Dir+group_name+'/'+'Clusters_combinations/'+cltr_id+'_comb.list')
                #Get data pdb models
                set_models = structural_align_report.get_set_of_models(comb_files_Dir+cltr_id+'_comb.list')
                total_models = total_models | set_models
            for model in total_models:
                shutil.copy2(data_pdbs_models+'/'+model, struct_packs_Dir+group_name+'/'+'data_codnas_rna_model_chains/')
    #        print(comb_list)
            with open(struct_packs_Dir+group_name+'/'+"unique_comb_list.log", 'a') as outfile:
                for afile in comb_list:
                    with open(afile, 'r') as outcomb:
                        for line in outcomb:
                            outfile.write(line)
        else:
            #Build pack folder
            os.makedirs(struct_packs_Dir+group_name, exist_ok=True)
            os.makedirs(struct_packs_Dir+group_name+'/'+'TMalign/', exist_ok=True)
            os.makedirs(struct_packs_Dir+group_name+'/'+'Clusters_combinations/', exist_ok=True)
            os.makedirs(struct_packs_Dir+group_name+'/'+'data_codnas_rna_model_chains/', exist_ok=True)
            #Get tmalign script
            shutil.copy2(tmalign_script, struct_packs_Dir+group_name+'/')
            #Get sbg-cetools script
            shutil.copy2(sbg_cetools_script, struct_packs_Dir+group_name+'/')
            #Get README file
            shutil.copy2(readme_file, struct_packs_Dir+group_name+'/')
            comb_list = []
            for cltr_id in group[0]:
                #Build pack files
                #Get comb files
                shutil.copy2(comb_files_Dir+cltr_id+'_comb.list', struct_packs_Dir+group_name+'/'+'Clusters_combinations/')
                comb_list.append(struct_packs_Dir+group_name+'/'+'Clusters_combinations/'+cltr_id+'_comb.list')
    #        print(comb_list)
            with open(struct_packs_Dir+group_name+'/'+"unique_comb_list.log", 'a') as outfile:
                for afile in comb_list:
                    with open(afile, 'r') as outcomb:
                        for line in outcomb:
                            outfile.write(line)
        #4 comb_child_list for group 2 (our election) and 10 for group 3.
        num_lines = sum(1 for line in open(struct_packs_Dir+group_name+'/'+"unique_comb_list.log"))
        print(group_name+'\t'+'Not-shuffled: '+str(num_lines))
        cmd = 'shuf '+struct_packs_Dir+group_name+'/'+'unique_comb_list.log'+' > '+struct_packs_Dir+group_name+'/'+'unique_comb_list_shuffled.log'
        os.system(cmd)
        #4 comb_child_list for group 2 (our election) and 10 for group 3.
        num_lines = sum(1 for line in open(struct_packs_Dir+group_name+'/'+"unique_comb_list_shuffled.log"))
        print(group_name+'\t'+'Shuffled: '+str(num_lines))
        if group_name == 'Group_2':
            print('Number of lines per comb_child_list:'+'\t'+str(math.ceil(num_lines/4))+'\n\n')
            cmd = 'cd '+struct_packs_Dir+group_name+'/'+';'+'split --lines='+str(math.ceil(num_lines/4))+' --numeric-suffixes=0 unique_comb_list_shuffled.log comb_child_list_'
            #cmd = 'cd '+struct_packs_Dir+group_name+'/'+';'+'split --number=4 --numeric-suffixes=0 unique_comb_list.log comb_child_list_'
            os.system(cmd)
        elif group_name == 'Group_3':
            print('Number of lines per comb_child_list:'+'\t'+str(math.ceil(num_lines/10))+'\n\n')
            cmd = 'cd '+struct_packs_Dir+group_name+'/'+';'+'split --lines='+str(math.ceil(num_lines/10))+' --numeric-suffixes=0 unique_comb_list_shuffled.log comb_child_list_'
            #cmd = 'cd '+struct_packs_Dir+group_name+'/'+';'+'split --number=10 --numeric-suffixes=0 unique_comb_list.log comb_child_list_'
            os.system(cmd)
        else:
            print('Number of lines per comb_child_list:'+'\t'+str(math.ceil(num_lines/12))+'\n\n')
            cmd = 'cd '+struct_packs_Dir+group_name+'/'+';'+'split --lines='+str(math.ceil(num_lines/12))+' --numeric-suffixes=0 unique_comb_list_shuffled.log comb_child_list_'
            #cmd = 'cd '+struct_packs_Dir+group_name+'/'+';'+'split --number=10 --numeric-suffixes=0 unique_comb_list.log comb_child_list_'
            os.system(cmd)
            #Get data pdb models
            child_list = glob.glob(struct_packs_Dir+group_name+'/'+'comb_child_list_*')
            #Get only 8 of 12 comb_child_list_* to run in 2nd laptop. Starting from comb_child_list_00 to comb_child_list_07.
            total_models = set()
            for child_path in sorted(child_list)[:-4]:
                set_models = structural_align_report.get_set_of_models(child_path)
                total_models = total_models | set_models
            for model in total_models:
                shutil.copy2(data_pdbs_models+'/'+model, struct_packs_Dir+group_name+'/'+'data_codnas_rna_model_chains/')
        #Compress each group folder
        cmd = 'cd '+struct_packs_Dir+';'+'tar -zcf '+group_name+'.tar.gz '+group_name
        os.system(cmd)
        #Removing uncompressed folders
        shutil.rmtree(struct_packs_Dir+group_name)
        #Increase 1 to print group name correctly
        num += 1

    return


def create_strategy( struct_alig_report
                   , struct_packs_Dir
                   , comb_files_Dir
                   , data_pdbs_models
                   , scripts_Dir
                   , time_percent ):
    """
    Handle the creation of gzip package groups with a run strategy.
    Parameters:
                    * struct_alig_report: a string. Correspond to a "Structural alignment report tsv table".
                    * struct_packs_Dir: a string. Correspond to path directory where all gzip packages are
                                        going to be stored.
                    * comb_files_Dir: a string. Correspond to a path where all clusters combinations files
                                      are stored. (Clusters_combinations/).
                    * data_pdbs_models: a string. Correspond to a path where all pdb models are stored.
                                        (data_codnas_rna_model_chains/).
                    * scripts_Dir: a string. Correspond to a path where all script files are stored.
                                   (run_tmalign.sh & sbg-cetools).
                    * time_percent: a float. Correspond to decide the percentage of total time per package group.
    Return: nothing.
    """
    struct_packs_Dir = '../output/2020-05-15/Structural_alignment_packages/'
    comb_files_Dir = '../processed_data/2020-05-15/lists/Clusters_combinations/'
    data_pdbs_models = '../data/2020-05-15/pdbs/data_codnas_rna_model_chains/'
    scripts_Dir = '../src/scripts/'

    packs_list = get_packs_list(struct_alig_report, time_percent)
    make_readme(packs_list, struct_packs_Dir)
    create_gz_groups(packs_list, struct_packs_Dir, comb_files_Dir, data_pdbs_models, scripts_Dir)

    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Create gzip package groups to run alignmets with a strategy.")
    # Positional arguments
    p.add_argument(
        "struct_alig_report",
        help="Structural alignment report tsv table.>"
    )
    p.add_argument(
        "struct_packs_Dir",
        help="Path to save all package groups."
    )
    p.add_argument(
        "comb_files_Dir",
        help="Path where all clusters combinations files are stored."
    )
    p.add_argument(
        "data_pdbs_models",
        help="Path where all pdb models are stored."
    )
    p.add_argument(
        "scripts_Dir",
        help="Path where all script files are stored."
    )
    # Optional arguments
    p.add_argument(
        "-t",
        "--time_percent",
        default=0.43,
        type=float,
        help="Build packages up to this percentage of total time."
    )

    args = p.parse_args()
    create_strategy(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
"""\
Purpose: With this module you can create a full CoDNaS-RNA
         cdrna_pairs.csv.gz website table.

Preconditions:
                - Files for input
                    * pdbchains_databases_codes
                    * tmalign_parsed_table
                    * cdrna_clusters
                    * cdrna_conformers
                    * rcsb_table
                    * clashsc_table
                    * cdrna_interactions
                - Libraries
                    * Pandas
                    * Numpy

Arguments:    -Positional
                * [pdbchains_databases_codes]    // string
                * [tmalign_parsed_table]         // string
                * [cdrna_clusters]               // string
                * [cdrna_conformers]             // string
                * [rcsb_table]                   // string
                * [clashsc_table]                // string
                * [cdrna_interactions]           // string
                * [outputDir]                    // string

Observations:
            - 

TODO: - Implement optional parameters for partially fill cdrna_pairs.csv.gz.
      - Improve create_dataframe_struct() function.

"""


import argparse
import os
from datetime import datetime
import pandas as pd
import numpy as np
from save_website_table import save_website_table
from get_website_df import get_df_if_file_exist
from create_dir import create_dir
from encrypt import encode


def create_dataframe_struct(tmalign_parsed_table, a_separator):
    """
    Create a dataframe structure using tmalign_persed_table as template.
    Then fill columns with \"NA\" and 0.
    Parameters:
                * tmalign_parsed_table:      a string. Correspond to a TSV table with all
                                                       TMalign parsed alignments.
                * a_separator:               a string. Correspond to the tmalign_parsed_table.tsv.gz
                                                       table separator.
    TODO: add list parameter to be used as a column builder.
    TODO: divide df creation from tmalign template table.
    Return: a dataframe
    """
    # Get a dataframe from table
    tmalign_table = get_df_if_file_exist(
        "tmalign_table", tmalign_parsed_table, a_separator, False
    )

    columns_present = [
        "Cluster_ID",
        "Chain1_name",
        "Chain2_name",
        "Seq_ID",
        "RMSD",
        "Ch1_align_seq",
        "Ch2_align_seq",
        "Align_positions",
        "Aligned_length"
    ]

    # Create the Dataframe to be used in cdrna_pairs.
    columns_to_match = [
        "cluster_id",           # Table TMalign
        "pdb_model_chain_1",    # Table TMalign
        "pdb_model_chain_2",    # Table TMalign
        "seq_ident",            # Table TMalign
        "rmsd",                 # Table TMalign
        "chain1_align_seq",     # Table TMalign
        "chain2_align_seq",     # Table TMalign
        "align_positions",      # Table TMalign
        "align_length"          # Table TMalign
    ]

    # Create a column dict
    col_dict = dict(zip(columns_present, columns_to_match))

    # Create the Dataframe to be used in cdrna_pairs.
    cdrna_pairs = tmalign_table.drop(
        columns=["LN_chain1", "D0_chain1", "LN_chain2", "D0_chain2", "CPU_time"]
    )

    # Rename columns
    cdrna_pairs = cdrna_pairs.rename(columns=col_dict)

    # Total columns to have
    columns_to_have = [
        "cluster_id",           # Table TMalign
        "pdb_1",                # Table TMalign
        "model_1",              # Table TMalign
        "chain_1",              # Table TMalign
        "pdb_model_chain_1",    # Table TMalign
        "pdb_2",                # Table TMalign
        "model_2",              # Table TMalign
        "chain_2",              # Table TMalign
        "pdb_model_chain_2",    # Table TMalign
        "seq_ident",            # Table TMalign
        "rmsd",                 # Table TMalign
        "tmscore",              # Table TMalign
        "is_max",               # Table TMalign
        "chain1_align_seq",     # Table TMalign
        "chain2_align_seq",     # Table TMalign
        "align_positions",      # Table TMalign
        "diff_nb_intra",        # Table DSSR
        "diff_nb_inter_nt",     # Table DSSR
        "diff_nb_inter_aa",     # Table DSSR
        "diff_nb_inter_mt",     # Table DSSR
        "diff_ph",              # Table RCSB
        "diff_temp",            # Table RCSB
        "diff_source"           # Table RCSB
    ]

    # Create missing columns and fill then with "NA" and "0" for "is_max".
    for acol in columns_to_have:
        if acol not in cdrna_pairs.columns:
            if acol == "is_max":
                cdrna_pairs[acol] = 0
            else:
                cdrna_pairs[acol] = "NA"

    # Create new columns splited from pdb_model_chain_# and then personalized conformers names in columns 'pdb_model_chain_#'
    for nb_member in ["1", "2"]:
        cdrna_pairs["pdb_" + nb_member], cdrna_pairs["model_" + nb_member], cdrna_pairs[
            "chain_" + nb_member
        ] = (cdrna_pairs["pdb_model_chain_" + nb_member].str.split("_").str)
        cdrna_pairs["pdb_model_chain_" + nb_member] = cdrna_pairs[
            "pdb_model_chain_" + nb_member
        ].map(encode)
    # Order columns
    columns_ordered = columns_to_have + (
        cdrna_pairs.columns.drop(columns_to_have).tolist()
    )
    # Now, latest columns are those to be used and then easly removed
    cdrna_pairs = cdrna_pairs[columns_ordered]

    return cdrna_pairs


def add_tmscore_value(cdrna_pairs, chain1, chain2):
    """
    Add tmscore value for a pair selecting longest sequence.
    Parameters:
                * cdrna_pairs:               a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA pairs website table.
                * chain1:                    a string. Correspond to one conformer's pair name.
                * chain2:                    a string. Correspond to one conformer's pair name.
    Return: nothing
    """

    length_chain1 = cdrna_pairs.loc[
        (cdrna_pairs["pdb_model_chain_1"] == chain1)
        & (cdrna_pairs["pdb_model_chain_2"] == chain2),
        "Length_chain1",
    ].values[0]
    length_chain2 = cdrna_pairs.loc[
        (cdrna_pairs["pdb_model_chain_1"] == chain1)
        & (cdrna_pairs["pdb_model_chain_2"] == chain2),
        "Length_chain2",
    ].values[0]
    if length_chain1 > length_chain2:
        tmscore1 = cdrna_pairs.loc[
            (cdrna_pairs["pdb_model_chain_1"] == chain1)
            & (cdrna_pairs["pdb_model_chain_2"] == chain2),
            "TM_score_norm_chain1",
        ].values[0]
        cdrna_pairs.loc[
            (cdrna_pairs["pdb_model_chain_1"] == chain1)
            & (cdrna_pairs["pdb_model_chain_2"] == chain2),
            "tmscore",
        ] = tmscore1
    else:
        tmscore2 = cdrna_pairs.loc[
            (cdrna_pairs["pdb_model_chain_1"] == chain1)
            & (cdrna_pairs["pdb_model_chain_2"] == chain2),
            "TM_score_norm_chain2",
        ].values[0]
        cdrna_pairs.loc[
            (cdrna_pairs["pdb_model_chain_1"] == chain1)
            & (cdrna_pairs["pdb_model_chain_2"] == chain2),
            "tmscore",
        ] = tmscore2
    return


def get_date_values(apair, pdb_date_dict):
    """
    Get deposition dates for each conformer from a pair.
    Parameters:
                * apair:               a tuple. Correspond to two PDB conformer
                                                names from a pair.
                * pdb_date_dict:       a dict. Correspond to a dictionary with
                                               all deposition dates for each PDB.
    Return: two strings
    """
    # PDB code must be upper because RCSB table has PDB ID in upper case.
    dt_pdb1 = pdb_date_dict[apair[0].upper()]
    dt_pdb2 = pdb_date_dict[apair[1].upper()]
    # Check deposition date (format YYYY-MM-DD, like: 2016-11-13)
    format_str = "%Y-%m-%d"  # The format
    dt_obj_1 = datetime.strptime(dt_pdb1, format_str).date()
    dt_obj_2 = datetime.strptime(dt_pdb2, format_str).date()
    #    if dt_obj_1 > dt_obj_2:
    # print('recent:'+str(dt_obj_1))
    return (dt_obj_1, dt_obj_2)


def add_is_max_value(cdrna_pairs, chain1, chain2, a_num):
    """
    Add is_max value (a_num) for a pair.
    Parameters:
                * cdrna_pairs:               a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA pairs website table.
                * chain1:                    a string. Correspond to one conformer's pair name.
                * chain2:                    a string. Correspond to one conformer's pair name.
                * a_num:                     an int. Correspond to \"is_max\" value (0=False;
                                                     1=True;2=Selected).
    Return: nothing
    """
    cdrna_pairs.loc[(
        (cdrna_pairs["pdb_model_chain_1"] == chain1)
        & (cdrna_pairs["pdb_model_chain_2"] == chain2),
        "is_max")
    ] = a_num

    return


def get_better_csc_pair(pdb_cs_dict, dict_pair_diff_pdb, cltr_id, cdrna_pairs):
    """
    Get better clashscore value for a pair and add it as \"is_max\" pair value.
    If clashscores are equals, returns both in a list and go to next filter.
    Parameters:
                * pdb_cs_dict:         a dict. Correspond to a dictionary with
                                               all clashscore values for each PDB.
                * dict_pair_diff_pdb:  a dict. Correspond to a dictionary with
                                               all conformers pairs from different PDB.
                * cltr_id:             a string. Correspond to a CoDNaS-RNA cluster.
                * cdrna_pairs:         a dataframe. Correspond to a pandas dataframe for
                                                    CoDNaS-RNA pairs website table.
    Return: a list
    """
    pdb_pair_list = []
    for apair in sorted(dict_pair_diff_pdb[cltr_id]):
        # Only choose first
        apdb = apair[0].split("_")[0]
        pdb_pair_list.append(apdb, apair)

    candidates_list = []
    for pdb_pair in pdb_pair_list:
        apdb = pdb_pair[0].upper()
        if apdb in pdb_cs_dict.keys():
            candidates_list.append(pdb_pair)

    if len(candidates_list) > 1:
        csc_and_pair_list = list(
            {
                (float(pdb_cs_dict[pdb_pair[0]]), pdb_pair[1])
                for pdb_pair in candidates_list
            }
        )
        csc_and_pair_list_sorted = sorted(csc_and_pair_list)
        csc_first_pair = csc_and_pair_list_sorted[0][0]
        csc_second_pair = csc_and_pair_list_sorted[1][0]
        if csc_first_pair != csc_second_pair:
            chain1 = csc_and_pair_list_sorted[0][1][0]
            chain2 = csc_and_pair_list_sorted[0][1][1]
            add_is_max_value(cdrna_pairs, chain1, chain2, 2)
            # Empty list of pairs to see
            dict_pair_diff_pdb[cltr_id] = []
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        else:
            # Return two with same clashscore and go to next filter
            dict_pair_diff_pdb[cltr_id] = [
                csc_and_pair_list_sorted[0],
                csc_and_pair_list_sorted[1],
            ]

    return dict_pair_diff_pdb[cltr_id]


def resolve_using_clashscores(
    dict_pair_diff_pdb, cltr_id, csc_table, cdrna_pairs, same_PDB
):
    """
    Perform filter 5) evaluating clashscore values for pairs.
    Parameters:
                * dict_pair_diff_pdb:  a dict. Correspond to a dictionary with
                                               all conformers pairs from different PDB.
                * cltr_id:             a string. Correspond to a CoDNaS-RNA cluster.
                * csc_table:           a dataframe. Correspond to a pandas dataframe for
                                                    PDB clashscore table.
                * cdrna_pairs:         a dataframe. Correspond to a pandas dataframe for
                                                    CoDNaS-RNA pairs website table.
                * same_PDB:            a bool. Correspond to a boolean value denoting if
                                               conformer's PDB are equals.
    Return: a list
    """
    # Start with filter 5) better clashscore value (table csc_table).
    # Create a dict of PDB:clashscore
    if csc_table is not None:
        pdb_cs_dict = dict(
            zip(csc_table["PDB"].tolist(), csc_table["Clashscore"].tolist())
        )
    else:
        raise ValueError('[Error] PDB clashscore dataframe is empty: {}.'.format(csc_table))

    if same_PDB:
        dict_pair_diff_pdb[cltr_id] = get_better_csc_pair(
            pdb_cs_dict, dict_pair_diff_pdb, cltr_id, cdrna_pairs
        )
        return dict_pair_diff_pdb[cltr_id]

    # Select better clashscore from several max pairs of NMRs
    better_worst_clashscore = []
    # Build a pair list
    pair_list = []
    for apair in dict_pair_diff_pdb[cltr_id]:
        # PDB code must be upper because clashscore table has PDB in upper case.
        pdb1 = apair[0].split("_")[0].upper()
        pdb2 = apair[1].split("_")[0].upper()
        # Evaluate that these two PDBs are present
        if (pdb1 in csc_table["PDB"]) & (pdb2 in csc_table["PDB"]):
            sc_pdb1 = pdb_cs_dict[pdb1]
            sc_pdb2 = pdb_cs_dict[pdb2]
            if sc_pdb1 > sc_pdb2:
                lower_sc = sc_pdb1
            else:
                lower_sc = sc_pdb2
            # Save better_worst_clashscore value
            if "better_worst_clashscore" in locals():
                if better_worst_clashscore > lower_sc:
                    better_worst_clashscore = lower_sc
                    # Remove latest pairs and add a new one
                    pair_list = [apair]
                elif better_worst_clashscore == lower_sc:
                    pair_list.append(apair)
            else:
                better_worst_clashscore = lower_sc
                pair_list.append(apair)
        continue
    if pair_list:
        if len(pair_list) == 1:
            chain1 = pair_list[0][0]
            chain2 = pair_list[0][1]
            add_is_max_value(cdrna_pairs, chain1, chain2, 2)
            # Empty list of pairs to see
            dict_pair_diff_pdb[cltr_id] = []
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        #                continue
        # Check clashscore for all pairs with better worst classcore
        for apair in pair_list:
            # PDB code must be upper because clashscore table has PDB in upper case.
            pdb1 = apair[0].split("_")[0].upper()
            pdb2 = apair[1].split("_")[0].upper()
            sc_pdb1 = pdb_cs_dict[pdb1]
            sc_pdb2 = pdb_cs_dict[pdb2]
            if sc_pdb1 == better_worst_clashscore:
                lower_sc = sc_pdb2
            else:
                lower_sc = sc_pdb1

            # Save cs seen value
            if "better_cs_seen" in locals():
                if better_cs_seen > lower_sc:
                    better_cs_seen = lower_sc
                    # Remove latest pairs and add a new one
                    pair_list = [apair]
                elif better_cs_seen == lower_sc:
                    pair_list.append(apair)
            else:
                better_cs_seen = lower_sc
                pair_list.append(apair)
            continue
        # Check how many pairs are candidates
        if len(pair_list) == 1:
            chain1 = pair_list[0][0]
            chain2 = pair_list[0][1]
            add_is_max_value(cdrna_pairs, chain1, chain2, 2)
            # Empty list of pairs to see
            dict_pair_diff_pdb[cltr_id] = []
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        #                continue
        else:
            print(
                "Error:"
                + "\t"
                + cltr_id
                + "\t"
                + "filter 5) clashscore. To many clashscore maxpairs with high quality clashscores:"
                + "\t"
                + str(pair_list)
            )
            print(
                "Start to implementing filter 6) deposition date for those rows that are from different PDB codes."
            )
            dict_pair_diff_pdb[cltr_id] = pair_list

    return dict_pair_diff_pdb[cltr_id]


def get_better_date_pair(pdb_date_dict, dict_pair_diff_pdb, cltr_id, cdrna_pairs):
    """
    Get most recent deposition date for a pair and add it as \"is_max\" pair value.
    If deposition dates are equals, returns both in a list and go to next filter.
    Parameters:
                * pdb_date_dict:       a dict. Correspond to a dictionary with
                                               all deposition dates for each PDB.
                * dict_pair_diff_pdb:  a dict. Correspond to a dictionary with
                                               all conformers pairs from different PDB.
                * cltr_id:             a string. Correspond to a CoDNaS-RNA cluster.
                * cdrna_pairs:         a dataframe. Correspond to a pandas dataframe for
                                                    CoDNaS-RNA pairs website table.
    Return: a list
    """
    pdb_pair_list = []
    for apair in sorted(dict_pair_diff_pdb[cltr_id]):
        # Only choose first
        apdb = apair[0].split("_")[0]
        pdb_pair_list.append(apdb, apair)

    candidates_list = []
    for pdb_pair in pdb_pair_list:
        apdb = pdb_pair[0].upper()
        if apdb in pdb_date_dict.keys():
            candidates_list.append(pdb_pair)

    if len(candidates_list) > 1:
        date_and_pair_list = list(
            {
                (float(pdb_date_dict[pdb_pair[0]]), pdb_pair[1])
                for pdb_pair in candidates_list
            }
        )
        date_and_pair_list_sorted = sorted(date_and_pair_list, reverse=True)
        date_recent_pair = date_and_pair_list_sorted[0][0]
        date_old_pair = date_and_pair_list_sorted[1][0]
        if date_recent_pair != date_old_pair:
            chain1 = date_and_pair_list_sorted[0][1][0]
            chain2 = date_and_pair_list_sorted[0][1][1]
            add_is_max_value(cdrna_pairs, chain1, chain2, 2)
            # Empty list of pairs to see
            dict_pair_diff_pdb[cltr_id] = []
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        else:
            # Return two with same clashscore and go to next filter
            dict_pair_diff_pdb[cltr_id] = [
                date_and_pair_list_sorted[0],
                date_and_pair_list_sorted[1],
            ]

    return dict_pair_diff_pdb[cltr_id]


def resolve_using_dates(dict_pair_diff_pdb, cltr_id, rcsb_table, cdrna_pairs, same_PDB):
    """
    Perform filter 6) evaluating deposition dates for pairs.
    Parameters:
                * dict_pair_diff_pdb:  a dict. Correspond to a dictionary with
                                               all conformers pairs from different PDB.
                * cltr_id:             a string. Correspond to a CoDNaS-RNA cluster.
                * rcsb_table:          a dataframe. Correspond to a pandas dataframe for
                                                    rcsb table.
                * cdrna_pairs:         a dataframe. Correspond to a pandas dataframe for
                                                    CoDNaS-RNA pairs website table.
                * same_PDB:            a bool. Correspond to a boolean value denoting if
                                               conformer's PDB are equals.
    Return: a list
    """
    # Get a unique PDB code row in RCSB table (to be used in filter 6) deposition date.
    rcsb_pdb_rows = rcsb_table.drop_duplicates(subset=["PDB ID"])

    # Build pair_list
    pair_list_to_see = [apair for apair in dict_pair_diff_pdb[cltr_id]]
    # Build a pdb_list
    pdb_list = list(
        {apair[num].split("_")[0] for apair in pair_list_to_see for num in [0, 1]}
    )
    # Check deposition date for all pdbs
    rcsb_pdb_rows_date = rcsb_pdb_rows.loc[rcsb_pdb_rows["PDB ID"].isin(pdb_list)]
    # Start filter 6) deposition date
    if rcsb_pdb_rows_date is not None:
        if rcsb_pdb_rows_date.shape[0] > 1:
            pdb_date_dict = dict(
                zip(
                    rcsb_pdb_rows_date["PDB ID"].tolist(),
                    rcsb_pdb_rows_date["Dep. Date"].tolist(),
                )
            )
        else:
            print(
                "Error:"
                + "\t"
                + cltr_id
                + "\t"
                + "filter 6) there are not two deposition dates to compare for these PDBs"
                + "\t"
                + str(pdb_list)
            )
            return dict_pair_diff_pdb[cltr_id]
    else:
        print(
            "Error:"
            + "\t"
            + cltr_id
            + "\t"
            + "filter 6) there is not deposition date for these PDBs"
            + "\t"
            + str(pdb_list)
        )

    if same_PDB:
        dict_pair_diff_pdb[cltr_id] = get_better_date_pair(
            pdb_date_dict, dict_pair_diff_pdb, cltr_id, cdrna_pairs
        )
        return dict_pair_diff_pdb[cltr_id]

    # Select recent max pairs
    better_worst_date = []
    # Build a pair list
    pair_list = []
    if pdb_date_dict:
        for apair in pair_list_to_see:
            # PDB code must be upper because RCSB table has PDB ID in upper case.
            pdb1 = apair[0].split("_")[0].upper()
            pdb2 = apair[1].split("_")[0].upper()
            if (pdb1 in pdb_date_dict.keys()) & (pdb2 in pdb_date_dict.keys()):
                dt_pdb1, dt_pdb2 = get_date_values(apair, pdb_date_dict)
                if dt_pdb1 < dt_pdb2:
                    older_dt = dt_pdb1
                else:
                    older_dt = dt_pdb2
                # Save better_worst_date value
                if "better_worst_date" in locals():
                    if better_worst_date < older_dt:
                        better_worst_date = older_dt
                        # Remove latest pairs and add a new one
                        pair_list = [apair]
                    elif better_worst_date == older_dt:
                        pair_list.append(apair)
                else:
                    better_worst_date = older_dt
                    pair_list.append(apair)
            continue
        if pair_list:
            if len(pair_list) == 1:
                chain1 = pair_list[0][0]
                chain2 = pair_list[0][1]
                add_is_max_value(cdrna_pairs, chain1, chain2, 2)
                # Empty list of pairs to see
                dict_pair_diff_pdb[cltr_id] = []
                # Go to next cluster
                return dict_pair_diff_pdb[cltr_id]
            #                    continue
            # Check date for all pairs with better worst date
            for apair in pair_list:
                dt_pdb1, dt_pdb2 = get_date_values(apair, pdb_date_dict)
                if dt_pdb1 == better_worst_date:
                    recent_dt = dt_pdb2
                else:
                    recent_dt = dt_pdb1

                # Save cs seen value
                if "better_dt_seen" in locals():
                    if better_dt_seen < recent_dt:
                        better_dt_seen = recent_dt
                        # Remove latest pairs and add a new one
                        pair_list = [apair]
                    elif better_dt_seen == recent_dt:
                        pair_list.append(apair)
                else:
                    better_dt_seen = recent_dt
                    pair_list.append(apair)
                continue
            # Check how many pairs are candidates
            if len(pair_list) == 1:
                chain1 = pair_list[0][0]
                chain2 = pair_list[0][1]
                add_is_max_value(cdrna_pairs, chain1, chain2, 2)
                # Empty list of pairs to see
                dict_pair_diff_pdb[cltr_id] = []
                # Go to next cluster
                return dict_pair_diff_pdb[cltr_id]
            else:
                print(
                    "Error:"
                    + "\t"
                    + cltr_id
                    + "\t"
                    + "filter 6) deposition date. To many max pairs with recent deposited date:"
                    + "\t"
                    + str(pair_list)
                )
                print(
                    "Start to implementing filter 7) alphabetical order for those rows that are from different PDB codes."
                )
                dict_pair_diff_pdb[cltr_id] = pair_list

    return dict_pair_diff_pdb[cltr_id]


def resolve_using_alphabetical_order(dict_pair_diff_pdb, cltr_id, cdrna_pairs):
    """
    Perform filter 7) selecting first pair from alphabetical order criteria.
    Parameters:
                * dict_pair_diff_pdb:  a dict. Correspond to a dictionary with
                                               all conformers pairs from different PDB.
                * cltr_id:             a string. Correspond to a CoDNaS-RNA cluster.
                * cdrna_pairs:         a dataframe. Correspond to a pandas dataframe for
                                                    CoDNaS-RNA pairs website table.
    Return: a list
    """
    # Check filter 7) alphabetical order
    pair_selected = sorted(dict_pair_diff_pdb[cltr_id])[0]
    chain1 = pair_selected[0]
    chain2 = pair_selected[1]
    add_is_max_value(cdrna_pairs, chain1, chain2, 2)
    # Empty list of pairs to see
    dict_pair_diff_pdb[cltr_id] = []

    return dict_pair_diff_pdb[cltr_id]


def get_better_min_resol_pair(
    cdrna_conformers, dict_of_XRD_pairs, cltr_id, cdrna_pairs
):
    """
    Get better resolution value for a pair and add it as \"is_max\" pair value.
    If resolution are equals, returns both in a list and go to next filter.
    Parameters:
                * cdrna_conformers:    a dataframe. Correspond to a pandas dataframe for
                                                    CoDNaS-RNA conformers website table.
                * dict_of_XRD_pairs:   a dict. Correspond to a dictionary with
                                               all XRD pairs.
                * cltr_id:             a string. Correspond to a CoDNaS-RNA cluster.
                * cdrna_pairs:         a dataframe. Correspond to a pandas dataframe for
                                                    CoDNaS-RNA pairs website table.
    Return: a list
    """
    # Get a unique PDB code row from cdrna_conformers table (to evaluate resolutions in this filter only)
    conformers_pdb_rows = cdrna_conformers.drop_duplicates(subset=["pdb"])

    pdb_pair_list = []
    for apair in sorted(dict_of_XRD_pairs[cltr_id]):
        # Only choose first
        apdb = apair[0].split("_")[0]
        pdb_pair_list.append(apdb, apair)

    # Get a list of pdb codes
    pdb_list = list({pdb_pair[0] for pdb_pair in pdb_pair_list})

    # Check resol for all pdbs
    conformers_pdb_rows_resol = conformers_pdb_rows.loc[
        conformers_pdb_rows["pdb"].isin(pdb_list)
    ]

    # Start filter 4) min resol
    if conformers_pdb_rows_resol:
        if conformers_pdb_rows_resol.shape[0] > 1:
            pdb_resol_dict = dict(
                zip(
                    conformers_pdb_rows_resol["pdb"].tolist(),
                    conformers_pdb_rows_resol["resolution"].tolist(),
                )
            )
        else:
            # Get the unique PDB code
            pdb = conformers_pdb_rows_resol["pdb"]
            # Get a list of pairs with this PDB code
            pdb_pair_list = list(
                {pdb_pair[1] for pdb_pair in pdb_pair_list if pdb_pair[0] == pdb}
            )
    else:
        print(
            "Error:"
            + "\t"
            + cltr_id
            + "\t"
            + "filter 4) there is not resolutions for these PDBs"
            + "\t"
            + str(pdb_list)
        )

    # Ask if there is only pair with that PDB code
    if len(pdb_pair_list) == 1:
        chain1 = pdb_pair_list[0][0]
        chain2 = pdb_pair_list[0][1]
        add_is_max_value(cdrna_pairs, chain1, chain2, 2)
        # Empty list of pairs to see
        dict_of_XRD_pairs[cltr_id] = []
        # Go to next cluster
        return dict_of_XRD_pairs[cltr_id]

    resol_and_pair_list = list(
        {
            (float(pdb_resol_dict[pdb_pair[0]]), pdb_pair[1])
            for pdb_pair in pdb_pair_list
        }
    )
    resol_and_pair_list_sorted = sorted(resol_and_pair_list)
    min_resol_first_pair = resol_and_pair_list_sorted[0][0]
    min_resol_second_pair = resol_and_pair_list_sorted[1][0]
    if min_resol_first_pair != min_resol_second_pair:
        chain1 = resol_and_pair_list_sorted[0][1][0]
        chain2 = resol_and_pair_list_sorted[0][1][1]
        add_is_max_value(cdrna_pairs, chain1, chain2, 2)
        # Empty list of pairs to see
        dict_of_XRD_pairs[cltr_id] = []
        # Go to next cluster
        return dict_of_XRD_pairs[cltr_id]
    else:
        # Return two with same resolution and go to next filter
        dict_of_XRD_pairs[cltr_id] = [
            resol_and_pair_list_sorted[0],
            resol_and_pair_list_sorted[1],
        ]

    return dict_of_XRD_pairs[cltr_id]


def resolve_using_min_resolution(
    dict_pair_diff_pdb,
    cltr_id,
    cdrna_conformers,
    rows_rmsdmax_max_alig,
    cdrna_pairs,
    same_PDB,
):
    """
    Perform filter 4) evaluating resolution values for pairs.
    Parameters:
                * dict_pair_diff_pdb:    a dict. Correspond to a dictionary with
                                                 all conformers pairs from different PDB.
                * cltr_id:               a string. Correspond to a CoDNaS-RNA cluster.
                * cdrna_conformers:      a dataframe. Correspond to a pandas dataframe for
                                                      CoDNaS-RNA conformers website table.
                * rows_rmsdmax_max_alig: a dataframe. Correspond to a pandas dataframe of
                                                      pairs with max RMSD values.
                * cdrna_pairs:           a dataframe. Correspond to a pandas dataframe for
                                                      CoDNaS-RNA pairs website table.
                * same_PDB:              a bool. Correspond to a boolean value denoting if
                                                 conformer's PDB are equals.
    Return: a list
    """
    dict_of_XRD_pairs = dict()
    chains_with_resol = []
    for apair in dict_pair_diff_pdb[cltr_id]:
        exp_met_pdb1 = cdrna_conformers.loc[
            (cdrna_conformers["pdb_model_chain"] == apair[0]), "method"
        ].tolist()[0]
        exp_met_pdb2 = cdrna_conformers.loc[
            (cdrna_conformers["pdb_model_chain"] == apair[1]), "method"
        ].tolist()[0]
        # See pair with resolution
        if (exp_met_pdb1 != "SOLUTION NMR") & (exp_met_pdb2 != "SOLUTION NMR"):
            if cltr_id in dict_of_XRD_pairs.keys():
                dict_of_XRD_pairs[cltr_id].append(apair)
            else:
                dict_of_XRD_pairs[cltr_id] = [apair]

    if cltr_id in dict_of_XRD_pairs.keys():
        if len(dict_of_XRD_pairs[cltr_id]) == 1:
            chain1 = dict_of_XRD_pairs[cltr_id][0][0]
            chain2 = dict_of_XRD_pairs[cltr_id][0][1]
            add_is_max_value(cdrna_pairs, chain1, chain2, 2)
            # Empty list of pairs to see
            dict_pair_diff_pdb[cltr_id] = []
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        #                continue

        if same_PDB:
            dict_pair_diff_pdb[cltr_id] = get_better_min_resol_pair(
                cdrna_conformers, dict_of_XRD_pairs, cltr_id, cdrna_pairs
            )
            return dict_pair_diff_pdb[cltr_id]

        chain_list = []
        # Check better_worst_resol_seen resolution for all pairs
        for apair in dict_of_XRD_pairs[cltr_id]:
            resol_chain1 = cdrna_conformers.loc[
                (cdrna_conformers["pdb_model_chain"] == apair[0]), "resolution"
            ].tolist()[0]
            resol_chain2 = cdrna_conformers.loc[
                (cdrna_conformers["pdb_model_chain"] == apair[1]), "resolution"
            ].tolist()[0]
            if resol_chain1 == resol_chain2:
                lower_resol = resol_chain1
                if apair[0] not in chain_list:
                    chain_list.append(apair[0])
            elif resol_chain1 > resol_chain2:
                lower_resol = resol_chain1
                if apair[0] not in chain_list:
                    chain_list.append(apair[0])
            else:
                lower_resol = resol_chain2
                if apair[1] not in chain_list:
                    chain_list.append(apair[1])
            # Save better_worst_resol_seen value
            if "better_worst_resol_seen" in locals():
                if better_worst_resol_seen > lower_resol:
                    better_worst_resol_seen = lower_resol
            else:
                better_worst_resol_seen = lower_resol
            continue

        # Check better_resol_seen resolution for all pairs that have better_worst_resol_seen
        rows_better_worst_resol = rows_rmsdmax_max_alig.loc[
            (rows_rmsdmax_max_alig["pdb_model_chain_1"].isin(chain_list))
            | (rows_rmsdmax_max_alig["pdb_model_chain_2"].isin(chain_list))
        ]
        nb_rows_better_worst_resol = rows_better_worst_resol.shape[0]
        if nb_rows_better_worst_resol == 1:
            chain1 = rows_better_worst_resol["pdb_model_chain_1"].tolist()[0]
            chain2 = rows_better_worst_resol["pdb_model_chain_2"].tolist()[0]
            add_is_max_value(cdrna_pairs, chain1, chain2, 2)
            # Empty list of pairs to see
            dict_pair_diff_pdb[cltr_id] = []
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        #                continue
        # Re-write dictionary with only XRD pairs that have better_worst_resol
        dict_of_XRD_pairs[cltr_id] = zip(
            rows_better_worst_resol["pdb_model_chain_1"].tolist(),
            rows_better_worst_resol["pdb_model_chain_2"].tolist(),
        )
        # Build a pair to list
        pair_list = []
        # Check better_resol_seen for all pairs with better_worst_resol_seen
        for apair in dict_of_XRD_pairs[cltr_id]:
            resol_chain1 = cdrna_conformers.loc[
                (cdrna_conformers["pdb_model_chain"] == apair[0]), "resolution"
            ].tolist()[0]
            resol_chain2 = cdrna_conformers.loc[
                (cdrna_conformers["pdb_model_chain"] == apair[1]), "resolution"
            ].tolist()[0]
            if resol_chain1 == better_worst_resol_seen:
                lower_resol = resol_chain2
            else:
                lower_resol = resol_chain1

            # Save better_resol_seen value
            if "better_resol_seen" in locals():
                if better_resol_seen > lower_resol:
                    better_resol_seen = lower_resol
                    # Remove latest pairs and add a new one
                    pair_list = [apair]
                elif better_resol_seen == lower_resol:
                    pair_list.append(apair)
            else:
                better_resol_seen = lower_resol
                pair_list.append(apair)
            continue
        # Check how many pairs are candidates
        if len(pair_list) == 1:
            chain1 = pair_list[0][0]
            chain2 = pair_list[0][1]
            add_is_max_value(cdrna_pairs, chain1, chain2, 2)
            # Empty list of pairs to see
            dict_pair_diff_pdb[cltr_id] = []
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        else:
            print(
                "Error:"
                + "\t"
                + cltr_id
                + "\t"
                + "To many XRD max pairs with high quality resolutions:"
                + "\t"
                + str(pair_list)
            )
            print(
                "Start to implementing filter 5) clashscore for those rows that are from different PDB codes."
            )
            dict_pair_diff_pdb[cltr_id] = pair_list

    return dict_pair_diff_pdb[cltr_id]


def resolve_diff_pdb_branch(
    rows_rmsdmax_max_alig,  # A dataframe subset with rows that are rmsd max and have max aligned sequences
    dict_pair_diff_pdb,     # A dict of cltrs with list of pairs (tuples)
    cltr_id,                # A cluster name
    cdrna_pairs,            # An entire dataframe to fill with values
    cdrna_conformers,       # A dataframe with conformer data
    csc_table,              # A dataframe with clashscore data (2017, 96% of our data is mapped there)
    rcsb_table              # A dataframe with deposition date info
):
    """
    Perform filter 3) evaluating different PDB entries from pairs.
    Parameters:
                * rows_rmsdmax_max_alig: a dataframe. Correspond to a pandas dataframe of
                                                      pairs with max RMSD values.
                * dict_pair_diff_pdb:    a dict. Correspond to a dictionary with
                                                 all conformers pairs from different PDB.
                * cltr_id:               a string. Correspond to a CoDNaS-RNA cluster.
                * cdrna_pairs:           a dataframe. Correspond to a pandas dataframe for
                                                      CoDNaS-RNA pairs website table.
                * cdrna_conformers:      a dataframe. Correspond to a pandas dataframe for
                                                      CoDNaS-RNA conformers website table.
                * csc_table:             a dataframe. Correspond to a pandas dataframe for
                                                      PDB clashscore table.
                * rcsb_table:            a dataframe. Correspond to a pandas dataframe for
                                                      rcsb table.
    Return: a list
    """
    # If has one, process, add and continue.
    if len(dict_pair_diff_pdb[cltr_id]) == 1:
        chain1 = dict_pair_diff_pdb[cltr_id][0][0]
        chain2 = dict_pair_diff_pdb[cltr_id][0][1]
        add_is_max_value(cdrna_pairs, chain1, chain2, 2)
        # Empty list of pairs to see
        dict_pair_diff_pdb[cltr_id] = []
        # Go to next cluster
        return dict_pair_diff_pdb[cltr_id]
    else:
        # Start evaluating filter 4) (min resol if it is possible because could be only NMR)
        dict_pair_diff_pdb[cltr_id] = resolve_using_min_resolution(
            dict_pair_diff_pdb,
            cltr_id,
            cdrna_conformers,
            rows_rmsdmax_max_alig,
            cdrna_pairs,
            False,
        )
        # Ask if list is empty
        if not dict_pair_diff_pdb[cltr_id]:
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        else:
            print(
                "Error:"
                + "\t"
                + cltr_id
                + "\t"
                + "filter 4) did not work to select one pair. Pairs:"
                + "\t"
                + str(dict_pair_diff_pdb[cltr_id])
            )

        # Start with filter 5) better clashscore value (table csc_table).
        # Get a list of pairs to keep seeing or an empty list stop seeing pairs and go to next cluster id
        dict_pair_diff_pdb[cltr_id] = resolve_using_clashscores(
            dict_pair_diff_pdb, cltr_id, csc_table, cdrna_pairs, False
        )
        # Ask if list is empty
        if not dict_pair_diff_pdb[cltr_id]:
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        else:
            print(
                "Error:"
                + "\t"
                + cltr_id
                + "\t"
                + "filter 5) did not work to select one pair. Pairs:"
                + "\t"
                + str(dict_pair_diff_pdb[cltr_id])
            )
        # Check filter 6) deposition date
        dict_pair_diff_pdb[cltr_id] = resolve_using_dates(
            dict_pair_diff_pdb, cltr_id, rcsb_table, cdrna_pairs, False
        )
        # Ask if list is empty
        if not dict_pair_diff_pdb[cltr_id]:
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        else:
            print(
                "Error:"
                + "\t"
                + cltr_id
                + "\t"
                + "filter 6) did not work to select one pair. Pairs:"
                + "\t"
                + str(dict_pair_diff_pdb[cltr_id])
            )

        # Check filter 7) alphabetical order
        dict_pair_diff_pdb[cltr_id] = resolve_using_alphabetical_order(
            dict_pair_diff_pdb, cltr_id, cdrna_pairs
        )
        # Ask if list is empty
        if not dict_pair_diff_pdb[cltr_id]:
            # Go to next cluster
            return dict_pair_diff_pdb[cltr_id]
        else:
            print(
                "Error:"
                + "\t"
                + cltr_id
                + "\t"
                + "filter 7) did not work to select one pair. Pairs:"
                + "\t"
                + str(dict_pair_diff_pdb[cltr_id])
            )

    return dict_pair_diff_pdb[cltr_id]


def validate_date(date_text):
    """
    Check if deposition date has a correct format.
    Parameters:
                * date_text:               a string. Correspond to a PDB
                                                     deposition date value.
    Return: a bool
    """
    try:
        if date_text != datetime.strptime(date_text, "%Y-%m-%d").strftime("%Y-%m-%d"):
            raise ValueError('[Error] Given date ({}) has not the correct format.'.format(date_text))
        return True
    except ValueError:
        return False


def resolve_same_pdb_branch(
    rows_rmsdmax_max_alig,
    dict_pair_same_pdb,
    cltr_id,
    cdrna_pairs,
    cdrna_conformers,
    csc_table,
    rcsb_table,
):
    """
    Perform filter 3) evaluating same PDB entries from pairs.
    Parameters:
                * rows_rmsdmax_max_alig: a dataframe. Correspond to a pandas dataframe of
                                                      pairs with max RMSD values.
                * dict_pair_same_pdb:    a dict. Correspond to a dictionary with
                                                 all conformers pairs with same PDB.
                * cltr_id:               a string. Correspond to a CoDNaS-RNA cluster.
                * cdrna_pairs:           a dataframe. Correspond to a pandas dataframe for
                                                      CoDNaS-RNA pairs website table.
                * cdrna_conformers:      a dataframe. Correspond to a pandas dataframe for
                                                      CoDNaS-RNA conformers website table.
                * csc_table:             a dataframe. Correspond to a pandas dataframe for
                                                      PDB clashscore table.
                * rcsb_table:            a dataframe. Correspond to a pandas dataframe for
                                                      rcsb table.
    Return: a list
    """
    # If has one, process, add and continue.
    if len(dict_pair_same_pdb[cltr_id]) == 1:
        chain1 = dict_pair_same_pdb[cltr_id][0][0]
        chain2 = dict_pair_same_pdb[cltr_id][0][1]
        add_is_max_value(cdrna_pairs, chain1, chain2, 2)
        # Empty list of pairs to see
        dict_pair_same_pdb[cltr_id] = []
        # Go to next cluster
        return dict_pair_same_pdb[cltr_id]
    else:
        # Start evaluating the filters
        # Are pairs from diff PDBs?
        # Build pair_list
        pair_list_to_see = [apair for apair in dict_pair_same_pdb[cltr_id]]
        # Build a pdb_list
        pdb_list = list(
            {apair[num].split("_")[0] for apair in pair_list_to_see for num in [0, 1]}
        )

        # Ask if all pairs come from the same PDB
        if len(pdb_list) == 1:
            dict_pair_same_pdb[cltr_id] = resolve_using_alphabetical_order(
                dict_pair_same_pdb, cltr_id, cdrna_pairs
            )
            # Ask if list is empty
            if not dict_pair_same_pdb[cltr_id]:
                # Go to next cluster
                return dict_pair_same_pdb[cltr_id]
        else:
            # At least two apairs come from diff PDBs

            # Start evaluating filter 4) (min resol if it is possible because could be only NMR)
            dict_pair_same_pdb[cltr_id] = resolve_using_min_resolution(
                dict_pair_same_pdb,
                cltr_id,
                cdrna_conformers,
                rows_rmsdmax_max_alig,
                cdrna_pairs,
                True,
            )
            # Ask if list is empty
            if not dict_pair_same_pdb[cltr_id]:
                # Go to next cluster
                return dict_pair_same_pdb[cltr_id]
            else:
                # Ask if list has same resolution values or simply these pairs have not resolution to compare
                tuple_to_see = dict_pair_same_pdb[cltr_id][0]
                # Has clashscore?
                if tuple_to_see[0].isdigit():
                    # Use these two apairs with same resolution for next filter
                    dict_pair_same_pdb[cltr_id] = [
                        resol_pair_tup[1]
                        for resol_pair_tup in dict_pair_same_pdb[cltr_id]
                    ]

            # Start evaluating filter 5) better clashscore value (table csc_table).
            # Get a list of pairs to keep seeing or an empty list stop seeing pairs and go to next cluster id
            dict_pair_same_pdb[cltr_id] = resolve_using_clashscores(
                dict_pair_same_pdb, cltr_id, csc_table, cdrna_pairs, True
            )
            # Ask if list is empty
            if not dict_pair_same_pdb[cltr_id]:
                # Go to next cluster
                return dict_pair_same_pdb[cltr_id]
            else:
                # Ask if list has same clashscores values or simply these pairs have not clashscores to compare
                tuple_to_see = dict_pair_same_pdb[cltr_id][0]
                # Has clashscore?
                if tuple_to_see[0].isdigit():
                    # Use these two apairs with same clashscore for next filter
                    dict_pair_same_pdb[cltr_id] = [
                        cs_pair_tup[1] for cs_pair_tup in dict_pair_same_pdb[cltr_id]
                    ]

            # Start evaluating filter 6) deposition date
            dict_pair_same_pdb[cltr_id] = resolve_using_dates(
                dict_pair_same_pdb, cltr_id, rcsb_table, cdrna_pairs, True
            )
            # Ask if list is empty
            if not dict_pair_same_pdb[cltr_id]:
                # Go to next cluster
                return dict_pair_same_pdb[cltr_id]
            else:
                # Ask if list has same date values or simply these pairs have not dates to compare
                tuple_to_see = dict_pair_same_pdb[cltr_id][0]
                # Has Date?
                if validate_date(tuple_to_see[0]):
                    # Use these two apairs with same date for next filter
                    dict_pair_same_pdb[cltr_id] = [
                        date_pair_tup[1]
                        for date_pair_tup in dict_pair_same_pdb[cltr_id]
                    ]

            # Start evaluating filter 7) alphabetical order
            dict_pair_same_pdb[cltr_id] = resolve_using_alphabetical_order(
                dict_pair_same_pdb, cltr_id, cdrna_pairs
            )
            # Ask if list is empty
            if not dict_pair_same_pdb[cltr_id]:
                # Go to next cluster
                return dict_pair_same_pdb[cltr_id]
            else:
                print(
                    "Error:"
                    + "\t"
                    + "filter 7) did not work to select one pair. Pairs:"
                    + "\t"
                    + str(dict_pair_same_pdb[cltr_id])
                )

    return dict_pair_same_pdb[cltr_id]


def add_tmscore_and_evaluate_rmsd_values(
    cdrna_pairs,
    cdrna_clusters,
    cdrna_conformers,
    rcsb_table,
    clashsc_table,
    a_separator,
    outputDir,
):
    """
    Add tmscore value and evaluate filters to get most accurate maxRMSD pair
    selection as it is possible so \"is_max\" is fill it with 0,1 or 2.
    Parameters:
                * cdrna_pairs:           a dataframe. Correspond to a pandas dataframe for
                                                      CoDNaS-RNA pairs website table.
                * cdrna_clusters:        a string. Correspond to a CSV CoDNaS-RNA conformers
                                                   website table.
                * cdrna_conformers:      a string. Correspond to a CSV CoDNaS-RNA conformers
                                                   website table.
                * rcsb_table:            a string. Correspond to a CSV table with all RCSB
                                                   information.
                * clashsc_table:         a string. Correspond to a TSV table with all
                                                   clashscore value per PDB.
                * a_separator:           a string. Correspond to the tmalign_parsed_table.tsv.gz
                                                   table separator.
                * outputDir:             a string. Correspond to an output directory to store
                                                   CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_pairs.csv.gz"):
        return cdrna_pairs

    # Fill 'tmscore' column with TM_scores from larger length seq.
    tuple_pairs_list = zip(
        cdrna_pairs["pdb_model_chain_1"].tolist(),
        cdrna_pairs["pdb_model_chain_2"].tolist()
    )
    for tuple_pair in tuple_pairs_list:
        add_tmscore_value(cdrna_pairs, tuple_pair[0], tuple_pair[1])

    ###################################################################################################################
    ### Start to evaluate max RMSD pair filters

    #### is_max - column
    # 0 - no-max
    # 1 - maxRMSD (without filters)
    # 2 - maxRMSD (with filters)

    #### Max pair filters
    # 1) maxRMSD                        # Table cdrna_clusters.csv.gz (Cluster_ID)
    # 2) max avg length                 # Table tmalign           (PDB_MODEL_CHAIN)
    # 3) PDB diff                       # Table cdrna_pairs       (PDB_MODEL_CHAIN_# with split('_')[0])
    # 4) min avg resolution (both conf) # Table cdrna_conformers  (PDB_MODEL_CHAIN)
    # 5) clashscore                     # Table clashscore        (PDB code)
    # 6) deposition date                # Table RCSB              (PDB or PDB_Chain)
    # 7) alphabetic order (first)

    # Get a dataframe from table
    cdrna_clusters = get_df_if_file_exist("cdrna_clusters", cdrna_clusters, ",", False)

    # Get a dataframe from table
    cdrna_conformers = get_df_if_file_exist(
        "cdrna_conformers", cdrna_conformers, ",", False
    )

    # Get a dataframe from table
    csc_table = get_df_if_file_exist("csc_table", clashsc_table, a_separator, False)

    # Get a dataframe from table
    rcsb_table = get_df_if_file_exist("rcsb_table", rcsb_table, ",", False)

    # Dictionaries
    dict_pair_diff_pdb = dict()
    dict_pair_same_pdb = dict()

    # Create a list of 1000 cluster ids
    cltrs_list = (
        cdrna_clusters["cluster_id"].replace("NA", np.nan).dropna().unique().tolist()
    )

    for cltr_id in cltrs_list:

        #        print('Working on:\t'+cltr_id)

        cltr_subset = (
            cdrna_pairs[cdrna_pairs["cluster_id"] == cltr_id]
            .replace("NA", np.nan)
            .dropna(subset=["rmsd"])
        )
        # Check that exist rows from this cluster. Could not run in TMalign.
        if cltr_subset.empty:
            continue
        # Get RMSD max value (TODO: replace this table and use directly cdrna_pairs to get maxRMSD value per cluster)
        rmsd_max = cdrna_clusters[cdrna_clusters["cluster_id"] == cltr_id][
            "rmsd_max"
        ].values[0]

        # Filter cdrna_pairs to select those columns with RMSD max.
        rows_rmsdmax = cltr_subset[(
            (cltr_subset["cluster_id"] == cltr_id)
            & (cltr_subset["rmsd"] == float(rmsd_max))
        )]
        nb_rows_maxRMDS = rows_rmsdmax.shape[0]
        if nb_rows_maxRMDS == 1:
            chain1 = rows_rmsdmax["pdb_model_chain_1"].tolist()[0]
            chain2 = rows_rmsdmax["pdb_model_chain_2"].tolist()[0]
            add_is_max_value(cdrna_pairs, chain1, chain2, 2)
            continue

        # Add is_max value to all these max pairs
        for index, row in rows_rmsdmax.iterrows():
            chain1 = row["pdb_model_chain_1"]
            chain2 = row["pdb_model_chain_2"]
            add_is_max_value(cdrna_pairs, chain1, chain2, 1)

        # Start evaluating for filter 2) (max align lenght)
        # Get the max align length of those max pairs
        max_align_length = rows_rmsdmax["align_length"].max()

        # Get dataframe subset with those rows that have max_lalign_length
        rows_rmsdmax_max_alig = rows_rmsdmax.loc[(
            rows_rmsdmax["align_length"] == max_align_length
        )]
        # How many rows?
        nb_rows_maxRMDS_max_align = rows_rmsdmax_max_alig.shape[0]
        if nb_rows_maxRMDS_max_align == 1:
            chain1 = rows_rmsdmax_max_alig["pdb_model_chain_1"].tolist()[0]
            chain2 = rows_rmsdmax_max_alig["pdb_model_chain_2"].tolist()[0]
            add_is_max_value(cdrna_pairs, chain1, chain2, 2)
            continue

        # Start evaluating for filter 3) (PDB  diff)
        for index, row in rows_rmsdmax_max_alig.iterrows():
            chain1 = row["pdb_model_chain_1"]
            chain2 = row["pdb_model_chain_2"]
            pdb1 = chain1.split("_")[0]
            pdb2 = chain2.split("_")[0]
            if pdb1 != pdb2:
                if cltr_id in dict_pair_diff_pdb.keys():
                    dict_pair_diff_pdb[cltr_id].append((chain1, chain2))
                else:
                    # Dict value = list of tuples of two elements
                    dict_pair_diff_pdb[cltr_id] = [(chain1, chain2)]
            else:
                if cltr_id in dict_pair_same_pdb.keys():
                    dict_pair_same_pdb[cltr_id].append((chain1, chain2))
                else:
                    # Dict value = list of tuples of two elements
                    dict_pair_same_pdb[cltr_id] = [(chain1, chain2)]

        ###################################################################################################################
        ### Start to see if dict_pair_diff_pdb has members
        # Check if exists an entry with diff PDBs for this cluster (check these two conditions)
        if (cltr_id in dict_pair_diff_pdb.keys()) and (
            len(dict_pair_diff_pdb[cltr_id]) > 0
        ):
            dict_pair_diff_pdb[cltr_id] = resolve_diff_pdb_branch(
                rows_rmsdmax_max_alig,
                dict_pair_diff_pdb,
                cltr_id,
                cdrna_pairs,
                cdrna_conformers,
                csc_table,
                rcsb_table,
            )
            # Ask if list is empty
            if not dict_pair_diff_pdb[cltr_id]:
                # Go to next cluster
                continue
        ###################################################################################################################
        ### Start to see if dict_pair_same_pdb has members
        # Check if exists an entry with same PDBs for this cluster (check these two conditions)
        if (cltr_id in dict_pair_same_pdb.keys()) and (
            len(dict_pair_same_pdb[cltr_id]) > 0
        ):
            dict_pair_same_pdb[cltr_id] = resolve_same_pdb_branch(
                rows_rmsdmax_max_alig,
                dict_pair_same_pdb,
                cltr_id,
                cdrna_pairs,
                cdrna_conformers,
                csc_table,
                rcsb_table,
            )
            # Ask if list is empty
            if not dict_pair_same_pdb[cltr_id]:
                # Go to next cluster
                continue

    return cdrna_pairs


def get_conformer_value_dict(cdrna_conformers, acolumn):
    """
    Get a dictionary of all PDB_MODEL_CHAIN with their column
    values using cdrna_conformers.csv.gz as table provider.
    Parameters:
                * cdrna_conformers:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA conformers website table.
                * acolumn:                   a string. Correspond to a column name
                                                       from cdrna_conformers dataframe.
    Return: a dict
    """
    # Get a dict with conformer and source
    conf_avalue_dict = dict()
    data = zip(cdrna_conformers["pdb_model_chain"], cdrna_conformers[acolumn])

    for aconf, val in data:
        conf_avalue_dict[aconf] = val

    return conf_avalue_dict


def extract_info_conformers_table(
    cdrna_pairs, cdrna_conformers, a_separator, outputDir
):
    """
    Extract ph, temp, source, nb_intra, nb_inter_nt and nb_inter_aa per CoDNaS-RNA conformer
    using cdrna_conformers.csv.gz as table provider.
    Parameters:
                * cdrna_pairs:               a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA pairs website table.
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * a_separator:               a string. Correspond to the cdrna_conformers.csv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_pairs.csv.gz"):
        return cdrna_pairs

    # Get a dataframe from table
    cdrna_conformers = get_df_if_file_exist(
        "cdrna_conformers", cdrna_conformers, a_separator, False
    )

    # Only conformers that are present in pairs
    cdrna_conformers = cdrna_conformers.dropna(subset=["cluster_id"]).copy()

    # Get conformer list
    conformer_list = cdrna_conformers["pdb_model_chain"].tolist()

    for column in [
        "ph",
        "temperature",
        "source",
        "nb_intra",
        "nb_inter_nt",
        "nb_inter_aa",
    ]:
        conf_value_dict = get_conformer_value_dict(cdrna_conformers, column)
        # Add value to all these pairs
        for index, row in cdrna_pairs.iterrows():
            chain1 = row["pdb_model_chain_1"]
            chain2 = row["pdb_model_chain_2"]
            aval_1 = conf_value_dict[chain1]
            aval_2 = conf_value_dict[chain2]
            if column == "source":  # We can say "Same" "Different" "NA"
                if (aval_1 == "NA") or (aval_2 == "NA"):
                    abs_val = "NA"
                elif (aval_1 != "NA") and (aval_2 != "NA"):
                    if aval_1 == aval_2:
                        abs_val = 0
                    else:
                        abs_val = 1
                cdrna_pairs.loc[(
                    (cdrna_pairs["pdb_model_chain_1"] == chain1)
                    & (cdrna_pairs["pdb_model_chain_2"] == chain2),
                    "diff_" + column
                )] = str(abs_val)
            else:
                if (aval_1 == "NA") or (aval_2 == "NA"):
                    abs_val = "NA"
                elif (aval_1 != "NA") and (aval_2 != "NA"):
                    abs_val = abs(float(aval_1) - float(aval_2))
                if column == "temperature":
                    column = "temp"
                cdrna_pairs.loc[(
                    (cdrna_pairs["pdb_model_chain_1"] == chain1)
                    & (cdrna_pairs["pdb_model_chain_2"] == chain2),
                    "diff_" + column
                )] = abs_val

    cdrna_pairs = cdrna_pairs.astype({
                                       "diff_nb_intra": "Int64",
                                       "diff_nb_inter_nt": "Int64",
                                       "diff_nb_inter_aa": "Int64",
                                       "diff_source": "object",
                                       "align_length": "Int64"},
                                       errors="ignore"
    )

    return cdrna_pairs


def fill_cdrna_pairs(
    pdbchains_databases_codes,
    tmalign_parsed_table,
    cdrna_clusters,
    cdrna_conformers,
    rcsb_table,
    clashsc_table,
    cdrna_interactions,
    outputDir,
):
    """
    Create and fill the website cdrna_pairs table.
    Parameters:
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * tmalign_parsed_table:      a string. Correspond to a TSV table with all
                                                       TMalign parsed alignments.
                * cdrna_clusters:            a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * rcsb_table:                a string. Correspond to a CSV table with all RCSB
                                                       information.
                * clashsc_table:             a string. Correspond to a TSV table with all
                                                       clashscore value per PDB.
                * cdrna_interactions:        a string. Correspond to a CSV CoDNaS-RNA interactions
                                                       website table.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """

    # Before start to do anything, check if outputDir is correct and create it
    create_dir(outputDir)

    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_pairs.csv.gz"):
        cdrna_pairs = get_df_if_file_exist(
            "cdrna_pairs", outputDir + "cdrna_pairs.csv.gz", ",", True
        )
    else:
        cdrna_pairs = create_dataframe_struct(tmalign_parsed_table, "\t")

    cdrna_pairs = add_tmscore_and_evaluate_rmsd_values(
        cdrna_pairs,
        cdrna_clusters,
        cdrna_conformers,
        rcsb_table,
        clashsc_table,
        "\t",
        outputDir,
    )

    cdrna_pairs = extract_info_conformers_table(
        cdrna_pairs, cdrna_conformers, ",", outputDir
    )

    save_website_table(cdrna_pairs, outputDir, "cdrna_pairs", ",")


    print("All done !")

    return


# pdbchains_databases_codes = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/PDBchains_with_databases_codes.tsv'
# tmalign_parsed_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/TMalign/tmalign_parsed_comparations.tsv.gz'
# cdrna_clusters = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_clusters.csv.gz'
# cdrna_conformers = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_conformers.csv.gz'
# rcsb_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/filtered_rna_conformers_from_PDB.csv.gz'
# clashsc_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/PDBs_clashscore.tsv'
# interactions_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/DSSR/lala'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create cdrna_pairs.csv.gz table to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "pdbchains_databases_codes",
        type=str,
        help="Table with all PDB chains with their related codes."
    )
    p.add_argument(
        "tmalign_parsed_table",
        type=str,
        help="Gzip table with all TMalign parsed alignments."
    )
    p.add_argument(
        "cdrna_clusters",
        type=str,
        help="Table with all CoDNaS-RNA clusters from website."
    )
    p.add_argument(
        "cdrna_conformers",
        type=str,
        help="Table with all CoDNaS-RNA conformers from website."
    )
    p.add_argument(
        "rcsb_table",
        type=str,
        help="Gzip table with all RCSB information."
    )
    p.add_argument(
        "clashsc_table",
        type=str,
        help="Table with all wwPDB clashscores."
    )
    p.add_argument(
        "interactions_table",
        type=str,
        help="Gzip table with all interactions."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store cdrna_pairs table."
    )

    args = p.parse_args()
    fill_cdrna_pairs(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
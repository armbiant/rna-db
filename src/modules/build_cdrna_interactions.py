"""\
Purpose: With this module you can create a full CoDNaS-RNA
         cdrna_interactions.csv.gz website table.

Preconditions:
                - Files for input
                    * cdrna_conformers
                    * pdbchains_databases_codes
                    * rcsb_table_main
                    * hbondDir/        (all hbonds tables)
                - Libraries
                    * Pandas
                    * Numpy

Arguments:    -Positional
                * [cdrna_conformers]             // string
                * [pdbchains_databases_codes]    // string
                * [rcsb_table_main]              // string
                * [clusters_path]                // string
                * [outputDir]                    // string
                * [outputDir_aux]                // string

Observations:
            - 

TODO: - Implement optional parameters for partially fill cdrna_interactions.csv.gz
      - Build own function file for get_pdb_chain_dict(pdbchains_databases_codes, a_separator).
        It is present in build_cdrna_conformers.py and build_cdrna_interactions.py.
      - Implement split() with regular expression.
"""


import argparse
import os
from collections import defaultdict
import numpy as np
import pandas as pd
from save_website_table import save_website_table
from guess_extension import guess_extension_from_separator
from get_website_df import get_df_if_file_exist
from create_dir import create_dir
from encrypt import encode


def get_pdb_chain_dict(pdbchains_databases_codes, a_separator):
    """
    Get a pdb_chain dictionary containing all unique chains codes per PDB. 
    Parameters:
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * a_separator:               a string. Correspond to the pdbchains_databases_codes.tsv
                                                       table separator.
    Return: a dict
    """
    # Get a dataframe from table
    mapping_cltrs = get_df_if_file_exist(
        "mapping_cltrs", pdbchains_databases_codes, a_separator, True
    )

    # Remove those rows that do not have cluster assigned. (we do not need to know their interactions)
    mapping_cltrs_copy = mapping_cltrs.dropna(subset=["Cluster_ID"]).copy()

    # Generate two columns from "PDB_chain" to be used in zip function.
    mapping_cltrs_copy["pdb"], mapping_cltrs_copy["chain"] = (
        mapping_cltrs_copy["PDB_chain"].str.split("\:").str
    )

    # Ordered dict chain-chain
    pdb_chain_dict = defaultdict(list)
    # Make a zip data variable of cluster with PDB:chain code.
    data = zip(mapping_cltrs_copy["pdb"], mapping_cltrs_copy["chain"])
    # Create a dict of clusters with a list of PDB:chain codes present per cluster.
    for pdb, chain in data:
        # PDB codes are in lower case
        pdb = pdb.lower()
        if pdb in pdb_chain_dict.keys():
            pdb_chain_dict[pdb].append(chain)
        else:
            pdb_chain_dict[pdb] = [chain]

    return pdb_chain_dict


def get_interactions(pdb, hbondDir, outputDir_aux, a_separator, not_res="aa:aa"):
    """
    Get a hbond_table dataframe using hbond.tsv as template.
    Parameters:
                * pdb:                       a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * hbondDir:                  a string. Correspond to a directory path where all
                                                       hbond PDB folders are stored.
                * outputDir_aux:             a string. Correspond to an output directory to store
                                                       processed auxiliary hbond tables.
                * a_separator:               a string. Correspond to the hbond.tsv table separator.
                * not_res:                   a string. Correspond to an interaction type not to be get it
                                                       (default='aa:aa').
    Return: a dataframe
    """
    a_separator, ext = guess_extension_from_separator(a_separator)

    # All hbond tables have the same column arrange.
    hb_columns = [
        "index",
        "atom1_serNum",
        "atom2_serNum",
        "donAcc_type",
        "distance",
        "atom1_id",
        "atom2_id",
        "atom_pair",
        "residue_pair"
    ]
    if os.path.exists(hbondDir + pdb + ext) and os.path.getsize(hbondDir + pdb + ext):
        hbond_table = pd.read_table(
            hbondDir + pdb + ext,
            sep=a_separator,
            dtype=str,
            keep_default_na=False,
            names=hb_columns,
        )
        for num in ["1", "2"]:
            # Generate 4 columns from "atom1_id" and "atom2_id" to be used in zip function. (ATOM@MODEL..CHAIN.RESID.RESID_NUM)
            hbond_table["atom_model_" + num], hbond_table[
                "chain_resid_redidNum_" + num
            ] = (hbond_table["atom" + num + "_id"].str.split("\.\.").str)
            hbond_table["atom_" + num], hbond_table["model_" + num] = (
                hbond_table["atom_model_" + num].str.split("\@").str
            )
            hbond_table["chain_" + num], hbond_table["resid_" + num], hbond_table[
                "residNum_" + num
            ], hbond_table["insertion_code_" + num] = (
                hbond_table["chain_resid_redidNum_" + num].str.split("\.").str
            )
            # Remove unused columns
            columns_to_remove = ["chain_resid_redidNum_" + num, "atom_model_" + num]
            hbond_table = hbond_table.drop(columns=columns_to_remove)
        # Save auxliary atom table for this wwPDB.
        save_website_table(hbond_table, outputDir_aux, pdb, "\t")
    else:
        if os.path.getsize(hbondDir + pdb + ext):
            print("Error:\t" + pdb + "\thas not a hbond" + ext + " in\t" + hbondDir)
        else:
            print("Error:\t" + pdb + "\tan empty hbond" + ext + " in\t" + hbondDir)
        hbond_table = pd.DataFrame()
        return hbond_table

    # Clean hbond table to keep working on
    # Get only rows with nt:nt and nt:aa interactions.
    hbond_table = hbond_table.loc[hbond_table["residue_pair"] != not_res]
    # Remove columns to not work on
    columns_to_remove = [
        "atom_1",
        "atom_2",
        "atom1_id",
        "atom2_id",
        "insertion_code_1",
        "insertion_code_2",
        "index",
        "atom1_serNum",
        "atom2_serNum",
        "donAcc_type",
        "distance",
        "atom_pair"
    ]

    hbond_table = hbond_table.drop(columns=columns_to_remove)

    return hbond_table


def get_clean_interaction(pdb, chain_list, hbond_table):
    """
    Get a clean hbonds dataframe from hbond_table dataframe.
    Parameters:
                * pdb:                       a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * chain_list:                a list. Correspond to a chains list.
                * hbond_table:               a dataframe. Correspond to a raw DSSR hbond dataframe.
    Return: a dataframe
    """
    # Get only importan columns
    hbonds = hbond_table[
        [
            "residue_pair",
            "model_1",
            "chain_1",
            "resid_1",
            "residNum_1",
            "model_2",
            "chain_2",
            "resid_2",
            "residNum_2"
        ]
    ].copy()
    # Get only importan rows
    hbonds = hbonds.loc[
        (hbonds["residue_pair"] != "aa:aa")
        & ((hbonds["chain_1"].isin(chain_list) | (hbonds["chain_2"].isin(chain_list))))
    ].copy()
    # Clean duplicates lines
    hbonds = hbonds.drop_duplicates()
    # Add pdb column
    hbonds["pdb"] = pdb

    return hbonds


def add_missing_columns(hbonds, chain_list, cdrna_conformers):
    """
    Add missing columns to cdrna_interactions dataframe structure using cdrna_conformers.csv.gz as template.
    Parameters:
                * hbonds:                    a dataframe. Correspond to a clean DSSR hbond dataframe.
                * chain_list:                a list. Correspond to a chains list.
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
    Return: a dataframe
    """
    columns_present = [
        "residue_pair",
        "model_1",
        "chain_1",
        "resid_1",
        "residNum_1",
        "model_2",
        "chain_2",
        "resid_2",
        "residNum_2",
        "pdb"
    ]

    # Create the Dataframe to be used in cdrna_interactions.
    columns_to_match = [
        "partner_dssr_type",    # Table DSSR (protein; nt)  <--- Para Ronaldo
        "model",                # Table cdrna_conformers
        "chain",                # Table cdrna_conformers
        "res_id",               # Table DSSR
        "res_num",              # Table DSSR
        "model_2",              # This is just for consistency (to be removed)
        "partner_chain_id",     # Table DSSR   <----- antes era chain_partner (Avisar a Ronaldo)
        "partner_res_id",       # Table DSSR
        "partner_res_num",      # Table DSSR
        "pdb",                  # This is just for consistency
    ]

    # Create a column dict
    col_dict = dict(zip(columns_present, columns_to_match))

    # Rename columns
    hbonds = hbonds.rename(columns=col_dict)

    subset_to_drop = hbonds[~hbonds["chain"].isin(chain_list)].copy()

    if not subset_to_drop.empty:
        # Build flip dict
        flip_col_name_dict = {
            "chain": "partner_chain_id_new",
            "res_id": "partner_res_id_new",
            "res_num": "partner_res_num_new",
            "partner_chain_id": "chain_new",
            "partner_res_id": "res_id_new",
            "partner_res_num": "res_num_new"
        }
        # Rename those columns
        subset_to_drop = subset_to_drop.rename(columns=flip_col_name_dict)

        # Build clean dict
        clean_col_name_dict = {
            "partner_chain_id_new": "partner_chain_id",
            "partner_res_id_new": "partner_res_id",
            "partner_res_num_new": "partner_res_num",
            "chain_new": "chain",
            "res_id_new": "res_id",
            "res_num_new": "res_num"
        }
        # Get a clean remaned column subset
        subset_to_drop = subset_to_drop.rename(columns=clean_col_name_dict)

        # Get only rows with correct conformers chains (let outside wrong chains)
        hbonds = hbonds[hbonds["chain"].isin(chain_list)]

        # Add clean subset to hbond (main hbond columns fixed)
        hbonds = pd.concat([subset_to_drop, hbonds], ignore_index=True)

    # Add missing columns to be used in cdrna_interactions.
    columns_to_have_ordered = [
        "cluster_id",           # Table cdrna_conformers
        "pdb",                  # Table cdrna_conformers
        "model",                # Table cdrna_conformers
        "chain",                # Table cdrna_conformers
        "pdb_model_chain",      # Table cdrna_conformers
        "res_id",               # Table DSSR
        "res_num",              # Table DSSR
        "partner_name",         # Table RCSB
        "partner_rcsb_type",    # Table RCSB
        "partner_dssr_type",    # Table DSSR (protein; nt)  <--- Para Ronaldo
        "partner_chain_id",     # Table DSSR   <----- antes era chain_partner (Avisar a Ronaldo)
        "partner_res_id",       # Table DSSR
        "partner_res_num",      # Table DSSR
    ]

    # Create missing columns and fill then with "NA" if is need it.
    for acol in columns_to_have_ordered:
        if not acol in hbonds.columns:
            if acol == "pdb_model_chain":
                # Create a pdb_model column
                hbonds["pdb_model"] = (
                    hbonds["pdb"].astype(str) + "_" + hbonds["model"].astype(str)
                )
                # Create pdb_model_chain column
                hbonds["pdb_model_chain"] = (
                    hbonds["pdb_model"].astype(str) + "_" + hbonds["chain"].astype(str)
                )
                # Hide pdb_model_chain values to be used in website
                hbonds["pdb_model_chain"] = hbonds["pdb_model_chain"].map(encode)
            else:
                hbonds[acol] = "NA"

    # Remove columns unused
    hbonds = hbonds.drop(columns=["model_2", "pdb_model"])

    # Get a dataframe from table
    cdrna_conformers = get_df_if_file_exist(
        "cdrna_conformers", cdrna_conformers, ",", False
    )
    # Create a hide pdb_model_chain with cluter assigned id dict.
    hide_name_with_cltr_dict = dict(
        zip(
            cdrna_conformers["pdb_model_chain"].tolist(),
            cdrna_conformers["cluster_id"].tolist(),
        )
    )
    # Start to fill 'cluster_id' column
    for hide_chain in hbonds["pdb_model_chain"].unique().tolist():
        if hide_chain in cdrna_conformers["pdb_model_chain"].unique().tolist():
            hbonds.loc[
                (hbonds["pdb_model_chain"] == hide_chain), "cluster_id"
            ] = hide_name_with_cltr_dict[hide_chain]
        else:
            print(
                "Error:\t" + hide_chain + "\tis not present in cdrna_conformers.csv.gz"
            )

    # Now, order the columns
    hbonds = hbonds[columns_to_have_ordered]

    return hbonds


def extract_info_from_hbonds_tables(
    cdrna_conformers,
    hbondDir,
    outputDir_aux,
    pdbchains_databases_codes,
    a_separator,
    outputDir
):
    """
    Create a dataframe structure using both hbond.tsv and cdrna_conformers.csv.gz as templates.
    Parameters:
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * hbondDir:                  a string. Correspond to a directory path where all
                                                       hbond PDB folders are stored.
                * outputDir_aux:             a string. Correspond to an output directory to store
                                                       processed auxiliary hbond tables.
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * a_separator:               a string. Correspond to the pdbchains_databases_codes.tsv
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_interactions.csv.gz"):
        cdrna_interactions = get_df_if_file_exist(
            "cdrna_interactions", outputDir + "cdrna_interactions.csv.gz", ",", False
        )
        return cdrna_interactions
    else:
        # Start from zero if cdrna_interactions.csv.gz doesn't exists.
        # Create an empty dataframe to be filled with hbonds
        cdrna_interactions = pd.DataFrame()

    # Get a dictionary with all pdb-chains from all conformers with cluster assigned.
    pdb_chain_dict = get_pdb_chain_dict(pdbchains_databases_codes, a_separator)

    count = 0
    # Start to create individual tables for each pdb
    for pdb, chain_list in pdb_chain_dict.items():
        count += 1
        print(pdb + "\t" + str(count))
        # Check if pdb is not present in cdrna_interactions, otherwise, go to next pdb.
        if pdb in cdrna_interactions["pdb"].unique().tolist():
            continue
        # Check if auxiliary table exists, otherwise create it.
        if not os.path.exists(outputDir_aux + pdb + ".tsv"):
            hbond_table = get_interactions(pdb, hbondDir, outputDir_aux, "\t", "aa:aa")
        else:
            # Get a dataframe from table
            hbond_table = get_df_if_file_exist(
                "hbond_table", outputDir_aux + pdb + ".tsv", "\t", False
            )

        # Check if hbond dataframe has content, otherwise, go to next pdb.
        if hbond_table.empty:
            continue

        # Strategy
        hbonds = get_clean_interaction(pdb, chain_list, hbond_table)
        # Add columns and rearange chains if they are flipped.
        hbonds = add_missing_columns(hbonds, chain_list, cdrna_conformers)
        # Add hbonds to cdrna_interactions to main table
        cdrna_interactions = pd.concat([cdrna_interactions, hbonds], ignore_index=True)
        # Save this pdb entry fro cdrna_interaction table.
        save_website_table(cdrna_interactions, outputDir, "cdrna_interactions", ",")

    # Replace empty cells with "NA"
    cdrna_interactions = cdrna_interactions.replace(np.nan, "NA", inplace=True)

    return cdrna_interactions


def extract_info_rcsb_table_main(
    cdrna_interactions, rcsb_table_main, a_separator, outputDir
):
    """
    Extract all RCSB releated values for each conformer per CoDNaS-RNA cluster using 
    rcsb_table_main (tabularResults_without_seq_IDE_and_rank_50_40_30.csv.gz) as table provider.
    Parameters:
                * cdrna_interactions:        a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA interactions website table.
                * rcsb_table_main:           a string. Correspond to a CSV table with all not
                                                       filtered RCSB information.
                * a_separator:               a string. Correspond to the rcsb_table_main table
                                                       separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_interactions.csv.gz"):
        return cdrna_interactions

    # Create a column to map type and partner chains. (After type assigment, this column is going to be removed)
    cdrna_interactions["pdb_chain"] = (
        cdrna_interactions["pdb"].astype(str)
        + ":"
        + cdrna_interactions["partner_chain_id"].astype(str)
    )

    # Get a dataframe from table
    rcsb_table_main = get_df_if_file_exist(
        "rcsb_table_main", rcsb_table_main, a_separator, False
    )

    rcsb_table_main = rcsb_table_main.dropna(subset=["Entity Macromol. Type"])

    # Create PDB_chain column to be used
    rcsb_table_main["PDB_chain"] = (
        rcsb_table_main["PDB ID"].astype(str).str.lower()
        + str(":")
        + rcsb_table_main["Chain ID"].astype(str)
    )

    # Get a unique PDB_chain code row in RCSB table (to be used in 'partner_rcsb_type' column)
    rcsb_chains_rows = rcsb_table_main.drop_duplicates(subset=["PDB_chain"]).copy()

    # Create dict with all report chains per Entity type
    chain_type_dict = defaultdict(list)
    # Make a zip data variable of each PDB_Chain with its type.
    data = zip(rcsb_chains_rows["PDB_chain"], rcsb_chains_rows["Entity Macromol. Type"])
    # Add a type to each chain (key)
    for achain, atype in data:
        if atype == "Polydeoxyribonucleotide (DNA)":
            atype = "dna"
        elif atype == "Polyribonucleotide (RNA)":
            atype = "rna"
        elif atype == "DNA/RNA Hybrid":
            atype = "na_hybrid"
        elif atype == "Polypeptide(L)":
            atype = "protein"
        elif atype == "Polypeptide(D)":
            atype = "protein"
        elif atype == "Other":
            atype = "other"
        elif atype == "Peptide nucleic acid":
            atype = "hybrid"
        chain_type_dict[achain].append(atype)

    # Start to fill 'partner_rcsb_type' column
    for pdb_chain in cdrna_interactions["pdb_chain"].unique().tolist():
        if pdb_chain in chain_type_dict.keys():
            cdrna_interactions.loc[
                (cdrna_interactions["pdb_chain"] == pdb_chain), "partner_rcsb_type"
            ] = chain_type_dict[pdb_chain]
        else:
            print(
                "Error:\t"
                + pdb_chain
                + "\tis not present in main RCSB table:\t"
                + rcsb_table_main
            )

    # Create dict with all report chains per Name ?
    chain_name_dict = defaultdict(list)
    # Make a zip data variable of each PDB_Chain with its Name ?.
    data = zip(rcsb_chains_rows["PDB_chain"], rcsb_chains_rows["Macromolecule Name"])
    # Add a name to each chain (key)
    for achain, aname in data:
        chain_name_dict[achain].append(aname)

    # Start to fill 'partner_name' column
    for pdb_chain in cdrna_interactions["pdb_chain"].unique().tolist():
        if pdb_chain in chain_name_dict.keys():
            cdrna_interactions.loc[
                (cdrna_interactions["pdb_chain"] == pdb_chain), "partner_name"
            ] = chain_name_dict[pdb_chain]
        else:
            print(
                "Error:\t"
                + pdb_chain
                + "\tis not present in main RCSB table:\t"
                + rcsb_table_main
            )

    # Remove this 'pdb_chain' column because is not necesary
    cdrna_interactions = cdrna_interactions.drop(columns=["pdb_chain"])

    return cdrna_interactions


def fill_cdrna_interactions(
    cdrna_conformers,
    pdbchains_databases_codes,
    rcsb_table_main,
    hbondDir,
    outputDir,
    outputDir_aux
):
    """
    Create and fill the website cdrna_interactions table.
    Parameters:
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * rcsb_table_main:           a string. Correspond to a CSV table with all not
                                                       filtered RCSB information.
                * hbondDir:                  a string. Correspond to a directory path where all
                                                       hbond PDB folders are stored.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
                * outputDir_aux:             a string. Correspond to an output directory to store
                                                       processed auxiliary hbond tables.
    Return: nothing
    """

    # Before start to do anything, check if outputDir and outputDir_aux are correct and create them.
    create_dir(outputDir)
    create_dir(outputDir_aux)

    cdrna_interactions = extract_info_from_hbonds_tables(
        cdrna_conformers,
        hbondDir,
        outputDir_aux,
        pdbchains_databases_codes,
        "\t",
        outputDir
    )

    cdrna_interactions = extract_info_rcsb_table_main(
        cdrna_interactions, 
        rcsb_table_main,
        ",",
        outputDir
    )

    save_website_table(cdrna_interactions, outputDir, "cdrna_interactions", ",")

    print("All done !")

    return


# cdrna_conformers = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_conformers.csv.gz'
# pdbchains_databases_codes = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/PDBchains_with_databases_codes.tsv'
# rcsb_table_main = '/home/martingb/Projects/2020/CoDNaS-RNA/data/2020-05-15/tables_and_lists/tabularResults_without_seq_IDE_and_rank_50_40_30.csv'
# hbondDir = '/home/martingb/Projects/2020/CoDNaS-RNA/outputs/2020-05-15/DSSR/hbonds/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'
# outputDir_aux = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/DSSR/hbonds/'

if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create cdrna_interactions.csv.gz table to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "cdrna_conformers",
        type=str,
        help="Table with all CoDNaS-RNA conformers from website."
    )
    p.add_argument(
        "pdbchains_databases_codes",
        type=str,
        help="Table with all PDB chains with their related codes."
    )
    p.add_argument(
        "rcsb_table_main",
        type=str,
        help="Main RCSB table with all information."
    )
    p.add_argument(
        "hbondDir",
        type=str,
        help="Path where are stored all row hbonds tables from DSSR."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store cdrna_interctions table."
    )
    p.add_argument(
        "outputDir_aux",
        type=str,
        help="Auxiliary interaction atom tables."
    )

    args = p.parse_args()
    fill_cdrna_interactions(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
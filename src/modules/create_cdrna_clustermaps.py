"""\
Purpose: With this module you can create all clustermap and thumbnail images.
         Created images have the following names:
            * Cluster_#_clustermap.png
            * Cluster_#_clustermap_thumbnail.jpg

Preconditions:
                - Files for input
                    * cdrna_pairs.csv.gz
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [website_tables_dir]           // string
                * [releaseDir]                   // string

Observations:
            - 

TODO: -
"""


import argparse
import os
import pandas as pd
from create_dir import create_dir
from get_website_df import get_df_if_file_exist
from build_ok_outputDir_string import build_ok_outputDir_string
from create_double_hcmap import create_hcmap_figure
from create_thumbnail import create_thumbnail


def get_cluster_subset_with_rmsd(aCltr, cdrna_pairs):
    """
    Get a cluster subset of CoDNaS-RNA with all non-empty RMSD values.
    Parameters:
                * aCltr:             a string. Correspond to a cluster name of
                                               CoDNaS-RNA.
                * cdrna_pairs:       a dataframe. Correspond to a pandas dataframe for
                                                  CoDNaS-RNA pairs website table.
    Return: a dataframe
    """
    cluster_df = cdrna_pairs[ (cdrna_pairs['cluster_id'] == aCltr) &
                              (cdrna_pairs['rmsd']       != 'NA')    ] # Cluster_43 has NA values with 1gsg_1_T and Cluster_70 with 5c0y_1_C
    return cluster_df


def has_two_conformers(aDf):
    """
    Indicates whether aDf has two conformers.
    Parameters:
                * aDf:        a dataframe. Correspond to a CoDNaS-RNA
                                           pairs dataframe.
    Return: a boolean
    """
    return aDf.shape[0] == 1


def are_rmsd_all_equal(aDf):
    """
    Indicates whether aDf has all the same rmsd values.
    Parameters:
                * aDf:        a dataframe. Correspond to a CoDNaS-RNA
                                           pairs dataframe.
    Return: a boolean
    """
    return aDf.rmsd.to_list().count(aDf.rmsd.to_list()[0]) == len(aDf.rmsd.to_list())


def get_cluster_hcmap_dataset(cluster_df):
    """
    Get a cluster dataset to plot a CoDNaS-RNA's clustermap.
    Parameters:
                * cluster_df:        a dataframe. Correspond to a cluster subset
                                                  of CoDNaS-RNA pairs dataframe.
    Observation:
                * Cluster has tree or more conformers.
                * All RMSD values are NOT equal.
    Return: a dataframe
    """

    cluster_df = cluster_df.drop(list(set(cluster_df.columns) - {'pdb_model_chain_1', 'pdb_model_chain_2', 'rmsd'}), axis=1)
    
    cluster_df_switched = cluster_df.rename(columns={'pdb_model_chain_1': 'pdb_model_chain_2', 'pdb_model_chain_2': 'pdb_model_chain_1'})
    
    cluster_df = cluster_df.append(cluster_df_switched, ignore_index=True)
    
    cluster_df['rmsd'] = cluster_df['rmsd'].astype(float)
    
    #cluster_df.to_csv('../tests/'+aCltr+'_cluster-heatmap_dataset.csv', header=True, index=False)
    
    dataset = cluster_df.pivot(index='pdb_model_chain_1', columns='pdb_model_chain_2', values='rmsd').fillna(0)

    return dataset


def get_fontSize(data):
    """
    Get a customized font size for clustermaps.
    Parameters:
                * data:        a dataframe. Correspond to a CoDNaS-RNA
                                            clustermap dataset.
    Observation:
        Tests were done using extreme clusters and 
        get_cellSizePixels() function:
            # Tested on (3, 100, 16) and (38, 30, 16).
                * Format (#conf, cellSizePixels, fontSize)
            #
    Return: an int
    """
    if data.shape[0] >= 5:
        font_size = 16 # (16 was chosen for all the database
                       #  using corresponding cellSizePixel
                       #  to gain readability)
    else:
        font_size = 12 #(adjust for not-overlapping labels)
    return font_size


def create_list_files_from_clustermap_warnings(cluster_warnings, outputDir):
    """
    Create and save list files from clustermap warnings to be used as auxiliary files
    in CoDNaS-RNA website.
    Dictionary structure: { 'clusters_with_two_confs': []
                          , 'clusters_with_all_equal_rmsd': [] }
    Parameters:
                * cluster_warnings:    a dict. Correspond to a dictionary of clustermap warnings.
                * outputDir:           a string. Correspond to an output directory to store
                                                 list files of warnings.
    Return: nothing
    """
    # Before start to do anything, check directories and if outputDir does not exists create it
    outputDir = build_ok_outputDir_string(outputDir)
    create_dir(outputDir)

    for aFileName in [*cluster_warnings]:
        aListFile = outputDir+aFileName+'.list'
        with open(aListFile, 'a') as aFile:
            aFile.write(','.join(cluster_warnings[aFileName]))

    return


def create_files(website_tables_dir, releaseDir):
    """
    Create all clustermaps and thumbnails for CoDNaS-RNA website.
    Warnings list files are created too as auxiliary file for the web.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * releaseDir:                a string. Correspond to an CoDNaS-RNA release directory
                                                       where to store img/clustermap/ and
                                                       web/img/thumbnail/clustermap/.
    Return: nothing
    """
    # Before start to do anything, check if input directory is OK
    website_tables_dir = build_ok_outputDir_string(website_tables_dir)
    releaseDir = build_ok_outputDir_string(releaseDir)

    # Load website tables
    if os.path.exists(website_tables_dir + "cdrna_pairs.csv.gz"):
            cdrna_pairs = get_df_if_file_exist(
            "cdrna_pairs", website_tables_dir + "cdrna_pairs.csv.gz", ",", False
        )
    else:
        raise FileNotFoundError('[Error] File not exists: {} and {}.'.format(website_tables_dir + "cdrna_pairs.csv.gz")
        )

    cdrna_pairs = cdrna_pairs[cdrna_pairs['cluster_id'] != 'NA'].copy()

    cluster_warnings = { 'clusters_with_two_confs': []
                       , 'clusters_with_all_equal_rmsd': [] }
    for aCltr in set(cdrna_pairs.cluster_id):

        cluster_subset = get_cluster_subset_with_rmsd(aCltr, cdrna_pairs)

        if has_two_conformers(cluster_subset):
            cluster_warnings['clusters_with_two_confs'].append(aCltr)
            continue
        elif are_rmsd_all_equal(cluster_subset):
            cluster_warnings['clusters_with_all_equal_rmsd'].append(aCltr)
            continue

        dataset = get_cluster_hcmap_dataset(cluster_subset)

        clustermap_image_name = releaseDir+'img/clustermap/'+aCltr+'_clustermap.png'
        thumbnail_image_name  = releaseDir+'web/img/thumbnail/clustermap/'+aCltr+'_clustermap_thumbnail.jpg'

        create_hcmap_figure( dataset
                           , xlabel_triu='RMSD [Å] ('+aCltr+' scale)'
                           , xlabel_tril='RMSD [Å] (CoDNaS-RNA scale)'
                           , vmin_cbar_tril=0
                           , vmax_cbar_tril=5
                           , font_size=get_fontSize(dataset)
                           , output_file=clustermap_image_name )

        create_thumbnail( img=clustermap_image_name
                        , output_file=thumbnail_image_name )

    create_list_files_from_clustermap_warnings(cluster_warnings, releaseDir+'web/img/')

    print("All done !")
    return

# website_tables_dir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'
# releaseDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/'

if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create double-clustermap PNGs, thumbnail JPG and warning list files to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        'website_tables_dir',
        type=str,
        help='Path where all website tables are located.'
    )
    p.add_argument(
        'releaseDir',
        type=str,
        help='Path where is stored a CoDNaS-RNA\'s release.'
    )

    args = p.parse_args()
    create_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
"""\
Purpose: With this function you can create a directory.

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * nothing
                - Modules
                    * build_ok_outputDir_string

Arguments:    -Positional
                * [a_dir]              // string

Observations:
            - 

TODO: - 

"""

import argparse
import os
from build_ok_outputDir_string import build_ok_outputDir_string


def create_dir(a_dir):
    """
    Create a directory or do nothing if exists.
    Parameters:
                * a_dir: a string. Correspond to a directory path.
    Return: nothing
    """
    # Directory to save results
    a_dir = build_ok_outputDir_string(a_dir)
    # Make dir or do nothing if exists
    os.makedirs(a_dir, exist_ok=True)
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Create a directory.")
    # Positional arguments
    p.add_argument(
        "a_dir",
        type=str,
        help="Directory path to be created."
    )

    args = p.parse_args()
    create_dir(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
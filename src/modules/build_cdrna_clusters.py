"""\
Purpose: With this module you can create a full CoDNaS-RNA
         cdrna_clusters.csv.gz website table.

Preconditions:
                - Files for input
                    * pdbchains_databases_codes
                    * tmalign_parsed_table
                    * tmalign_report
                    * rcsb_table
                    * clusters_path/        (all Clusters_#.csv tables)
                    * synthetics_table
                    * superclustering_table
                    * cdrna_conformers
                    * cdrna_interactions
                    * cdrna_rnacentral
                - Libraries
                    * Pandas
                    * Numpy

Arguments:    -Positional
                * [pdbchains_databases_codes]    // string
                * [tmalign_parsed_table]         // string
                * [tmalign_report]               // string
                * [rcsb_table]                   // string
                * [clusters_path]                // string
                * [synthetics_table]             // string
                * [superclustering_table]        // string
                * [cdrna_conformers]             // string
                * [cdrna_interactions]           // string
                * [cdrna_rnacentral]             // string
                * [outputDir]                    // string

Observations:
            - 

TODO: - Implement optional parameters for partially fill cdrna_clusters.csv.gz

"""

import argparse
import os
import collections
import numpy as np
import pandas as pd
from save_website_table import save_website_table
from get_website_df import get_df_if_file_exist
from add_cell_value import add_value_in_column_to_subset
from create_dir import create_dir


def create_dataframe_struct():
    """
    Create a dataframe structure.
    Parameters: nothing
    TODO: add list parameter to be used as a column builder.
    Return: an empty dataframe
    """
    # Create the Dataframe to be used in cdrna_clusters.
    columns = [ "cluster_id"            # + Table PDBchains_with_databases_codes.tsv
              , "gold"                  # Table Superclustering
              , "rna_name_main"         # RCSB (Macromolecular name; ver nombres de mRNA)
              , "rna_name_list"         # RCSB (Macromolecular name; ver nombres de mRNA)
              , "rna_type_main"         # Table RNAcentral por cluster  <---- por mayoría
              , "rna_type_list"         # Table RNAcentral por cluster
              , "num_conf"              # + Table TMAlign_Report
              , "num_conf_not_syn"      # + Table RNAcentral_PDB_synthetic_entries_mapped_and_present_in_CoDNaS-RNA.tsv   <----- [0, num_conf] Ronaldo lee esto para el filtro.
              , "seq_id_min"            # + Table TMalign             <---- solo las más largas ver %IDE en TMalign por alineamiento ( posible filtro de eval e/ 98 y 100, sino está mal)
              , "seq_id_max"            # + Table TMalign
              , "seq_id_avg"            # + Table TMalign
              , "seq_length_min"        # + RCSB
              , "seq_length_max"        # + RCSB
              , "seq_length_avg"        # + RCSB
              , "xrd_perc"              # + RCSB
              , "nmr_perc"              # + RCSB
              , "em_perc"               # + RCSB
              , "resol_min"             # + RCSB
              , "resol_max"             # + RCSB
              , "resol_avg"             # + RCSB
              , "urs_main"              # + Table PDBchains_with_databases_codes.tsv
              , "urs_list"              # + Table PDBchains_with_databases_codes.tsv
              , "has_RNA-Prot_inter"    # Table cdrna_interaction.csv (from DSSR; real-interactions) || RCSB (co-crystal) (no tiene que tener matches; va 0 o 1)
              , "has_RNA-RNA_inter"     # Table cdrna_interaction.csv (from DSSR; real-interactions) || RCSB (co-crystal) (no tiene que tener matches; va 0 o 1)
              , "has_RNA-Met_inter"     # Table cdrna_interaction.csv (from DSSR; real-interactions) || RCSB (co-crystal) (no tiene que tener matches; va 0 o 1)
              , "source_main"           # RNAcentral API   <--- por mayoría || RCSB
              , "source_list"           # RNAcentral API [] || RCSB
              , "rmsd_min"              # + Table TMalign
              , "rmsd_max"              # + Table TMalign
              , "rmsd_avg"              # + Table TMalign
              , "tm_score_min"          # + Table TMalign
              , "tm_score_max"          # + Table TMalign
              , "tm_score_avg"          # + Table TMalign
              , "aligned_length_min"
              , "aligned_length_max"
              , "aligned_length_avg" ]
    cdrna_clusters = pd.DataFrame(columns=columns)

    return cdrna_clusters


def get_urs_values(cltr_id, mapping_cltrs):
    """
    Get an URS_main list and an URS_list list for one CoDNaS-RNA cluster.
    Parameters:
                * cltr_id:                   a string. Correspond to a CoDNaS-RNA cluster.
                * mapping_cltrs:             a dataframe. Correspond to a pandas dataframe for
                                                          all PDB values and releated codes.
    Return: two lists
    """
    # Ask if RNAcentral_URS column exist in file, otherwise exit. (TODO)
    URS_list = (
        mapping_cltrs["RNAcentral_URS"]
        .loc[mapping_cltrs["Cluster_ID"] == cltr_id]
        .dropna()
        .tolist()
    )

    # Check if list is empty
    if not URS_list:
        # This is our internal convention to check errors ("-")
        URS_list = ["-"]
        URS_main = ["-"]
        return URS_main, URS_list
    else:
        sorted(URS_list)

    # Count total URS elements in cluster.
    total_urs_elements_in_cltr = len(URS_list)

    # Create URS dict with total counts per urs_code.
    urs_dict_counter = collections.Counter(URS_list)

    # Ask if urs_dict_counter has "RNAcentral id not found" as a key.
    if "RNAcentral id not found" in urs_dict_counter.keys():
        # Get percentage with format, like "20.00".
        percentage = (
            100 * urs_dict_counter["RNAcentral id not found"]
        ) / total_urs_elements_in_cltr
        urs_not_found = ("RNAcentral id not found", f"{percentage:04.2f}")
        del urs_dict_counter["RNAcentral id not found"]
    # Check if urs_dict_counter is empty (could happen that only was present "RNAcentral id not found" element)
    if not urs_dict_counter:
        URS_list = ["Not available."]
        URS_main = ["Not available."]
        return URS_main, URS_list

    # Taken from https://stackoverflow.com/questions/3989016/how-to-find-all-positions-of-the-maximum-value-in-a-list
    urs_main = max(urs_dict_counter.values())
    # This gives a list of how many maximums are, like [1, 1, 1, 1].
    total_urs_main = len(
        [a_urs for a_urs in urs_dict_counter.values() if a_urs == urs_main]
    )

    # Start to create both list (URS_main and URS_list)
    # Create URS_main
    URS_main = []
    for urs_tup in urs_dict_counter.most_common(total_urs_main):
        a_URS = urs_tup[0]
        a_URS_count = urs_tup[1]
        # Get percentage with format, like "20.00".
        percentage = (100 * a_URS_count) / total_urs_elements_in_cltr
        URS_main.append((a_URS, f"{percentage:04.2f}"))
        del urs_dict_counter[a_URS]
    # Create URS_list
    URS_list = []
    if urs_dict_counter:
        for urs_tup in urs_dict_counter.items():
            a_URS = urs_tup[0]
            a_URS_count = urs_tup[1]
            # Get percentage with format, like "20.00".
            percentage = (100 * a_URS_count) / total_urs_elements_in_cltr
            URS_list.append((a_URS, f"{percentage:04.2f}"))
        # Order by decreasing values
        urs_dict = {tup[0]: float(tup[1]) for tup in URS_list}
        urs_dict_counter = collections.Counter(urs_dict)
        URS_list = urs_dict_counter.most_common(len(urs_dict.keys()))
        # If exist, add in last position "RNAcentral id not found"
        if "urs_not_found" in locals():
            URS_list.append(urs_not_found)
    else:
        # If exist, add in last position "RNAcentral id not found"
        if "urs_not_found" in locals():
            URS_list.append(urs_not_found)
        else:
            URS_list = ["NA"]

    return URS_main, URS_list


def extract_info_databases_code_table(
    cdrna_clusters, pdbchains_databases_codes, a_separator, outputDir
):
    """
    Extract URS cluster related information using pdbchains_databases_codes.tsv as table provider.
    Parameters:
                * cdrna_clusters:            a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA clusters website table.
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * a_separator:               a string. Correspond to the pdbchains_databases_codes.tsv
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        return cdrna_clusters

    # Get a dataframe from table
    mapping_cltrs = get_df_if_file_exist(
        "mapping_cltrs", pdbchains_databases_codes, a_separator, False
    )

    # Create dict with all URS per Cluster_ID
    cltrs_URS_dic = dict()

    # Ask if column exist in file, otherwise exit. (TODO)
    # Create a dict of clusters with a list of unique URS codes present per cluster.
    for cltr_id in mapping_cltrs["Cluster_ID"].dropna().unique().tolist():
        # Get URS values from cluster
        URS_main, URS_list = get_urs_values(cltr_id, mapping_cltrs)
        # Add URS values to dictionary
        cltrs_URS_dic[cltr_id] = (URS_main, URS_list)

    for cltr_id, urs_tup in cltrs_URS_dic.items():
        URS_main = urs_tup[0]
        URS_list = urs_tup[1]
        data = [{ "cluster_id": cltr_id
                , "gold": "NA"
                , "rna_name_main": "NA"
                , "rna_name_list": "NA"
                , "rna_type_main": "NA"
                , "rna_type_list": "NA"
                , "num_conf": "NA"
                , "num_conf_not_syn": "NA"
                , "seq_id_min": "NA"
                , "seq_id_max": "NA"
                , "seq_id_avg": "NA"
                , "seq_length_min": "NA"
                , "seq_length_max": "NA"
                , "seq_length_avg": "NA"
                , "xrd_perc": "NA"
                , "nmr_perc": "NA"
                , "em_perc": "NA"
                , "resol_min": "NA"
                , "resol_max": "NA"
                , "resol_avg": "NA"
                , "urs_main": URS_main
                , "urs_list": URS_list
                , "has_RNA-Prot_inter": "NA"
                , "has_RNA-RNA_inter": "NA"
                , "has_RNA-Met_inter": "NA"
                , "source_main": "NA"
                , "source_list": "NA"
                , "rmsd_min": "NA"
                , "rmsd_max": "NA"
                , "rmsd_avg": "NA"
                , "tm_score_min": "NA"
                , "tm_score_max": "NA"
                , "tm_score_avg": "NA"
                , "aligned_length_min":"NA"
                , "aligned_length_max":"NA"
                , "aligned_length_avg":"NA" }]
        cdrna_clusters = cdrna_clusters.append(data, ignore_index=True, sort=False)

    return cdrna_clusters


def get_tmalign_dict(cltr_id, tmalign_table, acolumn_list):
    """
    Get a dictionary with tmalign values using acolumn_list as keys for one CoDNaS-RNA cluster.
    Parameters:
                * cltr_id:                   a string. Correspond to a CoDNaS-RNA cluster.
                * tmalign_table:             a dataframe. Correspond to a pandas dataframe for
                                                          tmalign parsed table.
                * acolumn_list:              a list. Correspond to a list of columns 
                                                     from tmalign_table dataframe.
    Return: a dict
    """
    # Create dict with all tmalign values
    tmalign_dic = dict()

    # Start tu create a subset that correspond to cluster id.
    # First, remove 'TM_score' mid-name column to use "acolumn_list" in dropna().
    # Second, replace "NA" string to "NaN" values so they can be drop them.
    # Third, append 'TM_score' mid-name column to acolumn_list so can be used below as a flag.
    acolumn_list.remove("TM_score")
    # Here, if Seq_ID or RSMD are NaN, TMscore are NaN.
    cltr_subset = (
        tmalign_table[tmalign_table["Cluster_ID"] == cltr_id]
        .replace("NA", np.nan)
        .dropna(subset=acolumn_list)
    )
    acolumn_list.append("TM_score")

    for acolumn in acolumn_list:
        # Check that exist rows from this cluster. Could not run in TMalign.
        if cltr_subset.empty:
            # Put everything in lower case to be used in final cdrna_clusters columns table.
            acolumn = acolumn.lower()
            tmalign_dic[acolumn] = ["NA", "NA", "NA"]
            continue
        if acolumn == "TM_score":
            # Iterating over two columns, use `zip`
            size_diff = [
                (int(c1) - int(c2))
                for c1, c2 in zip(
                    cltr_subset["Length_chain1"],
                    cltr_subset["Length_chain2"]
                )
            ]
            tmscores_tup = [
                (tmsc1, tmsc2)
                for tmsc1, tmsc2 in zip( cltr_subset["TM_score_norm_chain1"]
                                       , cltr_subset["TM_score_norm_chain2"] )
            ]
            tmscores = []
            for adiff, tmsc_tup in zip(size_diff, tmscores_tup):
                if adiff == 0:
                    tmscores.append(float(tmsc_tup[0]))
                elif adiff > 0:
                    tmscores.append(float(tmsc_tup[0]))
                elif adiff < 0:
                    tmscores.append(float(tmsc_tup[1]))
            acolumn_min = float(min(tmscores))
            acolumn_max = float(max(tmscores))
            acolumn_avg = sum(tmscores) / len(tmscores)
            column_values_list_formated = [
                f"{acol:04.4f}" for acol in [acolumn_min, acolumn_max, acolumn_avg]
            ]
            # Put everything in lower case to be used in final cdrna_clusters columns table.
            acolumn = acolumn.lower()
            tmalign_dic[acolumn] = column_values_list_formated
#            continue
            acolumn_min = float(min(cltr_subset[acolumn]))
            acolumn_max = float(max(cltr_subset[acolumn]))
            acolumn_avg = sum(float(alength) for alength in cltr_subset[acolumn]) / len(
                cltr_subset[acolumn]
            )
            column_values_list_formated = [
                f"{acol:04.4f}" for acol in [acolumn_min, acolumn_max, acolumn_avg]
            ]
            # Put everything in lower case to be used in final cdrna_clusters columns table.
            acolumn = acolumn.lower()
            tmalign_dic[acolumn] = column_values_list_formated
        else:
            cltr_subset[acolumn] = cltr_subset[acolumn].astype('float')
            acolumn_min = min(cltr_subset[acolumn])
            acolumn_max = max(cltr_subset[acolumn])
            acolumn_avg = sum(alength for alength in cltr_subset[acolumn]) / len(cltr_subset[acolumn])
            column_values_list_formated = [
                f"{acol:04.4f}" for acol in [acolumn_min, acolumn_max, acolumn_avg]
            ]
            if acolumn == 'Aligned_length':
                column_values_list_formated[0] = str(int(float(column_values_list_formated[0])))
                column_values_list_formated[1] = str(int(float(column_values_list_formated[1])))
                column_values_list_formated[2] = str(round(float(column_values_list_formated[2])))
            acolumn = acolumn.lower()
            tmalign_dic[acolumn] = column_values_list_formated

    return tmalign_dic


def extract_info_tmalign_table(
    cdrna_clusters, tmalign_parsed_table, a_separator, outputDir
):
    """
    Extract max, min and avg values for RMSD, TMscore and Seq. Length columns per CoDNaS-RNA cluster
    using tmalign_parsed_table.tsv.gz as table provider.
    Parameters:
                * cdrna_clusters:            a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA clusters website table.
                * tmalign_parsed_table:      a string. Correspond to a TSV table with all
                                                       TMalign parsed alignments.
                * a_separator:               a string. Correspond to the tmalign_parsed_table.tsv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        return cdrna_clusters

    # Get a dataframe from table
    tmalign_table = get_df_if_file_exist(
        "tmalign_table", tmalign_parsed_table, a_separator, False
    )

    # Create dict with all tmalign values per Cluster_ID
    cltrs_tmalign_dic = dict()
    for cltr_id in cdrna_clusters["cluster_id"].replace("NA", np.nan).dropna().tolist():

        tmalign_dic = get_tmalign_dict(
            cltr_id, tmalign_table, ["Seq_ID", "RMSD", "TM_score", "Aligned_length"]
        )

        # Add tmalign values to dictionary
        cltrs_tmalign_dic[cltr_id] = tmalign_dic
    # Fill values to cdrna_clusters
    for cltr_id, tmalign_dic in cltrs_tmalign_dic.items():
        for acolumn, list_values in tmalign_dic.items():
            a_min = list_values[0]
            a_max = list_values[1]
            a_avg = list_values[2]
            add_value_in_column_to_subset( cdrna_clusters
                                         , cltr_id
                                         , "cluster_id"
                                         , a_min
                                         , acolumn + "_" + "min" )
            add_value_in_column_to_subset( cdrna_clusters
                                         , cltr_id
                                         , "cluster_id"
                                         , a_max
                                         , acolumn + "_" + "max" )
            add_value_in_column_to_subset( cdrna_clusters
                                         , cltr_id
                                         , "cluster_id"
                                         , a_avg
                                         , acolumn + "_" + "avg" )

    return cdrna_clusters


def extract_info_tmalign_report(cdrna_clusters, tmalign_report, a_separator, outputDir):
    """
    Extract number of models per CoDNaS-RNA cluster using tmalign_report.tsv as table provider.
    Parameters:
                * cdrna_clusters:            a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA clusters website table.
                * tmalign_report:            a string. Correspond to a TSV table wit all TMalign
                                                       report information.
                * a_separator:               a string. Correspond to the tmalign_report.tsv
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        return cdrna_clusters

    # Get a dataframe from table
    tmalign_report_df = get_df_if_file_exist(
        "tmalign_report_df", tmalign_report, a_separator, False
    )

    # Create dict with all report values per Cluster_ID
    cltrs_report_dic = dict()
    for cltr_id in cdrna_clusters["cluster_id"].replace("NA", np.nan).dropna().tolist():

        num_models = tmalign_report_df[tmalign_report_df["Cluster_ID"] == cltr_id][
            "Total_models"
        ].tolist()[0]

        # Add report values to dictionary
        cltrs_report_dic[cltr_id] = num_models

    # Fill values to cdrna_clusters
    for cltr_id, num_models in cltrs_report_dic.items():
        add_value_in_column_to_subset( cdrna_clusters
                                     , cltr_id
                                     , "cluster_id"
                                     , num_models
                                     , "num_conf" )

    return cdrna_clusters


def get_rcsb_dict(cltr_file, rcsb_table, acolumn_list):
    """
    Get a dictionary with RCSB values using acolumn_list as keys for one CoDNaS-RNA cluster.
    Parameters:
                * cltr_id:                   a string. Correspond to a CoDNaS-RNA cluster.
                * rcsb_table:                a dataframe. Correspond to a pandas dataframe for
                                                          rcsb table.
                * acolumn_list:              a list. Correspond to a list of columns
                                                     from rcsb_table dataframe.
    Return: a dict
    """
    # Create dict with all cluster pdb chains rows (using cltr_file)
    rcsb_dic = dict()

    # Build cltr_subset
    pdb_chains_list = cltr_file["Seq_code"].tolist()
    # Start tu create a subset that correspond to cluster pdb chains.
    # Select only columns that are from interest. Like, df1 = df[['a','b']]
    if "PDB_chain" not in acolumn_list:
        acolumn_list.append("PDB_chain")
    rcsb_table_copy = rcsb_table[acolumn_list].copy()
    rcsb_table_cltr_subset = rcsb_table_copy.loc[
        rcsb_table_copy["PDB_chain"].isin(pdb_chains_list)
    ]
    rcsb_table_cltr_subset = rcsb_table_cltr_subset.drop_duplicates(
        subset=acolumn_list, ignore_index=True
    )

    for acolumn in acolumn_list:
        # Check that exist rows from this cluster. Could not have information.
        if rcsb_table_cltr_subset.empty:
            rcsb_dic[acolumn] = ["NA", "NA", "NA"]
            continue
        if acolumn == "Chain Length":
            length_values = rcsb_table_cltr_subset[acolumn].tolist()
            acolumn_min = min(length_values)
            acolumn_max = max(length_values)
            acolumn_avg = sum(
                int(leng) for leng in rcsb_table_cltr_subset[acolumn]
            ) / len(length_values)
            acolumn_avg = f"{acolumn_avg:04.2f}"
            rcsb_dic[acolumn] = [acolumn_min, acolumn_max, acolumn_avg]
        elif acolumn == "Resolution":
            rcsb_resolution_subset = rcsb_table_cltr_subset.copy()
            if rcsb_resolution_subset.isnull().values.any():
                rcsb_resolution_subset = rcsb_resolution_subset.dropna(subset=[acolumn])
            if rcsb_resolution_subset.empty:
                rcsb_dic[acolumn] = ["NA", "NA", "NA"]
                continue
            resol_values = rcsb_resolution_subset[acolumn].tolist()
            acolumn_min = min(resol_values)
            acolumn_max = max(resol_values)
            acolumn_avg = sum(
                float(resol) for resol in rcsb_resolution_subset[acolumn]
            ) / len(resol_values)
            acolumn_avg = f"{acolumn_avg:04.2f}"
            rcsb_dic[acolumn] = [acolumn_min, acolumn_max, acolumn_avg]
        #            del rcsb_resolution_subset
        elif acolumn == "Exp. Method":
            total_exp = len(rcsb_table_cltr_subset[acolumn].tolist())
            exp_methods_dict = dict(rcsb_table_cltr_subset[acolumn].value_counts())
            # Create a list to be filled of exp percentages
            exp_list = []
            for exp_met in exp_methods_dict.keys():
                if (exp_met == "X-RAY DIFFRACTION") or (
                    exp_met == "X-RAY DIFFRACTION, SOLUTION SCATTERING"
                ):
                    if "xrd_perc" in locals():
                        total_xrd = exp_methods_dict[exp_met]
                        xrd_perc_new = (100 * total_xrd) / total_exp
                        #                        xrd_perc = [perc for perc in exp_list if perc == xrd_perc][0]   #  Remove if is not used
                        xrd_perc = xrd_perc + xrd_perc_new
                        continue
                    total_xrd = exp_methods_dict[exp_met]
                    xrd_perc = (100 * total_xrd) / total_exp
                elif exp_met == "SOLUTION NMR":
                    total_nmr = exp_methods_dict[exp_met]
                    nmr_perc = (100 * total_nmr) / total_exp
                elif exp_met == "ELECTRON MICROSCOPY":
                    total_em = exp_methods_dict[exp_met]
                    em_perc = (100 * total_em) / total_exp
                else:
                    total_others = exp_methods_dict[exp_met]
                    others_perc = (100 * total_others) / total_exp
                    exp_list.append(others_perc)

            if "xrd_perc" in locals():
                exp_list.append(xrd_perc)
            else:
                xrd_perc = 0
                exp_list.append(xrd_perc)
            if "nmr_perc" in locals():
                exp_list.append(nmr_perc)
            else:
                nmr_perc = 0
                exp_list.append(nmr_perc)
            if "em_perc" in locals():
                exp_list.append(em_perc)
            else:
                em_perc = 0
                exp_list.append(em_perc)

            column_values_list_formated = [f"{acol:04.2f}" for acol in exp_list]
            rcsb_dic[acolumn] = column_values_list_formated
        continue

    return rcsb_dic


def extract_info_rcsb_table(
    cdrna_clusters, rcsb_table, clusters_path, a_separator, outputDir
):
    """
    Extract max, min and avg for chain length and resolution, and experimental methods percentages
    per CoDNaS-RNA cluster using rcsb_table.csv.gz and clusters_path/Cluster_#.csv as tables providers.
    Parameters:
                * cdrna_clusters:            a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA clusters website table.
                * rcsb_table:                a string. Correspond to a CSV table with all RCSB
                                                       information.
                * clusters_path:             a string. Correspond to a directory path where all
                                                       sequence clustering cluster tables are stored.
                * a_separator:               a string. Correspond to the rcsb_table.csv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        return cdrna_clusters

    # Get a dataframe from table
    rcsb_table = get_df_if_file_exist("rcsb_table", rcsb_table, a_separator, True)

    # Create dict with all report values per Cluster_ID
    cltrs_rcsb_dic = dict()
    for cltr_id in cdrna_clusters["cluster_id"].replace("NA", np.nan).dropna().tolist():

        cltr_file = get_df_if_file_exist(
            "cltr_file", clusters_path + cltr_id + ".csv", ",", False
        )

        rcsb_dic = get_rcsb_dict(
            cltr_file, rcsb_table, ["Chain Length", "Exp. Method", "Resolution"]
        )

        # Add rcsb dict to dictionary
        cltrs_rcsb_dic[cltr_id] = rcsb_dic

    # Fill values to cdrna_clusters
    for cltr_id, rcsb_dic in cltrs_rcsb_dic.items():
        for acol in rcsb_dic.keys():
            if acol == "Chain Length":
                acol_pattern = "seq_length"
            elif acol == "Resolution":
                acol_pattern = "resol"
            else:
                acol_pattern = "perc"
                add_value_in_column_to_subset( cdrna_clusters
                                             , cltr_id
                                             , "cluster_id"
                                             , rcsb_dic[acol][0]
                                             , "xrd" + "_" + acol_pattern )
                add_value_in_column_to_subset( cdrna_clusters
                                             , cltr_id
                                             , "cluster_id"
                                             , rcsb_dic[acol][1]
                                             , "nmr" + "_" + acol_pattern )
                add_value_in_column_to_subset( cdrna_clusters
                                             , cltr_id
                                             , "cluster_id"
                                             , rcsb_dic[acol][2]
                                             , "em" + "_" + acol_pattern )
                continue
            add_value_in_column_to_subset( cdrna_clusters
                                         , cltr_id
                                         , "cluster_id"
                                         , rcsb_dic[acol][0]
                                         , acol_pattern + "_" + "min" )
            add_value_in_column_to_subset( cdrna_clusters
                                         , cltr_id
                                         , "cluster_id"
                                         , rcsb_dic[acol][1]
                                         , acol_pattern + "_" + "max" )
            add_value_in_column_to_subset( cdrna_clusters
                                         , cltr_id
                                         , "cluster_id"
                                         , rcsb_dic[acol][2]
                                         , acol_pattern + "_" + "avg" )

    #        "Macromol. Type"            ? (RNA; DNA; RNA/DNA; Protein/RNA; Protein/DNA)
    #        "Macromolecular Name"        ? (More specific names of atype)
    #        "Macromolecule Name"        ? (More specific names)

    return cdrna_clusters


def extract_info_synthetics_table(
    cdrna_clusters, synthetics_table, a_separator, outputDir
):
    """
    Extract number of conformers that are not synthetics per CoDNaS-RNA cluster using
    synthetics_table.tsv as table provider.
    Parameters:
                * cdrna_clusters:            a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA clusters website table.
                * synthetics_table:          a string. Correspond to a TSV table with all
                                                       synthetic conformers present in CoDNaS-RNA.
                * a_separator:               a string. Correspond to the synthetics_table.tsv
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        return cdrna_clusters

    # Get a dataframe from table
    synthetic_table = get_df_if_file_exist(
        "synthetic_table", synthetics_table, a_separator, False
    )

    # Create dict with all report values per Cluster_ID
    cltrs_rcsb_dic = dict()
    for cltr_id in cdrna_clusters["cluster_id"].replace("NA", np.nan).dropna().tolist():

        if cltr_id in synthetic_table["Cluster_ID"].tolist():
            # Get the number of rows that correspond to the number of synthetic models.
            num_syn_models = synthetic_table.loc[
                synthetic_table["Cluster_ID"] == cltr_id
            ].shape[0]
        else:
            num_syn_models = 0

        total_num_models = cdrna_clusters.loc[
            cdrna_clusters["cluster_id"] == cltr_id, "num_conf"
        ].tolist()[0]

        num_not_syn = int(total_num_models) - int(num_syn_models)

        # Add rcsb dict to dictionary
        cltrs_rcsb_dic[cltr_id] = num_not_syn

    # Fill values to cdrna_clusters
    for cltr_id, num_not_syn in cltrs_rcsb_dic.items():
        add_value_in_column_to_subset(
            cdrna_clusters, cltr_id, "cluster_id", num_not_syn, "num_conf_not_syn"
        )

    return cdrna_clusters


def get_gold_status(cltr_file, sc_table):
    """
    Get gold status from original supercluster information mapping conformers
    from clusters_path/Cluster_#.csv.
    Parameters:
                * cltr_file:                 a dataframe. Correspond to a pandas dataframe for
                                                          a cluster with its conformers.
                * sc_table:                  a dataframe. Correspond to a pandas dataframe for
                                                          superclustering_table.
    Return: an int and a string
    """
    # Build cltr_subset
    pdb_chains_list = cltr_file["Seq_code"].tolist()

    sc_subset = (
        sc_table.loc[sc_table["query_code"].isin(pdb_chains_list)]["cluster_name"]
        .unique()
        .tolist()
    )

    sc_subset = {sc_name for sc_name in sc_subset}

    if len(sc_subset) == 1:
        if list(sc_subset)[0].startswith("O"):
            gold_status = 1
            sc_seen = list(sc_subset)[0]
        else:
            gold_status = 0
            sc_seen = "NA"
    else:
        raise ValueError('[Error] Number of superclustering subset is not 1: {}'.format(sc_subset))

    return gold_status, sc_seen


def extract_info_superclustering_table(
    cdrna_clusters, superclustering_table, clusters_path, a_separator, outputDir
):
    """
    Extract gold status per CoDNaS-RNA cluster using superclustering_table.csv and
    clusters_path/Cluster_#.csv as tables providers.
    Parameters:
                * cdrna_clusters:            a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA clusters website table.
                * superclustering_table:     a string. Correspond to a CSV table with all
                                                       superclustering information.
                * clusters_path:             a string. Correspond to a directory path where all
                                                       sequence clustering cluster tables are stored.
                * a_separator:               a string. Correspond to the superclustering_table.csv
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        return cdrna_clusters

    # superclusters_info.csv
    # Get a dataframe from table
    sc_table = get_df_if_file_exist(
        "sc_table", superclustering_table, a_separator, False
    )

    # Create dict with all report values per Cluster_ID
    cltrs_sc_dic = dict()
    sc_seen_list = []
    for cltr_id in cdrna_clusters["cluster_id"].replace("NA", np.nan).dropna().tolist():
        cltr_file = get_df_if_file_exist(
            "cltr_file", clusters_path + cltr_id + ".csv", ",", False
        )
        gold_status, sc_seen = get_gold_status(cltr_file, sc_table)

        if not sc_seen in sc_seen_list:
            if sc_seen != "NA":
                sc_seen_list.append(sc_seen)
        else:
            print("Supercluster O already seen it:" + "\t" + sc_seen)

        # Add supercluster dict to dictionary
        cltrs_sc_dic[cltr_id] = gold_status
    # Fill values to cdrna_clusters
    for cltr_id, gold_status in cltrs_sc_dic.items():
        add_value_in_column_to_subset(
            cdrna_clusters, cltr_id, "cluster_id", gold_status, "gold"
        )

    return cdrna_clusters


def get_column_values(cltr_id, cdrna_conformers, acolumn):
    """
    Get a main column value list (column_main) and a column value list (column_list)
    per CoDNaS-RNA cluster using cdrna_conformers.csv.gz as table provider.
    Parameters:
                * cltr_id:                   a string. Correspond to a CoDNaS-RNA cluster.
                * cdrna_conformers:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA conformers website table.
                * acolumn:                   a string. Correspond to a column name
                                                       from cdrna_conformers dataframe.
    Return: two lists
    """
    # Ask if macromolecule column exist in file, otherwise exit. (TODO)
    #    name_list = cdrna_conformers['macromolecule'].loc[cdrna_conformers['cluster_id'] == cltr_id].dropna().tolist()
    # Ask if macromolecule column exist in file, otherwise exit. (TODO)
    column_list = (
        cdrna_conformers[acolumn]
        .loc[cdrna_conformers["cluster_id"] == cltr_id]
        .dropna()
        .tolist()
    )

    # Check if list is empty
    if not column_list:
        # This is our internal convention to check errors ("-")
        column_list = ["-"]
        column_main = ["-"]
        return column_main, column_list
    else:
        sorted(column_list)

    # Count total column elements in cluster.
    total_column_elements_in_cltr = len(column_list)

    # Create column dict with total counts per column_string.
    column_dict_counter = collections.Counter(column_list)

    # Ask if column_dict_counter has "NA" as a key.
    if "NA" in column_dict_counter.keys():
        # Get percentage with format, like "20.00".
        percentage = (100 * column_dict_counter["NA"]) / total_column_elements_in_cltr
        column_not_found = ("NA", f"{percentage:04.2f}")
        del column_dict_counter["NA"]
    # Check if column_dict_counter is empty (could happen that only was present "NA" element)
    if not column_dict_counter:
        column_list = ["Not available."]
        column_main = ["Not available."]
        return column_main, column_list

    # Taken from https://stackoverflow.com/questions/3989016/how-to-find-all-positions-of-the-maximum-value-in-a-list
    column_main = max(column_dict_counter.values())
    # This gives a list of how many maximums are, like [1, 1, 1, 1].
    total_column_main = len(
        [
            a_column
            for a_column in column_dict_counter.values()
            if a_column == column_main
        ]
    )

    # Start to create both list (column_main and column_list)
    # Create na,e_main
    column_main = []
    for column_tup in column_dict_counter.most_common(total_column_main):
        a_column = column_tup[0]
        a_column_count = column_tup[1]
        # Get percentage with format, like "20.00".
        percentage = (100 * a_column_count) / total_column_elements_in_cltr
        column_main.append((a_column, f"{percentage:04.2f}"))
        del column_dict_counter[a_column]
    # Create column_list
    column_list = []
    if column_dict_counter:
        for column_tup in column_dict_counter.items():
            a_column = column_tup[0]
            a_column_count = column_tup[1]
            # Get percentage with format, like "20.00".
            percentage = (100 * a_column_count) / total_column_elements_in_cltr
            column_list.append((a_column, f"{percentage:04.2f}"))
        # Order by decreasing values
        column_dict = {tup[0]: float(tup[1]) for tup in column_list}
        column_dict_counter = collections.Counter(column_dict)
        column_list = column_dict_counter.most_common(len(column_dict.keys()))
        # If exist, add in last position "NA"
        if "column_not_found" in locals():
            column_list.append(column_not_found)
    else:
        # If exist, add in last position "NA"
        if "column_not_found" in locals():
            column_list.append(column_not_found)
        else:
            column_list = ["NA"]

    return column_main, column_list


def extract_info_conformers_table(
    cdrna_clusters, cdrna_conformers, a_separator, outputDir
):
    """
    Extract rna names and sources per CoDNaS-RNA cluster using cdrna_conformers.csv.gz as table provider.
    Parameters:
                * cdrna_clusters:            a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA clusters website table.
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * a_separator:               a string. Correspond to the cdrna_conformers.csv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        return cdrna_clusters

    # Get a dataframe from table
    cdrna_conformers = get_df_if_file_exist(
        "cdrna_conformers", cdrna_conformers, a_separator, False
    )

    # Drop conformers without clusters
    cdrna_conformers = cdrna_conformers.dropna(subset=["cluster_id"]).copy()

    # Create dict with all names per Cluster_ID
    cltrs_name_dict = dict()
    # Create dict with all sources per Cluster_ID
    cltrs_source_dict = dict()

    # Ask if column exist in file, otherwise exit. (TODO)
    # Create a dict of clusters with a list of unique name string present per cluster.
    for cltr_id in cdrna_conformers["cluster_id"].dropna().unique().tolist():
        # Get name values from cluster
        name_main, name_list = get_column_values(
            cltr_id, cdrna_conformers, "macromolecule"
        )
        # Get source values from cluster
        source_main, source_list = get_column_values(
            cltr_id, cdrna_conformers, "source"
        )
        # Add name values to dictionary
        cltrs_name_dict[cltr_id] = (name_main, name_list)
        # Add source values to dictionary
        cltrs_source_dict[cltr_id] = (source_main, source_list)

    # Fill values to cdrna_clusters
    for cltr_id, name_tup in cltrs_name_dict.items():
        name_main = name_tup[0]
        name_list = name_tup[1]
        add_value_in_column_to_subset( cdrna_clusters
                                     , cltr_id
                                     , "cluster_id"
                                     , str(name_main)
                                     , "rna_name_main" )
        add_value_in_column_to_subset( cdrna_clusters
                                     , cltr_id
                                     , "cluster_id"
                                     , str(name_list)
                                     , "rna_name_list" )

    # Fill values to cdrna_clusters
    for cltr_id, source_tup in cltrs_source_dict.items():
        source_main = source_tup[0]
        source_list = source_tup[1]
        add_value_in_column_to_subset( cdrna_clusters
                                     , cltr_id
                                     , "cluster_id"
                                     , str(source_main)
                                     , "source_main" )
        add_value_in_column_to_subset( cdrna_clusters
                                     , cltr_id
                                     , "cluster_id"
                                     , str(source_list)
                                     , "source_list" )

    return cdrna_clusters


def extract_info_interactions_table( cdrna_clusters
                                   , cdrna_interactions
                                   , a_separator
                                   , outputDir ):
    """
    Extract interaction info per CoDNaS-RNA cluster using cdrna_interactions.csv.gz as table provider.
    Parameters:
                * cdrna_clusters:            a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA clusters website table.
                * cdrna_interactions:        a string. Correspond to a CSV CoDNaS-RNA interactions
                                                       website table.
                * a_separator:               a string. Correspond to the cdrna_interactions.csv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        return cdrna_clusters

    # Get a dataframe from table
    cdrna_interactions = get_df_if_file_exist(
        "cdrna_interactions", cdrna_interactions, a_separator, False
    )

    # Create dict with all dssr types per Cluster_ID
    cltrs_dssr_types_dict = collections.defaultdict(set)
    # Make a zip data variable of cluster with dssr types code.
    data = zip(
        cdrna_interactions["cluster_id"], cdrna_interactions["partner_dssr_type"]
    )
    # Create a dict of clusters with a list of PDB:chain codes present per cluster.
    for cltr, dssr_types in data:
        cltrs_dssr_types_dict[cltr].add(dssr_types)

    # Fill values to cdrna_clusters
    for cltr_id, dssr_types_set in cltrs_dssr_types_dict.items():
        if ("nt:nt" in dssr_types_set) or ("nt:misc" in dssr_types_set):
            na_set = set(
                cdrna_interactions[(cdrna_interactions["cluster_id"] == cltr_id)][
                    "partner_rcsb_type"
                ]
                .unique()
                .tolist()
            )
            if "dna" in na_set:
                add_value_in_column_to_subset( cdrna_clusters
                                             , cltr_id
                                             , "cluster_id"
                                             , 1
                                             , "has_RNA-DNA_inter" )
            if "rna" in na_set:
                add_value_in_column_to_subset( cdrna_clusters
                                             , cltr_id
                                             , "cluster_id"
                                             , 1
                                             , "has_RNA-RNA_inter" )
        if (
            ("nt:aa" in dssr_types_set)
            or ("nt:misc" in dssr_types_set)
            or ("aa:misc" in dssr_types_set)
        ):
            np_set = set(
                cdrna_interactions[(cdrna_interactions["cluster_id"] == cltr_id)][
                    "partner_rcsb_type"
                ]
                .unique()
                .tolist()
            )
            if "protein" in np_set:
                add_value_in_column_to_subset( cdrna_clusters
                                             , cltr_id
                                             , "cluster_id"
                                             , 1
                                             , "has_RNA-Prot_inter" )
    # Add zeros to empty values or "NA"
    cdrna_clusters["has_RNA-DNA_inter"]  = cdrna_clusters["has_RNA-DNA_inter"].replace("", 0)
    cdrna_clusters["has_RNA-RNA_inter"]  = cdrna_clusters["has_RNA-RNA_inter"].replace("NA", 0)
    cdrna_clusters["has_RNA-Prot_inter"] = cdrna_clusters["has_RNA-Prot_inter"].replace("NA", 0)

    #            "has_RNA-Prot_inter":"NA", ok
    #            "has_RNA-RNA_inter":"NA",  ok
    #            "has_RNA-DNA_inter":"NA",  ok
    #            "has_RNA-Met_inter":"NA",       <------- Llenar con tablas de interaccion con metales

    return cdrna_clusters


def extract_info_rnacentral( cdrna_clusters
                           , cdrna_rnacentral
                           , a_separator
                           , outputDir ):  # From RNAcentral table merged with postgres
    """
    Extract RNAcentral info (rna type, name and species) per CoDNaS-RNA cluster
    using cdrna_rnacentral.csv.gz as table provider.
    Parameters:
                * cdrna_clusters:            a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA clusters website table.
                * cdrna_rnacentral:          a string. Correspond to a CSV CoDNaS-RNA interactions
                                                       website table.
                * a_separator:               a string. Correspond to the cdrna_rnacentral.csv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        return cdrna_clusters

    # Get a dataframe from table
    cdrna_rnacentral = get_df_if_file_exist(
        "cdrna_rnacentral", cdrna_rnacentral, a_separator, False
    )

    # Drop rnacentral lines without clusters
    cdrna_rnacentral = cdrna_rnacentral.dropna(subset=["cluster_id"]).copy()

    # Create these two columns if not exist
    if "rnacentral_rna_name_main" not in cdrna_clusters.columns:
        cdrna_clusters["rnacentral_rna_name_main"] = "NA"
    if "rnacentral_rna_name_list" not in cdrna_clusters.columns:
        cdrna_clusters["rnacentral_rna_name_list"] = "NA"
    if "rnacentral_specie_main" not in cdrna_clusters.columns:
        cdrna_clusters["rnacentral_specie_main"] = "NA"
    if "rnacentral_specie_list" not in cdrna_clusters.columns:
        cdrna_clusters["rnacentral_specie_list"] = "NA"
 
    cdrna_clusters["rnacentral_type_main"] = cdrna_clusters["rna_type_main"]
    cdrna_clusters["rnacentral_type_list"] = cdrna_clusters["rna_type_list"]

    # Create dict with all types per Cluster_ID
    cltrs_rna_type_dict = dict()
    # Create dict with all names per Cluster_ID
    cltrs_rna_name_dict = dict()
    # Create dict with all species per Cluster_ID
    cltrs_specie_dict = dict()

    # Ask if column exist in file, otherwise exit. (TODO)
    # Create a dict of clusters with a list of unique name string present per cluster.
    for cltr_id in cdrna_rnacentral["cluster_id"].dropna().unique().tolist():
        print(cltr_id)
        # Get name values from cluster
        rna_type_main, rna_type_list = get_column_values( cltr_id
                                                        , cdrna_rnacentral
                                                        , "rna_type" )
        # Get name values from cluster
        rna_name_main, rna_name_list = get_column_values( cltr_id
                                                        , cdrna_rnacentral
                                                        , "rna_name" )
        # Get source values from cluster
        specie_main, specie_list = get_column_values( cltr_id
                                                    , cdrna_rnacentral
                                                    , "specie" )
        # Add name values to dictionary
        cltrs_rna_type_dict[cltr_id] = (rna_type_main, rna_type_list)
        # Add name values to dictionary
        cltrs_rna_name_dict[cltr_id] = (rna_name_main, rna_name_list)
        # Add source values to dictionary
        cltrs_specie_dict[cltr_id] = (specie_main, specie_list)

    # Fill values to cdrna_clusters
    for cltr_id, type_tup in cltrs_rna_type_dict.items():
        rna_type_main = type_tup[0]
        rna_type_list = type_tup[1]
        add_value_in_column_to_subset(
            cdrna_clusters,
            cltr_id,
            "cluster_id",
            str(rna_type_main),
            "rnacentral_type_main",
        )
        add_value_in_column_to_subset(
            cdrna_clusters,
            cltr_id,
            "cluster_id",
            str(rna_type_list),
            "rnacentral_type_list",
        )

    # Fill values to cdrna_clusters
    for cltr_id, name_tup in cltrs_rna_name_dict.items():
        rna_name_main = name_tup[0]
        rna_name_list = name_tup[1]
        add_value_in_column_to_subset( cdrna_clusters
                                     , cltr_id
                                     , "cluster_id"
                                     , str(rna_name_main)
                                     , "rnacentral_rna_name_main" )
        add_value_in_column_to_subset( cdrna_clusters
                                     , cltr_id
                                     , "cluster_id"
                                     , str(rna_name_list)
                                     , "rnacentral_rna_name_list" )

    # Fill values to cdrna_clusters
    for cltr_id, specie_tup in cltrs_specie_dict.items():
        specie_main = specie_tup[0]
        specie_list = specie_tup[1]
        add_value_in_column_to_subset( cdrna_clusters
                                     , cltr_id
                                     , "cluster_id"
                                     , str(specie_main)
                                     , "rnacentral_specie_main" )
        add_value_in_column_to_subset( cdrna_clusters
                                     , cltr_id
                                     , "cluster_id"
                                     , str(specie_list)
                                     , "rnacentral_specie_list" )

    return cdrna_clusters


def fill_cdrna_clusters( pdbchains_databases_codes
                       , tmalign_parsed_table
                       , tmalign_report
                       , rcsb_table
                       , clusters_path
                       , synthetics_table
                       , superclustering_table
                       , cdrna_conformers
                       , cdrna_interactions
                       , cdrna_rnacentral
                       , outputDir ):
    """
    Create and fill the website cdrna_clusters table.
    Parameters:
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * tmalign_parsed_table:      a string. Correspond to a TSV table with all
                                                       TMalign parsed alignments.
                * tmalign_report:            a string. Correspond to a TSV table wit all TMalign
                                                       report information.
                * rcsb_table:                a string. Correspond to a CSV table with all RCSB
                                                       information.
                * clusters_path:             a string. Correspond to a directory path where all
                                                       sequence clustering cluster tables are stored.
                * synthetics_table:          a string. Correspond to a TSV table with all
                                                       synthetic conformers present in CoDNaS-RNA.
                * superclustering_table:     a string. Correspond to a CSV table with all
                                                       superclustering information.
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * cdrna_interactions:        a string. Correspond to a CSV CoDNaS-RNA interactions
                                                       website table.
                * cdrna_rnacentral:          a string. Correspond to a CSV CoDNaS-RNA rnacentral
                                                       website table.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """

    # Before start to do anything, check if outputDir is correct and create it
    create_dir(outputDir)

    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_clusters.csv.gz"):
        cdrna_clusters = get_df_if_file_exist(
            "cdrna_clusters", outputDir + "cdrna_clusters.csv.gz", ",", True
        )
    else:
        cdrna_clusters = create_dataframe_struct()

    cdrna_clusters = extract_info_databases_code_table(
        cdrna_clusters, pdbchains_databases_codes, "\t", outputDir
    )  # PDBchains_with_databases_codes.tsv

    #    save_website_table(cdrna_clusters, outputDir, "cdrna_clusters", ",")

    cdrna_clusters = extract_info_tmalign_table(
        cdrna_clusters, tmalign_parsed_table, "\t", outputDir
    )  # tmalign_parsed_table.tsv.gz

    #    save_website_table(cdrna_clusters, outputDir, "cdrna_clusters", ",")

    cdrna_clusters = extract_info_tmalign_report(
        cdrna_clusters, tmalign_report, "\t", outputDir
    )  # Try to replace and not use this table (simplicity purpose).

    #    save_website_table(cdrna_clusters, outputDir, "cdrna_clusters", ",")

    cdrna_clusters = extract_info_rcsb_table(
        cdrna_clusters, rcsb_table, clusters_path, ",", outputDir
    )

    #    save_website_table(cdrna_clusters, outputDir, "cdrna_clusters", ",")

    cdrna_clusters = extract_info_synthetics_table(
        cdrna_clusters, synthetics_table, "\t", outputDir
    )  # I can use it to compute same info in PDBchains_with_databases_codes.tsv but we need to discuss this first.

    #    save_website_table(cdrna_clusters, outputDir, "cdrna_clusters", ",")

    cdrna_clusters = extract_info_superclustering_table(
        cdrna_clusters, superclustering_table, clusters_path, ",", outputDir
    )

    #    save_website_table(cdrna_clusters, outputDir, "cdrna_clusters", ",")

    cdrna_clusters = extract_info_conformers_table(
        cdrna_clusters, cdrna_conformers, ",", outputDir
    )  # From cdrna_conformers.csv

    #    save_website_table(cdrna_clusters, outputDir, "cdrna_clusters", ",")

    cdrna_clusters = extract_info_interactions_table(
        cdrna_clusters, cdrna_interactions, ",", outputDir
    )  # From DSSR results

    #    save_website_table(cdrna_clusters, outputDir, "cdrna_clusters", ",")

    cdrna_clusters = extract_info_rnacentral(
        cdrna_clusters, cdrna_rnacentral, ",", outputDir
    )  # From RNAcentral table merged with postgres

    save_website_table(cdrna_clusters, outputDir, "cdrna_clusters", ",")

    print("All done !")

    return



# pdbchains_databases_codes = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/PDBchains_with_databases_codes.tsv'
# tmalign_parsed_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/TMalign/tmalign_parsed_comparations.tsv.gz'
# tmalign_report = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/TMalign_alignment_report.tsv'
# rcsb_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/filtered_rna_conformers_from_PDB.csv.gz'
# clusters_path = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/CDHit_processed_most_similar_cluster/cdhit_ide_1.00_cov_0.98.clstr/'
# synthetics_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/RNAcentral/release-15/RNAcentral_synthetics_models_from_CoDNaS-RNA.tsv'
# superclustering_table = '/home/martingb/Projects/2020/CoDNaS-RNA/outputs/2020-05-15/Superclustering/CDHit_Blastclust/100ide_98cov_CDHit_98ide_90cov_Blastclust/superclusters_info.csv'
# cdrna_conformers = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_conformers.csv.gz'
# cdrna_interactions = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_interactions.csv.gz'
# cdrna_rnacentral = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_rnacentral.csv.gz'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'

if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create cdrna_clusters.csv.gz table to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "pdbchains_databases_codes",
        type=str,
        help="Table with all PDB chains with their related codes."
    )
    p.add_argument(
        "tmalign_parsed_table",
        type=str,
        help="Gzip table with all TMalign parsed alignments."
    )
    p.add_argument(
        "tmalign_report",
        type=str,
        help="Table with TMalign report."
    )
    p.add_argument(
        "rcsb_table",
        type=str,
        help="Gzip table with all RCSB information."
    )
    p.add_argument(
        "clusters_path",
        type=str,
        help="Path where are stored all clusters."
    )
    p.add_argument(
        "synthetics_table",
        type=str,
        help="Table with all synthetic conformers present in CoDNaS-RNA."
    )
    p.add_argument(
        "superclustering_table",
        type=str,
        help="Table with all superclustering information."
    )
    p.add_argument(
        "cdrna_conformers",
        type=str,
        help="Table with all conformer informations."
    )
    p.add_argument(
        "cdrna_interactions",
        type=str,
        help="Gzip table with all conformer interactions."
    )
    p.add_argument(
        "cdrna_rnacentral",
        type=str,
        help="Gzip table with all rnacentral information."
    )
    p.add_argument(
        "outputDir", type=str,
        help="Path where to store cdrna_clusters table."
    )

    args = p.parse_args()
    fill_cdrna_clusters(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
"""Purpose: With this module you can create all downloadable clusters files
            (cluster_<ID>.tar.gz) of CoDNaS-RNA.

Preconditions:
                - Files for input
                    * cdrna_clusters.csv.gz
                    * cdrna_conformers.csv.gz
                    * cdrna_pairs.csv.gz
                    * cdrna_interactions.csv.gz
                    * cdrna_cluster_rnatype.csv.gz
                    * <stdout>.pdb.gz
                    * <superposition>.pdb.gz
                    * <conformer>.dbn
                    * <conformer>.kts
                    * <conformer>.rev.dbn
                    * <conformer>.svg
                    * <conformer>.fasta
                    * cluster_<ID>_clustermap.png
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [website_tables_dir]             // string
                * [tmalign_output_dir]             // string
                * [clustering_images_dir]          // string
                * [cif_dir]                        // string
                * [requirements]                   // string
                * [releaseDir]                     // string
                * [outputDir]                      // string

Observations:
            - 

TODO: - 

"""

import argparse
import os
import shutil
import pandas as pd
import subprocess
import glob
from get_website_df import get_df_if_file_exist
from create_release_file import get_dic_of_ext_databases
from create_release_file import get_dic_of_software
from get_filename import get_filename
from create_dir import create_dir
from build_ok_outputDir_string import build_ok_outputDir_string


def get_clusters_list(website_tables_dir):
    """
    Get a list of clusters located in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
    Returns: a list
    """
    if os.path.exists(website_tables_dir + 'cdrna_clusters.csv.gz'):
        cdrna_clusters = get_df_if_file_exist('cdrna_clusters', website_tables_dir + 'cdrna_clusters.csv.gz', ',', True)
    else:
        raise FileNotFoundError('[Error] File not exists: {}.'.format(website_tables_dir + 'cdrna_clusters.csv.gz'))
    cltrs_list = sorted([int(cltr_id.strip('Cluster_')) for cltr_id in cdrna_clusters['cluster_id'].to_list() if not pd.isna(cltr_id)], reverse=True)
    cltrs_list = ['Cluster_' + str(cltr_id) for cltr_id in cltrs_list]
    return cltrs_list


def create_readme_file(outputDir, requirements):
    """
    Create readme file to be used in each cluster_<ID>.tar.gz.
    Parameters:
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
                * requirements:              a string. Correspond to abs path of requirements.txt file.
    Return: nothing.
    """
    os.makedirs(outputDir, exist_ok=True)
    readme_file = outputDir + 'README.txt'
    ext_databases_dict = get_dic_of_ext_databases()
    software_dict = get_dic_of_software(requirements)
    if not os.path.exists(readme_file):
        with open(readme_file, 'w') as (outfile):
            outfile.write('# CoDNaS-RNA cluster data description\n')

            outfile.write('\n## CoDNaS-RNA\n')
            outfile.write('\n\tRelease: 2021-09\n')
            outfile.write('\tWebsite version on release: v1.3.0\n')
            outfile.write('\tURL: http://ufq.unq.edu.ar/codnasrna\n')
            outfile.write('\tAlternative URL: https://codnas-rna.bioinformatica.org\n')
            outfile.write('\tSee Website for more information, FAQs and Tutorial.\n')


            outfile.write('\n\n## Databases\n')
            outfile.write('\n### RCSB PDB\n')
            outfile.write('\n\tURL: ' + ext_databases_dict['RCSB_PDB']['url'] + '\n')
            outfile.write('\tRetrieved: ' + ext_databases_dict['RCSB_PDB']['date'] + '\n')
            outfile.write('\tInformation: ' + ext_databases_dict['RCSB_PDB']['message'] + '\n')
            outfile.write('\n### RNAcentral\n')
            outfile.write('\n\tURL: ' + ext_databases_dict['RNAcentral']['url'] + '\n')
            outfile.write('\tRetrieved: ' + ext_databases_dict['RNAcentral']['date'] + '\n')
            outfile.write('\tRelease: ' + ext_databases_dict['RNAcentral']['release'] + '\n')
            outfile.write('\tInformation: ' + ext_databases_dict['RNAcentral']['message'] + '\n')


            outfile.write('\n\n## Software\n')
            outfile.write('\n### Standalone\n\n')
            for aSoft in [*software_dict['standalone']]:
                outfile.write('\t' + aSoft + ': ' + software_dict['standalone'][aSoft] + '\n')

            outfile.write('\n### Python 3 libraries\n\n')
            for aSoft in [*software_dict['python_libraries']]:
                outfile.write('\t' + aSoft + ': ' + software_dict['python_libraries'][aSoft] + '\n')


            outfile.write('\n\n## Files\n')
            outfile.write('\n### Cluster_ID-general_info.tsv\n')
            outfile.write('\tTable with general information about a cluster.\n')
            outfile.write('\n\tKEY\tTYPE\tDESCRIPTION\n')
            outfile.write('\tcluster_id\tSTRING\tCluster identifier.\n')
            outfile.write('\tgold\tSTRING\t"YES" if the cluster is a gold cluster, i.e. it does not change under different clustering procedures. "NO" otherwise.\n')
            outfile.write('\tnum_conformers\tINT\tNumber of conformers present in the cluster.\n')
            outfile.write('\tconformers\tSET\tIdentifiers of conformers present in the cluster. Follows the notation <PDB>_<MODEL>_<CHAIN>.\n')
            outfile.write('\tseq_id-min_max\tTUPLE\tTwo floats representing the minimum and maximum sequence identity values between members of the cluster.\n')
            outfile.write('\tseq_length\tINT\tMaximum sequence length among members of the cluster.\n')
            outfile.write('\tmax_rmsd_pair\tTUPLE\tTwo strings representing a pair of conformers with maximum RMSD present in the cluster.\n')
            outfile.write('\turs_id-perc\tLIST\tPairs of URS ID and the percentage of members of the cluster with that URS ID.\n')
            outfile.write('\torganism\tSET\tOrganisms present in the cluster.\n')
            outfile.write('\trna_type\tSET\tRNA types present in the cluster.\n')
            outfile.write('\tresol-min_max\tTUPLE\tTwo floats representing the minimum and maximum resolution values present in the cluster.\n')
            outfile.write('\texp_methods-xrd_nmr_em-perc\tTUPLE\tThree floats representing the percentage of structures in the cluster solved by X-Ray diffraction, NMR and EM methods.\n')
            outfile.write('\trmsd-min_max_avg\tTUPLE\tThree floats representing the minimum, maximum and average of RMSD values between members of the cluster.\n')
            outfile.write('\ttmscore-min_max_avg\tTUPLE\tThree floats representing the minimum, maximum and average of TMscore values between members of the cluster.\n')
            outfile.write('\taligned_length-min_max_avg\tTUPLE\tThree ints representing the minimum, maximum and average (rounded) of aligned length values by TM-align between members of the cluster.\n')
            outfile.write('\n\tExample:\n')
            outfile.write('\tcluster_id\tCluster_32\n')
            outfile.write('\tgold\tNO\n')
            outfile.write('\tnum_conformers\t24\n')
            outfile.write('\tconformers\t{4v8a_1_AB, 4v8a_1_BB, 5j4d_1_C, 5uq7_1_B, 6b4v_1_C, 6boh_1_HB, 6fkr_1_2B}\n')
            outfile.write('\tseq_id-min_max\t(0.983, 1.00)\n')
            outfile.write('\tseq_length\t122\n')
            outfile.write('\tmax_rmsd_pair\t(5uq7_1_B, 6b4v_1_C)\n')
            outfile.write('\turs_id-perc\t[(URS000080DFF2, 91.67)],[(URS000080E019, 8.33)]\n')
            outfile.write('\torganism\t{Thermus thermophilus, Thermus parvatiensis}\n')
            outfile.write('\trna_type\t{rRNA}\n')
            outfile.write('\tresol-min_max\t(2.8, 3.5)\n')
            outfile.write('\texp_methods-xrd_nmr_em-perc\t(91.67, 0.00, 8.33)\n')
            outfile.write('\trmsd-min_max_avg\t(0.03, 1.09, 0.6361)\n')
            outfile.write('\ttmscore-min_max_avg\t(0.9356, 1.00, 0.9736)\n')
            outfile.write('\taligned_length-min_max_avg\t(118, 120, 120)\n')

            outfile.write('\n### Cluster_ID-interaction_data.csv\n')
            outfile.write('\tTable with interaction data (from DSSR) for each conformer present in the cluster.\n')
            outfile.write('\n\tKEY\tTYPE\tDESCRIPTION\n')
            outfile.write('\tcluster_id\tSTRING\tCluster identifier.\n')
            outfile.write('\tpdb_model_chain\tSTRING\tIdentifier of a conformer present in the cluster. Follows the notation <PDB>_<MODEL>_<CHAIN>.\n')
            outfile.write('\tres_id\tSTRING\tIdentifier of a conformer residue.\n')
            outfile.write('\tres_num\tINT\tResidue position number that belongs to the "res_id" value.\n')
            outfile.write('\tpartner_name\tSTRING\tName of the interacting partner of "res_id" value.\n')
            outfile.write('\tpartner_rcsb_type\tSTRING\tPartner biomolecule type {rna, dna, protein} derived from RCSB.\n')
            outfile.write('\tpartner_dssr_type\tSTRING\tInteraction type {nt:nt, nt:aa, etc} derived from DSSR.\n')
            outfile.write('\tpartner_chain_id\tSTRING\tIdentifier of a partner chain.\n')
            outfile.write('\tpartner_res_id\tSTRING\tIdentifier of a partner residue.\n')
            outfile.write('\tpartner_res_num\tINT\tPartner residue position number that belongs to the "partner_res_id" value.\n')
            outfile.write('\n\tExample line from file:\n')
            outfile.write('\tCluster_32,5v8i,1,1B,5v8i_1_1B,C,3,5S Ribosomal RNA,rna,nt:nt,1B,G,118\n')

            outfile.write('\n### Cluster_ID-sequence_data.fasta\n')
            outfile.write('\tMulti FASTA file with all conformers sequences present in the cluster. Header follows the notation <PDB>_<MODEL>_<CHAIN>\n')
            outfile.write('\n\tExample line from file:\n')
            outfile.write('\t>4v8a_1_AB\n')
            outfile.write('\tUCCCCCGUGCCCAUAGCGGCGUGGAACCACCCGUUCCCAUUCCGAACACGGAAGUGAAACGCGCCAGCGCCGAUGGUACUGGGCGGGCGACCGCCUGGGAGAGUAGGUCGGUGCGGGGGGUU\n')
            outfile.write('\n### Cluster_ID-superposition_data/\n')
            outfile.write("\tFolder containing TM-align's 'superposed' tertiary structure files for all pairs of conformers in the cluster. Files follow standard PDB format\n")
            outfile.write("\tFilename indicates (superposed <PDB>_<MODEL>_<CHAIN>)-(fixed <PDB>_<MODEL>_<CHAIN>) for an aligned pair. Find fixed structure file in 'Cluster_ID-tertiary_structure_data/' folder.\n")
            outfile.write('\n\tExample 6fkr_1_2B-4v8a_1_AB.pdb.gz:\n')
            outfile.write('\tCRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1\n')
            outfile.write('\tATOM      1  P     U ?   1      80.963 -62.684-127.322  1.00114.98           P\n')
            outfile.write('\t...\n')
            outfile.write('\tHETATM 2648  O   HOH ?3146      87.234 -68.607-155.286  1.00 44.11           O\n')
            outfile.write('\tEND\n')
            outfile.write('\n### Cluster_ID-tertiary_structure_data/\n')
            outfile.write("\tFolder containing Gemmi's extracted tertiary structure files for all conformers in the cluster. Files follow standard PDB format\n")
            outfile.write('\n\tExample 4v8a_1_AB.pdb.gz:\n')
            outfile.write('\tCRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1\n')
            outfile.write("\tATOM      1  O5'   U ?   1      80.962 -62.769-127.941  1.00 96.61           O\n")
            outfile.write('\t...\n')
            outfile.write('\tTER    2575        G ? 120\n')
            outfile.write('\tEND\n')

            outfile.write('\n### Cluster_ID-secondary_structure_data/\n')
            outfile.write('\tFolder containing secondary structure files for all conformers in the cluster.\n')
            outfile.write('\n\tExample 4v8a_1_AB/:\n')
            outfile.write('\t4v8a_1_AB.dbn\n')
            outfile.write('\tDot-bracket notation file (DBN) generated by DSSR.\n')
            outfile.write('\t4v8a_1_AB.rev.dbn\n')
            outfile.write('\tRevised dot-bracket notation file from DBN generated by DSSR.\n')
            outfile.write('\t4v8a_1_AB.svg\n')
            outfile.write('\tTwo-Dimensional (2D) secondary structure plot generated by RNArtist from revised DBN file.\n')
            outfile.write('\t4v8a_1_AB.kts\n')
            outfile.write('\tKotlin file generated for RNArtist.\n')

            outfile.write('\n### Cluster_ID-pairs_info.csv\n')
            outfile.write('\tTable with information associated with comparisons between all pairs of conformers in the cluster.\n')
            outfile.write('\n\tKEY\tTYPE\tDESCRIPTION\n')
            outfile.write('\tcluster_id\tSTRING\tCluster identifier.\n')
            outfile.write('\tpdb_model_chain_1\tSTRING\tIdentifier of conformer 1 of a pair present in the cluster. Follows the notation <PDB>_<MODEL>_<CHAIN>.\n')
            outfile.write('\tpdb_model_chain_2\tSTRING\tIdentifier of conformer 2 of a pair present in the cluster. Follows the notation <PDB>_<MODEL>_<CHAIN>.\n')
            outfile.write('\tis_max\tSTRING\t"YES" if the pair is the maximum RMSD selected from the cluster, i.e. it has the maximum RMSD value. "NO" otherwise.\n')
            outfile.write('\tclassification_1\tSTRING\tAssigned RNA type for conformer 1 from a controlled vocabulary maintained by RCSB.\n')
            outfile.write('\tclassification_2\tSTRING\tAssigned RNA type for conformer 2 from a controlled vocabulary maintained by RCSB.\n')
            outfile.write('\tdiff_classification\tSTRING\t"YES" if the assigned RNA type for both conformers are the same. "NO" otherwise.\n')
            outfile.write('\tresolution_1\tFLOAT\tResolution [Å] for conformer 1 of a pair present in the cluster. Only applies to X-Ray Diffraction (XRD) and cryo-Electron Microscopy (cryo-EM) experiments\n')
            outfile.write('\tresolution_2\tFLOAT\tResolution [Å] for conformer 2 of a pair present in the cluster. Only applies to X-Ray Diffraction (XRD) and cryo-Electron Microscopy (cryo-EM) experiments\n')
            outfile.write('\tdiff_resolution\tSTRING\t"YES" if both conformers have the same resolution. "NO" otherwise.\n')
            outfile.write('\tmethod_1\tSTRING\tExperimental Method for conformer 1. Whether XRD, cryo-EM or NMR were used for structure determination.\n')
            outfile.write('\tmethod_2\tSTRING\tExperimental Method for conformer 2. Whether XRD, cryo-EM or NMR were used for structure determination.\n')
            outfile.write('\tdiff_method\tSTRING\t"YES" if both conformers have the same Exp. Method. "NO" otherwise.\n')
            outfile.write('\tsource_1\tSTRING\torganism of conformer 1. Source of the original RNA used for structure determination.\n')
            outfile.write('\tsource_2\tSTRING\torganism of conformer 2. Source of the original RNA used for structure determination.\n')
            outfile.write('\tdiff_source\tSTRING\t"YES" if both conformers have the same organism. "NO" otherwise.\n')
            outfile.write('\ttemperature_1\tFLOAT\tTemperature [K] of experimental physicochemical conditions for conformer 1. As established at the time of the experiment.\n')
            outfile.write('\ttemperature_2\tFLOAT\tTemperature [K] of experimental physicochemical conditions for conformer 2. As established at the time of the experiment.\n')
            outfile.write('\tdiff_temp\tSTRING\t"YES" if both conformers have the same temperature. "NO" otherwise.\n')
            outfile.write('\tph_1\tFLOAT\tpH of experimental physicochemical conditions for conformer 1. As established at the time of the experiment.\n')
            outfile.write('\tph_2\tFLOAT\tpH of experimental physicochemical conditions for conformer 2. As established at the time of the experiment.\n')
            outfile.write('\tdiff_ph\tSTRING\t"YES" if both conformers have the same pH. "NO" otherwise.\n')
            outfile.write('\tseqres_length_1\tINT\tNumber of residues (SEQRES) in conformer 1 as used for structure determination.\n')
            outfile.write('\tseqres_length_2\tINT\tNumber of residues (SEQRES) in conformer 2 as used for structure determination.\n')
            outfile.write('\tdiff_seqres_length\tSTRING\t"YES" if both conformers have the same SEQRES length. "NO" otherwise.\n')
            outfile.write('\ttaxon_id_1\tFLOAT\tTaxonomic identifier of the organism for conformer 1.\n')
            outfile.write('\ttaxon_id_2\tFLOAT\tTaxonomic identifier of the organism for conformer 2.\n')
            outfile.write('\tdiff_taxon_id\tSTRING\t"YES" if both conformers have the same taxon ID. "NO" otherwise.\n')
            outfile.write('\tbiological_process_1\tSTRING\tBiological process (Gene Ontology term) for conformer 1.\n')
            outfile.write('\tbiological_process_2\tSTRING\tBiological process (Gene Ontology term) for conformer 2.\n')
            outfile.write('\tdiff_biological_process\tSTRING\t"YES" if both conformers have the same biological process. "NO" otherwise.\n')
            outfile.write('\tcelullar_component_1\tSTRING\tCellular component (Gene Ontology term) for conformer 1.\n')
            outfile.write('\tcelullar_component_2\tSTRING\tCellular component (Gene Ontology term) for conformer 2.\n')
            outfile.write('\tdiff_celullar_component\tSTRING\t"YES" if both conformers have the same cellular component. "NO" otherwise.\n')
            outfile.write('\tmolecular_function_1\tSTRING\tMolecular function (Gene Ontology term) for conformer 1.\n')
            outfile.write('\tmolecular_function_2\tSTRING\tMolecular function (Gene Ontology term) for conformer 2.\n')
            outfile.write('\tdiff_molecular_function\tSTRING\t"YES" if both conformers have the same molecular function. "NO" otherwise.\n')
            outfile.write('\thetatm_id_1\tSTRING\tIdentifiers of hetero atoms co-determined with the conformer 1.\n')
            outfile.write('\thetatm_id_2\tSTRING\tIdentifiers of hetero atoms co-determined with the conformer 2.\n')
            outfile.write('\tdiff_hetatm_id\tSTRING\t"YES" if both conformers have the same hetero atoms "NO" otherwise.\n')
            outfile.write('\tis_synthetic_1\tSTRING\t"YES" if conformer 1 is a synthetic entry. "NO" otherwise.\n')
            outfile.write('\tis_synthetic_2\tSTRING\t"YES" if conformer 2 is a synthetic entry. "NO" otherwise.\n')
            outfile.write('\tdiff_is_synthetic\tSTRING\t"YES" if both conformers are synthetics. "NO" otherwise.\n')
            outfile.write('\tnb_inter_nt_1\tINT\tNumber of nucleotide-nucleotide (nt-nt) inter-chain contacts. Established between residues in conformer 1 and other nucleic acids molecules (dna or rna) co-determined.\n')
            outfile.write('\tnb_inter_nt_2\tINT\tNumber of nt-nt inter-chain contacts. Established between residues in conformer 2 and other nucleic acids molecules (dna or rna) co-determined.\n')
            outfile.write('\tdiff_nb_inter_nt\tSTRING\t"YES" if both conformers have the same number of nt-nt inter-chain contacts. "NO" otherwise.\n')
            outfile.write('\tnb_inter_aa_1\tINT\tNumber of nucleotide-aminoacid (nt-aa) inter-chain contacts. Established between residues in conformer 1 and a protein or peptide co-determined.\n')
            outfile.write('\tnb_inter_aa_2\tINT\tNumber of nt-aa inter-chain contacts. Established between residues in conformer 2 and a protein or peptide co-determined.\n')
            outfile.write('\tdiff_nb_inter_aa\tSTRING\t"YES" if both conformers have the same number of nt-aa inter-chain contacts. "NO" otherwise.\n')
            outfile.write('\tnb_intra_1\tINT\tNumber of nucleotide-nucleotide (nt-nt) intra-chain contacts. Established between residues in conformer 1 only.\n')
            outfile.write('\tnb_intra_2\tINT\tNumber of nt-nt intra-chain contacts. Established between residues in conformer 2 only.\n')
            outfile.write('\tdiff_nb_intra\tSTRING\t"YES" if both conformers have the same number of nt-nt intra-chain contacts. "NO" otherwise.\n')
            outfile.write('\n\tExample:\n')
            outfile.write('\tCluster_32,6fkr_1_2B,4v8a_1_AB,NO,RIBOSOME,RIBOSOME/antibiotic,YES,3.2,3.2,NO,X-RAY DIFFRACTION,X-RAY DIFFRACTION,NO,Thermus thermophilus,Thermus thermophilus,NO,293.0,293.0,NO,7.6,7.6,NO,120,122,YES,274,274,NO,GO:0006412,NA,NA,"GO:0005840, GO:0015935",NA,NA,"GO:0003723, GO:0003735, GO:0019843",NA,NA,MG,NA,NA,NO,NO,NO,0,0,NO,73,67,YES,107,108,YES\n')

            outfile.write('\n### Cluster_ID-RMSD-clustering_info.png\n')
            outfile.write('\tImage of RMSD-based hierarchical clustering of conformers in the cluster. An image is provided for the cluster if it has three or more conformers and non-identical RMSD values.\n')


            outfile.write('\n\n## Citing this work\n')
            outfile.write('\n\tCoDNaS-RNA: a database of conformational diversity in the native state of RNA. Gonzalez Buitron M, Tunque Cahui RR, Garcia Rios E, Hirsh L, Fornasari MS, Parisi G, Palopoli N. bioRxiv https://doi.org/10.1101/2020.10.30.362590.\n')


            outfile.write('\n\n## Contact\n')
            outfile.write('\n\tFor general enquiries, please contact Nicolas Palopoli (npalopoli@unq.edu.ar).\n')
            outfile.write('\tFor questions on the database generation, please contact Martin Gonzalez Buitron (martingonzalezbuitron@gmail.com).\n')
            outfile.write('\tFor questions about the website please contact Ronaldo Tunque (ronaldo.tunque@pucp.edu.pe).\n')
    return

def create_general_info(website_tables_dir, cltrs_list, outputDir):
    """
    Create cluster_<ID>.tar.gz files. One per cluster located in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    if os.path.exists(website_tables_dir + 'cdrna_clusters.csv.gz'):
        if os.path.exists(website_tables_dir + 'cdrna_conformers.csv.gz'):
            if os.path.exists(website_tables_dir + 'cdrna_pairs.csv.gz'):
                if os.path.exists(website_tables_dir + 'annexed/' + 'cdrna_cluster_rnatype.csv.gz'):
                    cdrna_clusters = get_df_if_file_exist('cdrna_clusters', website_tables_dir + 'cdrna_clusters.csv.gz', ',', True)
                    cdrna_conformers = get_df_if_file_exist('cdrna_conformers', website_tables_dir + 'cdrna_conformers.csv.gz', ',', True)
                    cdrna_pairs = get_df_if_file_exist('cdrna_pairs', website_tables_dir + 'cdrna_pairs.csv.gz', ',', True)
                    cdrna_cluster_rnatype = get_df_if_file_exist('cdrna_cluster_rnatype', website_tables_dir + 'annexed/' + 'cdrna_cluster_rnatype.csv.gz', ',', True)
    else:
        raise FileNotFoundError('[Error] File not exists: {}.'.format(website_tables_dir + 'cdrna_clusters.csv.gz'))
    list_of_keys = [ 'cluster_id'
                   , 'gold'
                   , 'num_conformers'
                   , 'conformers'
                   , 'seq_id-min_max'
                   , 'seq_length'
                   , 'max_rmsd_pair'
                   , 'urs_id-perc'
                   , 'organism'
                   , 'rna_type'
                   , 'resol-min_max'
                   , 'exp_methods-xrd_nmr_em-perc'
                   , 'rmsd-min_max_avg'
                   , 'tmscore-min_max_avg'
                   , 'aligned_length-min_max_avg' ]
    dict_general_info = {akey:None for akey in list_of_keys}
    for cltr_id in cltrs_list:
        filename = outputDir + cltr_id + '-' + 'general_info' + '.tsv'
        cltr_subset = cdrna_clusters[(cdrna_clusters['cluster_id'] == cltr_id)]
        conf_subset = cdrna_conformers[(cdrna_conformers['cluster_id'] == cltr_id)]
        pair_subset = cdrna_pairs[(cdrna_pairs['cluster_id'] == cltr_id)]
        rnatype_subset = cdrna_cluster_rnatype[(cdrna_cluster_rnatype['cluster_id'] == cltr_id)]
        if cltr_subset.empty:
            print(cltr_id + ' does not exists in ' + website_tables_dir + 'cdrna_clusters.csv.gz')
        else:
            for akey in dict_general_info.keys():
                if akey == 'cluster_id':
                    dict_general_info[akey] = cltr_id
                elif akey == 'gold':
                    if '1' == cltr_subset['gold'].iloc[0]:
                        dict_general_info[akey] = 'YES'
                    else:
                        dict_general_info[akey] = 'NO'
                elif akey == 'num_conformers':
                    dict_general_info[akey] = cltr_subset['num_conf'].apply(eval).iloc[0]
                elif akey == 'conformers':
                    dict_general_info[akey] = set(conf_subset.loc[(conf_subset['cluster_id'] == cltr_id, 'pdb_model_chain')])
                elif akey == 'seq_id-min_max':
                    if pd.isnull(cltr_subset['seq_id_min'].iloc[0]):
                        dict_general_info[akey] = 'Not available.'
                    else:
                        if pd.isnull(cltr_subset['seq_id_max'].iloc[0]):
                            dict_general_info[akey] = 'Not available.'
                        else:
                            seq_id_min = cltr_subset['seq_id_min'].apply(eval).iloc[0]
                            seq_id_max = cltr_subset['seq_id_max'].apply(eval).iloc[0]
                            dict_general_info[akey] = (float(seq_id_min), float(seq_id_max))
                elif akey == 'seq_length':
                    dict_general_info[akey] = int(cltr_subset['seq_length_max'].apply(eval).iloc[0])
                elif akey == 'max_rmsd_pair':
                    if cltr_id not in cltr_subset['cluster_id']:
                        dict_general_info[akey] = 'Not available.'
                    else:
                        pdb_model_chain_1 = pair_subset.loc[(pair_subset['is_max'] == '2', 'pdb_model_chain_1')].to_list()[0]
                        pdb_model_chain_2 = pair_subset.loc[(pair_subset['is_max'] == '2', 'pdb_model_chain_2')].to_list()[0]
                        dict_general_info[akey] = (pdb_model_chain_1, pdb_model_chain_2)
                elif akey == 'urs_id-perc':
                    urs_main_list = cltr_subset['urs_main'].apply(eval).iloc[0]
                    urs_list = cltr_subset['urs_list'].apply(eval).iloc[0]
                    total_urs_list = list(set(urs_main_list) | set(urs_list))
                    if urs_main_list == ['Not available.']:
                        dict_general_info[akey] = [
                         'Not available.']
                    else:
                        total_urs_list = [(elem[0], float(elem[1])) for elem in total_urs_list if type(elem) == tuple]
                        dict_general_info[akey] = total_urs_list
                elif akey == 'organism':
                    if pd.isnull(cltr_subset['rnacentral_specie_main'].iloc[0]):
                        source_main_list = cltr_subset['source_main'].apply(eval).iloc[0]
                        source_list = cltr_subset['source_list'].apply(eval).iloc[0]
                        total_source_list = list(set(source_main_list) | set(source_list))
                        if source_main_list == ['Not available.']:
                            dict_general_info[akey] = {
                             'Not available.'}
                        else:
                            total_source_list = {elem[0] for elem in total_source_list if type(elem) == tuple if elem[0] != 'NA' if elem[0] != 'NA'}
                            dict_general_info[akey] = total_source_list
                    else:
                        rnacentral_specie_main_list = cltr_subset['rnacentral_specie_main'].apply(eval).iloc[0]
                        rnacentral_specie_list = cltr_subset['rnacentral_specie_list'].apply(eval).iloc[0]
                        total_rnacentral_specie_list = list(set(rnacentral_specie_main_list) | set(rnacentral_specie_list))
                        total_rnacentral_specie_set = {elem[0] for elem in total_rnacentral_specie_list if type(elem) == tuple if elem[0] if elem[0]}
                        dict_general_info[akey] = total_rnacentral_specie_set
                elif akey == 'rna_type':
                    dict_general_info[akey] = set(rnatype_subset.rna_type)
                elif akey == 'resol-min_max':
                    if pd.isnull(cltr_subset['resol_min'].iloc[0]):
                        dict_general_info[akey] = 'Not available.'
                    else:
                        resol_min = cltr_subset['resol_min'].apply(eval).iloc[0]
                        resol_max = cltr_subset['resol_max'].apply(eval).iloc[0]
                        dict_general_info[akey] = (float(resol_min), float(resol_max))
                elif akey == 'exp_methods-xrd_nmr_em-perc':
                    xrd_perc = cltr_subset['xrd_perc'].apply(eval).iloc[0]
                    nmr_perc = cltr_subset['nmr_perc'].apply(eval).iloc[0]
                    em_perc = cltr_subset['em_perc'].apply(eval).iloc[0]
                    dict_general_info[akey] = (float(xrd_perc), float(nmr_perc), float(em_perc))
                elif akey == 'rmsd-min_max_avg':
                    if pd.isnull(cltr_subset['rmsd_min'].iloc[0]):
                        dict_general_info[akey] = 'Not available.'
                    else:
                        rmsd_min = cltr_subset['rmsd_min'].apply(eval).iloc[0]
                        rmsd_max = cltr_subset['rmsd_max'].apply(eval).iloc[0]
                        rmsd_avg = cltr_subset['rmsd_avg'].apply(eval).iloc[0]
                        dict_general_info[akey] = (float(rmsd_min), float(rmsd_max), float(rmsd_avg))
                elif akey == 'tmscore-min_max_avg':
                    if pd.isnull(cltr_subset['tm_score_min'].iloc[0]):
                        dict_general_info[akey] = 'Not available.'
                    else:
                        tm_score_min = cltr_subset['tm_score_min'].apply(eval).iloc[0]
                        tm_score_max = cltr_subset['tm_score_max'].apply(eval).iloc[0]
                        tm_score_avg = cltr_subset['tm_score_avg'].apply(eval).iloc[0]
                        dict_general_info[akey] = (float(tm_score_min), float(tm_score_max), float(tm_score_avg))
                else:
                    if akey == 'aligned_length-min_max_avg':
                        if pd.isnull(cltr_subset['aligned_length_min'].iloc[0]):
                            dict_general_info[akey] = 'Not available.'
                        else:
                            aligned_length_min = cltr_subset['aligned_length_min'].apply(eval).iloc[0]
                            aligned_length_max = cltr_subset['aligned_length_max'].apply(eval).iloc[0]
                            aligned_length_avg = cltr_subset['aligned_length_avg'].apply(eval).iloc[0]
                            dict_general_info[akey] = (int(aligned_length_min), int(aligned_length_max), int(aligned_length_avg))
                    else:
                        print('[Error]: ' + akey + ' not found it for ' + cltr_id + ' general_info.tsv')

            with open(filename, 'a') as (afile):
                for akey in list_of_keys:
                    afile.write(akey + '\t' + str(dict_general_info[akey]) + '\n')
    return


def create_interaction_data(website_tables_dir, cltrs_list, outputDir):
    """
    Create cluster_<ID>-interaction_data files. One per conformer of a cluster located
    in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    if os.path.exists(website_tables_dir + 'cdrna_interactions.csv.gz'):
        cdrna_interactions = get_df_if_file_exist('cdrna_interactions', website_tables_dir + 'cdrna_interactions.csv.gz', ',', True)
    else:
        raise FileNotFoundError('[Error] File not exists: {}.'.format(website_tables_dir + 'cdrna_interactions.csv.gz'))
    cdrna_interactions = cdrna_interactions.dropna(subset=[
     'pdb', 'model', 'chain'])
    for cltr_id in cltrs_list:
        filename = outputDir + cltr_id + '-' + 'interaction_data' + '.csv'
        interactions_subset = cdrna_interactions[(cdrna_interactions['cluster_id'] == cltr_id)]
        interactions_subset.to_csv( filename
                                  , sep=','
                                  , na_rep='NA'
                                  , index=False
                                  , header=True )
    return


def create_sequence_data(website_tables_dir, cltrs_list, outputDir):
    """
    Create cluster_<ID>.tar.gz files. One per cluster located in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    if os.path.exists(website_tables_dir + 'cdrna_conformers.csv.gz'):
        cdrna_conformers = get_df_if_file_exist('cdrna_conformers', website_tables_dir + 'cdrna_conformers.csv.gz', ',', True)
    for cltr_id in cltrs_list:
        filename = outputDir + cltr_id + '-' + 'sequence_data' + '.fasta'
        conf_subset = cdrna_conformers[(cdrna_conformers['cluster_id'] == cltr_id)]
        cltr_df = pd.DataFrame()
        cltr_df['fasta_lines'] = '>' + conf_subset.pdb_model_chain.astype(str) + '\n' + conf_subset.seqres + '\n'
        with open(filename, 'w') as (output):
            for aline in cltr_df['fasta_lines']:
                output.write(aline)
    return


def create_superposition_data(cltrs_list, tmalign_output_dir, outputDir):
    """
    Create cluster_<ID>.tar.gz files. One per cluster located in CoDNaS-RNA.
    Parameters:
                * tmalign_output_dir:        a string. Correspond to a path where are stored all
                                                       TMalign results.
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    for cltr_id in cltrs_list:
        sup_cltr_dest = outputDir + cltr_id + '-' + 'superposition_data'
        sup_cltr_target = tmalign_output_dir + 'Sup_files' + '/' + cltr_id
        shutil.copytree(sup_cltr_target, sup_cltr_dest)
    return


def create_tertiary_structure_data(cltrs_list, website_tables_dir, cif_dir, outputDir):
    """
    Create cluster_<ID>.tar.gz files. One per cluster located in CoDNaS-RNA.
    Parameters:
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * cif_dir:                   a string. Correspond to a path where are stored all
                                                       PDB_MODEL_CHAIN files extracted by gemmi.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    if os.path.exists(website_tables_dir + 'cdrna_conformers.csv.gz'):
        cdrna_conformers = get_df_if_file_exist('cdrna_conformers', website_tables_dir + 'cdrna_conformers.csv.gz', ',', True)
    else:
        raise FileNotFoundError('[Error] File not exists: {}.'.format(website_tables_dir + 'cdrna_conformers.csv.gz'))
    for cltr_id in cltrs_list:
        tertiary_structure_folder = outputDir + cltr_id + '-' + 'tertiary_structure_data'
        os.makedirs(tertiary_structure_folder, exist_ok=True)
        cdrna_conformers_subset = cdrna_conformers[(cdrna_conformers['cluster_id'] == cltr_id)]
        conformers_list = cdrna_conformers_subset['pdb_model_chain'].tolist()
        files_to_copy = [cif_dir + aconf + '.pdb.gz' for aconf in conformers_list]
        if not files_to_copy:
            print('[Error]: empty list file error. ' + cltr_id + ' has not a pdb.gz associated files.')
        else:
            for afile in files_to_copy:
                if os.path.exists(afile):
                    filename = get_filename(afile)
                    shutil.copy2(afile, tertiary_structure_folder + '/' + filename)
                else:
                    print('[Error]: copy error. ' + afile + ' does not exists.')
    return


def create_secondary_structure_data(cltrs_list, website_tables_dir, releaseDir, outputDir):
    """
    Create cluster_<ID>.tar.gz files. One per cluster located in CoDNaS-RNA.
    Parameters:
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * releaseDir:                a string. Correspond to an CoDNaS-RNA release directory.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    if os.path.exists(website_tables_dir + 'cdrna_conformers.csv.gz'):
        cdrna_conformers = get_df_if_file_exist('cdrna_conformers', website_tables_dir + 'cdrna_conformers.csv.gz', ',', True)
    else:
        raise FileNotFoundError('[Error] File not exists: {}.'.format(website_tables_dir + 'cdrna_conformers.csv.gz'))
    for cltr_id in cltrs_list:
        secondary_structure_folder = outputDir + cltr_id + '-' + 'secondary_structure_data'
        os.makedirs(secondary_structure_folder, exist_ok=True)
        cdrna_conformers_subset = cdrna_conformers[(cdrna_conformers['cluster_id'] == cltr_id)]
        conformers_list = cdrna_conformers_subset['pdb_model_chain'].tolist()
        for aconf in conformers_list:
            secondary_structure_conf_folder = secondary_structure_folder + '/' + aconf
            os.makedirs(secondary_structure_conf_folder, exist_ok=True)
            svg_folder = releaseDir + 'img/ss_2D/'
            abs_filename_svg = svg_folder + aconf + '.svg'
            main_folder = releaseDir + 'files/structures/' + aconf + '/'
            os.makedirs(main_folder, exist_ok=True)
            if os.path.exists(abs_filename_svg):
                filename = get_filename(abs_filename_svg)
                shutil.copy2(abs_filename_svg, secondary_structure_conf_folder + '/' + filename)
            else:
                print('[Error]: copy error. ' + abs_filename_svg + ' does not exists for ' + cltr_id)
            files_to_archive_main = glob.glob(main_folder + '*.*')
            files_to_archive_main = [afile for afile in files_to_archive_main if not afile.endswith('pdb.gz') if not afile.endswith('tar.gz')]
            if not files_to_archive_main:
                print('[Error]: empty list file error. ' + aconf + ' has not associated files for ' + cltr_id)
            else:
                cp_cmd = [
                 'cp'] + ['-p'] + files_to_archive_main + [secondary_structure_conf_folder + '/']
                subprocess.run(cp_cmd)
    return


def create_pairs_info(website_tables_dir, cltrs_list, outputDir):
    """
    Create cluster_<ID>.tar.gz files. One per cluster located in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    if os.path.exists(website_tables_dir + 'cdrna_differences.csv.gz'):
        cdrna_differences = get_df_if_file_exist('cdrna_differences', website_tables_dir + 'cdrna_differences.csv.gz', ',', True)
    else:
        raise FileNotFoundError('[Error] File not exists: {}.'.format(website_tables_dir + 'cdrna_differences.csv.gz'))
    for cltr_id in cltrs_list:
        filename = outputDir + cltr_id + '-' + 'pairs_info' + '.csv'
        cdrna_differences_subset = cdrna_differences[(cdrna_differences['cluster_id'] == cltr_id)]
        cdrna_differences_subset.to_csv( filename
                                       , sep=','
                                       , na_rep='NA'
                                       , index=False
                                       , header=True )
    return


def create_clustering_info(cltrs_list, clustering_images_dir, outputDir):
    """
    Create cluster_<ID>.tar.gz files. One per cluster located in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    for cltr_id in cltrs_list:
        clustering_file_dest = outputDir + cltr_id + '-' + 'RMSD' + '-' + 'clustering_info' + '.png'
        clustering_file_target = clustering_images_dir + cltr_id + '_clustermap.png' # Before was '_hclust.png'
        if os.path.exists(clustering_file_target):
            shutil.copy2(clustering_file_target, clustering_file_dest)
        else:
            print('[Error]: copy error. ' + clustering_file_target + ' does not exists.')
    return


def generate_tar_gzip_files(cltrs_list, outputDir):
    """
    Generate cluster_<ID>.tar.gz files using all corresponding files for each cluster
    located in CoDNaS-RNA.
    Parameters:
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    for cltr_id in cltrs_list:
        main_folder = outputDir + cltr_id + '/'
        os.makedirs(main_folder, exist_ok=True)
        readme_file = outputDir + 'README.txt'
        if os.path.exists(readme_file):
            shutil.copy2(readme_file, main_folder + 'README.txt')
        else:
            print('[Error]: copy error. ' + readme_file + ' does not exists.')
        files_to_archive = glob.glob(outputDir + cltr_id + '-' + '*')
        output_filename = outputDir + cltr_id + '.tar.gz'
        if os.path.exists(output_filename):
            os.remove(output_filename)
        mv_cmd = ['mv'] + files_to_archive + [main_folder]
        #subprocess.run(mv_cmd)
        tar_cmd = [
        'tar', '-czf', output_filename, '-C', outputDir, cltr_id, '--remove-files']
        #subprocess.run(tar_cmd)
    # Clean readme file from download/ 
    os.remove(readme_file)
    return


def create_files( website_tables_dir
                , tmalign_output_dir
                , clustering_images_dir
                , cif_dir
                , requirements
                , releaseDir
                , outputDir ):
    """
    Create cluster_<ID>.tar.gz files. One per cluster located in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * tmalign_output_dir:        a string. Correspond to a path where are stored all
                                                       TMalign results.
                * clustering_images_dir:     a string. Correspond to a path where are stored all
                                                       clustering images files.
                * cif_dir:                   a string. Correspond to a path where are stored all
                                                       PDB_MODEL_CHAIN files extracted by gemmi.
                * requirements:              a string. Correspond to abs path of requirements.txt file.
                * releaseDir:                a string. Correspond to an CoDNaS-RNA release directory.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all cluster_<ID>.tar.gz files.
    Return: nothing
    """
    outputDir = build_ok_outputDir_string(outputDir)
    website_tables_dir = build_ok_outputDir_string(website_tables_dir)
    tmalign_output_dir = build_ok_outputDir_string(tmalign_output_dir)
    clustering_images_dir = build_ok_outputDir_string(clustering_images_dir)
    create_dir(outputDir)

    cltrs_list = get_clusters_list(website_tables_dir)

    create_readme_file(outputDir, requirements)
    create_general_info(website_tables_dir, cltrs_list, outputDir)
    create_interaction_data(website_tables_dir, cltrs_list, outputDir)
    create_sequence_data(website_tables_dir, cltrs_list, outputDir)
    create_superposition_data(cltrs_list, tmalign_output_dir, outputDir)
    create_tertiary_structure_data(cltrs_list, website_tables_dir, cif_dir, outputDir)
    create_secondary_structure_data(cltrs_list, website_tables_dir, releaseDir, outputDir)
    create_pairs_info(website_tables_dir, cltrs_list, outputDir)
    create_clustering_info(cltrs_list, clustering_images_dir, outputDir)

    generate_tar_gzip_files(cltrs_list, outputDir)

    print('All done !')

    return


# website_tables_dir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'
# tmalign_output_dir = '/home/martingb/Projects/2020/CoDNaS-RNA/outputs/2020-05-15/TMalign/'
# clustering_images_dir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/img/clustermap/' # Before was hclust/
# cif_dir = '/home/martingb/Projects/2020/CoDNaS-RNA/data/2020-05-15/pdbs/data_codnas_rna_model_chains/'
# requirements = '/home/martingb/Projects/2020/CoDNaS-RNA/src/requirements.txt'
# releaseDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/files/download/'


if __name__ == '__main__':
    p = argparse.ArgumentParser(description='Create cluster_<ID>.tar.gz files to be used for downloads in CoDNaS-RNA website.'
    )
    p.add_argument(
      'website_tables_dir',
      type=str,
      help='Path where all website tables are located.'
    )
    p.add_argument(
      'tmalign_output_dir',
      type=str,
      help='Path where all superposition and stdout files are located.'
    )
    p.add_argument(
      'clustering_images_dir',
      type=str,
      help='Path where all RMSD clustering images are located.'
    )
    p.add_argument(
      'cif_dir',
      type=str,
      help='Output directory to search *.cif file.'
    )
    p.add_argument(
      'requirements',
      type=str,
      help='Path to requirements.txt file.'
    )
    p.add_argument(
      'releaseDir',
      type=str,
      help="Path where is stored a CoDNaS-RNA's release."
    )
    p.add_argument(
      'outputDir',
      type=str,
      help='Path where to store all cluster_<ID>.tar.gz files.'
    )

    args = p.parse_args()
    create_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
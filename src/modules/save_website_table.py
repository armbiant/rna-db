"""\
Purpose: With this function you can save a dataframe as a compressed table.

Preconditions:
                - Files for input
                    * A dataframe
                - Libraries
                    * Pandas
                - Modules
                    * guess_extension

Arguments:    -Positional
                * [website_df]         // pandas dataframe
                * [outputDir]          // string
                * [a_separator]        // string

Observations:
            - 

TODO: - 

"""


import argparse
import pandas as pd
from guess_extension import guess_extension_from_separator


def save_website_table(website_df, outputDir, table_name, a_separator, na_rep="NA"):
    """
    Save website dataframe as a table.
    Parameters:
                * website_df: a dataframe. Correspond to a dataframe name.
                * outputDir: a string. Correspond to an output directory.
                * a_separator: a string. Correspond to a website table separator.
                * na_rep: a string. Correspond to a NaN value. Default: "NA".
    Return: nothing
    """
    a_separator, ext = guess_extension_from_separator(a_separator)

    # Save dataframa as a file.
    website_df.to_csv(
        outputDir + table_name + ext + ".gz",
        sep=a_separator,
        na_rep="NA",
        index=False,
        header=True,
        compression="gzip",
    )
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Save website dataframe as a table in gzip compression format."
    )
    # Positional arguments
    p.add_argument(
        "website_df",
        help="A website dataframe to be saved."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store website table."
    )
    p.add_argument(
        "table_name",
        type=str,
        help="String to be used as a table name."
    )
    p.add_argument(
        "a_separator",
        type=str,
        help="A separator for website table."
    )

    args = p.parse_args()
    save_website_table(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
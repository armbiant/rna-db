"""\
Purpose: With this module you can create a full CoDNaS-RNA
         cdrna_pubmed.csv.gz website table.

Preconditions:
                - Files for input
                    * pdb_chains
                    * rcsb_table
                - Libraries
                    * Pandas
                    * Numpy

Arguments:    -Positional
                * [pdb_chains]                   // string
                * [rcsb_table]                   // string
                * [outputDir]                    // string

Observations:
            - 

TODO: - Implement optional parameters for partially fill cdrna_pubmed.csv.gz


"""

import argparse
import os
import pandas as pd
from save_website_table import save_website_table
from get_website_df import get_df_if_file_exist
from add_cell_value import add_value_in_column_to_subset
from create_dir import create_dir


def create_dataframe_struct():
    """
    Create a dataframe structure.
    Parameters: nothing
    TODO: add list parameter to be used as a column builder.
    Return: an empty dataframe
    """
    # Create the Dataframe to be used in cdrna_pudmed.
    columns = [
        "pdb",                      # Table RCSB
        "pub_med_id",               # Table RCSB
        "abstract_text",            # Table RCSB
        "doi",                      # Table RCSB
        "journal_name",             # Table RCSB
        "first_page",               # Table RCSB
        "last_page",                # Table RCSB
        "primary_citation_author",  # Table RCSB
        "pub_med_central_ID",       # Table RCSB
        "publication_year",         # Table RCSB
        "title",                    # Table RCSB
        "volume"                    # Table RCSB
    ]
    cdrna_pudmed = pd.DataFrame(columns=columns)

    return cdrna_pudmed


def extract_info_pdb_chains(cdrna_pudmed, pdb_chains, outputDir):
    """
    Extract PDB code  cluster related information using pdbchains_databases_codes.tsv as table provider.
    Parameters:
                * cdrna_pudmed:              a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA pudmed website table.
                * pdb_chains:                a string. Correspond to a file that has a list of
                                                       PDB:Chains for those that have passed
                                                       all quality filters.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_pudmed.csv.gz"):
        return cdrna_pudmed

    # Ask if file exists
    if os.path.exists(pdb_chains):
        with open(pdb_chains, "r") as afile:
            pdb_codes_list = list({achain.split(":")[0] for achain in afile})
    else:
        raise FileNotFoundError('[Error] File not exists: {}.'.format(pdb_chains))

    for pdb in pdb_codes_list:
        data = [
            {
                "pdb": pdb,
                "pub_med_id": "NA",
                "abstract_text": "NA",
                "doi": "NA",
                "journal_name": "NA",
                "first_page": "NA",
                "last_page": "NA",
                "primary_citation_author": "NA",
                "pub_med_central_ID": "NA",
                "publication_year": "NA",
                "title": "NA",
                "volume": "NA"
            }
        ]
        cdrna_pudmed = cdrna_pudmed.append(data, ignore_index=True, sort=False)
    return cdrna_pudmed


def get_rcsb_dict(pdb_codes_list, rcsb_table, acolumn_list):
    """
    Get a dictionary with RCSB values using acolumn_list as keys for a pdb_code_list.
    Parameters:
                * pdb_codes_list:            a list. Correspond to a PDB list.
                * rcsb_table:                a dataframe. Correspond to a pandas dataframe for
                                                          rcsb table.
                * acolumn_list:              a list. Correspond to a list of columns
                                                     from rcsb_table dataframe.
    Return: a dict
    """
    # Create dict with all cluster pdb chains rows (using cltr_file)
    rcsb_dic = dict()

    # Select only columns that are from interest. Like, df1 = df[['a','b']]
    acolumn_list_plus_PDB = acolumn_list + ["PDB ID"]
    rcsb_table_copy = rcsb_table[acolumn_list_plus_PDB].copy()
    rcsb_table_subset = rcsb_table_copy.loc[
        rcsb_table_copy["PDB ID"].isin(pdb_codes_list)
    ]
    rcsb_table_subset = rcsb_table_subset.drop_duplicates(
        subset="PDB ID", ignore_index=True
    )

    for apdb in pdb_codes_list:
        # Check that exist rows from this pdb code. Could not have information.
        if rcsb_table_subset[rcsb_table_subset["PDB ID"] == apdb].empty:
            rcsb_dic[apdb] = {"NA": "NA"}
            continue
        rcsb_dic[apdb] = dict()
        for acolumn in acolumn_list:
            avalue = rcsb_table_subset.loc[rcsb_table_subset["PDB ID"] == apdb][
                acolumn
            ].values[0]
            if not avalue:
                avalue = "NA"
            rcsb_dic[apdb][acolumn] = avalue
            continue
    return rcsb_dic


def extract_info_rcsb_table(cdrna_pudmed, rcsb_table, a_separator, outputDir):
    """
    Extract publication features per CoDNaS-RNA PDB using rcsb_table.csv.gz as table provider.
    Parameters:
                * cdrna_pudmed:              a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA pubmed website table.
                * rcsb_table:                a string. Correspond to a CSV table with all RCSB
                                                       information.
                * a_separator:               a string. Correspond to the rcsb_table.csv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_pudmed.csv.gz"):
        return cdrna_pudmed

    # Get a dataframe from table
    rcsb_table = get_df_if_file_exist("rcsb_table", rcsb_table, a_separator, False)
    pdb_codes_list = cdrna_pudmed["pdb"].tolist()

    columns_to_fill = [
        "pub_med_id",
        "abstract_text",
        "doi",
        "journal_name",
        "first_page",
        "last_page",
        "primary_citation_author",
        "pub_med_central_ID",
        "publication_year",
        "title",
        "volume"
    ]

    columns_to_search = [
        "PubMed ID",
        "PubMed Abstract (Text Short)",
        "DOI",
        "Journal Name",
        "First Page",
        "Last Page",
        "Primary Citation Author",
        "PubMed Central ID",
        "Pub. Year",
        "Title",
        "Volume"
    ]

    column_match = dict(zip(columns_to_search, columns_to_fill))

    rcsb_dic = get_rcsb_dict(pdb_codes_list, rcsb_table, columns_to_search)

    # Fill values to cdrna_pudmed
    for pdb_code, acolumn_dic in rcsb_dic.items():
        for acolumn, avalue in acolumn_dic.items():
            if acolumn == "NA":
                for pudmed_col in column_match.values():
                    add_value_in_column_to_subset(
                        cdrna_pudmed, pdb_code, "pdb", avalue, pudmed_col
                    )
                continue
            add_value_in_column_to_subset(
                cdrna_pudmed, pdb_code, "pdb", avalue, column_match[acolumn]
            )

    return cdrna_pudmed


def fill_cdrna_pubmed(pdb_chains, rcsb_table, outputDir):
    """
    Create and fill the website cdrna_pubmed table.
    Parameters:
                * pdb_chains:                a string. Correspond to a file that has a list of
                                                       PDB:Chains for those that have passed
                                                       all quality filters.
                * rcsb_table:                a string. Correspond to a CSV table with all RCSB
                                                       information.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """
    # Before start to do anything, check if outputDir is correct and create it
    create_dir(outputDir)

    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_pudmed.csv.gz"):
        cdrna_pudmed = get_df_if_file_exist(
            "cdrna_pudmed", outputDir + "cdrna_pudmed.csv.gz", ",", True
        )
    else:
        cdrna_pudmed = create_dataframe_struct()

    cdrna_pudmed = extract_info_pdb_chains(cdrna_pudmed, pdb_chains, outputDir)

    cdrna_pudmed = extract_info_rcsb_table(cdrna_pudmed, rcsb_table, ",", outputDir)

    save_website_table(cdrna_pudmed, outputDir, "cdrna_pudmed", ",")

    print("All done !")

    return


# pdb_chains = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/lists/PDB_chains.list'
# rcsb_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/filtered_rna_conformers_from_PDB.csv.gz'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create cdrna_pubmed.csv.gz table to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "pdb_chains",
        type=str,
        help="File with PDB chains in list format."
    )
    p.add_argument(
        "rcsb_table",
        type=str,
        help="Gzip table with all RCSB information."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store cdrna_pudmed table."
    )

    args = p.parse_args()
    fill_cdrna_pubmed(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
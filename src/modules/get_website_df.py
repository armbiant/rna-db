"""\
Purpose: With this function you can read a table as a dataframe. If file
         table does not exist, an error message is printed on screen.

Preconditions:
                - Files for input
                    * afile (typically a website compressed file)
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [df_name]            // string
                * [afile]              // string
                * [a_separator]        // string
                * [nan_status]         // bool

Observations:
            - Website table could be compressed or not.


TODO: - Ask if file exist, otherwise exit. (TODO handle propertly errors)
      - Need to guess separator or guess extension file (.CSV, .TSC, .PSV, etc)
      - Implement Exceptions and Errors (try: ... | except Exception as e:)

"""


import argparse
import os.path
import pandas as pd


def get_df_if_file_exist(df_name, afile, a_separator, nan_status):
    """
    Get a dataframe from a table file.
    Parameters:
                * df_name: a string. Correspond to a dataframe name.
                * afile: a file. Correspond to a website table.
                * a_separator: a string. Correspond to a website table separator.
                * nan_status: a bool. Correspond to keep or not default nan values.
    Return: a pandas dataframe
    """
    # Taken from: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_table.html
    pandas_nan_list = [
                        '',
                        '#N/A',
                        '#N/A N/A',
                        '#NA',
                        '-1.#IND',
                        '-1.#QNAN',
                        '-NaN',
                        '-nan',
                        '1.#IND',
                        '1.#QNAN',
                        '<NA>',
                        'N/A',
                        'NA',
                        'NULL',
                        'NaN',
                        'n/a',
                        'nan',
                        'null'
                       ]

    if nan_status:
        pandas_nan_list
    else:
        pandas_nan_list.remove('NA')

    # Check first if file exists.
    if os.path.exists(afile):
        if afile.endswith(".gz"):
            # Read table (Cluster_ID\t<COLUMNS>)
            df_name = pd.read_table(
                afile,
                sep=a_separator,
                dtype=str,
                compression="gzip",
                na_values=pandas_nan_list,
                keep_default_na=nan_status,
                header=0,
            )
        else:
            # Read tsv (Cluster_ID\t<COLUMNS>)
            df_name = pd.read_table(
                afile,
                sep=a_separator,
                dtype=str,
                na_values=pandas_nan_list,
                keep_default_na=nan_status,
                header=0
            )
    else:
        print("Error: file does not exist" + "\t" + afile)
    return df_name


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Gives a dataframe from a website table.")
    # Positional arguments
    p.add_argument(
        "df_name",
        type=str,
        help="Name to be used in dataframe generation."
    )
    p.add_argument(
        "afile",
        type=str,
        help="Path to a website table."
    )
    p.add_argument(
        "a_separator",
        type=str,
        help="A separator for website table."
    )
    p.add_argument(
        "nan_status",
        type=bool,
        help="A boolean state to keep default nan values in dataframe."
    )

    args = p.parse_args()
    get_df_if_file_exist(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
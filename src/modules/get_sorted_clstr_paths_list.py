"""\
Purpose: With this function you can get a sorted list of CoDNaS-RNA's cluster paths.

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * nothing

Arguments:    -Positional
                * [clusters_dir]    // string
                * [extention]       // string

Observations:
            - 

TODO: - 

"""

import argparse
import glob
import os.path


def get_sorted_clstr_paths_list(clusters_dir, extention):
    """
    Get a list of *.csv cluster paths sorted from Cluster_0 to latest
    one.
    Parameters:
                    * clusters_dir: a string. Correspond to path directory
                                    where all *.CSV clusters files are stored.
    Return: a list of *.csv cluster paths.
    """
    # Get a list of each cluster path sorted
    # (clusters converted to ints for proper sorting)
    clusters_csv_list = sorted(
        [
            int(os.path.basename(f).strip(extention).strip("Cluster_"))
            for f in glob.glob(clusters_dir + "*" + extention)
        ], reverse=True
    )
    # Re-make names to get all correct paths
    clusters_csv_list = [
        clusters_dir + "Cluster_" + str(f) + extention for f in clusters_csv_list
    ]
    return clusters_csv_list


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Get ordered CoDNaS-RNA's clusters paths list.")
    # Positional arguments
    p.add_argument(
        "clusters_dir",
         type=str,
         help="Path to search *.csv clusters files."
    )
    p.add_argument(
        "extention",
         type=str,
         help="Extention of clusters files (.csv, .tsv, .psv)"
    )

    args = p.parse_args()
    get_sorted_clstr_paths_list(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
"""\
Purpose: With this module you can run DSSR, create json files, parse their results
         and generate TSV or CSV tables.

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * nothing
                - Modules
                    * build_ok_outputDir_string
                - Software
                    * 3D analizer: DSSR
                    * Json parser: jq

Arguments:    -Positional
                * [cif_dir]               // string
                * [json_outputDir]        // string
                * [table_outputDir]       // string
               -Optional
                * "-ifmt",       "--inputformat"       // str,   default=cif (cif or pdb)
                * "-ext",        "--table_extension"   // str,   default=tsv (tsv or csv)
                * "-prot",       "--only_protein"      // bool,  default=False
                * "-hbonds",     "--get_hbonds"        // bool,  default=False
                * "-pairs",      "--get_pairs"         // bool,  default=False
                * "-angle",      "--get_angles"        // bool,  default=False
                * "-multiplets", "--get_multiplets"    // bool,  default=False
                * "-dbn", "--get_dbn"    // bool,  default=False
                * "-v",          "--verbose"           // bool,  default=False
Observations:
            - DSSR and jq can be installed using sbg-cetools

TODO: - Implement auxiliary files generation and store.

"""


import argparse
import os
import subprocess
import glob
from build_ok_outputDir_string import build_ok_outputDir_string


def get_filename(afile):
    """
    Get a file name from an absolute file path.
    Parameters:
                * afile:                 a string. Correspond to a full
                                                   path of a file.
    Return: a string
    """
    file_name = os.path.basename(afile)
    file_name = os.path.splitext(file_name)[0]
    return file_name


def create_stdout_if_exist(stdout_name, subprocess_lines, file_name, table_outputDir, verbose):
    """
    Create a standard output file.
    Parameters:
                * stdout_name:               a string. Correspond to a standard output name.
                * subprocess_lines:          a list. Correspond to list of DSSR standard output lines.
                * file_name:                 a string. Correspond to a file name.
                * table_outputDir:           a string. Correspond to an output directory to store
                                                       all tables.
                * verbose:                   a string. Correspond to bool condition so standard output
                                                       messages are printed on screen.
    Return: nothing
    """
    #Report stdout lines
    if subprocess_lines[0] == '':
        if verbose:
            stdout_name = os.path.splitext(stdout_name)[0]
            stdout_name = stdout_name.split("_", 1)[0]
            print(file_name+'\t'+'has not std output for\t'+stdout_name)
    else:
        outputDir = table_outputDir+'stdout_reports/'
        # Make dir or do nothing if exists
        os.makedirs(outputDir, exist_ok=True)
        with open(outputDir+stdout_name, 'a') as f:
            f.write("%s\n" % ('>'+file_name))
        for line in subprocess_lines:
            with open(outputDir+stdout_name, 'a') as f:
                f.write("%s\n" % (line))
    return


def execute_and_save_dssr(file_list, json_outputDir, only_protein, table_outputDir, verbose):
    """
    Execute DSSR.
    Parameters:
                * file_list:                 a list.   Correspond to a list of cif files to work with.
                * json_outputDir:            a string. Correspond to an output directory to store
                                                       all jsons files.
                * only_protein:              a string. Correspond to a bool condition so only protein
                                                       calculations are taken on account.
                * table_outputDir:           a string. Correspond to an output directory to store
                                                       all tables.
                * verbose:                   a string. Correspond to bool condition so standard output
                                                       messages are printed on screen.
    Return: nothing
    """
    # Create json directory
    outputDir_json = json_outputDir
    # Create auxfiles directory (need to be done in this level)
    outputDir_aux = json_outputDir + 'auxfiles' + '/'
    # Create aux file directory
#    outputDir_aux = outputDir + 'auxfiles' + '/' # # <----- TODO for future changes
    # Make dir or do nothing if exists
    os.makedirs(outputDir_json, exist_ok=True)
    # Make dir or do nothing if exists
    os.makedirs(outputDir_aux, exist_ok=True)

    # Start to run DSSR for each file.
    for afile in file_list:
        file_name = get_filename(afile)
        # Build full path for PDB auxiliary files (only
        outputDir_aux = outputDir_aux + file_name
        # Make dir or do nothing if exists
        os.makedirs(outputDir_aux, exist_ok=True)
        if os.path.exists(outputDir_json+file_name+'.json'):
            continue
        # Make dir or do nothing if exists
#        os.makedirs(outputDir_aux, exist_ok=True) # <----- TODO for future changes
#        cmd = 'x3dna-dssr -i='+afile+' --idstr=long --u-turn --more --non-pair --po4 --nmr --json -o='+outputDir_json+file_name+'.json --prefix='+outputDir_aux+file_name+'/'+file_name # <----- TODO for future changes
        if only_protein:
            cmd = 'x3dna-dssr -i='+afile+' --idstr=long --get-hbond --models=all --json -o='+outputDir_json+file_name+'.json'
        else:
            # With metal and tertiary info
            cmd = 'x3dna-dssr -i='+afile+' --idstr=long --u-turn --more --non-pair --po4 --nmr --metal --tertiary --json -o='+outputDir_json+file_name+'.json --prefix='+outputDir_aux+file_name
            # With symm, metal, tertiary info and auxiliary files (18 files) (option --models=all didn't work)
#            cmd = 'x3dna-dssr -i='+afile+' --idstr=long --u-turn --more --non-pair --po4 --symm --metal --tertiary --auxfiles=all --json -o='+outputDir_json+file_name+'.json
            # With metal, tertiary info and auxiliary files (9 files) (option --models=all didn't work)
#            cmd = 'x3dna-dssr -i='+afile+' --idstr=long --u-turn --more --non-pair --po4 --metal --tertiary --auxfiles=all --json -o='+outputDir_json+file_name+'.json
            # With nmr and without metal and tertiary info (0 files)
#            cmd = 'x3dna-dssr -i='+afile+' --idstr=long --u-turn --more --non-pair --po4 --nmr --json -o='+outputDir_json+file_name+'.json
        #Get a list of output report lines
        dssr_output_lines = subprocess.getoutput(cmd).split('\n')

        create_stdout_if_exist('dssr_stdout.txt', dssr_output_lines, file_name, table_outputDir, verbose)
    return


def get_list_of_files(cif_dir, inputformat):
    """
    Get a list of absolute paths for all mmCIF or PDB files present in "cif_dir".
    Parameters:
                * cif_dir:                   a string. Correspond to a directory where all
                                                       *.cif or *.pdb files are stored.
                * inputformat:               a string. Correspond to DSSR input format (cif or pdb).
    Return: a list
    """
    if os.path.exists(cif_dir):
        cif_dir = os.path.dirname(cif_dir)
        #Get data
        file_list = glob.glob(cif_dir+'/'+'*.'+inputformat)
        if not file_list:
            file_list = glob.glob(cif_dir+'/'+'*.'+inputformat+'?')
        # See if there is another cif folder (codnas-rna folder)
    cif_dir = cif_dir+'_without_cltr'
    if os.path.exists(cif_dir):
        file_list2 = glob.glob(cif_dir+'/'+'*.'+inputformat)
        file_list = file_list + file_list2

    return file_list


def execute_table_generation(json_outputDir, table_list, table_outputDir, table_extension, only_protein, verbose):
    """
    Execute table generation usign jq software.
    Parameters:
                * json_outputDir:            a string. Correspond to an output directory to store
                                                       all jsons files.
                * table_list:                a list.   Correspond to a list of tables to work with.
                * table_outputDir:           a string. Correspond to an output directory to store
                                                       all tables.
                * table_extension:           a string. Correspond to format table (tsv or csv).
                * only_protein:              a string. Correspond to a bool condition so only protein
                                                       calculations are taken on account.
                * verbose:                   a string. Correspond to bool condition so standard output
                                                       messages are printed on screen.
    Return: nothing
    """

    for atable in table_list:
        # Create json directory
        outputDir_table = table_outputDir + atable + '/'
        # Make dir or do nothing if exists
        os.makedirs(outputDir_table, exist_ok=True)
        json_file_list = get_list_of_files(json_outputDir, 'json')
        # To build a TABLE file from *.json
        for afile in json_file_list:
            file_name = get_filename(afile)
            full_file_name = outputDir_table+file_name+'.'+table_extension
            if os.path.exists(full_file_name):
                continue
            if atable == 'hbonds':
                if only_protein:
                    # Create a TABLE from all hbonds from all models (aa:aa/aa:ligand/ligand:ligand)
                    cmd = 'jq -r \'.hbonds[] | [.index, .atom1_serNum, .atom2_serNum, .donAcc_type, .distance, .atom1_id, .atom2_id, .atom_pair, .residue_pair] | @'+table_extension+'\''+' '+afile+' > '+full_file_name 
                else:
                    # Create a TABLE from all hbonds from all models (nt-nt, nt-aa, aa-aa, nt:ligand, aa:ligand; ligand:ligand)
                    cmd = 'jq -r \'.models[] .parameters.hbonds[] | [.index, .atom1_serNum, .atom2_serNum, .donAcc_type, .distance, .atom1_id, .atom2_id, .atom_pair, .residue_pair] | @'+table_extension+'\''+' '+afile+' > '+full_file_name
                #Get a list of output report lines
                output_lines = subprocess.getoutput(cmd).split('\n')
                create_stdout_if_exist(atable+'_stdout.txt', output_lines, file_name, table_outputDir, verbose)
            if atable == 'metals':
                # Create a TABLE from metals from all models.
                # Pairs section has alot of more parameters to extract (look json file in detail).
                cmd = 'jq -r \'.models[] .parameters.metals[] | [.index, .symbol, .id, .num_ligands, .ligands_short, .ligands_long] | @'+table_extension+'\''+' '+afile+' > '+full_file_name
                #Get a list of output report lines
                output_lines = subprocess.getoutput(cmd).split('\n')
                create_stdout_if_exist(atable+'_stdout.txt', output_lines, file_name, table_outputDir, verbose)
            if atable == 'pairs':
                # Create a TABLE from pairs from all models.
                # Pairs section has alot of more parameters to extract (look json file in detail).
                cmd = 'jq -r \'.models[] .parameters.pairs[] | [.index, .nt1, .nt2, .bp, .name, .Saenger, .LW, .DSSR] | @'+table_extension+'\''+' '+afile+' > '+full_file_name
                #Get a list of output report lines
                output_lines = subprocess.getoutput(cmd).split('\n')
                create_stdout_if_exist(atable+'_stdout.txt', output_lines, file_name, table_outputDir, verbose)
            if atable == 'angles':
                # Create a TABLE from selected nucleotide id, delta torsion, sugar puckering and cluster of suite name from all models.
                # Angle section has alot of more parameters to extract (look json file in detail).
                cmd = 'jq -r \'.models[] .parameters.nts[] | [.nt_id, .delta, .puckering, .cluster] | @'+table_extension+'\''+' '+afile+' > '+full_file_name
                #Get a list of output report lines
                output_lines = subprocess.getoutput(cmd).split('\n')
                create_stdout_if_exist(atable+'_stdout.txt', output_lines, file_name, table_outputDir, verbose)
            if atable == 'multiplets':  # Definition: "multiplet" is "3+ bases in a coplanar geometry".
                # Create a TABLE from multiplets
                cmd = 'jq -r \'.multiplets[] | [.index, .num_nts, .nts_short, .nts_long] | @'+table_extension+'\''+' '+afile+' > '+full_file_name
                #Get a list of output report lines
                output_lines = subprocess.getoutput(cmd).split('\n')
                create_stdout_if_exist(atable+'_stdout.txt', output_lines, file_name, table_outputDir, verbose)
            if atable == 'dbn':  # Definition: "multiplet" is "3+ bases in a coplanar geometry".
                # Create a TABLE of dot bracket notation from each chain and model
                cmd = 'jq -r \'.models[] | {model: .index, chains: .parameters.chains} | .chains | foreach keys[] as $item (.; .; {($item): {"sstr": .[$item].sstr, "num_nts": .[$item].num_nts, "form": .[$item].form, "bseq": .[$item].bseq}}) | foreach keys[] as $item (.;.; [$item, .[$item].num_nts, .[$item].bseq, .[$item].sstr, .[$item].form]) | @'+table_extension+'\''+' '+afile+' | awk -F\'[,_]\' \'{split($1,a,/m/);{print("\\\""a[2]"\\\""",\\\""$3","$4","$5","$6","$7)}}\' | sed \'1i"model","chain_id","num_nts","bseq","sstr","form"\''+' > '+full_file_name
                #Get a list of output report lines
                output_lines = subprocess.getoutput(cmd).split('\n')
                create_stdout_if_exist(atable+'_stdout.txt', output_lines, file_name, table_outputDir, verbose)
    return


def get_json_files(cif_dir, json_outputDir, table_outputDir, inputformat, table_extension="tsv", only_protein=False, get_hbonds=False, get_pairs=False, get_angles=False, get_multiplets=False, get_dbn=False, verbose=False):
    """
    Run DSSR, get json files and execute parsed table generation.
    Parameters:
                * cif_dir:                   a string. Correspond to a directory where all
                                                       *.cif or *.pdb files are stored.
                * json_outputDir:            a string. Correspond to an output directory to store
                                                       all jsons files.
                * table_outputDir:           a string. Correspond to an output directory to store
                                                       all tables.
                * inputformat:               a string. Correspond to DSSR input format (cif or pdb).
                * table_extension:           a string. Correspond to format table (tsv or csv).
                * only_protein:              a bool. Correspond to a bool condition so only protein
                                                     calculations are taken on account.
                * get_hbonds:                a bool. Correspond to bool condition so hbonds
                                                     are taken in account.
                * get_pairs:                 a bool. Correspond to bool condition so nucleic acid
                                                     pairs are taken in account.
                * get_angles:                a bool. Correspond to bool condition so nucleic acid
                                                     angles are taken in account.
                * get_multiplets:            a bool. Correspond to bool condition so nucleic acid
                                                     multiples are taken in account.
                * get_dbn:                      a bool. Correspond to bool condition so dot-bracket
                                                     notations are taken in account.
                * verbose:                   a bool. Correspond to bool condition so standard output
                                                     messages are printed on screen.
    Return: nothing
    """
    # Build Ok directory
    json_outputDir = build_ok_outputDir_string(json_outputDir)
    table_outputDir = build_ok_outputDir_string(table_outputDir)
    # Make dir or do nothing if exists
    os.makedirs(json_outputDir, exist_ok=True)
    os.makedirs(table_outputDir, exist_ok=True)
    # Get file list
    file_list = get_list_of_files(cif_dir, inputformat)
    # Execute DSSR and save results in json_outputDir
    execute_and_save_dssr(file_list, json_outputDir, only_protein, table_outputDir, verbose)

#    if (get_hbonds) or (get_pairs) or (get_angles) or (get_multiplets) or (get_dbn):
    table_list = []
    if get_hbonds:
        table_list.append('hbonds')
    if get_pairs:
        table_list.append('pairs')
    if get_angles:
        table_list.append('angles')
    if get_multiplets:
        table_list.append('multiplets')
    if get_dbn:
        table_list.append('dbn')
    if table_list:
        execute_table_generation(json_outputDir, table_list, table_outputDir, table_extension, only_protein, verbose)

    print("All done!")
    return


#cif_dir = '/home/martingb/Projects/2020/CoDNaS-RNA/data/2020-05-15/pdbs/data_codnas_rna_cif/'
#json_outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/data/2020-05-15/databases/DSSR/json_files/'
#table_outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/outputs/2020-05-15/DSSR/'
#OLD outputDir = '/media/martingb/Seagate_Expansion_Drive/Projects/2020/CoDNaS-RNA/data/2020-05-15/databases/DSSR/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Run DSSR and generate JSON files and tables.")
    # Positional arguments
    p.add_argument(
        "cif_dir",
        type=str,
        help="Directory to search *.cif files."
    )
    p.add_argument(
        "json_outputDir",
        type=str,
        help="Path where to store all json files."
    )
    p.add_argument(
        "table_outputDir",
        type=str,
        help="Path where to store all tables."
    )
    # Optional arguments
    p.add_argument(
        "-ifmt",
        "--inputformat",
        default="cif",
        type=str,
        help="Input format files: cif or pdb."
    )
    p.add_argument(
        "-ext",
        "--table_extension",
        default="tsv",
        type=str,
        help="Format separated value (default: tsv)."
    )
    p.add_argument(
        "-prot",
        "--only_protein",
        action="store_true",
        default=False,
        type=bool,
        help="Active only protein mode."
    )
    p.add_argument(
        "-hbonds",
        "--get_hbonds",
        action="store_true",
        default=False,
        type=bool,
        help="Get hbonds table from json file (nt:nt, nt:aa, aa:aa."
    )
    p.add_argument(
        "-pairs",
        "--get_pairs",
        action="store_true",
        default=False,
        type=bool,
        help="Get nt pairs table from json file."
        )
    p.add_argument(
        "-angle",
        "--get_angles",
        action="store_true",
        default=False,
        type=bool,
        help="Get table of delta torsion, sugar puckering and cluster of suite."
    )
    p.add_argument(
        "-multiplets",
        "--get_multiplets",
        action="store_true",
        default=False,
        type=bool,
        help="Get multiplets table from json file."
    )
    p.add_argument(
        "-dbn",
        "--get_dbn",
        action="store_true",
        default=False,
        type=bool,
        help="Get dot bracket notation table from json file."
    )
    p.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="Print messages."
    )

    args = p.parse_args()
    get_json_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
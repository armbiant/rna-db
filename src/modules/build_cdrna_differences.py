"""\
Purpose: With this module you can create a full CoDNaS-RNA
         cdrna_differences.csv.gz website table.

Preconditions:
                - Files for input
                    * cdrna_conformers.csv.gz
                    * cdrna_pairs.csv.gz
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [website_tables_dir]           // string
                * [outputDir]                    // string

Observations:
            - 

TODO:  - Make better solution using types for each column.

convert_dict = { 'cluster_id'               : str
               , 'pdb_model_chain_1'        : str
               , 'pdb_model_chain_2'        : str
               , 'is_max'                   : str
               , 'classification_1'         : str
               , 'classification_2'         : str
               , 'diff_classification'      : str
               , 'resolution_1'             : float
               , 'resolution_2'             : float
               , 'diff_resolution'          : str
               , 'method_1'                 : str
               , 'method_2'                 : str
               , 'diff_method'              : str
               , 'source_1'                 : str
               , 'source_2'                 : str
               , 'diff_source'              : str
               , 'temperature_1'            : float
               , 'temperature_2'            : float
               , 'diff_temp'                : str
               , 'ph_1'                     : float
               , 'ph_2'                     : float
               , 'diff_ph'                  : str
               , 'seqres_length_1'          : int
               , 'seqres_length_2'          : int
               , 'diff_seqres_length'       : str
               , 'taxon_id_1'               : float
               , 'taxon_id_2'               : float
               , 'diff_taxon_id'            : str
               , 'biological_process_1'     : str
               , 'biological_process_2'     : str
               , 'diff_biological_process'  : str
               , 'celullar_component_1'     : str
               , 'celullar_component_2'     : str
               , 'diff_celullar_component'  : str
               , 'molecular_function_1'     : str
               , 'molecular_function_2'     : str
               , 'diff_molecular_function'  : str
               , 'hetatm_id_1'              : str
               , 'hetatm_id_2'              : str
               , 'diff_hetatm_id'           : str
               , 'is_synthetic_1'           : str
               , 'is_synthetic_2'           : str
               , 'diff_is_synthetic'        : str
               , 'nb_inter_nt_1'            : int
               , 'nb_inter_nt_2'            : int
               , 'diff_nb_inter_nt'         : str
               , 'nb_inter_aa_1'            : int
               , 'nb_inter_aa_2'            : int
               , 'diff_nb_inter_aa'         : str
               , 'nb_intra_1'               : int
               , 'nb_intra_2'               : int
               , 'diff_nb_intra'            : str }

FROMPAIRS = [ 'cluster_id'
            , 'pdb_model_chain_1'
            , 'pdb_model_chain_2'
            , 'is_max'                  # 0, 1, 2       // str(int)
            , 'diff_nb_intra'           # >= 0, NA      // str(int)
            , 'diff_nb_inter_nt'        # >= 0, NA      // str(int)
            , 'diff_nb_inter_aa'        # >= 0, NA      // str(int)
            , 'diff_ph'                 # >= 0, NA      // str(float)
            , 'diff_temp'               # >= 0, NA      // str(float)
            , 'diff_source' ]           # 0, 1 y NA     // str(float)

"""

import argparse
import os
import pandas as pd
from get_website_df import get_df_if_file_exist
from create_dir import create_dir
from save_website_table import save_website_table



def reorder_columns(cdrna_differences):
    """
    Re-order the cdrna_differences columns for CoDNaS-RNA's release.
    Parameters:
                * cdrna_differences:               a dataframe. Correspond to a pandas dataframe for
                                                                CoDNaS-RNA differences website table.
    Return: dataframe
    """

    # NOTE: diff_temperature is re-written to diff_temp to keep same name as in cdrna_pairs.
    cdrna_differences = cdrna_differences.rename(columns={'diff_temperature': 'diff_temp'})

    columns_order = [ 'cluster_id'                       # pairs
                    , 'pdb_model_chain_1'                # pairs
                    , 'pdb_model_chain_2'                # pairs
                    , 'is_max'                           # pairs
                    , 'classification_1'                 # from conf
                    , 'classification_2'                 # from conf
                    , 'diff_classification'              # hay que comparar
                    , 'resolution_1'                     # from conf
                    , 'resolution_2'                     # from conf
                    , 'diff_resolution'                  # hay que comparar
                    , 'method_1'                         # from conf
                    , 'method_2'                         # from conf
                    , 'diff_method'                      # hay que comparar
                    , 'source_1'                         # from conf
                    , 'source_2'                         # from conf
                    , 'diff_source'                      # pairs
                    , 'temperature_1'                    # from conf
                    , 'temperature_2'                    # from conf
                    , 'diff_temp'                        # pairs
                    , 'ph_1'                             # from conf
                    , 'ph_2'                             # from conf
                    , 'diff_ph'                          # pairs
                    , 'seqres_length_1'                  # from conf
                    , 'seqres_length_2'                  # from conf
                    , 'diff_seqres_length'               # hay que comparar
                    , 'taxon_id_1'                       # from conf
                    , 'taxon_id_2'                       # from conf
                    , 'diff_taxon_id'                    # hay que comparar
                    , 'biological_process_1'             # from conf
                    , 'biological_process_2'             # from conf
                    , 'diff_biological_process'          # hay que comparar
                    , 'celullar_component_1'             # from conf
                    , 'celullar_component_2'             # from conf
                    , 'diff_celullar_component'          # hay que comparar
                    , 'molecular_function_1'             # from conf
                    , 'molecular_function_2'             # from conf
                    , 'diff_molecular_function'          # hay que comparar
                    , 'hetatm_id_1'                      # from conf
                    , 'hetatm_id_2'                      # from conf
                    , 'diff_hetatm_id'                   # hay que comparar
                    , 'is_synthetic_1'                   # from conf
                    , 'is_synthetic_2'                   # from conf
                    , 'diff_is_synthetic'                # hay que comparar
                    , 'nb_inter_nt_1'                    # from conf
                    , 'nb_inter_nt_2'                    # from conf
                    , 'diff_nb_inter_nt'                 # pairs
                    , 'nb_inter_aa_1'                    # from conf
                    , 'nb_inter_aa_2'                    # from conf
                    , 'diff_nb_inter_aa'                 # pairs
                    , 'nb_intra_1'                       # from conf
                    , 'nb_intra_2'                       # from conf
                    , 'diff_nb_intra' ]                  # pairs

    return cdrna_differences.reindex(columns=[*columns_order])


def convert_is_max_to_yes_no(value):
    """
    Convert string values of 'is_max' column (0, 1, 2) to
    'YES' or 'NO'.
    Parameters:
                * value:          a string. Correspond to a value to be compare.
    Return: string
    """
    if value == "2": 
        value = "YES" 
    else: 
        value = "NO" 
    return value


def compare_to_yes_no(value1, value2):
    """
    Compare two strings values.
    Final results have 'YES', 'NO' and 'NA' as values.
    Parameters:
                * value1:          a string. Correspond to a value to be compare.
                * value2:          a string. Correspond to a value to be compare.
    Return: string
    """
    if value1 == "NA" or value2 == "NA": 
        x = "NA" 
    elif value1 != value2:
        x = "YES"
    else: 
        x = "NO" 
    return x


def compare_two_columns(df, acolumn):
    """
    Compare two columns that begins with 'acolumn' name and has
    two suffix as '_1' and '_2'. The comparation result is added
    on a new column that begins with 'diff_' and ends with 'acolumn'.
    Final results have 'YES', 'NO' and 'NA' as values.
    Parameters:
                * df:               a dataframe. Correspond to a pandas dataframe for
                                                 CoDNaS-RNA website table.
                * acolumn:          a string. Correspond to a preffix column name of
                                              the pandas dataframe 'df'.
    Return: nothing
    """
    df['diff_'+acolumn] = df.apply(lambda x: compare_to_yes_no(x[acolumn+'_1'], x[acolumn+'_2']), axis=1)

    return


def add_diff_columns(cdrna_differences):
    """
    Add all differences columns that left and overwrite existing ones.
    Final results have 'YES', 'NO' and 'NA' as values.
    Parameters:
                * cdrna_differences:               a dataframe. Correspond to a pandas dataframe for
                                                                CoDNaS-RNA differences website table.
    Return: nothing
    """
    columns_to_compare = [ 'nb_intra'
                         , 'nb_inter_nt'
                         , 'nb_inter_aa'
                         , 'ph'
                         , 'temperature'
                         , 'source'
                         , 'celullar_component'
                         , 'classification'
                         , 'is_synthetic'
                         , 'molecular_function'
                         , 'seqres_length'
                         , 'resolution'
                         , 'method'
                         , 'taxon_id'
                         , 'biological_process'
                         , 'hetatm_id' ]

    for acol in columns_to_compare:
        compare_two_columns(cdrna_differences, acol)

    return 


def get_diff_info(website_tables_dir):
    """
    Get columns from cdrna_pairs.csv.gz and cdrna_conformers.csv.gz tables
    to make a base cdrna_difference dataframe to work with.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
    Return: dataframe
    """
    # Load website tables
    if os.path.exists(website_tables_dir + "cdrna_pairs.csv.gz") and \
       os.path.exists(website_tables_dir + "cdrna_conformers.csv.gz"):
        cdrna_pairs = get_df_if_file_exist(
            "cdrna_pairs", website_tables_dir + "cdrna_pairs.csv.gz", ",", False
        )
        cdrna_conformers = get_df_if_file_exist(
            "cdrna_conformers", website_tables_dir + "cdrna_conformers.csv.gz", ",", False
        )
    else:
        raise FileNotFoundError('[Error] Files not exists: {} and {}.'.format( website_tables_dir + "cdrna_pairs.csv.gz"
                                                                      , website_tables_dir + "cdrna_conformers.csv.gz")
        ) 

    # Keep only those columns.
    cdrna_pairs_subset = cdrna_pairs.drop(columns=cdrna_pairs.columns.difference([ 'cluster_id'
                                                                                 , 'pdb_model_chain_1'
                                                                                 , 'pdb_model_chain_2'
                                                                                 , 'is_max'
                                                                                 , 'diff_nb_intra'
                                                                                 , 'diff_nb_inter_nt'
                                                                                 , 'diff_nb_inter_aa'
                                                                                 , 'diff_ph'
                                                                                 , 'diff_source' ]))

    cdrna_conformers_subset = cdrna_conformers.drop(columns=cdrna_conformers.columns.difference([ 'pdb_model_chain'
                                                                                                , 'classification'
                                                                                                , 'resolution'
                                                                                                , 'method'
                                                                                                , 'source'
                                                                                                , 'temperature'
                                                                                                , 'ph'
                                                                                                , 'seqres_length'
                                                                                                , 'taxon_id'
                                                                                                , 'biological_process'
                                                                                                , 'celullar_component'
                                                                                                , 'molecular_function'
                                                                                                , 'hetatm_id'
                                                                                                , 'is_synthetic'
                                                                                                , 'nb_intra'
                                                                                                , 'nb_inter_nt'
                                                                                                , 'nb_inter_aa' ]))

    merged1 = pd.merge( cdrna_pairs_subset
                      , cdrna_conformers_subset.add_suffix('_1')
                      , on='pdb_model_chain_1' )

    cdrna_differences = pd.merge( merged1
                                , cdrna_conformers_subset.add_suffix('_2')
                                , on='pdb_model_chain_2' )

    # Changes 0,1,2 to YES or NO in 'is_max' column.
    cdrna_differences['is_max'] = cdrna_differences.is_max.map(convert_is_max_to_yes_no)

    return cdrna_differences


def fill_cdrna_differences(website_tables_dir, outputDir):
    """
    Create and fill the website cdrna_differences table.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """

    # Before start to do anything, check if outputDir is correct and create it
    create_dir(outputDir)

    cdrna_differences = get_diff_info(website_tables_dir)

    add_diff_columns(cdrna_differences)

    cdrna_differences = reorder_columns(cdrna_differences)

    save_website_table(cdrna_differences, outputDir, "cdrna_differences", ",")

    print("All done !")

    return

# website_tables_dir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'

if __name__ == '__main__':

    p = argparse.ArgumentParser(description='Create cdrna_differences.csv.gz table to be used in CoDNaS-RNA website.')
    # Positional arguments
    p.add_argument(
        'website_tables_dir',
        type=str,
        help='Path where all website tables are located.'
    )
    p.add_argument(
        'outputDir',
        type=str,
        help='Path where to store cdrna_differences table.'
    )

    args = p.parse_args()
    fill_cdrna_differences(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
"""\
Purpose: With this module you can restore and order all alignment files
         from each structural alignment strategy group package used in
         CoDNaS-RNA.

Preconditions:
                - Inputs
                    * struct_packs_Dir:           string. "Path where all package groups are stored."
                    * cluster_combinations_dir:   string. "Path where all clusters combinations files are stored
                                                           (Clusters_combinations/)."
                    * outputDir:                  string. "Path to save all TMalign results ordered."

                - Libraries
                    * pandas
                - Modules
                    * In-House: build_ok_outputDir_string

Arguments:    -Positional
                * [struct_packs_Dir]            // string
                * [cluster_combinations_dir]    // string
                * [outputDir]                   // string

Observations:
            - 

TODO: - 

"""

import argparse
import glob
import os
import shutil
import subprocess
import pandas as pd
from build_ok_outputDir_string import build_ok_outputDir_string


def delete_file_if_exists(afile):
    """
    Delete the file if exists.
    Parameters:
                * afile:                 a string. Correspond to a full
                                                   path of a file.
    Return: nothing
    """
    #Remove file if exists.
    if os.path.exists(afile):
        os.remove(afile)
    else:
        print("Can not delete the "+afile+" as it doesn't exists")
    return


def order_all_alignments(struct_packs_Dir, cluster_combinations_list, outputDir):
    """
    Moves all files from each Groups_#/TMalign/{stdout,Sup_files}/ and order them in
    to outputDir under {stdout,Sup_files} folders.
    Also, saves the following files in outputDir/:
            * all_files_paths_from_struct_align_pkgs.list
            * clusters_without_alignments.tsv
            * ausent_files.tsv
            * duplicates_files.tsv
    And gives, in the screen, total counts of:
            * combination files
            * cluster that have not alignments
            * files processed by grep
            * ausent total combination files
            * alignments

    Parameters:
                * struct_packs_Dir:                 a string. Correspond to a path where
                                                              all package groups are stored.
                * cluster_combinations_list:        a list.   Corresponding to a list of all clusters
                                                              combinations.
                * outputDir:                        a string. Corresponding to a path to save results
    Return: nothing
    """
    # Make dir or do nothing if exists
    os.makedirs(outputDir, exist_ok=True)
    os.makedirs(struct_packs_Dir, exist_ok=True)

    #Create a dataframe to store all clusters that did not run at all (i.e.: does not exist any file for each combination).
    cltr_columns = ['Cluster_ID', 'Combination']
    cltr_ausent_file = pd.DataFrame(columns = cltr_columns)

    #Create a dataframe to store all ausent files that have not matched in all_files_paths_from_struct_align_pkgs_per_cluster.list.
    cltr_columns = [ 'Cluster_ID'
                   , 'Combination'
                   , 'Stdout'
                   , 'Sup_file' ]
    ausent_file = pd.DataFrame(columns = cltr_columns)

    #Create a dataframe to store all duplicates files if they exist.
    cltr_columns = [ 'Cluster_ID'
                   , 'Combination'
                   , 'Stdout'
                   , 'Sup_file' ]
    dup_file = pd.DataFrame(columns = cltr_columns)


    with open(outputDir+"all_files_paths_from_struct_align_pkgs.list", 'w') as outfile:
        cmd = "find "+struct_packs_Dir+"Group_*/TMalign/ -type f \( -iname *.out -o -iname *.pdb \)"
        output_lines = subprocess.getoutput(cmd).split('\n')
        for afile in output_lines:
            outfile.write("%s\n" % (afile))

    # Start counting combination files
    comb_files_counter = 0
    # Start counting cluster that have not alignments done
    cltr_empty_counter = 0
    # Start counting files processed by grep
    file_counter = 0
    # Start counting ausent total combination files
    ausent_comb_counter = 0
    # Start counting alignments
    alig_counter = 0

    for cltr_comb_file in cluster_combinations_list:
        comb_files_counter+=1
        cltr_id = os.path.basename(cltr_comb_file).strip("_comb.list")
        stdout_Dir = outputDir+"stdout"+"/"+cltr_id
        Sup_files_Dir = outputDir+"Sup_files"+"/"+cltr_id
        # Make dir or do nothing if exists
        os.makedirs(stdout_Dir, exist_ok=True)
        os.makedirs(Sup_files_Dir, exist_ok=True)

        with open(cltr_comb_file, 'r') as comb_file:
            with open(outputDir+"pattern_comb.list", 'w') as outfile:
                for line in comb_file:
                    alig_counter+=1
                    line = line.rstrip('\n')
                    pdb_model_chain1 = os.path.splitext(line.split(' ')[0])[0]
                    pdb_model_chain2 = os.path.splitext(line.split(' ')[1])[0]
                    pattern_comb = pdb_model_chain1 + "-" + pdb_model_chain2
                    outfile.write("%s\n" % (pattern_comb+".out"))
                    outfile.write("%s\n" % (pattern_comb+".pdb"))
                # Re-start to first line readed comb_list file to be used below
                comb_file.seek(0)

            cmd = "grep -F -f "+outputDir+"pattern_comb.list "+outputDir+"all_files_paths_from_struct_align_pkgs.list > "+outputDir+"all_files_paths_from_struct_align_pkgs_per_cluster.list"
            os.system(cmd)

            cltr_path_size = os.path.getsize(outputDir+"all_files_paths_from_struct_align_pkgs_per_cluster.list")
            if cltr_path_size == 0:
                cltr_empty_counter+=1
                for line in comb_file:
                    line = line.rstrip('\n')
                    pdb_model_chain1 = os.path.splitext(line.split(' ')[0])[0]
                    pdb_model_chain2 = os.path.splitext(line.split(' ')[1])[0]
                    pattern_comb = pdb_model_chain1 + "-" + pdb_model_chain2
                    data = [{'Cluster_ID':cltr_id, 'Combination':pattern_comb}]
                    cltr_ausent_file = cltr_ausent_file.append(data,ignore_index=True,sort=False)
                # Close comb_list file and go to next one.
                comb_file.close()
                continue

            for line in comb_file:
                line = line.rstrip('\n')
                pdb_model_chain1 = os.path.splitext(line.split(' ')[0])[0]
                pdb_model_chain2 = os.path.splitext(line.split(' ')[1])[0]
                pattern_comb = pdb_model_chain1 + "-" + pdb_model_chain2
                cmd = "grep -E "+"\""+pattern_comb+"\.out"+"|"+pattern_comb+"\.pdb"+"\""+" "+outputDir+"all_files_paths_from_struct_align_pkgs_per_cluster.list"
                grep_lines = subprocess.getoutput(cmd).split('\n')

                #Report total ausent files cases for this cluster combination
                if grep_lines[0] == '':
                    data = [{ 'Cluster_ID':cltr_id
                            , 'Combination':pattern_comb
                            , 'Stdout':"NA"
                            , 'Sup_file':"NA" }]
                    ausent_file = ausent_file.append(data,ignore_index=True,sort=False)
                    ausent_comb_counter+=1
                    continue

                #Start to process each grep line
                # Start adding false value for duplicate files
                stdout_dup_status = False
                supfile_dup_status = False
                for afile_path in grep_lines:
                    file_counter+=1
                    afile_name = os.path.basename(afile_path)
                    if afile_path.endswith(".out"):
                        #Check first if afile_name.pdb file does not exists.
                        if not os.path.exists(stdout_Dir+"/"+afile_name):
                            shutil.move(afile_path, stdout_Dir+"/"+afile_name)
                            stdout_status = "Exist"
                            stdout_dup_status = False
                            #Search if you already have stored
                            if not pattern_comb in ausent_file['Combination'].to_list():
                                data = [{'Cluster_ID':cltr_id, 'Combination':pattern_comb, 'Stdout':stdout_status, 'Sup_file':"NA"}]
                                ausent_file = ausent_file.append( data
                                                                , ignore_index=True
                                                                , sort=False )
                            else:
                                ausent_file.loc[ausent_file['Combination'] == pattern_comb, 'Stdout'] = stdout_status
                        else:
                            stdout_dup_status = "Exist"
                    elif afile_path.endswith(".pdb"):
                        #Check first if afile_name.pdb file does not exists.
                        if not os.path.exists(Sup_files_Dir+"/"+afile_name):
                            shutil.move(afile_path, Sup_files_Dir+"/"+afile_name)
                            supfile_status = "Exist"
                            supfile_dup_status = False
                            #Search if you already have stored
                            if not pattern_comb in ausent_file['Combination'].to_list():
                                data = [{ 'Cluster_ID':cltr_id
                                        , 'Combination':pattern_comb
                                        , 'Stdout':"NA"
                                        , 'Sup_file':supfile_status }]
                                ausent_file = ausent_file.append( data
                                                                , ignore_index=True
                                                                , sort=False )
                            else:
                                ausent_file.loc[ausent_file['Combination'] == pattern_comb, 'Sup_file'] = supfile_status
                        else:
                            supfile_dup_status = "Exist"
                    else:
                        print("Error:"+"\t"+afile_path+"\t"+"has another extention than .out or .pdb")
                        continue

                # Save duplicated files if exists.
                if stdout_dup_status or supfile_dup_status:
                    if stdout_dup_status and supfile_dup_status:
                        data = [{ 'Cluster_ID':cltr_id
                                , 'Combination':pattern_comb
                                , 'Stdout':stdout_dup_status
                                , 'Sup_file':supfile_dup_status }]
                        dup_file = dup_file.append( data
                                                  , ignore_index=True
                                                  , sort=False )
                    elif stdout_dup_status:
                        data = [{ 'Cluster_ID':cltr_id
                                , 'Combination':pattern_comb
                                , 'Stdout':stdout_dup_status
                                , 'Sup_file':"NA" }]
                        dup_file = dup_file.append( data
                                                  , ignore_index=True
                                                  , sort=False )
                    elif supfile_dup_status:
                        data = [{ 'Cluster_ID':cltr_id
                                , 'Combination':pattern_comb
                                , 'Stdout':"NA"
                                , 'Sup_file':supfile_dup_status }]
                        dup_file = dup_file.append( data
                                                  , ignore_index=True
                                                  , sort=False )

            #Clean everything for this cluster
            delete_file_if_exists(outputDir+"pattern_comb.list")
            delete_file_if_exists(outputDir+"all_files_paths_from_struct_align_pkgs_per_cluster.list")

    #Check for last cluster
    #Clean everything for this cluster
    delete_file_if_exists(outputDir+"pattern_comb.list")
    delete_file_if_exists(outputDir+"all_files_paths_from_struct_align_pkgs_per_cluster.list")

    # Ending message
    print("Total cluster combination files seen:"+"\t"+str(comb_files_counter))
    print("Total clusters without any alignment file seen:"+"\t"+str(cltr_empty_counter))
    print("Total alignments seen:"+"\t"+str(alig_counter))
    print("Total alignments without any file seen:"+"\t"+str(ausent_comb_counter))
    print("Total files seen:"+"\t"+str(file_counter))

    #Save in tsv file all clusters that did not run any alignment.
    cltr_ausent_file.to_csv( outputDir + "clusters_without_alignments.tsv"
                           , sep='\t'
                           , index=False
                           , header=True )
    #Save in tsv file all combinations that have at least one file ausent.
    ausent_file.to_csv( outputDir + "ausent_files.tsv"
                      , sep='\t'
                      , index=False
                      , header=True )
    #Save in tsv file all combinations that have at least one file ausent.
    dup_file.to_csv( outputDir + "duplicates_files.tsv"
                   , sep='\t'
                   , index=False
                   , header=True )
    
    return


def get_list_cluster_combinations(cluster_combinations_dir):
    """
    Get a list of Cluster_#_comb.list files paths.
    Parameters:
                    * cluster_combinations_dir: a string. Corresponding to 
                                                a directory where all clusters
                                                combinations list are stored.
    Return: a list.
    """
    # Get a list of each cluster sorted
    # (clusters converted to ints for proper sorting)
    cluster_combinations_list = sorted(
        [
            int(os.path.basename(f).strip("Cluster_").strip("_comb.list"))
            for f in glob.glob(cluster_combinations_dir + "Cluster_*")
        ], reverse=False
    )
    # Re-make names to get all correct names
    cluster_combinations_list = [
        cluster_combinations_dir + "Cluster_" + str(f) + '_comb.list' for f in cluster_combinations_list
    ]
    
    return cluster_combinations_list


def restore_order( struct_packs_Dir
                 , cluster_combinations_dir
                 , outputDir ):
    """
    Restore and order all alignment files from each strategy group package.
    Parameters:
                * struct_packs_Dir:         a string. Correspond to a path where
                                                      all package groups are stored.
                * cluster_combinations_dir: a string. Corresponding to 
                                                      a directory where all clusters
                                                      combinations list are stored.
                * outputDir:                a string. Corresponding to a path to save results
    Return: a list.
    """
    #Directory to save mapping results
    outputDir = build_ok_outputDir_string(outputDir)

    cluster_combinations_list = get_list_cluster_combinations(cluster_combinations_dir)

    order_all_alignments( struct_packs_Dir
                        , cluster_combinations_list
                        , outputDir )
    
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Restore and order all alignment files from each strategy group package.")
    # Positional arguments
    p.add_argument(
        "struct_packs_Dir",
        type=str,
        help="Path where all package groups are stored."
    )
    p.add_argument(
        "cluster_combinations_dir",
        type=str,
        help="Path where all clusters combinations files are stored."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path to save all TMalign results ordered."
    )

    args = p.parse_args()
    restore_order(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
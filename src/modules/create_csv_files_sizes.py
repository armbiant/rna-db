"""\
Purpose: With this module you can create a \"aFileName\".csv file
         to be used in CoDNaS-RNA website.

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * pandas

Arguments:    -Positional
                * [aFileName]                // string
                * [aDir]                     // string
                * [outputDir]                // string

Observations: - Needs a GNU/LINUX --> du

TODO: - 

"""

import argparse
import glob
import subprocess
import pandas as pd
from get_filename import get_filename
from create_dir import create_dir
from build_ok_outputDir_string import build_ok_outputDir_string


def get_df_files_sizes(aDir, **kwargs):
    """
    Get a dataframe with all file sizes thatmatch with **kwargs,
    this is: startsWith= and/or extensions= .
    Parameters:
                * aDir:                      a string. Correspond to a path where are stored
                                                       *tar.gz files.
    Observations:
                * GNU/LINUX 'du' software is used to get sizes.
    Return: a dataframe
    """
    for aKey in [*kwargs]:
        if aKey == "startsWith":
            startsWith = kwargs[aKey]
        elif aKey == "extensions":
            if type(kwargs[aKey]) is list:
                extensions = kwargs[aKey]
            else:
                raise ValueError('[Error] Value of {}= is not a list.'.format(aKey))

    files_and_folders = []
    if 'extensions' in locals():
        for ext in extensions:
            if 'startsWith' in locals():
                files_and_folders += glob.glob(aDir + startsWith + '*' + ext)
            else:
                files_and_folders += glob.glob(aDir + '*' + ext)
    elif 'startsWith' in locals():
        files_and_folders += glob.glob(aDir + startsWith + '*')

    # du options --> show size and total of directory in h-read.
    cmd_du = ['du', '-sch'] +  files_and_folders
    process = subprocess.run(cmd_du, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    if process.returncode:
        print(process.stderr.decode('ascii').splitlines())
        return pd.DataFrame()
    else:
        stdout_lines = process.stdout.decode('ascii').splitlines()
        if stdout_lines[0] == "0\ttotal":
            print(process.stderr.decode('ascii').splitlines())
            return pd.DataFrame()
        elif process.stderr:
            print(process.stderr.decode('ascii').splitlines())

    file_size_df = pd.DataFrame(columns=['filename', 'size'])
    # Remove line total size (last output line)
    stdout_lines = stdout_lines[:-1]
    for afile in stdout_lines:
        size, abs_filename = afile.split('\t')
        filename = get_filename(abs_filename, without_exts=False, force=False)
        file_size_df = file_size_df.append({'filename': filename, 'size': size}, ignore_index = True)

    return file_size_df


def create_file( aFileName
               , aDir
               , outputDir ):
    """
    Create aFileName.csv file to be used in CoDNaS-RNA website.
    Parameters:
                * aFileName:                 a string. Correspond to the filename to be created
                                                       with out ".csv" extension"
                * aDir:                      a string. Correspond to a path where are stored
                                                       *tar.gz files.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       file_size.csv file.
    Return: nothing
    """
    # Before start to do anything, check directories and if outputDir does not exists create it
    aDir = build_ok_outputDir_string(aDir)
    outputDir = build_ok_outputDir_string(outputDir)
    create_dir(outputDir)

    file_size_df = get_df_files_sizes(aDir, startsWith="Cluster_", extensions=[".tar.gz"])
    
    file_size_df.to_csv(outputDir + aFileName + ".csv", sep=',', index=False, header=True)

    print("All done !")

    return


# aDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/files/download/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/files/structures/'

if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create a \"aFileName\".csv file to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "aFileName",
        type=str,
        help="Filename to be used on the output CSV file."
    )
    p.add_argument(
        "aDir",
        type=str,
        help="Path where are stored *tar.gz files."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store the  \"aFileName\".csv file."
    )

    args = p.parse_args()
    create_file(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
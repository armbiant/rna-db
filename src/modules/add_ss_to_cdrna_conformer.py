"""Purpose: With this module you can add and fill secondary structures
            columns to CoDNaS-RNA's cdrna_conformers.csv.gz website table.
            Columns to added:
                            * dbn_bseq_orig
                            * dbn_sstr_orig
                            * dbn_bseq_rev
                            * dbn_sstr_rev

Preconditions:
                - Files for input
                    * cdrna_conformers.csv.gz
                    * <pdb_model_chain>.dbn
                    * <pdb_model_chain>.rev.dbn
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [cdrna_conformers]             // string
                * [structuresDir]                // string
                * [outputDir]                    // string

Observations:
            - 

TODO: -

"""

import argparse
import os
import glob
import pandas as pd
from save_website_table import save_website_table
from get_website_df import get_df_if_file_exist
from add_cell_value import add_value_in_column_to_subset
from build_ok_outputDir_string import build_ok_outputDir_string
from create_dir import create_dir
from vienna import SSFormat


def get_list_of_files(aDir, extformat, recursive=False):
    """
    Get a list of absolute paths for all "extformat" files present in "aDir".
    Parameters:
                * aDir:                     a string. Correspond to a directory path where all
                                                      extformat files are stored.
                * extformat:                a string. Correspond to format extention (e.g.: tsv).

                * recursive:                a bool. Correspond to a state of recursive search.
    Return: a list
    """
    if os.path.exists(aDir):
        if recursive:
            expansion = '**/*.'
        else:
            expansion = '*.'
        file_list = glob.glob((aDir + expansion + extformat + '*'), recursive=recursive)
        if not file_list:
            raise ValueError('[Error] No files ended with {} were found on {}.'.format('.'+extformat, aDir))
    else:
        raise NameError('[Error] Directory not exists: {}.'.format(aDir))
    return file_list


def exists_columns_in(column_list, cdrna_website):
    """
    Indicates True if columns_list exists in cdrna_website dataframe.
    Parameters:
                * column_list:                  a list. Correspond to a list of column names.
                * cdrna_website:                a dataframe. Correspond to a pandas dataframe for
                                                             a CoDNaS-RNA website table.
    Return: a bool
    """
    return all([acol in cdrna_website.columns for acol in column_list])


def create_empty_columns_in(column_list, cdrna_website):
    """
    Create empty columns (column_list) in a given dataframe (cdrna_website).
    Parameters:
                * column_list:                  a list. Correspond to a list of column names.
                * cdrna_website:                a dataframe. Correspond to a pandas dataframe for
                                                             a CoDNaS-RNA website table.
    Return: a dataframe
    """
    for acol in column_list:
        if acol not in list(cdrna_website.columns):
            cdrna_website[acol] = 'NA'

    return cdrna_website


def get_vienna_values(vienna_file):
    """
    Get the vienna values from a given file (vienna format).
    Parameters:
                * vienna_file                a string. Correspond to a vienna file path.
    Return: a tuple of 3 values (name, seq, sstr)
    """
    rna = SSFormat(vienna_file)
    return (rna.name, rna.sequence, rna.sstr)


def write_vienna_to(a_ss_file, cdrna_conformers, dbn_columns):
    """
    Write vienna values to given columns (dbn_columns) in cdrna_conformers dataframe.
    Valid column names: "bseq" and "sstr"
    Parameters:
                * a_ss_file:                 a string. Correspond to a vienna file path.
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * dbn_columns:               a list. Correspond to a list of column names.
    Return: nothing
    """
    name, bseq, sstr = get_vienna_values(a_ss_file)
    for acol in dbn_columns:
        if 'bseq' in acol:
            add_value_in_column_to_subset(cdrna_conformers, name, 'pdb_model_chain', bseq, acol)
        elif 'sstr' in acol:
            add_value_in_column_to_subset(cdrna_conformers, name, 'pdb_model_chain', sstr, acol)
    return


def add_bseq_and_sstr_values(cdrna_conformers, ss_files_list):
    """
    Add bseq and sstr (vienna values) to given files (ss_files_list) in
    cdrna_conformers dataframe.
    Parameters:
                * a_ss_file:                 a list. Correspond to a list of vienna file paths.
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
    Return: a dataframe
    """
    dbn_orig_columns = [ 'dbn_bseq_orig'
                       , 'dbn_sstr_orig' ]
    dbn_rev_columns = [ 'dbn_bseq_rev'
                      , 'dbn_sstr_rev' ]
    for a_ss_file in ss_files_list:
        if a_ss_file.endswith('.rev.dbn'):
            write_vienna_to(a_ss_file, cdrna_conformers, dbn_rev_columns)
        elif a_ss_file.endswith('.dbn'):
            write_vienna_to(a_ss_file, cdrna_conformers, dbn_orig_columns)
        else:
            raise FileNotFoundError('[Error] Files not exists: {}.rev.dbn and {}.dbn.'.format(a_ss_file))

    return cdrna_conformers


def fill_cdrna_conformers(cdrna_conformers, structuresDir, outputDir):
    """
    Add secondary structure columns to website cdrna_conformers table and fill them.
    Parameters:
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * structuresDir:             a string. Correspond to a directory where to search
                                                       CoDNaS-RNA *.rev.dbn *.dbn files.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """
    create_dir(outputDir)
    structuresDir = build_ok_outputDir_string(structuresDir)
    cdrna_conformers = get_df_if_file_exist('cdrna_conformers', cdrna_conformers, ',', False)
    ss_files_list = get_list_of_files(structuresDir, 'dbn', recursive=True)

    columns = [ 'dbn_bseq_orig'
              , 'dbn_sstr_orig'
              , 'dbn_bseq_rev'
              , 'dbn_sstr_rev' ]

    if ss_files_list:
        if not exists_columns_in(columns, cdrna_conformers):
            cdrna_conformers = create_empty_columns_in(columns, cdrna_conformers)
        cdrna_conformers = add_bseq_and_sstr_values(cdrna_conformers, ss_files_list)

    save_website_table(cdrna_conformers, outputDir, 'cdrna_conformers', ',')

    print('All done !')

    return


# cdrna_conformers_path = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_conformers.csv.gz'
# structuresDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/files/structures/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'


if __name__ == '__main__':
    p = argparse.ArgumentParser(description='Add secondary structure (ss) information to cdrna_conformers.csv.gz table.')
    # Positional arguments
    p.add_argument(
        'cdrna_conformers',
        type=str,
        help='Table with all CoDNaS-RNA conformers from website.'
    )
    p.add_argument(
        'structuresDir',
        type=str,
        help='Directory to search *.rev.dbn and *.dbn files.'
    )
    p.add_argument(
        'outputDir',
        type=str,
        help='Path where to store cdrna_conformers table.'
    )

    args = p.parse_args()
    fill_cdrna_conformers(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
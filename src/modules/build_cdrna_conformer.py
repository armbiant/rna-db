"""\
Purpose: With this module you can create a full CoDNaS-RNA
         cdrna_conformers.csv.gz website table.

Preconditions:
                - Files for input
                    * pdbchains_databases_codes
                    * rcsb_table
                    * cdrna_interactions
                    * synthetics_table
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [pdbchains_databases_codes]    // string
                * [rcsb_table]                   // string
                * [cdrna_interactions]           // string
                * [synthetics_table]             // string
                * [cif_dir]                      // string
                * [outputDir]                    // string

Observations:
            - 

TODO: - Implement optional parameters for partially fill cdrna_conformers.csv.gz

"""


import argparse
import os
from collections import defaultdict
import pandas as pd
from save_website_table import save_website_table
from get_website_df import get_df_if_file_exist
from add_cell_value import add_value_in_column_to_subset
from get_pdb_models_of_chain_list import get_pdb_models_of_chain_list
from create_dir import create_dir
from encrypt import encode


def create_dataframe_struct():
    """
    Create a dataframe structure.
    Parameters: nothing
    TODO: add list parameter to be used as a column builder.
    Return: an empty dataframe
    """
    # Create the Dataframe to be used in cdrna_conformers.
    columns = [
        "cluster_id",
        "pdb",                  # Table RCSB
        "model",                # Table RCSB
        "chain",                # Table RCSB
        "pdb_model_chain",      # In house
        "classification",       # Table RCSB
        "macromolecule",        # Table RCSB
        "resolution",           # Table RCSB
        "method",               # Table RCSB
        "source",               # Table RCSB
        "temperature",          # Table RCSB
        "ph",                   # Table RCSB
        "seqres_length",        # Table RCSB
        "seqatom_length",       # Table ?       <-------- Tenemos varias formas de obtenerlo (Tabla DSSR, usando DSSR, gemmi o Biopython)
        "seqres",               # Table RCSB
        "seqatom",              # Table RCSB
        "taxon_id",             # Table RCSB
        "taxonomy",             # Table RCSB
        "biological_process",   # Table RCSB
        "celullar_component",   # Table RCSB
        "molecular_function",   # Table RCSB
        "hetatm_id",            # Table RCSB
        "mutations",            # Table RCSB
        "is_synthetic",         # Table synthetics_models.tsv   <----- Under RNAcentral folder
        "nb_intra",             # Table DSSR
        "nb_inter_nt",          # Table DSSR
        "nb_inter_aa",          # Table DSSR
        "nb_inter_mt"           # Table DSSR
    ]
    cdrna_conformers = pd.DataFrame(columns=columns)

    return cdrna_conformers


def extract_info_databases_code_table(
    cdrna_conformers, pdbchains_databases_codes, a_separator, cif_dir, outputDir
):
    """
    Extract PDBS MODELS CHAINS from clusters using pdbchains_databases_codes.tsv as table provider.
    This function also gives a pdb_chains_list containing all unique PDB_Chain codes. 
    Parameters:
                * cdrna_conformers:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA conformers website table.
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * a_separator:               a string. Correspond to the pdbchains_databases_codes.tsv
                                                       table separator.
                * cif_dir:                   a string. Correspond to a directory where all mmcif
                                                       files are stored.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe and a list
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_conformers.csv.gz"):
        return cdrna_conformers, "_"

    # Get a dataframe from table
    mapping_cltrs = get_df_if_file_exist(
        "mapping_cltrs", pdbchains_databases_codes, a_separator, False
    )

    for cltr_id in mapping_cltrs["Cluster_ID"].unique().tolist():
        # Get pdb chains list
        pdb_chains_list = (
            mapping_cltrs["PDB_chain"]
            .loc[(mapping_cltrs["Cluster_ID"] == cltr_id)]
            .tolist()
        )
        if cltr_id == "NA":
            cif_dir = os.path.dirname(cif_dir)
            cif_dir = cif_dir + "_without_cltr"
        for achain in pdb_chains_list:
            pdb_chain = achain.replace("_", ":")
            achain_models_list = get_pdb_models_of_chain_list(pdb_chain, cif_dir)
            # Remove extension
            achain_models_list = [
                achain_model.split(".cif")[0] for achain_model in achain_models_list
            ]
            for amodel_chain in achain_models_list:
                pdb = amodel_chain.split("_")[0]
                model = amodel_chain.split("_")[1]
                chain = amodel_chain.split("_")[2]
                conf_hide = encode(amodel_chain, True)
                data = [
                    {
                        "cluster_id": cltr_id,
                        "pdb": pdb,                     # Table RCSB
                        "model": model,                 # Table RCSB
                        "chain": chain,                 # Table RCSB
                        "pdb_model_chain": conf_hide,   # In house
                        "classification": "NA",         # Table RCSB <---- tiene una columna de Classification
                        "macromolecule": "NA",          # Table RCSB
                        "resolution": "NA",             # Table RCSB
                        "method": "NA",                 # Table RCSB
                        "source": "NA",                 # Table RCSB
                        "temperature": "NA",            # Table RCSB
                        "ph": "NA",                     # Table RCSB
                        "seqres_length": "NA",          # Table RCSB
                        "seqatom_length": "NA",         # Table ?       <-------- Tenemos varias formas de obtenerlo (Tabla DSSR, usando DSSR, gemmi o Biopython)
                        "seqres": "NA",                 # Table RCSB
                        "seqatom": "NA",                # Table RCSB
                        "taxon_id": "NA",               # Table RCSB
                        "taxonomy": "NA",               # Table RCSB
                        "biological_process": "NA",     # Table RCSB
                        "celullar_component": "NA",     # Table RCSB
                        "molecular_function": "NA",     # Table RCSB
                        "hetatm_id": "NA",              # Table RCSB
                        "mutations": "NA",              # Table RCSB
                        "is_synthetic": "NA",           # Table synthetics_models.tsv   <----- Under RNAcentral folder
                        "nb_intra": "NA",               # Table DSSR
                        "nb_inter_nt": "NA",            # Table DSSR
                        "nb_inter_aa": "NA",            # Table DSSR
                        "nb_inter_mt": "NA"             # Table DSSR
                    }
                ]
                cdrna_conformers = cdrna_conformers.append(
                    data, ignore_index=True, sort=False
                )

    pdb_chains_list = mapping_cltrs["PDB_chain"].unique().tolist()

    return cdrna_conformers, pdb_chains_list


def get_rnacentral_dict(synthetics_table):
    """
    Get a dictionary with taxon values using conformers as keys for one CoDNaS-RNA cluster.
    Parameters:
                * synthetics_table:          a dataframe. Correspond to a pandas dataframe for
                                                          synthetics_table table.
    Return: a dict
    """
    synthetics_table["pdb_model"] = (
        synthetics_table["PDB_code"].astype(str)
        + "_"
        + synthetics_table["Model"].astype(str)
    )
    synthetics_table["pdb_model_chain"] = (
        synthetics_table["pdb_model"].astype(str)
        + "_"
        + synthetics_table["Chain"].astype(str)
    )
    synthetics_table["pdb_model_chain"] = synthetics_table["pdb_model_chain"].map(encode)

    pdb_synthetic_dict = dict()
    data = zip(synthetics_table["pdb_model_chain"], synthetics_table["Taxon_ID"])
    for conf, atax in data:
        pdb_synthetic_dict[conf] = atax

    return pdb_synthetic_dict


def extract_rnacentral_info(cdrna_conformers, synthetics_table, a_separator, outputDir):
    """
    Extract synthetic information of conformers per CoDNaS-RNA cluster using
    synthetics_table.tsv as table provider.
    Parameters:
                * cdrna_conformers:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA conformers website table.
                * synthetics_table:          a string. Correspond to a TSV table with all
                                                       synthetic conformers present in CoDNaS-RNA.
                * a_separator:               a string. Correspond to the synthetics_table.tsv
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_conformers.csv.gz"):
        return cdrna_conformers

    # Get a dataframe from table
    synthetics_table = get_df_if_file_exist(
        "synthetics_table", synthetics_table, a_separator, False
    )

    # Get values from clusters
    pdb_synthetic_dict = get_rnacentral_dict(synthetics_table)

    # Create dict with all values per Cluster_ID
    #    cltrs_dic = dict()
    # Ask if column exist in file, otherwise exit. (TODO)
    # Create a dict of clusters with a list of unique URS codes present per cluster.
    for aconf in cdrna_conformers["pdb_model_chain"].unique().tolist():
        if aconf in pdb_synthetic_dict.keys():
            if pdb_synthetic_dict[aconf] == "32630":
                cdrna_conformers.loc[
                    (cdrna_conformers["pdb_model_chain"] == aconf), "is_synthetic"
                ] = 1
            else:
                print("Error:\t" + aconf + "\tis not a synthetic entry.")

    # Replace NA for 0
    cdrna_conformers["is_synthetic"] = cdrna_conformers["is_synthetic"].replace("NA", 0)

    return cdrna_conformers


def get_rcsb_dict(pdb_chains_list, rcsb_table, acolumn_list):
    """
    Get a dictionary with RCSB values using acolumn_list as keys for one CoDNaS-RNA cluster.
    Parameters:
                * pdb_chains_list:           a list. Correspond to all PDB_Chains present in
                                                     CoDNaS-RNA database.
                * rcsb_table:                a dataframe. Correspond to a pandas dataframe for
                                                          rcsb table.
                * acolumn_list:              a list. Correspond to a list of columns
                                                     from rcsb_table dataframe.
    Return: a dict
    """
    # Create dict with all cluster pdb chains rows (using cltr_file)
    rcsb_dic = dict()

    # Select only columns that are from interest. Like, df1 = df[['a','b']]
    acolumn_list_plus_PDB_chain = acolumn_list + ["PDB_chain"]
    rcsb_table_copy = rcsb_table[acolumn_list_plus_PDB_chain].copy()

    rcsb_table_subset = rcsb_table_copy.loc[
        rcsb_table_copy["PDB_chain"].isin(pdb_chains_list)
    ]

    rcsb_table_subset = rcsb_table_subset.drop_duplicates(
        subset="PDB_chain", ignore_index=True
    )

    for apdb_chain in pdb_chains_list:
        # Check that exist rows from this pdb code. Could not have information.
        if rcsb_table_subset[rcsb_table_subset["PDB_chain"] == apdb_chain].empty:
            rcsb_dic[apdb_chain] = {"NA": "NA"}
            continue
        rcsb_dic[apdb_chain] = dict()
        for acolumn in acolumn_list:
            avalue = rcsb_table_subset.loc[
                rcsb_table_subset["PDB_chain"] == apdb_chain
            ][acolumn].values[0]
            if not avalue:
                avalue = "NA"
            rcsb_dic[apdb_chain][acolumn] = avalue
            continue
    return rcsb_dic


def extract_info_rcsb_table(
    cdrna_conformers, pdb_chains_list, rcsb_table, a_separator, outputDir
):
    """
    Extract all RCSB releated values for each conformer per CoDNaS-RNA cluster using rcsb_table.csv.gz
    and clusters_path/Cluster_#.csv as tables providers.
    Parameters:
                * cdrna_conformers:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA conformers website table.
                * pdb_chains_list:           a list. Correspond to all PDB_Chains present in
                                                     CoDNaS-RNA database.
                * rcsb_table:                a string. Correspond to a CSV table with all RCSB
                                                       information.
                * a_separator:               a string. Correspond to the rcsb_table.csv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_conformers.csv.gz"):
        return cdrna_conformers

    # Get a dataframe from table
    rcsb_table = get_df_if_file_exist("rcsb_table", rcsb_table, a_separator, False)

    columns_to_fill = [
        "macromolecule",        # Table RCSB
        "resolution",           # Table RCSB
        "method",               # Table RCSB
        "source",               # Table RCSB
        "temperature",          # Table RCSB
        "ph",                   # Table RCSB
        "seqres_length",        # Table RCSB
        "taxon_id",             # Table RCSB
        "taxonomy",             # Table RCSB
        "biological_process",   # Table RCSB
        "celullar_component",   # Table RCSB
        "molecular_function",   # Table RCSB
        "seqres",               # Table RCSB
        "classification",       # Table RCSB
        "hetatm_id"             # Table RCSB
    ]

    columns_to_search = [
        "Macromolecule Name",
        "Resolution",
        "Exp. Method",
        "Source",
        "Temp (K)",
        "pH Value",
        "Chain Length",
        "Taxonomy ID",
        "Taxonomy",
        "Biological Process",
        "Cellular Component",
        "Molecular Function",
        "Sequence",
        "Classification",
        "HET ID"
    ]

    column_match = dict(zip(columns_to_search, columns_to_fill))

    rcsb_dic = get_rcsb_dict(pdb_chains_list, rcsb_table, columns_to_search)

    # Fill values to cdrna_conformers
    for pdb_chain, acolumn_dic in rcsb_dic.items():
        for acolumn, avalue in acolumn_dic.items():
            pdb_code = pdb_chain.split(":")[0].lower()
            chain_code = pdb_chain.split(":")[1]
            model_list = cdrna_conformers.loc[
                (cdrna_conformers["pdb"] == pdb_code)
                & (cdrna_conformers["chain"] == chain_code)
            ]["model"].tolist()
            for amodel in model_list:
                if acolumn == "NA":
                    for rcsb_col in column_match.values():
                        cdrna_conformers.loc[
                            (cdrna_conformers["pdb"] == pdb_code)
                            & (cdrna_conformers["model"] == amodel)
                            & (cdrna_conformers["chain"] == chain_code),
                            rcsb_col,
                        ] = avalue
                    #                        add_value_in_column_to_subset(cdrna_conformers, pdb_chain, 'pdb', avalue, rcsb_col)
                    continue
                cdrna_conformers.loc[
                    (cdrna_conformers["pdb"] == pdb_code)
                    & (cdrna_conformers["model"] == amodel)
                    & (cdrna_conformers["chain"] == chain_code),
                    column_match[acolumn],
                ] = avalue
    #                add_value_in_column_to_subset(cdrna_conformers, pdb_chain, 'pdb', avalue, column_match[acolumn])

    return cdrna_conformers


def get_pdb_chain_dict(pdbchains_databases_codes, a_separator):
    """
    Get a pdb_chain dictionary containing all unique chains codes per PDB. 
    Parameters:
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * a_separator:               a string. Correspond to the pdbchains_databases_codes.tsv
                                                       table separator.
    Return: a dict
    """
    # Get a dataframe from table
    mapping_cltrs = get_df_if_file_exist(
        "mapping_cltrs", pdbchains_databases_codes, a_separator, True
    )

    # Remove those rows that do not have cluster assigned. (we do not need to know their interactions)
    mapping_cltrs_copy = mapping_cltrs.dropna(subset=["Cluster_ID"]).copy()

    # Generate two columns from "PDB_chain" to be used in zip function.
    mapping_cltrs_copy["pdb"], mapping_cltrs_copy["chain"] = (
        mapping_cltrs_copy["PDB_chain"].str.split("\:").str
    )

    # Ordered dict chain-chain
    pdb_chain_dict = defaultdict(list)
    # Make a zip data variable of cluster with PDB:chain code.
    data = zip(mapping_cltrs_copy["pdb"], mapping_cltrs_copy["chain"])
    # Create a dict of clusters with a list of PDB:chain codes present per cluster.
    for pdb, chain in data:
        # PDB codes are in lower case
        pdb = pdb.lower()
        if pdb in pdb_chain_dict.keys():
            pdb_chain_dict[pdb].append(chain)
        else:
            pdb_chain_dict[pdb] = [chain]

    return pdb_chain_dict


def extract_info_interactions_table(
    cdrna_conformers,
    cdrna_interactions,
    pdbchains_databases_codes,
    a_separator,
    outputDir
):
    """
    Extract interaction info per CoDNaS-RNA conformer using cdrna_interactions.csv.gz as table provider.
    Parameters:
                * cdrna_conformers:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA conformers website table.
                * cdrna_interactions:        a string. Correspond to a CSV CoDNaS-RNA interactions
                                                       website table.
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * a_separator:               a string. Correspond to the cdrna_interactions.csv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_conformers.csv.gz"):
        return cdrna_conformers

    # Get a dataframe from table
    cdrna_interactions = get_df_if_file_exist(
        "cdrna_interactions", cdrna_interactions, a_separator, False
    )

    # Create a column to map type and partner chains. (After type assigment, this column is going to be removed)
    #    cdrna_interactions['pdb_chain'] = cdrna_interactions['pdb'].astype(str) + ':' + cdrna_interactions['partner_chain_id'].astype(str)

    # Get pdb chains dictionary
    pdb_chains_dict = get_pdb_chain_dict(pdbchains_databases_codes, "\t")

    # Start to see each chain from each pdb code
    for apdb, chains_list in pdb_chains_dict.items():
        for achain in chains_list:
            models_with_interactions_list = (
                cdrna_interactions.loc[
                    (cdrna_interactions["pdb"] == apdb)
                    & (cdrna_interactions["chain"] == achain)
                ]["model"]
                .unique()
                .tolist()
            )

            for amodel in models_with_interactions_list:
                nb_intra_subset = cdrna_interactions.loc[
                    (cdrna_interactions["pdb"] == apdb)
                    & (cdrna_interactions["model"] == amodel)
                    & (cdrna_interactions["chain"] == achain)
                    & (cdrna_interactions["partner_chain_id"] == achain)
                ]
                nb_inter_nt_subset = cdrna_interactions.loc[
                    (cdrna_interactions["pdb"] == apdb)
                    & (cdrna_interactions["model"] == amodel)
                    & (cdrna_interactions["chain"] == achain)
                    & (cdrna_interactions["partner_chain_id"] != achain)
                    & (
                        (cdrna_interactions["partner_rcsb_type"] == "dna")
                        | (cdrna_interactions["partner_rcsb_type"] == "rna")
                    )
                ]
                nb_inter_aa_subset = cdrna_interactions.loc[
                    (cdrna_interactions["pdb"] == apdb)
                    & (cdrna_interactions["model"] == amodel)
                    & (cdrna_interactions["chain"] == achain)
                    &
                    #  Make a bug when tRNA has aminoacid bound it.
                    #                                                    (cdrna_interactions['partner_chain_id'] != achain) &
                    (cdrna_interactions["partner_rcsb_type"] == "protein")
                ]
                # Get rows values as number of items to find
                if nb_intra_subset.empty:
                    nb_intra_val = 0
                else:
                    nb_intra_val = nb_intra_subset.shape[0]
                if nb_inter_nt_subset.empty:
                    nb_inter_nt_val = 0
                else:
                    nb_inter_nt_val = nb_inter_nt_subset.shape[0]
                if nb_inter_aa_subset.empty:
                    nb_inter_aa_val = 0
                else:
                    nb_inter_aa_val = nb_inter_aa_subset.shape[0]

                # Add values to columns
                for atup in [
                    ("nb_intra", nb_intra_val),
                    ("nb_inter_nt", nb_inter_nt_val),
                    ("nb_inter_aa", nb_inter_aa_val),
                ]:
                    acolumn = atup[0]
                    avalue = atup[1]
                    cdrna_conformers.loc[(
                        (cdrna_conformers["pdb"] == apdb)
                        & (cdrna_conformers["model"] == amodel)
                        & (cdrna_conformers["chain"] == achain),
                        acolumn
                    )] = avalue
    #    "nb_inter_mt"         # Table DSSR       <------- Llenar con tablas de interaccion con metales

    return cdrna_conformers


def fill_cdrna_conformers(
    pdbchains_databases_codes,
    rcsb_table,
    cdrna_interactions,
    synthetics_table,
    cif_dir,
    outputDir,
):
    """
    Create and fill the website cdrna_conformers table.
    Parameters:
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * rcsb_table:                a string. Correspond to a CSV table with all RCSB
                                                       information.
                * cdrna_interactions:        a string. Correspond to a CSV CoDNaS-RNA interactions
                                                       website table.
                * synthetics_table:          a string. Correspond to a TSV table with all
                                                       synthetic conformers present in CoDNaS-RNA.
                * cif_dir:                   a string. Correspond to a directory where all mmcif
                                                       files are stored.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """

    # Before start to do anything, check if outputDir is correct and create it
    create_dir(outputDir)

    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_conformers.csv.gz"):
        cdrna_conformers = get_df_if_file_exist(
            "cdrna_conformers", outputDir + "cdrna_conformers.csv.gz", ",", True
        )
    else:
        cdrna_conformers = create_dataframe_struct()

    cdrna_conformers, pdb_chains_list = extract_info_databases_code_table(
        cdrna_conformers, pdbchains_databases_codes, "\t", cif_dir, outputDir
    )

    cdrna_conformers = extract_info_rcsb_table(
        cdrna_conformers, pdb_chains_list, rcsb_table, ",", outputDir
    )

    cdrna_conformers = extract_info_interactions_table(
        cdrna_conformers, cdrna_interactions, pdbchains_databases_codes, ",", outputDir
    )

    cdrna_conformers = extract_rnacentral_info(
        cdrna_conformers, synthetics_table, "\t", outputDir
    )  # PDBchains_with_databases_codes.tsv

    save_website_table(cdrna_conformers, outputDir, "cdrna_conformers", ",")

    print("All done !")

    return


# pdbchains_databases_codes = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/PDBchains_with_databases_codes.tsv'
# rcsb_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/filtered_rna_conformers_from_PDB.csv.gz'
# cdrna_interactions = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_interactions.csv.gz'
# synthetics_table = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/RNAcentral/release-15/RNAcentral_synthetics_models_from_CoDNaS-RNA.tsv'
# cif_dir = '/home/martingb/Projects/2020/CoDNaS-RNA/data/2020-05-15/pdbs/data_codnas_rna_cif/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create cdrna_conformers.csv.gz table to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "pdbchains_databases_codes",
        type=str,
        help="Table with all PDB chains with their related codes."
    )
    p.add_argument(
        "rcsb_table",
        type=str,
        help="Gzip table with all RCSB information."
    )
    p.add_argument(
        "cdrna_interactions",
        type=str,
        help="Gzip table with all conformer interactions."
    )
    p.add_argument(
        "synthetics_table",
        type=str,
        help="Table with all PDB_Chain synthetic entries mapped."
    )
    p.add_argument(
        "cif_dir",
        type=str,
        help="Directory to search *.cif files."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store cdrna_conformers table."
    )

    args = p.parse_args()
    fill_cdrna_conformers(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
"""\
Purpose: With this function you can save a TSV table of PDB:chain with databases codes
         for all CoDNaS-RNA's clusters as a "PDB_chains_with_databases_codes.tsv" file.

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [clusters_csv_list]       // list
                * [extention]               // string

Observations:
            - 

TODO: - 
"""

import argparse
import os.path
import pandas as pd
from remove_if_exist import remove_if_exist


def save_tsv_clstrs_with_pdb_chains(clusters_csv_list, outputDir):
    """
    Save a tsv table of PDB:chain for all clusters with databases codes in outputDir.
    Parameters:
                    * clusters_csv_list: a list of each cluster path.
                    * outputDir: a string. Corresponding to
                                 table directory.
    Returns: nothing.
    """
    clusters_csv_list_to_see = clusters_csv_list.copy()
    # Define output file
    output_pdb_chain = outputDir + "PDB_chains_with_databases_codes.tsv"
    # Remove this file first so everything is prepared for every database runing test
    remove_if_exist([output_pdb_chain])
    # Save pdb_code + chain results
    with open(output_pdb_chain, "a") as outfile:
        #Make a starter and simpler header
        outfile.write("%s\t%s\n" % ('Cluster_ID','PDB_chain'))
        # Start to process each cluster
        while len(clusters_csv_list_to_see) != 0:
            file = clusters_csv_list_to_see.pop(0)
            cluster_id = os.path.basename(file).strip('.csv')
            cluster = pd.read_csv(file, dtype=str, header=0)
            pdbs_chains_list = cluster["Seq_code"].to_list()
            for pdb_chain in pdbs_chains_list:
                outfile.write("%s\t%s\n" % (cluster_id,pdb_chain))
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Save TSV table of PDB:chain with databses codes for all clusters in outputDir."
    )
    # Positional arguments
    p.add_argument(
        "clusters_csv_list",
        help="List of clusters *.csv paths."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Output directory to save PDB_chains_with_databases_codes.tsv file."
    )

    args = p.parse_args()
    save_tsv_clstrs_with_pdb_chains(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
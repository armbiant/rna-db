"""\
Purpose: With this function you can download CoDNaS-RNA mmCIF files.

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * Biopython
                - Modules
                    * PDBList_mod

Arguments:    -Positional
                * [aset_of_pdb_codes]              // set
                * [cif_dir]                        // string
                * [compress]                       // bool

Observations:
            - 

TODO: - 

"""

import argparse
import os.path
from PDBList_mod import PDBList


def download_codnas_rna_cif_files(aset_of_pdb_codes, cif_dir, compress):
    """
    Download all *.cif or *.cif.gz files from every pdb code entry present
    in CoDNaS-RNA database (i.e. unique pdb entries per cluster
    in all clusters). Files are save them in "cif_dir".
    Parameters:
                    * aset_of_pdb_codes:    a set of pdb codes from all clusters.
                    * cif_dir:              a string. Correspond to path directory
                                                      where all cif files are going
                                                      to be stored.
                    * compress:             a boolean. Correspond to decide compress
                                                       files.
    Returns: nothing
    """
    # Download every *.cif entry from every clusters
    for pdb_code in aset_of_pdb_codes:
        pdb_code = pdb_code.lower()
        filename = cif_dir + pdb_code + ".cif"
        gz_filename = filename + ".gz"
        if (not os.path.isfile(filename)) & (not os.path.isfile(gz_filename)):
            PDBList().retrieve_pdb_file( pdb_code
                                       , obsolete=False
                                       , pdir=cif_dir
                                       , file_format="mmCif"
                                       , overwrite=False
                                       , compress=compress )
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Download all *.cif or *.cif.gz files from every pdb code entry\npresent in CoDNaS-RNA database."
    )
    # Positional arguments
    p.add_argument(
        "aset_of_pdb_codes",
        type=str,
        help="Set of PDB codes used to download *.cif.gz.",
    )
    p.add_argument(
        "cif_dir",
        type=str,
        help="Output directory to store *.cif or *.cif.gz files."
    )
    p.add_argument(
        "--compress",
        action="store_true",
        help="To store gzip compressed files."
    )

    args = p.parse_args()
    download_codnas_rna_cif_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
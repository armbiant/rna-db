"""\
Purpose: With this function you can get the file name from a absolute path.
         You may choose the file name with or without extensions.
         Extensions are read from the tail of the absolute path as dot
         separated fields.
         If file name is asked without extensions (without_exts=True), 'get_filename'
         uses interal set of known extensions to be removed.
         If file name is asked without_exts=True and force=True, 'get_filename'
         is going to removed all extensions no matter if know it or not.

Preconditions:
                - nothing

Arguments:    -Positional
                * [afile]      // string

Observations:
            - Keep feeding ext_set so it can works for a bunch of files.

TODO: - 

"""

import argparse
import os


def get_filename(afile, without_exts=False, force=False):
    """
    Get a file name from an absolute file path. If without_exts
    is True, known extensions (dot separated) are going to be
    considered to be removed from file name. If force is True,
    all dot extensions are going to be removed.
    Parameters:
                * afile:                 a string. Correspond to a full
                                                   path of a file.
    Observations:
                * example_paths = [ "FileName"
                                  , "./FileName"
                                  , "../../FileName"
                                  , "FileName.txt"
                                  , "./FileName.txt.zip.asc"
                                  , "/path/to/some/FileName.TXT"
                                  , "/path/to/some/FileName.txt.zip.asc"
                                  , "/path/to/some/nvdcve-1.1-2002.json.zip ]
                * Tested on nvdcve-1.1-2002.json.zip (filename: nvdcve-1.1-2002)
    Return: a string
    """
    ext_set = {
                ".txt", ".md",
                ".gz", ".gzip", ".xz", ".zstd", ".ztd",
                ".csv", ".tsv", ".psv",
                ".json", ".js",
                ".sql",
                ".zip", ".rar",
                ".tar", ".tgz",
                ".py", ".R", ".c", ".cpp", ".hs",
                ".pdb", ".pdbemb",
                ".cif", ".mmcif",
                ".rev", ".dbn",
                ".png", ".jpg",
                ".svg",
                ".pdf"
    }
    filename = os.path.basename(afile)
    if without_exts:
        if "." in filename:
            while "." in filename:
                filename = os.path.splitext(filename)[0]
                if (not force) and (not [ext for ext in ext_set if ext.lower() in filename]):
                    break
    return filename

if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Gives a filename with/out its extension.")
    # Positional arguments
    p.add_argument(
        "afile",
        type=str,
        help="A full path of a file."
    )
    p.add_argument(
        "-r",
        "--without_ext",
        action="store_true",
        help="Remove extensions."
    )
    p.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="Remove all existing extensions."
    )

    args = p.parse_args()
    get_filename(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
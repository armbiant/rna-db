"""\
Purpose: With this function you can guess the extension from a separator value.
         If the separator is not recognized, returns a CSV extension and its
         corresponding separator.
         Example: a_separator, ext = guess_extension_from_separator("\t")
                  a_separator --> "\t"
                  ext         --> ".tsv"

Preconditions:
                - nothing

Arguments:    -Positional
                * [a_separator]      // string

Observations:
            - If separator is not recognized, a CSV with its separator it is returned.
            - Works for comma, tabular and pipe separators.

TODO: - 

"""

import argparse


def guess_extension_from_separator(a_separator):
    """
    Takes a table separator and guess to which extension is related.
    If the separator is not recognized, returns a CSV extension and its
    corresponding separator.
    Parameters:
                a_separator:                a string. Correspond to a table separator.
    Return: two strings
    """
    # Guess separator
    if a_separator == "\t":
        ext = ".tsv"
    elif a_separator == ",":
        ext = ".csv"
    elif a_separator == "|":
        ext = ".psv"
    else:
        ext = ".csv"
        a_separator = ","
    return a_separator, ext


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Gives a separator with its extension.")
    # Positional arguments
    p.add_argument(
        "a_separator",
        help="A string denoting a separator."
    )

    args = p.parse_args()
    guess_extension_from_separator(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
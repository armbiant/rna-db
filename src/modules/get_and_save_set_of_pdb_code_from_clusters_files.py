"""\
Purpose: With this module you can get the set of wwPDB codes from
         a list of clusters *.csv files. It also save those codes
         in a file called "PDBs_from_clstrs.list".

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [clusters_csv_list]              // list
                * [outputDir]                      // string

Observations: -

TODO: - 
"""

import argparse
import pandas as pd
from remove_if_exist import remove_if_exist


def get_set_of_pdb_codes(alist):
    """
    Get a set of PDB codes.
    Parameters:
                    * alist:                a list of PDB:Chains from one cluster.
    Returns: a set of PDB codes.
    """
    return {pdb_chain.split(":")[0] for pdb_chain in alist}


def get_and_save_set_of_pdb_code_from_clusters_files(clusters_csv_list, outputDir):
    """
    Get a set and save it in list file all PDB codes from clusters in outputDir.
    Parameters:
                    * clusters_csv_list:    a list of each cluster path.
                    * outputDir:            a string. Directory to save the
                                                      PDBs_from_clstrs.list file.
    Returns: a set of PDB codes.
    """
    clusters_csv_list_to_see = clusters_csv_list.copy()
    set_pdbs = set()
    while len(clusters_csv_list_to_see) != 0:
        file = clusters_csv_list_to_see.pop(0)
        cluster = pd.read_csv(file, dtype=str, header=0)
        # cluster_id = os.path.basename(file).strip('.csv')
        pdbs_chains_list = cluster["Seq_code"].to_list()
        # Save pdb_code results for all clusters
        set_pdbs = set_pdbs | get_set_of_pdb_codes(pdbs_chains_list)
    # Define output file
    output_pdb = outputDir + "PDBs_from_clstrs.list"
    # Remove this file first so everything is prepared for every database runing test
    remove_if_exist([output_pdb])
    with open(output_pdb, "a") as outfile:
        for pdb_code in set_pdbs:
            outfile.write("%s\n" % (pdb_code))
    return set_pdbs


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Get a set and save it in list file all PDB codes from clusters."
    )
    # Positional arguments
    p.add_argument(
        "clusters_csv_list",
        help="List of clusters *.csv paths."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Output directory to save PDBs_from_clstrs.list file."
    )

    args = p.parse_args()
    get_and_save_set_of_pdb_code_from_clusters_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
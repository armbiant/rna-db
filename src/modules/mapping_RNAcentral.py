"""\
Purpose: With this module you can create a several tsv tables with mapped info from
         RNAcentral. You can know which and which not wwPDB entry that had passed
         data quality filters is present in CoDNaS-RNA; which of those entries are
         synthetics; and finally which RNAcentral entry is (not kidding) ausent in
         id_mapping.tsv (maybe an error from RNAcentral).
         The main purpose is to build a nice table (PDBchains_with_databases_codes)
         where any CoDNaS-RNA entry has mapped information with unique IDs taken
         from any database, for example all databases present in RNAcentral.

Preconditions:
                - Inputs
                    * fasta_file:      file,   "Multi-fasta file with all RNA
                                                entries that have passed all
                                                quality data filtes".
                    * clusters_Dir:    string, "Path where all CoDNaS-RNA CSV
                                                clusters are stored".
                    * id_mapping:      file,   "Gzip file to perform mapping
                                                of RNA type from URS IDs".
                    * outputDir:       string, "Path where to store all clusters
                                                mapping tables".

                - Libraries
                    * pandas
                    * Bio
                    * requests
                - Modules
                    * In-House: build_ok_outputDir_string
                    * In-House: get_sorted_clstr_paths_list

Arguments:    -Positional
                * [fasta_file]          // string
                * [clusters_Dir]        // string
                * [id_mapping]          // string
                * [outputDir]           // string

              -Optional

Observations:
            - Files that are saved in "outputDir" when this modules ends:
              + PDBchains_with_databases_codes.tsv
              + Clusters_RNAcentral_mapped/<cltr_id>.tsv
              + Conformers_without_CoDNaS-RNA_cluster.tsv
              + RNAcentral_PDB_entries_mapped_and_not_present_in_CoDNaS-RNA.tsv
              + RNAcentral_PDB_synthetic_entries_mapped.tsv
              + RNAcentral_failed_mapped_entries_in_id_mapping_file.tsv
              + RNAcentral_external_databases_mapped.tsv


TODO: - 

"""

import argparse
from Bio import SeqIO
import pandas as pd
import hashlib
import requests
import os
import subprocess
from collections import defaultdict
from build_ok_outputDir_string import build_ok_outputDir_string
from get_sorted_clstr_paths_list import get_sorted_clstr_paths_list


def create_file_pdb_chains_with_db_codes(clusters_Dir, outputDir):
    """
    Create the PDBchains_with_databases_codes.tsv file.
    Parameters:
                * clusters_Dir:             a string. Correspond to a directory path where
                                                      all CoDNaS-RNA Cluster_#.csv are located.
                * outputDir:                a string. Correspond to an output directory to store
                                                      CoDNaS-RNA tables.
    Return: nothing
    """
    #Get a list of clusters csv files
    clusters_csv_list = get_sorted_clstr_paths_list(clusters_Dir, ".csv")
    clusters_csv_list_to_see = clusters_csv_list.copy()
    # Save Cluster_ID,pdb_chain tsv
    with open(outputDir+"PDBchains_with_databases_codes.tsv", "a") as outfile:
        outfile.write("%s\t%s\t%s\t%s\t%s\n" % ("Cluster_ID","PDB_chain","fasta_length","DNA_md5","RNAcentral_URS"))
        while len(clusters_csv_list_to_see) != 0:
        # Start to process each cluster
            file = clusters_csv_list_to_see.pop(0)
            cluster = pd.read_csv(file, dtype=str, header=0)
            cluster_id = os.path.basename(file).split('.csv')[0]
            pdbs_chains_list = cluster["Seq_code"].to_list()
            for pdb_chain in pdbs_chains_list:
                outfile.write("%s\t%s\t%s\t%s\t%s\n" % (cluster_id,pdb_chain,"","",""))
    return


# get the md5 of seq. Taken from retrieve_rnacentral_id.py (RNAcentral.org)
def get_md5(sequence):
    """
    Calculate md5 for an RNA sequence
    Parameters:
                * sequence                  a string. Corresponds to a RNA sequence.
    Return: a string
    """
    # RNAcentral stores DNA md5 hashes
    sequence = str(sequence).replace('U','T').encode('utf-8')
    # get the md5 digest
    m = hashlib.md5()
    m.update(sequence)
    return m.hexdigest()


# get the RNAcentral id. Taken from retrieve_rnacentral_id.py (RNAcentral.org)
def get_rnacentral_id(md5):
    """
    Parse json output and return the RNAcentral id.
    Parameters:
                * md5:                      a string. Correspond to a md5 RNA code.
    Observation:
                * Example
                    "results": [{ "url": "http://rnacentral.org/api/v1/rna/URS00008A0D05"
                                , "rnacentral_id": "URS00008A0D05"
                                , "md5": "2c0fd7cd89cbc65b2e7068267bd44b70"
                                , "sequence": "AGAGUUUGAUCAUGGCUCAGGACGAACGCUGGCGGCGUGCUUAACACAUGCAAGUCGAACGGUAAGGCCUUCGGGUACACGAGUGGCGAACGGGUGAGUAACACGUGGGUGAUAUGCCCUGGACUCUGGGAUAAGCUUGGGAAACUGGGUCUAAUACCGGAUAUGACCAUGCUGCGCAUGUGGUGUGGUGGAAAGCUUUUGCGGUCUGGGAUGGGCCCGCGGCCUAUCAGCUUGUUGGUGGGGUAAUGGCCUACCAAGGCGACGACGGGUAGCCGGCCUGAGAGGGUGUCCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUGGGGAAUAUUGCACAAUGGGCGGAAGCCUGAUGCAGCGACGCCGCGUGGGGGAUGACGGCCUUCGGGUUGUAAACCUCUUUCGGUUGGGACGAAGCCUUUCGGGGUGACGGUACCUUCAGAAGAAGCACCGGCCAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGGUGCGAGCGUUGUCCGGAAUUACUGGGCGUAAAGAGCUCGUAGGCGGUUUGUCGCGUCGAUCGUGAAAACUCGCAGCUCAACUGUGGGCGUGCGGUCGAUACGGGCAGACUUGAGUACUGCAGGGGAGACUGGAAUUCCUGGUGUAGCGGUGAAAUGCGCAGAUAUCAGGAGGAACACCGGUGGCGAAGGCGGGUCUCUGGGCAGUAACUGACGCUGAGGAGCGAAAGCGUGGGUAGCGAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGGUGGGCGCUAGGUGUGGGUUUCCUUCCACGGGAUCCGUGCCGUAGCUAACGCAUUAAGCGCCCCGCCUGGGGAGUACGCCGCAAGGCUAAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGCGGAGCAUGUGGAUUAAUUCGAUGCAACGCGAAGAACCUUACCUGGGUUUGACAUACACCAGACGCUGGUAGAGAUAUCAGUUCCCUUGUGGUUGGUGUACAGGUGGUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGUCCUGUAUUGCCAGCGCGUUAUGGCGGGGACUUGCAGGAGACUGCCGGGGUCAACUCGGAGGAAGGUGGGGACGACGUCAAGUCAUCAUGCCCCUUAUGUCCAGGGCUUCACACAUGCUACAAUGGCCGGUACAGAGGGUUGCGAUAUCGUGAGGUGGAGCGAAUCCCUUAAAGCCGGUCUCAGUUCGGAUCGGGGUCUGCAACUCGACCCCGUGAAGUCGGAGUCGCUAGUAAUCGCAGAUCAGCAAUGCUGCGGUGAAUACGUUCCCGGGCCUUGUACACACCGCCCGUCACGUCAUGAAAGUCGGUAACACCCGAAGCCCGUGGCCUAACCCCUUUGUGGGAGGGAGCGGUCGAAGGUGGGAUCGGCGAUUGGGACGAAGUCGUAACAAGGUAGCCGUACCGGAAGGUGCGGCUGGAUCACCUCCUUUCU"
                                , "length": 1516
                                , "xrefs": "http://rnacentral.org/api/v1/rna/URS00008A0D05/xrefs"
                                , "publications": "http://rnacentral.org/api/v1/rna/URS00008A0D05/publications"
                                , "is_active": true
                                , "description": "Skermania piniformis rRNA"
                                , "rna_type": "rRNA"
                                , "count_distinct_organisms": 1
                                , "distinct_databases": ["Greengenes"] }]
    Return: a string
    """
    url = 'https://rnacentral.org/api/v1/rna'
    r = requests.get(url, params = {'md5': md5})
    data = r.json()
    if data['count'] > 0:
        return data['results'][0]['rnacentral_id']
    else:
        return 'RNAcentral id not found'


def add_value_in_column_to_df( chain
                             , avalue
                             , acolumn
                             , mapping_cltrs ):
    """
    Save a particular value in a selected chain in the given acolumn from
    specific dataframe subset.
    Parameters:
                * chain:                    a string. Correspond to a wwPDB Chain ID.
                * avalue:                   a value. Correspond to a value to be save it.
                * acolumn:                  a column. Correspond to a dataframe column where to
                                                      save the "avalue".
                * mapping_cltrs:            a dataframe. Correspond to a dataframe where value
                                                         is going to be saved it.
    Return: nothing
    """
#    mapping_cltrs = pd.read_table(pdb_chains_with_db_codes, sep='\t', dtype=str, header=0)
    #Adding new database code value
    mapping_cltrs.loc[mapping_cltrs['PDB_chain'] == chain, acolumn] = avalue
#    #Save file
#    mapping_cltrs.to_csv(pdb_chains_with_db_codes, sep='\t', index=False, header=True)
    return


def get_URS_md5_dict( fasta_file
                    , clusters_Dir
                    , outputDir ):
    """
    Get two dictionaries using seqs from the gievn fasta_file:
                * A dict of URSs with their md5 values
                * A dict of mapping clusters
    Parameters:
                * fasta_file:               a string. Correspond to a path of a fasta file.
                * clusters_Dir:             a string. Correspond to a directory path where
                                                      all CoDNaS-RNA Cluster_#.csv are located.
                * outputDir:                a string. Correspond to an output directory to store
                                                      CoDNaS-RNA tables.
    Return: URS_md5_dict, mapping_cltrs
    """
    #Check first if pdb_chains_with_db_codes.tsv file exists.
    if not os.path.exists(outputDir+"PDBchains_with_databases_codes.tsv"):
        create_file_pdb_chains_with_db_codes(clusters_Dir, outputDir)

    # Read tsv (Cluster_ID\tPDB:Chain\t<LIST-OF-DATABASES-TAB-SEPARATED>)
    mapping_cltrs = pd.read_table( outputDir+"PDBchains_with_databases_codes.tsv"
                                 , sep='\t'
                                 , dtype=str
                                 , keep_default_na=False
                                 , header=0 )

    #Start to process each fasta from (all seqs that pass filter selections)
    for seq_record in SeqIO.parse(fasta_file, "fasta"):
        pdb_chain = seq_record.id.split('|')[0]
        sequence = seq_record.seq

        #Search if you already have stored the pdb_chain
        if not pdb_chain in mapping_cltrs['PDB_chain'].to_list():
            chain_row = pd.DataFrame([{"Cluster_ID": 'NA', "PDB_chain": pdb_chain}])
            mapping_cltrs = mapping_cltrs.append(chain_row, ignore_index = True, sort=False)

        #Search if you already have stored the fasta_length
        if not pd.isnull(mapping_cltrs['fasta_length'].loc[mapping_cltrs['PDB_chain'] == pdb_chain].to_list()[0]):
            seq_length = mapping_cltrs['fasta_length'].loc[mapping_cltrs['PDB_chain'] == pdb_chain].to_list()[0]
        else:
            seq_length = len(seq_record)
            #Add URS value to have store it the RNAcentral URS code in file.
            add_value_in_column_to_df(pdb_chain, seq_length,'fasta_length', mapping_cltrs)

        #Search if you already have stored the md5 code
        if not pd.isnull(mapping_cltrs['DNA_md5'].loc[mapping_cltrs['PDB_chain'] == pdb_chain].to_list()[0]):
            md5 = mapping_cltrs['DNA_md5'].loc[mapping_cltrs['PDB_chain'] == pdb_chain].to_list()[0]
        else:
            md5 = get_md5(sequence)
            #Add md5 code to have store it and save execution in other moment.
            add_value_in_column_to_df(pdb_chain, md5, 'DNA_md5', mapping_cltrs)

        #Search if you already have stored the URS code
        if not pd.isnull(mapping_cltrs['RNAcentral_URS'].loc[mapping_cltrs['PDB_chain'] == pdb_chain].to_list()[0]):
            URS_ID = mapping_cltrs['RNAcentral_URS'].loc[mapping_cltrs['PDB_chain'] == pdb_chain].to_list()[0]
        else:
            URS_ID = get_rnacentral_id(md5)  # Example: URS00002C9E9D
            #Add URS value to have store it the RNAcentral URS code in file.
            add_value_in_column_to_df(pdb_chain, URS_ID, 'RNAcentral_URS', mapping_cltrs)

    URS_md5_dict = dict()
    URS_md5_dict = dict(zip(mapping_cltrs['RNAcentral_URS'], mapping_cltrs['DNA_md5']))

    #Save tsv file (Cluster_ID\tPDB:Chain\t<LIST-OF-DATABASES-TAB-SEPARATED>)
    mapping_cltrs.to_csv( outputDir+"PDBchains_with_databases_codes.tsv"
                        , sep='\t'
                        , index=False
                        , header=True )
    return URS_md5_dict, mapping_cltrs


def create_id_mapping_filtered_tsv(URS_md5_dict, id_mapping):
    """
    Create a filtered id_mapping with unique lines called id_mapping_filtered_unique.tsv
    from a gzip compressed id_mapping file (id_mapping.tsv.gz).
    Moreover, this function returns the full filename path of id_mapping_filtered_unique.tsv
    and a unique databases list (at 2020-05-12, RNAcentral has 40 databases).

    Observations:
                * Its use zgrep.
    To do that, first creates a URS.list to be used like pattern file on
    zgrep usinf -F -f options. Then, create a unique file running sort -u to
    id_mapping_filtered.tsv. Finally, id_mapping_filtered.tsv and URS.list are deleted.
    
    Parameters:
                * URS_md5_dict:             a dict. Correspond to a dictionary of {URS-ID:md5}.
                * id_mapping:               a string. Correspond to a path of a Gzip file
                                                      to perform mapping of RNA type from
                                                      URS IDs.

    Return: id_mapping_filtered_unique, ext_db_list
    """
    id_mapping_filtered_name = os.path.basename(id_mapping).split('.tsv')[0]
    id_mapping_filtered_name = id_mapping_filtered_name + '_filtered.tsv'
    id_mapping_filtered_dir = os.path.dirname(id_mapping)
    id_mapping_filtered = id_mapping_filtered_dir + "/" + id_mapping_filtered_name

    URS_ID_list = list({URS_ID for URS_ID in URS_md5_dict.keys()})
    with open(id_mapping_filtered_dir+"/"+"URS.list", 'a+') as outfile:
        for URS_ID in URS_ID_list:
            outfile.write("%s\n" % (URS_ID))

    #Extract all databases names and stored in a list (use AWK and sort -u)
    #If this step is performed to id_mapping_filtered.tsv, it is possible
    #that not all databases are present.
    cmd = "zcat "+id_mapping+" | awk '{print($2)}' | sort -u > "+id_mapping_filtered_dir+"/"+"ext_dbs.list"
    os.system(cmd)
    ext_dbs_file = pd.read_table( id_mapping_filtered_dir+"/"+"ext_dbs.list"
                                , sep='\t'
                                , dtype=str
                                , header=None )
    ext_db_list = sorted(ext_dbs_file[0].unique().tolist())

    #Filered id_mapping getting only our URS rows and no repeated.
    cmd = "zgrep -F -f "+id_mapping_filtered_dir+"/"+"URS.list"+" "
    cmd_id_mapping = cmd+id_mapping+" > "+id_mapping_filtered
    os.system(cmd_id_mapping)
    id_mapping_filtered_unique = id_mapping_filtered_dir + "/" +"id_mapping_filtered_unique.tsv"
    cmd = "cat "+id_mapping_filtered+" | sort -u > "+id_mapping_filtered_unique
    os.system(cmd)

    #Clean everything
    delete_file_if_exists(id_mapping_filtered_dir+"/"+"URS.list")
    delete_file_if_exists(id_mapping_filtered_dir+"/"+"ext_dbs.list")
    delete_file_if_exists(id_mapping_filtered)
    return id_mapping_filtered_unique, ext_db_list


def get_data_from_rnacentral_mapping( URS_md5_dict
                                    , id_mapping_filtered_unique
                                    , mapping_cltrs
                                    , ext_db_list
                                    , outputDir ):
    """
    Get mappings from RNAcentral and CoDNaS-RNA and fill (or creates if not exists)
    the following tables and folder under outputDir:
                * RNAcentral_external_databases_mapped.tsv
                * Conformers_without_CoDNaS-RNA_cluster.tsv
                * RNAcentral_PDB_entries_mapped_and_not_present_in_CoDNaS-RNA.tsv
                * RNAcentral_PDB_synthetic_entries_mapped.tsv
                * RNAcentral_failed_mapped_entries_in_id_mapping_file.tsv
                * Clusters_RNAcentral_mapped/

    All mappings that are PDB entries but are not present in CoDNaS-RNA (due ours criterias) 
    are saved in Clusters_RNAcentral_mapped/ as Cluster_#.tsv (see header below).

    HEADER: <URS_ID    DNA_md5    Database_name    Database_ID    Taxon_ID    RNA_type    Gene_name>

    Parameters:
                * URS_md5_dict:                 a dict. Correspond to a dictionary of {URS-ID:md5}.
                * id_mapping_filtered_unique:   a string. Correspond to the path of
                                                          id_mapping_filtered_unique.tsv
                * mapping_cltrs:                a dataframe. Correspond to a dataframe where value
                                                             is going to be saved it.
                * ext_db_list:                  a list. Correspond to a unique RNAcentral databases list.
                * outputDir:                    a string. Correspond to an output directory to store
                                                          CoDNaS-RNA tables.

    Return: nothing
    """
    #Directory to save mapping results
    outputDir_mapping_results = os.path.abspath(outputDir)
#    outputDir_mapping_results = outputDir_mapping_results + "/" + "tables"
    outputDir_mapping_results = build_ok_outputDir_string(outputDir_mapping_results)

    #Directory to save mapping results
    outputDir_cltrs_mapping_results = outputDir_mapping_results + "Clusters_RNAcentral_mapped"
    outputDir_cltrs_mapping_results = build_ok_outputDir_string(outputDir_cltrs_mapping_results)
    # Make dir or do nothing if exists
    os.makedirs(outputDir_cltrs_mapping_results, exist_ok=True)


    #Check first if RNAcentral_external_databases_mapped.tsv file exists.
    if not os.path.exists(outputDir_mapping_results + "RNAcentral_external_databases_mapped.tsv"):
        #Create a dataframe to store all databases that have mapped from RNAcentral.
        cltr_columns = [ 'Database_name'
                       , 'Database_ID'
                       , 'Taxon_ID'
                       , 'RNA_type'
                       , 'Gene_name'
                       , 'URL'
                       , 'URL_example'
                       , 'Resources'
                       , 'Notes' ]
        ext_dbs = pd.DataFrame(columns = cltr_columns)
    else:
        ext_dbs = pd.read_table( outputDir_mapping_results + "RNAcentral_external_databases_mapped.tsv"
                               , sep='\t'
                               , dtype=str
                               , header=0 )
        for db in ext_db_list:
            #Search if you already have stored the database
            if db in ext_dbs['Database_name'].to_list():
                continue
            else:
                data = [{'Database_name':db}]
                ext_dbs = ext_dbs.append(data,ignore_index=True,sort=False)
    #Save in tsv file all databases that have mapped from RNAcentral.
    ext_dbs.to_csv( outputDir_mapping_results + "RNAcentral_external_databases_mapped.tsv"
                  , sep='\t'
                  , index=False
                  , header=True )


    #Create dict with all URS per Cluster_ID
    cltrs_URS_dic = dict()
    # Read tsv (Cluster_ID\tURS)
#    mapping_cltrs = pd.read_table(pdb_chains_with_db_codes, sep='\t', dtype=str, header=0)
    #Create a dict of clusters with a list unique URS codes present per cluster.
    for cltr_id in mapping_cltrs['Cluster_ID'].unique().tolist():
        cltrs_URS_dic[cltr_id] = mapping_cltrs['RNAcentral_URS'].loc[mapping_cltrs['Cluster_ID'] == cltr_id].unique().tolist()

    cltrs_pdb_chain_dic = defaultdict(list)
    #Make a zip data variable of cluster with PDB:chain code.
    data = zip(mapping_cltrs['Cluster_ID'], mapping_cltrs['PDB_chain'])
    #Create a dict of clusters with a list of PDB:chain codes present per cluster.
    for cltr, chain in data:
        cltrs_pdb_chain_dic[cltr].append(chain)


    #Create a dataframe to store all PDB entries that are going to be mapped from RNAcentral and are not present in CoDNaS-RNA
    cltr_columns = [ 'Cluster_ID'
                   , 'RNAcentral_URS'
                   , 'DNA_md5'
                   , 'Database_name'
                   , 'Database_ID'
                   , 'Taxon_ID'
                   , 'RNA_type'
                   , 'Gene_name'
                   , 'Exclusion_reason' ]
    chains_filtered = pd.DataFrame(columns = cltr_columns)

    #Create a dataframe to store all PDB entries that are synthetic and will be mapped from RNAcentral and are present or not in CoDNaS-RNA.
    cltr_columns = [ 'Cluster_ID'
                   , 'RNAcentral_URS'
                   , 'DNA_md5'
                   , 'Database_name'
                   , 'Database_ID'
                   , 'Taxon_ID'
                   , 'RNA_type'
                   , 'Gene_name'
                   , 'is_CoDNaS-RNA' ]
    syn_chains = pd.DataFrame(columns = cltr_columns)

    #Create a dataframe to store all entries that have URS and don't have any match in id_mapping.tsv.gz from RNAcentral.
    cltr_columns = [ 'Cluster_ID'
                   , 'DNA_md5'
                   , 'RNAcentral_URS' ]
    failed_chains = pd.DataFrame(columns = cltr_columns)


    #Start to mapping each cluster
    for cltr_id, URS_list in cltrs_URS_dic.items():
        #Start to fill RNAcentral info for mapped lines conformers present in one cluster
        cltr_columns = [ 'URS_ID'
                       , 'DNA_md5'
                       , 'Database_name'
                       , 'Database_ID'
                       , 'Taxon_ID'
                       , 'RNA_type'
                       , 'Gene_name' ]
        cluster_tsv = pd.DataFrame(columns = cltr_columns)
        print(str(cltr_id)+'\t'+str(URS_list))
        #Start to mapping each conformer
        for URS_ID in URS_list:
            md5 = URS_md5_dict[URS_ID]

            #Add 'NA' to empty value if exist
            if md5 == '':
                md5 = 'NA'

            #Now check if that chain has URS_ID code or was not present in RNAcentral.
            if len(URS_ID.split(' ')) > 1:
                #Add line to cluster.tsv
                data = [{ 'URS_ID':URS_ID
                        , 'DNA_md5':md5
                        , 'Database_name':'NA'
                        , 'Database_ID':'NA'
                        , 'Taxon_ID':'NA'
                        , 'RNA_type':'NA'
                        , 'Gene_name':'NA' }]
                cluster_tsv = cluster_tsv.append( data
                                                , ignore_index=True
                                                , sort=False )
                continue

            #Get all lines with grep
            cmd = "grep \""+URS_ID+"\" "
            cmd_id_mapping = cmd+id_mapping_filtered_unique
            #Get a list of grep matches
            grep_lines = subprocess.getoutput(cmd_id_mapping).split('\n')

            #Report failed URS mapped cases
            if grep_lines[0] == '':
                data = [{ 'Cluster_ID':cltr_id
                        , 'DNA_md5':md5
                        , 'RNAcentral_URS':URS_ID }]
                failed_chains = failed_chains.append( data
                                                    , ignore_index=True
                                                    , sort=False )
                continue                

            #Start to process each grep line
            for line in grep_lines:
                URS_ID = line.split('\t')[0]
                ext_db = line.split('\t')[1]
                ext_id = line.split('\t')[2]
                ncbi_taxon_id = line.split('\t')[3]
                RNA_type = line.split('\t')[4]
                gene_name = line.split('\t')[5]

                #Add 'NA' to empty mapped values
                if ext_db == '':
                    ext_db = 'NA'
                elif ext_id == '':
                    ext_id = 'NA'
                elif ncbi_taxon_id == '':
                    ncbi_taxon_id = 'NA'
                elif RNA_type == '':
                    RNA_type = 'NA'
                elif gene_name == '':
                    gene_name = 'NA'

                if ncbi_taxon_id == '32630':
                    #Ask if ext_db is PDB, otherwise recorder as synthetic from other database.
                    if ext_db == 'PDB':
                        pdb_chain = ext_id.replace('_',':')
                        #Ask if ext_id is a valid entry in CoDNaS-RNA, otherwise skip it.
                        if not pdb_chain in mapping_cltrs['PDB_chain'].to_list():
                            is_codnas_rna = 0
                        else:
                            is_codnas_rna = 1
                        #Save synthethic mapped line
                        data = [{ 'Cluster_ID':cltr_id
                                , 'RNAcentral_URS':URS_ID
                                , 'DNA_md5':md5
                                , 'Database_name':ext_db
                                , 'Database_ID':ext_id
                                , 'Taxon_ID':ncbi_taxon_id
                                , 'RNA_type':RNA_type
                                , 'Gene_name':gene_name
                                , 'is_CoDNaS-RNA':is_codnas_rna }]
                        syn_chains = syn_chains.append( data
                                                      , ignore_index=True
                                                      , sort=False )

                #Ask if ext_db is PDB, otherwise recorder as synthetic from other database.
                if ext_db == 'PDB':
                    pdb_chain = ext_id.replace('_',':')
                    #Ask if ext_id is a valid entry in CoDNaS-RNA, otherwise skip it.
                    if not pdb_chain in mapping_cltrs['PDB_chain'].to_list():
                        data = [{ 'Cluster_ID':cltr_id
                                , 'RNAcentral_URS':URS_ID
                                , 'DNA_md5':md5
                                , 'Database_name':ext_db
                                , 'Database_ID':ext_id
                                , 'Taxon_ID':ncbi_taxon_id
                                , 'RNA_type':RNA_type
                                , 'Gene_name':gene_name
                                , 'Exclusion_reason':'NA' }]
                        chains_filtered = chains_filtered.append( data
                                                                , ignore_index=True
                                                                , sort=False )

                #Add line to cluster.tsv
                data = [{ 'URS_ID':URS_ID
                        , 'DNA_md5':md5
                        , 'Database_name':ext_db
                        , 'Database_ID':ext_id
                        , 'Taxon_ID':ncbi_taxon_id
                        , 'RNA_type':RNA_type
                        , 'Gene_name':gene_name }]
                cluster_tsv = cluster_tsv.append( data
                                                , ignore_index=True
                                                , sort=False )
                continue

        #Save tsv file all entries that were mapped from RNAcentral, are PDB entries and are not present in CoDNaS-RNA.
        if cltr_id != 'NA':
            cluster_tsv.to_csv( outputDir_cltrs_mapping_results + cltr_id + '.tsv'
                              , sep='\t'
                              , index=False
                              , header=True )
            del cluster_tsv
        else:
            cluster_tsv.to_csv( outputDir_mapping_results + 'Conformers_without_CoDNaS-RNA_cluster.tsv'
                              , sep='\t'
                              , index=False
                              , header=True )
    #Save in tsv file all entries that were mapped from RNAcentral, are PDB entries and are not present in CoDNaS-RNA.
    chains_filtered.to_csv( outputDir_mapping_results + "RNAcentral_PDB_entries_mapped_and_not_present_in_CoDNaS-RNA.tsv"
                          , sep='\t'
                          , index=False
                          , header=True )
    #Save in tsv file all PDB synthetic entries that were mapped from RNAcentral and are present in CoDNaS-RNA.
    syn_chains.to_csv( outputDir_mapping_results + "RNAcentral_PDB_synthetic_entries_mapped.tsv"
                     , sep='\t'
                     , index=False
                     , header=True )
    #Save in tsv file all entries that have URS and don't have any match in id_mapping.tsv.gz from RNAcentral.
    failed_chains.to_csv( outputDir_mapping_results + "RNAcentral_failed_mapped_entries_in_id_mapping_file.tsv"
                        , sep='\t'
                        , index=False
                        , header=True )

    return


def delete_file_if_exists(afile):
    """
    Delete the file if exists.
    Parameters:
                * afile:                    a string. Correspond to the path of a file.

    Return: nothing
    """
    #Remove file if exists.
    if os.path.exists(afile):
        os.remove(afile)
    else:
        print("Can not delete the "+afile+" as it doesn't exists")
    return


def create_TSV_with_conformers_data( fasta_file
                                   , clusters_Dir
                                   , id_mapping
                                   , outputDir ):
    """
    Create several tables from RNAcentral mappings, like:
                * RNAcentral_external_databases_mapped.tsv
                * Conformers_without_CoDNaS-RNA_cluster.tsv
                * RNAcentral_PDB_entries_mapped_and_not_present_in_CoDNaS-RNA.tsv
                * RNAcentral_PDB_synthetic_entries_mapped.tsv
                * RNAcentral_failed_mapped_entries_in_id_mapping_file.tsv
                * Clusters_RNAcentral_mapped/Cluster_#.tsv
    Parameters:
                * fasta_file:               a string. Correspond to a path of a fasta file.
                * clusters_Dir:             a string. Correspond to a directory path where
                                                      all CoDNaS-RNA Cluster_#.csv are located.
                * id_mapping:               a string. Correspond to a path of a Gzip file
                                                      to perform mapping of RNA type from
                                                      URS IDs.
                * outputDir:                a string. Correspond to an output directory to store
                                                      CoDNaS-RNA tables.

    Return: nothing
    """
    # Directory to save mapping results
    outputDir = build_ok_outputDir_string(outputDir)

    URS_md5_dict, mapping_cltrs = get_URS_md5_dict( fasta_file
                                                  , clusters_Dir
                                                  , outputDir )
    id_mapping_filtered_unique, ext_db_list = create_id_mapping_filtered_tsv(URS_md5_dict, id_mapping)
    get_data_from_rnacentral_mapping( URS_md5_dict
                                    , id_mapping_filtered_unique
                                    , mapping_cltrs
                                    , ext_db_list
                                    , outputDir )
    delete_file_if_exists(id_mapping_filtered_unique)
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Create a folder with a TSV for each cluster describing PDB:chain RNAcentral mapping results.")
    # Positional arguments
    p.add_argument(
        "fasta_file",
        type=str,
        help="A fasta file path with all RNA sequences."
    )
    p.add_argument(
        "clusters_Dir",
        type=str,
        help="Path where all CoDNaS-RNA CSV clusters are stored."
    )
    p.add_argument(
        "id_mapping",
        type=str,
        help="Gzip file path to perform mapping of RNA type from URS IDs."
    )
#    p.add_argument("pdb_chains_with_db_codes", help="A TSV file with PDB:Chain with specific databases codes for all conformers.")
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store all clusters mapping tables."
    )

    args = p.parse_args()
    create_TSV_with_conformers_data(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
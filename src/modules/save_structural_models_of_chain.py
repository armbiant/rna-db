"""\
Purpose: With this module you can get and save chain models from
         one wwPDB.cif file using Biopython.

Preconditions:
                - Files for input
                    * cif files
                - Libraries
                    * Pandas
                    * Bio

Arguments:    -Positional
                * [cluster_id]              // string
                * [pdb_chain]               // string
                * [cif_dir]                 // string
                * [outputDir]               // string

Observations: -

TODO: - 
"""

import argparse
import os
import gzip
from Bio.PDB.MMCIFParser import MMCIFParser
from Bio.PDB.mmcifio import MMCIFIO
from build_ok_outputDir_string import build_ok_outputDir_string


def get_structure_cif_file(pdb_code, cif_dir):
    """
    Get a stucture file.
    Parameters:
                   * pdb_code:          a string. Correspond to PDB code.
                   * cif_dir:           a string. Correspond to path directory
                                                  where all cif files are stored.
    Return: a file of one PDB structure.
    """
    cif_dir = build_ok_outputDir_string(cif_dir)
    filename = cif_dir + pdb_code + ".cif"
    if os.path.exists(filename):
        cif_file = open(filename, "r")
        structure_file = MMCIFParser().get_structure(pdb_code, cif_file)
    elif os.path.exists(filename + ".gz"):
        gz_filename = filename + ".gz"
        with gzip.open(gz_filename, "rb") as gz_cif_file:
            with open(filename, "wb") as out:
                out.writelines(gz_cif_file)
        os.remove(gz_filename)
        cif_file = open(filename, "r")
        structure_file = MMCIFParser().get_structure(pdb_code, cif_file)
    return structure_file


def get_model_name(cluster_id, pdb_chain, model):
    """
    Get model filenname.
    Parameters:
                   * cluster_id:        a string. Correspond to cluster id.
                   * pdb_chain:         a string. Correspond to PDB:Chain code.
                   * model:             an int. Correspond to one model number.
    Return: a string denoting "model_filename".
    """
    pdb_code = pdb_chain.split(":")[0]
    chain = pdb_chain.split(":")[1]
    modelname = str(
        int(str(model).split("=")[1].replace(">", "")) + 1
    )  # Genero el nombre del modelo, ej.: 1
    model_filename = (
        cluster_id + "_" + pdb_code + "_" + modelname + "_" + chain + ".cif"
    )
    return model_filename


def save_structural_models_of_chain( cluster_id
                                   , pdb_chain
                                   , cif_dir
                                   , outputDir ):
    """
    Save PDB_MODEL_CHAIN files.
    Parameters:
                   * cluster_id:        a string. Correspond to cluster id.
                   * pdb_chain:         a string. Correspond to PDB:Chain code.
                   * cif_dir:           a string. Correspond to path directory
                                                  where all cif files are stored.
                   * outputDir:         a string. Correspond to output path
                                                  directory.
    Return: nothing. Save a file in outputDir.
    """
    pdb_code = pdb_chain.split(":")[0]
    chain = pdb_chain.split(":")[1]
    structure_file = get_structure_cif_file(pdb_code, cif_dir)
    outputDir = build_ok_outputDir_string(outputDir)
    for model in structure_file:
        model_filename = get_model_name(cluster_id, pdb_chain, model)
        if not os.path.exists(outputDir + model_filename):
            MMCIFIO().set_structure(model[chain])
            MMCIFIO().save(outputDir + model_filename)
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Get and save chain models from one PDB.cif file."
    )
    # Positional arguments
    p.add_argument(
        "cluster_id",
        type=str,
        help="Cluster ID used to build output filename."
    )
    p.add_argument(
        "pdb_chain",
        type=str,
        help="PDB:chain code to get models from .cif file."
    )
    p.add_argument(
        "cif_dir",
        type=str,
        help="Output directory to search *.cif file."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Output directory to save all model files extrated."
    )

    args = p.parse_args()
    save_structural_models_of_chain(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
"""\
Purpose: With this module you can read dbn table files (as DSSR's outputs) and create
         secondary structure (SS) format specific files (as vienna's dot-bracket-notation).

Preconditions:
                - Files for input
                    * pdb.csv (raw DSSR tables located unther processed_data)
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [dbnDir]                         // string
                * [a_separator]                    // string
                * [outputDir]                      // string

Observations:
            - Current SS formats supported:
                * vienna (default outputs are like <input-filename>.<dbn>)

TODO: - 

"""


import argparse
import os
import pandas as pd
import glob
from get_filename import get_filename
from get_website_df import get_df_if_file_exist
from guess_extension import guess_extension_from_separator
from create_dir import create_dir
from build_ok_outputDir_string import build_ok_outputDir_string


def get_list_of_files(aDir, extformat):
    """
    Get a list of absolute paths for all "extformat" files present in "aDir".
    Parameters:
                * aDir:                     a string. Correspond to a directory path where all
                                                      extformat files are stored.
                * extformat:                a string. Correspond to format extention (e.g.: tsv).
    Return: a list
    """
    if os.path.exists(aDir):
        #Get data
        file_list = glob.glob(aDir + '*.' + extformat + "*")
        if not file_list:
            raise ValueError('[Error] No files ended with {} were found on {}.'.format('.'+extformat, aDir))
    else:
        raise NameError('[Error] Directory not exists: {}.'.format(aDir))
    return file_list


def get_dbn_table(a_file_path, a_separator):
    """
    Get a dataframe from an absolute file path using the given separator.
    Parameters:
                * a_file_path                a string. Correspond to a raw DSSR dbn table.
                * a_separator:               a string. Correspond to the dbn tables separator.
    Return: a dataframe
    """
    # Get a dataframe from table
    dbn_cdrna_table = get_df_if_file_exist(
        "dbn_cdrna_table", a_file_path, a_separator, True
    )
    return dbn_cdrna_table


def get_dbn_conformers_dict(dbn_cdrna_table):
    """
    Get a dictionary with dbn information to be used to create vienna files
    and a main table. Returns a dictionary with all the data collected per
    MODEL_CHAIN.
    Parameters:
                * dbn_cdrna_table            a string. Correspond to a CoDNaS-RNA DSSR dbn table.
    Return: a dict
    """
    dbn_conformers_dict = {}
    for index, conformer in dbn_cdrna_table.iterrows():
        achain = conformer["chain_id"]
        amodel = conformer["model"]
        model_chain_name = amodel + "_" + achain
        bseq = conformer["bseq"]
        sstr = conformer["sstr"]
        dbn_conformers_dict.update({
                                     model_chain_name: {
                                         "bseq": bseq,
                                         "sstr": sstr
                                     }
                                }
        )
    return dbn_conformers_dict


def collect_dbn(dbn_conformers_dict, vienna_files_dict):
    """
    Create a dictionary with dbn vienna files information.
    Returns a dictionary with all the data collected per
    PDB_MODEL_CHAIN.
    Parameters:
                * dbn_conformers_dict            a dict. A PDB_CHAIN vienna information
                                                         dictionary.
                * vienna_files_dict              a dict. An empty PDB vienna information
                                                         dictionary.
    Return: a dict
    """
    # Here is the pdb code that correspont to the filename
    pdb = [*vienna_files_dict][0] # From >= Python3.5, much faster to get a list of keys from dict. Taken from https://stackoverflow.com/questions/16819222/how-to-return-dictionary-keys-as-a-list-in-python
    for amodel_chain in list(dbn_conformers_dict):
        # End to build the "pdb_model_chain" for this conformer and save it
        pdb_model_chain_name = pdb + "_" + amodel_chain
        dbn_dict = dbn_conformers_dict[amodel_chain]
        vienna_rows = {}
        vienna_rows = {"bseq": dbn_dict["bseq"], "sstr": dbn_dict["sstr"]}
        vienna_files_dict[pdb].update({pdb_model_chain_name: vienna_rows})

    return vienna_files_dict


def create_vienna_files(vienna_files_dict, outputDir):
    """
    Creates raw dbn vienna-like files.
    Parameters:
                * vienna_files_dict              a dict. A full PDB vienna information
                                                         dictionary.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       the vienna dbn files.
    Return: nothing
    """
    if not os.path.exists(outputDir):
        create_dir(outputDir)
    for pdb in list(vienna_files_dict):
        for conformer in list(vienna_files_dict[pdb]):
            conformerDir = outputDir + conformer + "/"
            if not os.path.exists(conformerDir):
                create_dir(conformerDir)
                vienna_filename = conformerDir + conformer + ".dbn"
                if not os.path.exists(vienna_filename):
                    bseq_value = vienna_files_dict[pdb][conformer]["bseq"]
                    sstr_value = vienna_files_dict[pdb][conformer]["sstr"]
                    with open(vienna_filename, "w") as afile:
                        afile.write(">" + conformer + "\n")
                        afile.write(bseq_value + "\n")
                        afile.write(sstr_value + "\n")
    return


def create_files( dbnDir
                , a_separator
                , outputDir ):
    """
    Creates two files: a main derived table dbn_main(TSV) and a reduced table report (TSV) from DSSR dbn/ files.
    Parameters:
                * dbnDir:                    a string. Correspond to a path where are stored all
                                                       dbn tables.
                * a_separator:               a string. Correspond to the raw DSSR dbn tables separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       the vienna dbn files.
    Return: nothing
    """
    # In English:
    # () Parentheses
    # [] Square brackets
    # {} Curly brackets
    # <> Angle brackets

    # For more details: http://rnafrabase.cs.put.poznan.pl/?act=help
    # Dot-bracket notation (dbn)
    # In the dot-bracket notation:
        # an unpaired nucleotide is represented as a dot "."
        # a base pair is represented as a pair of opening (left) and closing (right) brackets, i.e. "(" and ")". This notation is extended to represent secondary structure of pseudoknots, by inserting squared "[" and "]" brackets for lower-order structures. The curly brackets "{" and "}" and angle brackets "<" and ">" are used for higher and most complicated pseudoknots.
        # a missing residue/region is represented as an ampersand "&"

        # Following wildcards were introduced to the search pattern:
        # "^" (up arrow) determines search for the 5'-end fragment only
        # "$" (dollar sign) determines search for the 3'-end fragment only
        # "?" (question mark) stands for any residue which might be paired or unpaired.
        # Wildcards "^" and "$" can be placed only at the beginning or at end of the strand both within a single or multistrand fragments of interest. Wildcard "?" can be placed in any place but only for single strand fragments. In case of multistrand fragments this wildcard operates only on the 5'- and 3'-ends of the respective strand (see examples below).

    # Before start to do anything, check input directory name is OK
    dbnDir = build_ok_outputDir_string(dbnDir)

    a_separator, ext = guess_extension_from_separator(a_separator)
    dbn_files_list = get_list_of_files(dbnDir, ext.replace(".", ""))

    for a_file_path in dbn_files_list:
        # Load CoDNaS-RNA's dbn table
        dbn_cdrna_table = get_dbn_table(a_file_path, a_separator)
        if not dbn_cdrna_table.empty:
            afilename = get_filename(a_file_path, without_exts=True)
            # Go to collect the dbn of afilename
            collected_dbn_info_dict = {afilename: "NA"}
            vienna_files_dict = {afilename: {}}
            dbn_conformers_dict = get_dbn_conformers_dict(dbn_cdrna_table)
            vienna_files_dict = collect_dbn(dbn_conformers_dict, vienna_files_dict)
            create_vienna_files(vienna_files_dict, outputDir)

    print("All done !")

    return


# dbnDir = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/DSSR/dbn/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/files/structures/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create dot-bracket-notation files from DSSR dbn/ files."
    )
    # Positional arguments
    p.add_argument(
        "dbnDir",
        type=str,
        help="Path where are stored all raw DSSR dbn tables."
    )
    p.add_argument(
        "a_separator",
        type=str,
        help="Correspond to the raw DSSR dbn tables separator."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store vienna dbn files."
    )

    args = p.parse_args()
    create_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
"""\
Purpose: With this module you can read RNA 2D files and create objects of them.
         At the moment, supports only vienna fasta-like files:
            >HEADER-IDENTIFIER
            BASESEQ
            SECONDARY-STRUCTURE-NOTATION

Preconditions:
                - nothing

Arguments:    -Positional
                * [afile]      // string

Observations:
            * Main idea taken from "Managing Your Biological Data with Python" (#page 422).
            * Some functions where taken from Biotite and then where modified.
            * Testing:
                    vienna = '''> two hairpin loops
                    AAACCCCGUUUCGGGGAACCACCA
                    ((((...)))).((((..)).)).
                    '''
                    rna = SSFormat(vienna)
                    print(rna.name)
                    print(rna.sequence)
                    print(rna.sstr)

TODO: - 

"""

import argparse
import io


class SSFormat:
    """
    Create a class to handle vienna values.
    Parameters:
                    * afile:       a string. A vienna file path.
    Returns: a class
    """
    def __init__(self, afile):
        lines = read(afile)
        self.name = get_name(lines[0])
        self.sequence = lines[1]
        self.sstr = lines[2]


def get_name(firstLine):
    """
    Get header of fasta-like file.
    Parameters:
                    * firstLine:       a list. A list of strings.
    Returns: a string
    """
    if firstLine[0] == ">":
        # New entry
        if firstLine[1:]:
            return firstLine[1:]
        else:
            return "empty_header"
    else:
        raise SyntaxError('[Error] First line of given file does not start with \"{}\".'.format('>'))


def read(afile):
    """
    Get all lines from a file.
    Parameters:
                    * afile:       a string. A flat file path.
    Returns: a list of strings
    """
    # File name
    if isinstance(afile, str):
        with open(afile, "r") as f:
            lines = f.read().splitlines()
    # File object
    else:
        if not is_text(afile):
            raise TypeError("[Error] A file opened in 'text' mode is required")
        lines = afile.read().splitlines()
    return lines


def is_text(file):
    """
    Indicates if a geven file is text readable.
    Parameters:
                    * file:       a string. A file path.
    Returns: a bool
    """
    if isinstance(file, io.TextIOBase):
        return True
    # for file wrappers, e.g. 'TemporaryFile'
    elif hasattr(file, "file") and isinstance(file.file, io.TextIOBase):
        return True
    else:
        return False


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Parse RNA 2D files. Default vienna fasta-like file.")
    # Positional arguments
    p.add_argument(
        "afile",
        type=str,
        help="A full path of a RNA 2D vienna fasta-like file."
    )

    args = p.parse_args()
    SSFormat(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com
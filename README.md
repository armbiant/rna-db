CoDNaS-RNA/  
├── README.md             # overview of the project  
├── NEWS.md               # registry of releases  
├── data/                 # data files used in the project  
│   ├── README.md         # describes where data came from  
│   └── sub-folder/       # may contain subdirectories  
├── processed_data/       # intermediate files from the analysis  
├── src/                  # contains all code in the project  
│   ├── LICENSE           # license for your code  
│   ├── requirements.txt  # software requirements and dependencies  
│   ├── scripts/          # scripts that are usually used  
│   └── ...               # main code  
├── inputs/               # files needed for some src  
│   └── sub-folder/       # may contain subdirectories  
├── outputs/              # files created for some src  
│   └── sub-folder/       # may contain subdirectories  
├── results/              # results of the analysis (data, tables, figures)  
│   └── sub-folder/       # may contain subdirectories  
└── doc/                  # documentation for your project  
     └── manuscript/      # manuscript describing the results  
